
const
    gulp = require('gulp'),
    notify = require("gulp-notify"),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    ttf2woff = require('gulp-ttf2woff'),
    ttf2woff2 = require('gulp-ttf2woff2'),
    terser = require('gulp-terser');

sass.compiler = require('node-sass');


let projectName = "Nectar/urbanfarmcy";

let paths = {
    module: "app/code/Nectar",
    root: "app/design/frontend/" + projectName,
    dest: "app/design/frontend/" + projectName + "/web/css",
    pub: "pub/static/frontend/" + projectName + "/pt_BR",
    fonts: "app/design/frontend/" + projectName + "/web/fonts",
    img: "app/design/frontend/" + projectName + "/web/images"
};

const compileSass = () => {

    return gulp
        .src(paths.root + '/web/scss/style.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .on('error', (err) => {
            displayError(err);
        })
        .pipe(gulp.dest(paths.dest, {overwrite: true}));

}

const compileModuleSass = () => {

    return gulp
        .src(paths.module + '/**/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .on('error', (err) => {
            displayError(err);
        })
        .pipe(rename(function(file) {
            file.dirname = file.dirname.replace('scss', 'css');
        }))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }, {overwrite: true}));

}

const updateImages = () => {

    return gulp
        .src(paths.img + "/*")
        .pipe(gulp.dest(paths.pub + "/images", {overwrite: true}));

}

const minifyJS = (path) => {
    // console.log(path);
    
    // return gulp.src(path)
    //     .pipe(terser())
    //     .pipe(rename(function(file) {
    //         file.extname = ".min.js";
    //     }))
    //     .pipe(gulp.dest(function(file) {
    //         return file.base;
    //     }, {overwrite: true}));
}

const watchAll = () => {

    let cssWatcher = gulp.watch( [paths.root + '/web/scss/**/*.scss'], compileSass );
    cssWatcher.on('change', (path, stats) => { console.log('O arquivo ' + path + ' foi modificado'); });

    let moduleCssWatcher = gulp.watch( [paths.module + '/**/*.scss'], compileModuleSass );
    moduleCssWatcher.on('change', (path, stats) => { console.log('O arquivo ' + path + ' foi modificado'); });

    let imageWatcher = gulp.watch( [paths.img + '/*'], updateImages );
    imageWatcher.on('add', (path, stats) => { console.log('A imagem ' + path + ' foi adicionada'); });
    imageWatcher.on('change', (path, stats) => { console.log('A imagem ' + path + ' foi modificada'); });
    imageWatcher.on('unlink', (path, stats) => { console.log('A imagem ' + path + ' foi removida'); });

    // let javascriptWatcher = gulp.watch([
    //     paths.root + "/**/!(*.min).js",
    //     "!" + paths.root + "/**/requirejs-config.js",
    //     paths.module + "/**/!(*.min).js",
    //     "!" + paths.module + "/**/requirejs-config.js"
    // ]);
    // javascriptWatcher.on('change', (path, stats) => {
    //     minifyJS(path);
    //     console.log('O arquivo ' + path + ' foi modificado');
    // });

    console.log('Manda ver, consagrado(a)!');

}

const convertWoff = () => {

    return gulp.src([paths.fonts + '/**/*.ttf'])
        .pipe(ttf2woff())
        .pipe(gulp.dest(paths.fonts));

}

const convertWoff2 = () => {

    return gulp.src([paths.fonts + '/**/*.ttf'])
        .pipe(ttf2woff2())
        .pipe(gulp.dest(paths.fonts));

}

const displayError = function(error) {

    let errorString = '[' + error.plugin + ']';
    errorString += ' ' + error.message.replace("\n",'');

    if(error.fileName)
        errorString += ' in ' + error.fileName;

    if(error.lineNumber)
        errorString += ' on line ' + error.lineNumber;

    console.error(errorString);
    notify(errorString);

}

gulp.task("compile", compileSass);
gulp.task("compile_module", compileModuleSass);
gulp.task("minify_js", minifyJS);
gulp.task("watch", watchAll);
gulp.task("convert:woff", convertWoff);
gulp.task("convert:woff2", convertWoff2);