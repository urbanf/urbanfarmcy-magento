<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Newsletter;

class SubscriberSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;

    public function __construct(\Rdshop\Rdstation\Helper\Connect $_connect, \Psr\Log\LoggerInterface $_logger, \Magento\Framework\DataObjectFactory $objectFactory)
    {
        $this->_connect = $_connect;
        $this->_logger = $_logger;
        $this->objectFactory = $objectFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        try{
            if ($this->_connect->isEnabled()) {
                $subscriber = $observer->getEvent()->getSubscriber();
                $data = $this->objectFactory->create();
                $data->setData('email', $subscriber->getSubscriberEmail()); 
                $data->setData('cf_store_name', $this->_connect->storeManager->getStore()->getName());
                if($subscriber->getSubscriberStatus() == \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED){
                    $this->_connect->addLeadConversion('newsletter-subscribe', $data);
                }else{
                    $this->_connect->addLeadConversion('newsletter-unsubscribe',$data);
                }
            }
        }catch(Exception $e){
            $this->logger->error($e->getMessage());
        } 
    } 
}

