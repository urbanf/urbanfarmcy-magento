<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Checkout;

class CartAddProductComplete implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;
    protected $_customerSession;
    protected $priceCurrency; 

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency)
    {
        $this->_connect = $_connect;
        $this->_logger = $_logger;
        $this->objectFactory = $objectFactory;
        $this->_customerSession = $session;
        $this->priceCurrency = $priceCurrency;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    { 
        try{
            
            if ($this->_connect->isEnabled() && $this->_customerSession->isLoggedIn()) {
                $product = $observer->getEvent()->getProduct();
                $data = $this->objectFactory->create();
                $data->setEmail($this->_customerSession->getCustomer()->getEmail());
                $data->setCfProdutoSku($product->getSku());
                $data->setCfProdutoQty((string)$product->getQty());
                $data->setCfProdutoPrice($this->priceCurrency->format($product->getPrice(),false,2));
                $this->_connect->addLeadConversion('product-added-to-cart', $data); 
            }  
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        } 
    }
}

