<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Customer;

class RegisterSuccess implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;
    private $customerFactory;
    protected $addressFactory;
    protected $regionFactory;

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory, 
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory)
    {
        $this->_connect = $_connect;
        $this->_logger = $_logger;
        $this->objectFactory = $objectFactory;
        $this->customerFactory = $customerFactory; 
        $this->addressFactory = $addressFactory;  
        $this->regionFactory = $regionFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    { 
        try{
            if ($this->_connect->isEnabled()) {
                $customer = $observer->getEvent()->getCustomer();
                $customer = $this->customerFactory->create()->load($customer->getId());
    
                $data = $this->objectFactory->create();
                $data->setEmail($customer->getEmail());
                $data->setName($customer->getName());
                if($customer->getData('dob')){
                    $data->setCfAniversario(strftime('%d de %B de %Y', strtotime($customer->getDob())));
                    $data->setCfMesAniversario(strftime('%B', strtotime($customer->getDob())));
                }
                if($customer->getData('gender')){
                    $data->setCfGender($customer->getAttribute('gender')->getSource()->getOptionText($customer->getData('gender')));
                }
                if($customer->getData('taxvat')){
                    $data->setCfCpfCnpj($customer->getTaxvat());
                }
                
                if($customer->getDefaultShipping()){
                    $shippingAddress = $this->addressFactory->create()->load($customer->getDefaultShipping());

                    $data->setCity($shippingAddress->getCity());
                    $data->setPersonalPhone($shippingAddress->getTelephone());
                    $data->setMobilePhone($shippingAddress->getFax());
                    $data->setCfCep($shippingAddress->getPostcode());
                    $data->setCfBairro($shippingAddress->getStreet4());

                    if ($regionId = $shippingAddress->getRegionId()) {
                        $region = $this->regionFactory->create()->load($regionId);
                        $uf = $region->getCode();
                        if ($uf) {
                            $data->setUf($uf);  
                        }
                    }
                }

                $data->setCfTipoPessoa($customer->getGroupId() == 2 ? 'Pessoa Jurídica' : 'Pessoa Física');
                $data->setData('cf_store_name', $this->_connect->storeManager->getStore()->getName()); 
    
                $this->_connect->addLeadConversion('account-create', $data); 

            } 
        }catch(Exception $e){
            $this->logger->error($e->getMessage());
        } 
    }
}

