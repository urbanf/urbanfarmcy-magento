<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Sales;
use Magento\Framework\App\ObjectManager;

class OrderInvoicePay implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;
    protected $priceCurrency; 

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency)
    { 
        $this->_connect = $_connect;
        $this->_logger = $_logger; 
        $this->objectFactory = $objectFactory;
        $this->priceCurrency = $priceCurrency;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        try{       
            if ($this->_connect->isEnabled()) {
                $invoice = $observer->getEvent()->getInvoice();
                $data = $this->objectFactory->create();
                $data->setEmail($invoice->getOrder()->getCustomerEmail());
                $data->setValue($invoice->getGrandTotal());
                $data->setFunnelName('default'); 
                $this->_connect->addLeadConversion('mark-sale', $data); 
            }  
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        } 
    }
}

