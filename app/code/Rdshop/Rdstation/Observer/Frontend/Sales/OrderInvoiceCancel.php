<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Sales;
use Magento\Framework\App\ObjectManager;

class OrderInvoiceCancel implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory)
    { 
        $this->_connect = $_connect;
        $this->_logger = $_logger; 
        $this->objectFactory = $objectFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        try{       
            if ($this->_connect->isEnabled()) { 
                $creditMemo = $observer->getEvent()->getCreditmemo();
                $data = $this->objectFactory->create();
                $data->setEmail($creditMemo->getOrder()->getCustomerEmail());
                $data->setData('cf_order_id', $creditMemo->getOrder()->getIncrementId());
                $data->setData('cf_order_status', 'closed');
                $this->_connect->addLeadConversion('order-cancel', $data);
            }   
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        } 
    }
}

