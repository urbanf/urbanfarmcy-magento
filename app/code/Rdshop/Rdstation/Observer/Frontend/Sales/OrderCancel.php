<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Sales;
use Magento\Framework\App\ObjectManager;

class OrderCancel implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory)
    { 
        $this->_connect = $_connect;
        $this->_logger = $_logger; 
        $this->objectFactory = $objectFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        try{       
            if ($this->_connect->isEnabled()) {
                $order = $observer->getEvent()->getOrder();
                $data = $this->objectFactory->create();
                $data->setEmail($order->getCustomerEmail());
                $data->setData('cf_order_id', $order->getIncrementId());
                $data->setData('cf_order_status', $order->getStatus());
                $this->_connect->addLeadConversion('order-cancel', $data);
            }  
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        } 
    }
}

