<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rdshop\Rdstation\Observer\Frontend\Sales;
use Magento\Framework\App\ObjectManager;

class OrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{
    protected $_connect;
    private $_logger;
    private $objectFactory;
    protected $priceCurrency; 

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency)
    {
        $this->_connect = $_connect;
        $this->_logger = $_logger; 
        $this->objectFactory = $objectFactory;
        $this->priceCurrency = $priceCurrency;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) 
    {
        try{
            if ($this->_connect->isEnabled()) {
                $orderId = $observer->getEvent()->getOrder()->getId();
                $order = ObjectManager::getInstance()->create('\Magento\Sales\Model\Order')->load($orderId);

                $i = 0;
                $orderData = array();
        
                $data = $this->objectFactory->create();
                $data->setName($order->getCustomerFirstname().' '.$order->getCustomerLastname());
                $data->setEmail($order->getCustomerEmail());
                $data->setData('cf_order_id', $order->getIncrementId());
                $data->setData('cf_order_total_items', (int)$order->getTotalItemCount());
                $data->setData('cf_order_status', $order->getStatus());
                $data->setData('cf_order_payment_method', strpos(strtolower($order->getPayment()->getMethodInstance()->getTitle()), 'cartão') !== false ? 'Credit Card' : 'Others'); 
                $data->setData('cf_order_payment_amount', (float)number_format((float)$order->getGrandTotal(),2));

                $orderData[$i]['event_type'] = 'ORDER_PLACED';
                $orderData[$i]['event_family'] = 'CDP';
                $orderData[$i]['payload'] = $data->getData(); 
                 
                foreach($order->getAllItems() as $item) {
                    $i++; 
                    $orderData[$i]['event_type'] = 'ORDER_PLACED_ITEM';
                    $orderData[$i]['event_family'] = 'CDP';
                    $orderData[$i]['payload']['name'] = $order->getCustomerFirstname().' '.$order->getCustomerLastname(); 
                    $orderData[$i]['payload']['email'] = $order->getCustomerEmail(); 
                    $orderData[$i]['payload']['cf_order_id'] = $order->getIncrementId(); 
                    $orderData[$i]['payload']['cf_order_product_id'] = $item->getSku(); 
                    $orderData[$i]['payload']['cf_order_product_sku'] = $item->getName(); 
                }
    
                //$this->_logger->debug(json_encode($orderData));
                $this->_connect->multiCallAction(json_encode($orderData));  

            }  
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        } 
    }
}

