<?php
namespace Rdshop\Rdstation\Model;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Comment implements \Magento\Config\Model\Config\CommentInterface
{
    protected $scopeConfig;
    public function __construct(ScopeConfigInterface $scopeConfig) 
    {
        $this->scopeConfig = $scopeConfig;
    }
    /**
     * Retrieve element comment by element value
     * @param string $elementValue
     * @return string
     */
    public function getCommentText($elementValue)
    { 
        if($this->scopeConfig->getValue('rdshop_rdstation/general/account', $this->scopeConfig::SCOPE_TYPE_DEFAULT)){
            return '<a class="rd-oauth-disconnect" href="javascript:void(0)">Desconectar da Minha Conta</a>';
        }else{
            return '<a class="rd-oauth-integration" href="javascript:void(0)">Clique aqui para prosseguir com a configuração de sua conta.</a>';
        }
    }
}