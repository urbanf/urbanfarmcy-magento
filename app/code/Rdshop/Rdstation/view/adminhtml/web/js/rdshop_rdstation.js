require(
    [
        'jquery',
        'mage/translate',
    ],
    function ($) { 
        if($("#rdshop_rdstation_general_active").length ){
            (function RDStationIntegration() {

                var SERVER_ORIGIN = 'http://rdshop.digital';
                var CLIENT_ID = 'd71a1ed8-3c90-4e83-96a0-61546100e080';
                var REDIRECT_URL = 'http://rdshop.digital/auth.php';
                var AUTHENTICATION_ENDPOINT = 'https://api.rd.services/auth/dialog';
                var newWindowInstance = null;
                    
                function oauthIntegration(message) {
                    if (message.origin === SERVER_ORIGIN) {
                        persist(message);
                    
                        if (newWindowInstance) {
                            newWindowInstance.close();
                        }
                    } 
                }
                
                function bindConnectButton() {
                    var button = document.querySelector('.rd-oauth-integration');
                    if(button){
                        button.addEventListener('click', function () {
                        newWindowInstance = window.open(AUTHENTICATION_ENDPOINT + '?client_id=' + CLIENT_ID + '&;redirect_url=' + REDIRECT_URL, '_blank')
                        })
                    }
                }
                
                function bindDisconnectButton(SERVER_TOKEN) {
                    var disconnectButton = document.querySelector('.rd-oauth-disconnect');
                
                    if(disconnectButton){
                        disconnectButton.addEventListener('click', function() {
                            var data = { action: 'rdsm-disconnect-oauth' };
                
                            $('body').trigger('processStart');

                            $.ajax({
                                method: "GET",
                                url: SERVER_TOKEN+'disconnect',
                                data: {},
                                success: function() {
                                    window.location.reload(); 
                                }
                            });
                        })
                    }
                }
                
                function listenForMessage() {
                    window.addEventListener('message', oauthIntegration);
                }
                
                function persist(message) {
                    $(document).ready(function ($) {
                        var tokens = JSON.parse(message.data);
                        accessToken = tokens.accessToken;
                        $('body').trigger('processStart');
                        $.ajax({
                            method: "GET", 
                            url: document.getElementById('rdshop_rdstation_general_base_url').value+'rdstation/index/'+'tokens',
                            data: { code: accessToken},
                            success: function() {
                                $.ajax({
                                    method: "GET",
                                    url: document.getElementById('rdshop_rdstation_general_base_url').value+'rdstation/index/'+'createfields',
                                    data: {},
                                    success: function() {
                                        window.location.reload();
                                    }
                                });                                
                            } 
                        });
                    });
                }
                
                function hideFields(){
                    $("#row_rdshop_rdstation_general_client_id").hide();
                    $("#row_rdshop_rdstation_general_client_secret").hide();
                    $("#row_rdshop_rdstation_general_access_token").hide();
                    $("#row_rdshop_rdstation_general_tracking_code").hide();
                    $("#row_rdshop_rdstation_general_refresh_token").hide();
                    $("#row_rdshop_rdstation_general_code").hide();
                    $("#row_rdshop_rdstation_general_cart_abandoned_time").hide();
                    $("#row_rdshop_rdstation_general_base_url").hide();
                    $("#row_rdshop_rdstation_general_user_shop").hide();
                    $("#row_rdshop_rdstation_general_pass_shop").hide();
                }
                
                function init() {
                    bindConnectButton();
                    bindDisconnectButton(document.getElementById('rdshop_rdstation_general_base_url').value+'rdstation/index/');
                    listenForMessage();
                    //hideFields(); 
                }
                window.addEventListener('load', init);
            })();
        }
    }
);