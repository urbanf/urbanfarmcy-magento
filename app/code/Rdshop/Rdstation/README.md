# Module Rdshop Rdstation

    ``rdshop/module-rdstation``

## Main Functionalities
Integração entre Magento 2.x e RdStation

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Rdshop`
 - Enable the module by running `php bin/magento module:enable Rdshop_Rdstation`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

## Configuration

 - Ativo (config/general/active)

 - account (config/general/account)

 - client_id (config/general/client_id)

 - client_secret (config/general/client_secret)

 - access_token (config/general/access_token)

 - tracking_code (config/general/tracking_code)

 - refresh_token (config/general/refresh_token)

 - expires_in (config/general/expires_in)

 - code (config/general/code)

 - cart_abandoned_time (config/general/cart_abandoned_time)

 - base_url (config/general/base_url)

 - user_shop (config/general/user_shop)

 - pass_shop (config/general/pass_shop)


## Specifications

 - Observer
	- sales_order_invoice_register > Rdshop\Rdstation\Observer\Frontend\Sales\OrderInvoicePay

 - Observer
	- sales_order_creditmemo_refund > Rdshop\Rdstation\Observer\Frontend\Sales\OrderInvoiceCancel

 - Observer
	- order_cancel_after > Rdshop\Rdstation\Observer\Frontend\Sales\OrderCancel

 - Observer
	- customer_register_success > Rdshop\Rdstation\Observer\Frontend\Customer\RegisterSuccess

 - Observer
	- checkout_cart_add_product_complete > Rdshop\Rdstation\Observer\Frontend\Checkout\CartAddProductComplete

 - Observer
	- newsletter_subscriber_save_after > Rdshop\Rdstation\Observer\Frontend\Newsletter\SubscriberSaveAfter

 - Observer
	- checkout_onepage_controller_success_action > Rdshop\Rdstation\Observer\Frontend\Sales\OrderPlaceAfter