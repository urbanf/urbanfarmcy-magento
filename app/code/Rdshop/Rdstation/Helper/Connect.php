<?php
namespace Rdshop\Rdstation\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Connect extends \Magento\Framework\App\Helper\AbstractHelper
{
    const LEAD_CONTACTFORM                     = 'contact-form';
    const LEAD_ORDERPLACE                      = 'order-place';
    const LEAD_ACCOUNTCREATE                   = 'account-create';
    const LEAD_PRODUCTADDEDTOCART              = 'product-added-to-cart';
    const LEAD_MARKSALE                        = 'mark-sale';

    protected $configWriter;
    protected $scopeConfig;
    protected $logger;
	public $storeManager;
	protected $_helper;

    public function __construct(WriterInterface $configWriter, ScopeConfigInterface $scopeConfig, LoggerInterface $logger, StoreManagerInterface $storeManager, \Rdshop\Rdstation\Helper\Data $helper)
    {
        $this->configWriter = $configWriter; 
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
		$this->storeManager = $storeManager; 
		$this->_helper = $helper;
    }
 
	public function isEnabled()
	{
        return $this->scopeConfig->getValue('rdshop_rdstation/general/active', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }
    
	public function getAuthToken()
	{ 		
        $expiresTime = $this->scopeConfig->getValue('rdshop_rdstation/general/expires_in', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
		if($expiresTime){
			if($expiresTime > date('Y-m-d H:i:s')){
                return $this->scopeConfig->getValue('rdshop_rdstation/general/access_token', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
			}else{
				return $this->getNewToken();
			}
		}
		return false;
    }
    
	public function getNewToken()
	{
		$client_id = $this->scopeConfig->getValue('rdshop_rdstation/general/client_id', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
		$client_secret = $this->scopeConfig->getValue('rdshop_rdstation/general/client_secret', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
		$refresh_token = $this->scopeConfig->getValue('rdshop_rdstation/general/refresh_token', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
		$url_base = parse_url('http://' . str_replace(array('https://', 'http://'), '', $this->storeManager->getStore()->getBaseUrl()), PHP_URL_HOST);

		if($client_id && $client_secret && $refresh_token && $url_base){
			$curl = curl_init();
			ob_start();
			$user = $this->scopeConfig->getValue('rdshop_rdstation/general/user_shop', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
			$pass = $this->scopeConfig->getValue('rdshop_rdstation/general/pass_shop', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://rdshop.digital/token.php' ); 
			curl_setopt($ch, CURLOPT_HEADER, 0 );
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode( $user . ':' . $pass ) ) );
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('client_id' => $client_id, 'client_secret' => $client_secret, 'refresh_token' => $refresh_token, 'url_base' => $url_base)));
			curl_exec($ch);
			$response = json_decode(ob_get_contents());
			ob_end_clean();
			$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
			curl_close($ch);					
			
			if($response->success){
                $expires_hours = sprintf('%02d', ($response->expires_in / 3600), ($response->expires_in / 60 % 60), 24 % 60);
                $this->configWriter->save('rdshop_rdstation/general/access_token', $response->access_token, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
                $this->configWriter->save('rdshop_rdstation/general/refresh_token', $response->refresh_token, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
				$this->configWriter->save('rdshop_rdstation/general/expires_in', date("Y-m-d H:i:s",strtotime('+'.$expires_hours.' hours', strtotime(date("Y-m-d H:i:s")))), ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
				$this->_helper->clearCache();
				return $response->access_token;
			}else{ 
				$this->configWriter->save('rdshop_rdstation/general/access_token', '', ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
				$this->configWriter->save('rdshop_rdstation/general/refresh_token', '', ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
				$this->configWriter->save('rdshop_rdstation/general/expires_in', '', ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0); 
				return false;
			}
			
			curl_close($curl);
		}
    }
    
    public function addLeadConversion($identifier, $data) 
	{
		if($token = $this->getAuthToken()){
			$curl = curl_init();
			$payload = $data->getData();
			if($identifier == 'mark-sale'){
				$params = '{
					"event_type": "SALE",
					"event_family":"CDP",
					"payload": '.json_encode($payload).'
				}';
			}else{
				$payload['conversion_identifier'] = $identifier;
				$params = '{
					"event_type": "CONVERSION",
					"event_family":"CDP",
					"payload": '.json_encode($payload).'
				}';	
			}
			 
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.rd.services/platform/events",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $params,
				CURLOPT_HTTPHEADER => array( 
					"authorization: Bearer  ".$token."",
					"cache-control: no-cache",
					"content-type: application/json",
				),
			));

			try{
				$response = curl_exec($curl);
				//$this->logger->debug(json_encode($response));
			}catch(Exception $e){
                $this->logger->error($e->getMessage());
			}
			
			curl_close($curl);
		}
    }

    public function multiCallAction($params) 
	{
		if($token = $this->getAuthToken()){
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.rd.services/platform/events/batch",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $params,
				CURLOPT_HTTPHEADER => array( 
					"authorization: Bearer  ".$token."",
					"cache-control: no-cache",
					"content-type: application/json",
				),
			));

			try{
				$response = curl_exec($curl);
				//$this->logger->debug($response);
			}catch(Exception $e){
				$this->logger->error($e->getMessage());
			}
			
			curl_close($curl);
		}
    }

}
