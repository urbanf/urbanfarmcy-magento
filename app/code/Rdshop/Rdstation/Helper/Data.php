<?php
namespace Rdshop\Rdstation\Helper;
date_default_timezone_set('America/Sao_Paulo');
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $configWriter;
    protected $storeManager;
    protected $scopeConfig;
    protected $logger;
    protected $cacheTypeList;
    protected $cacheFrontendPool;

    public function __construct(WriterInterface $configWriter, StoreManagerInterface $storeManager, ScopeConfigInterface $scopeConfig, LoggerInterface $logger, TypeListInterface $cacheTypeList, Pool $cacheFrontendPool) 
    {
        $this->configWriter = $configWriter; 
        $this->storeManager = $storeManager; 
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->cacheTypeList = $cacheTypeList;
    }

    public function setConfig($path, $value)
    {
        $this->configWriter->save($path, $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 0);
    }

    public function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    public function getStoreManager()
    {
        return $this->storeManager;
    }

    public function logger($level, $function, $log)
    {
        $this->logger->log($level, $function, array($log)); 
    }

    public function clearCache()
    {
        $types = array('config');
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
    }
}