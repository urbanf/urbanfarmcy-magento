<?php
namespace Rdshop\Rdstation\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Tracking extends \Magento\Framework\View\Element\Template
{
    protected $scopeConfig;
    
    public function __construct(\Magento\Framework\View\Element\Template\Context $context, array $data = [], ScopeConfigInterface $scopeConfig) 
    {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
    }

    public function getTracking()
    {
        return $this->scopeConfig->getValue('rdshop_rdstation/general/tracking_code', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    public function getActive()
    {
        return $this->scopeConfig->getValue('rdshop_rdstation/general/active', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }
}