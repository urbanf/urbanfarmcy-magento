<?php
namespace Rdshop\Rdstation\Block;
class Disabled extends \Magento\Config\Block\System\Config\Form\Field
{
    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = []) 
    {
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element) 
    {
        $element->setDisabled('disabled');
        return parent::_getElementHtml($element); 
    }
}