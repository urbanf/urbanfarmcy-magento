<?php
namespace Rdshop\Rdstation\Block;

class Base extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_helper;

    public function __construct(\Magento\Backend\Block\Template\Context $context,\Rdshop\Rdstation\Helper\Data $helper) 
    {
         parent::__construct($context);
         $this->_helper = $helper;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element) 
    {
        $element->setDisabled('disabled');
        if(!$element->getValue()){
            $element->setValue($this->_storeManager->getStore()->getBaseUrl());
            $this->_helper->setConfig('rdshop_rdstation/general/base_url', $this->_storeManager->getStore()->getBaseUrl());
        }
        return parent::_getElementHtml($element); 
    }
} 