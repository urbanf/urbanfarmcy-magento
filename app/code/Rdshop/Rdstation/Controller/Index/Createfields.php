<?php
namespace Rdshop\Rdstation\Controller\Index;

class Createfields extends \Magento\Framework\App\Action\Action
{
	protected $_helper;

    public function __construct(\Magento\Framework\App\Action\Context $context,\Rdshop\Rdstation\Helper\Data $helper) 
    {
         parent::__construct($context);
         $this->_helper = $helper;
	} 
	
	public function execute()
	{ 
		$access_token = $this->_helper->getConfig('rdshop_rdstation/general/access_token');
		if($access_token){
			$fields = array('cf_mensagem' => 'Mensagem','cf_aniversario' => 'Aniversário','cf_mes_aniversario' => 'Mês de Aniversário','cf_gender' => 'Sexo','cf_cpf_cnpj' => 'CPF/CNPJ','cf_tipo_pessoa' => 'Tipo de Pessoa','cf_store_name' => 'Nome da Loja','cf_produto_sku' => 'Código do Produto','cf_produto_qty' => 'Quantidade do Produto','cf_produto_price' => 'Preço do Produto','cf_valor' => 'Total do Pedido','cf_cep' => 'CEP','cf_bairro' => 'Bairro','cf_produto_nome' => 'Nome do Produto','cf_produto_categoria' => 'Categoria do Produto','cf_metodo_pagamento' => 'Método de Pagamento','cf_metodo_entrega' => 'Método de Frete');
			foreach($fields as $key => $field){
				$row = array('name' => array('pt-BR' => $field), 'label' => array('pt-BR' => $field), 'api_identifier' => $key, 'data_type' => 'STRING', 'presentation_type' => 'TEXT_INPUT');
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://api.rd.services/platform/contacts/fields",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => json_encode($row),
					CURLOPT_HTTPHEADER => array( 
						"authorization: Bearer  ".$access_token."",
						"cache-control: no-cache",
						"content-type: application/json",
					),
				));

				try{
					$result = curl_exec($curl);
					//$this->_helper->logger('ERROR', 'createFieldsControllerIndex', $result);
				}catch(Exception $e){ 
					$this->_helper->logger('ERROR', 'createFieldsControllerIndex', $e->getMessage());
				}
			}
			curl_close($curl); 
		}
    }
}  