<?php
namespace Rdshop\Rdstation\Controller\Index;

class Disconnect extends \Magento\Framework\App\Action\Action
{
	protected $_helper;

    public function __construct(\Magento\Framework\App\Action\Context $context,\Rdshop\Rdstation\Helper\Data $helper) 
    {
         parent::__construct($context);
         $this->_helper = $helper;
	}
	
	public function execute()
	{ 
		$this->_helper->setConfig('rdshop_rdstation/general/code', '');
		$this->_helper->setConfig('rdshop_rdstation/general/access_token', '');
		$this->_helper->setConfig('rdshop_rdstation/general/refresh_token', '');
		$this->_helper->setConfig('rdshop_rdstation/general/expires_in', ''); 
		$this->_helper->setConfig('rdshop_rdstation/general/tracking_code', ''); 
		$this->_helper->setConfig('rdshop_rdstation/general/account', ''); 
		$this->_helper->setConfig('rdshop_rdstation/general/cart_abandoned_time', ''); 
		$this->_helper->clearCache();
    }
}  