<?php
namespace Rdshop\Rdstation\Controller\Index;
date_default_timezone_set('America/Sao_Paulo');

class Tokens extends \Magento\Framework\App\Action\Action
{
    protected $_helper;

    public function __construct(\Magento\Framework\App\Action\Context $context,\Rdshop\Rdstation\Helper\Data $helper) 
    {
         parent::__construct($context);
         $this->_helper = $helper;
    }

	public function execute()
	{
		if($code = $this->getRequest()->getParam('code')){
			$client_id = $this->_helper->getConfig('rdshop_rdstation/general/client_id');
			$client_secret = $this->_helper->getConfig('rdshop_rdstation/general/client_secret');
			$url_base = parse_url('http://' . str_replace(array('https://', 'http://'), '', $this->_helper->getStoreManager()->getStore()->getBaseUrl()), PHP_URL_HOST);
            
			if($client_id && $client_secret && $url_base){
				$curl = curl_init();
				ob_start();
				$user = $this->_helper->getConfig('rdshop_rdstation/general/user_shop');
				$pass = $this->_helper->getConfig('rdshop_rdstation/general/pass_shop');
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'http://rdshop.digital/token.php' ); 
				curl_setopt($ch, CURLOPT_HEADER, 0 );
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode( $user . ':' . $pass ) ) );
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('client_id' => $client_id, 'client_secret' => $client_secret, 'code' => $code, 'url_base' => $url_base)));
				curl_exec($ch);
				$response = json_decode(ob_get_contents());
				ob_end_clean();
				$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
				curl_close($ch);		 			
				
				if($response->success){
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.rd.services/marketing/tracking_code",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => array( 
							"authorization: Bearer  ".$response->access_token."",
							"cache-control: no-cache",
							"content-type: application/json",
						),
					));
					try{
						$responseTracking = curl_exec($curl);
						if($responseTracking){
							$responseTracking = json_decode($responseTracking);
							date_default_timezone_set('America/Sao_Paulo');
							$expires_hours = sprintf('%02d', ($response->expires_in / 3600), ($response->expires_in / 60 % 60), 24 % 60);
							$this->_helper->setConfig('rdshop_rdstation/general/code', $code);
							$this->_helper->setConfig('rdshop_rdstation/general/access_token', $response->access_token);
							$this->_helper->setConfig('rdshop_rdstation/general/refresh_token', $response->refresh_token);
							$this->_helper->setConfig('rdshop_rdstation/general/tracking_code', $responseTracking->path);
							$this->_helper->setConfig('rdshop_rdstation/general/expires_in', date("Y-m-d H:i:s",strtotime('+'.$expires_hours.' hours', strtotime(date("Y-m-d H:i:s")))));				
							$this->_helper->setConfig('rdshop_rdstation/general/cart_abandoned_time', date("Y-m-d H:i:s"));
							$this->_helper->setConfig('rdshop_rdstation/general/base_url', $this->_helper->getStoreManager()->getStore()->getBaseUrl()); 
							$this->getAccountName($response->access_token);
							$this->_helper->clearCache();
						}

					}catch(Exception $e){
                        $this->_helper->logger('ERROR', 'tokensControllerIndex', $e->getMessage());
					}
					
				}else{ 
					$this->_helper->setConfig('rdshop_rdstation/general/code', '');
					$this->_helper->setConfig('rdshop_rdstation/general/access_token', ''); 
					$this->_helper->setConfig('rdshop_rdstation/general/refresh_token', '');
					$this->_helper->setConfig('rdshop_rdstation/general/expires_in', ''); 
					$this->_helper->setConfig('rdshop_rdstation/general/tracking_code', '');
					$this->_helper->clearCache(); 
				}
				
				curl_close($curl);
			}
		}
    }

	public function getAccountName($access_token)
	{
		if($access_token){
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.rd.services/marketing/account_info",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array( 
					"authorization: Bearer  ".$access_token."",
					"cache-control: no-cache",
					"content-type: application/json",
				),
			));

			try{ 
				$response = curl_exec($curl);
				if($response){
					$response = json_decode($response);
					$this->_helper->setConfig('rdshop_rdstation/general/account', $response->name);
				}
			}catch(Exception $e){ 
                $this->_helper->logger('ERROR', 'tokensControllerIndex.getAccountName', $e->getMessage());
			}
			curl_close($curl); 
		}
	}
}