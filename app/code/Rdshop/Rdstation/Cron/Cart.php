<?php
/**
 * Copyright © RdShop Digital All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Rdshop\Rdstation\Cron;
use Magento\Framework\App\ObjectManager;

class Cart {

    protected $_connect;
    private $_logger;
    private $objectFactory;
    protected $priceCurrency; 
    protected $_helper;

    public function __construct(
        \Rdshop\Rdstation\Helper\Connect $_connect, 
        \Psr\Log\LoggerInterface $_logger, 
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Rdshop\Rdstation\Helper\Data $helper)
    {
        $this->_connect = $_connect;
        $this->_logger = $_logger; 
        $this->objectFactory = $objectFactory;
        $this->priceCurrency = $priceCurrency;
        $this->_helper = $helper;
    }

    public function execute() 
    {
        try{       
            if ($this->_connect->isEnabled()) {
                $lastUpdated = $this->_helper->getConfig('rdshop_rdstation/general/cart_abandoned_time');
                $collectionCart = ObjectManager::getInstance()->get('Magento\Reports\Model\ResourceModel\Quote\Collection');
                $collectionCart->addFieldToFilter('customer_email', array('neq' => 'NULL'));
                $collectionCart->addFieldToFilter('is_active', array('eq' => 1 ));
                $collectionCart->addFieldToFilter('items_count', array('gt' => 0 ));
                $collectionCart->addFieldToFilter('updated_at', array('gt' => $lastUpdated));
                $collectionCart->getSelect()->order('updated_at ASC');
        
                foreach($collectionCart as $cart){
					if($cart->getCustomerEmail()){
						$lastSent = false;
						$dateFrom = $cart->getUpdatedAt(); 
						$dateTo = date("Y/m/d H:i:s"); 
						$diff = abs(strtotime($dateTo) - strtotime($dateFrom)); 
						$minuts  = floor(($diff * 365*60*60*24 - 30*60*60*24 - 60*60*24 - 60*60)/ 60); 						
						
						if($minuts > 30){
							$lastSent = $cart->getUpdatedAt();																			
							$i = 0;
                            $cartData = array(); 
							$data = $this->objectFactory->create();
							$data->setName($cart->getCustomerFirstname() ? $cart->getCustomerFirstname()." ".$cart->getCustomerLastname() : $cart->getCustomerEmail());
							$data->setEmail($cart->getCustomerEmail());
							$data->setData('cf_cart_id', $cart->getEntityId());
							$data->setData('cf_cart_total_items', (int)$cart->getItemsCount());
							$data->setData('cf_cart_status', 'in_progress');
							 
							$cartData[$i]['event_type'] = 'CART_ABANDONED';
							$cartData[$i]['event_family'] = 'CDP';
							$cartData[$i]['payload'] = $data->getData();

							foreach($cart->getAllVisibleItems() as $item) {
								$i++; 
								$cartData[$i]['event_type'] = 'CART_ABANDONED_ITEM';
								$cartData[$i]['event_family'] = 'CDP';
								$cartData[$i]['payload']['name'] = $cart->getCustomerFirstname() ? $cart->getCustomerFirstname()." ".$cart->getCustomerLastname() : $cart->getCustomerEmail(); 
								$cartData[$i]['payload']['email'] = $cart->getCustomerEmail(); 
								$cartData[$i]['payload']['cf_cart_id'] = $cart->getEntityId(); 
								$cartData[$i]['payload']['cf_cart_product_id'] = $item->getSku(); 
								$cartData[$i]['payload']['cf_cart_product_sku'] = $item->getName(); 
							}

							$this->_connect->multiCallAction(json_encode($cartData));   
                        }
						
						if($lastSent){
                            $this->_helper->setConfig('rdshop_rdstation/general/cart_abandoned_time', $lastSent);
                            $this->_helper->clearCache(); 	
						}
					}
                }
            }   
        }catch(Exception $e){
            $this->_logger->debug($e->getMessage());
        }
    }
}