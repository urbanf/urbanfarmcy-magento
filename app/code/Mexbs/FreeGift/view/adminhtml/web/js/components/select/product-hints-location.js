define([
    'Magento_Ui/js/form/element/select',
    'Mexbs_ApBase/js/model/ap-simple-actions'
], function (Element, ApSimpleActions) {
    'use strict';
    return Element.extend({
        isInSupportedActionArray: function(action){
            return (typeof ApSimpleActions.getConfig() != 'undefined')
                && (typeof ApSimpleActions.getConfig().productHintsLocationSupportingActions != 'undefined')
                && (ApSimpleActions.getConfig().productHintsLocationSupportingActions.indexOf(action) > -1);
        },
        setProductHintsChecked: function (value){
            if (value == 0){
                this.productCartHintsChecked = false;
            }else{
                this.productCartHintsChecked = true;
            }
            this.visible(this.productCartHintsChecked && this.isInSupportedActionArray(this.simpleAction));
        },
        setSimpleAction: function (value){
            this.simpleAction = value;
            this.visible(this.productCartHintsChecked && this.isInSupportedActionArray(this.simpleAction));
        },

        initConfig: function (config) {
            this._super();
            return this;
        }
    });
});
