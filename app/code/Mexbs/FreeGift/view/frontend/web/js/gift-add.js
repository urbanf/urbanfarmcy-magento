define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'Mexbs_ApBase/js/model/promo-add'
], function (
    $,
    modal,
    promoAdd
    ) {
    'use strict';

    return function(options){
        $(document).ready(function(){
            var popupTpl = '<aside ' +
                'class="modal-<%= data.type %> <%= data.modalClass %> ' +
                '<% if(data.responsive){ %><%= data.responsiveClass %><% } %> ' +
                '<% if(data.innerScroll){ %><%= data.innerScrollClass %><% } %>"'+
                'data-role="modal"' +
                'data-type="<%= data.type %>"' +
                'tabindex="0">'+
                '    <div data-role="focusable-start" tabindex="0"></div>'+
                '    <div class="modal-inner-wrap"'+
                'data-role="focusable-scope">'+
                '    <div '+
                'class="modal-content" '+
                'data-role="content">' +
                '<button '+
                'class="action-close" '+
                'data-role="closeBtn" '+
                'type="button">'+
                '<span><%= data.closeText %></span>' +
                '</button>' +
                '</div>'+
                '   </div>'+
                '   </aside>';

            var modalOptions = {
                type: 'popup',
                responsive: false,
                innerScroll: false,
                buttons: [],
                popupTpl: popupTpl,
                modalClass: 'gift-add-to-cart-modal-wrapper',
                closeText: 'X'
            };

            var refreshCartGiftHintMessages = function(){
                $(".top_gift_cart_hint").remove();
                $.ajax({
                    url: options.getCartGiftHintMessagesAjaxUrl,
                    type: 'post',
                    dataType: 'json',
                    showLoader: true,
                    success: function (response) {
                        if(response.hasOwnProperty('status')
                            && (response['status'] == 'success')){
                            if(response.hasOwnProperty('cart_gift_hints_html')){
                                $(".page.messages").append($(response['']));
                            }
                        }
                    }
                });
            };

            var currentGiftTriggerItemsOfSameGroup = null;
            var currentVisibleStep = 1;

            var hideStep = function(stepNumber){
                $('.gift-add-promo-group-wrapper[data-step-index="'+stepNumber+'"]').hide();
            };
            var showStep = function(stepNumber){
                $('.gift-add-promo-group-wrapper[data-step-index="'+stepNumber+'"]').show();
            };

            var selectionsPerStep = [];


            var getMissingProductsText = function(){
                var currentGroupSelected = 0;
                if(typeof selectionsPerStep[currentVisibleStep] != 'undefined'){
                    currentGroupSelected = promoAdd.getStepSelectionsQtyExclProduct(currentVisibleStep, 0, selectionsPerStep);
                }
                var currentGroupQty = $('.gift-add-promo-group-wrapper[data-step-index="' + currentVisibleStep + '"]').attr("data-group-qty");
                var addS = "s";
                if((currentGroupQty - currentGroupSelected) == 1){
                    addS = "";
                }
                return "Please select " + (currentGroupQty - currentGroupSelected) + " more product" + addS;

            };

            var updateProductQtyOfCurrentStep = function(productId){
                if(typeof selectionsPerStep[currentVisibleStep] == 'undefined'){
                    selectionsPerStep[currentVisibleStep] = [];
                }
                var currentGroupQty = $('.gift-add-promo-group-wrapper[data-step-index="' + currentVisibleStep + '"]').attr("data-group-qty");
                var qtyInput = $('.gift-add-promo-product-item-info[data-product-id="'+productId+'"]').find(".gift-add-promo-product-item-qty input");
                var checkbox = $('.gift-add-promo-product-item-info[data-product-id="'+productId+'"]').find(".gift-add-promo-product-checkbox-container input");

                var productQty = 1;
                if(qtyInput.length > 0
                    && $.isNumeric(qtyInput.val())){
                    productQty = qtyInput.val();
                }

                var qtyLeftToSelect = currentGroupQty - promoAdd.getStepSelectionsQtyExclProduct(currentVisibleStep, productId, selectionsPerStep);

                if(productQty > qtyLeftToSelect){
                    productQty = qtyLeftToSelect;
                }

                if(checkbox.is(":checked")
                    && (productQty > 0)){
                    selectionsPerStep[currentVisibleStep][productId] = productQty;
                    qtyInput.val(productQty);
                }else{
                    checkbox.prop("checked", false);
                    if(typeof selectionsPerStep[currentVisibleStep][productId] != 'undefined'){
                        selectionsPerStep[currentVisibleStep].splice(productId, 1);
                    }

                    qtyInput.val("");
                }
            };

            $("body").on("click", "[data-message-type='gift-hint-message']", function(){
                var ajaxUrl = options.promoAddToCartHtmlUrl;

                currentGiftTriggerItemsOfSameGroup = $(this).attr("data-gift-trigger-item-ids-qtys-of-same-group");

                var params = {
                    rule_id: $(this).attr("data-rule-id"),
                    gift_trigger_item_ids_qtys_of_same_group: currentGiftTriggerItemsOfSameGroup,
                    gift_qtys_can_add_per_group: $(this).attr("data-gift-qtys-can-add-per-group")
                };


                promoAdd.sendPromoAddToCartRequest(ajaxUrl, params, '.gift-add-to-cart-modal', modalOptions);
                }).on("click", 'button[data-action="go-to-previous-step"]',
                function(){
                    hideStep(currentVisibleStep);
                    currentVisibleStep--;
                    showStep(currentVisibleStep);
                }).on("click", 'div.swatch-option',
                function(){
                    var checkboxContainer = $(this).closest(".gift-add-promo-product-item-info").find(".gift-add-promo-product-checkbox-container");

                    promoAdd.swatchOptionClickedHandle(this, checkboxContainer);
                }).on("click", '.gift-add-promo-product-checkbox-container',
                function(event){
                    if(!($(this).find('input').is(":checked"))){
                        $(this).find("input").prop("checked", true);
                    }else{
                        $(this).find("input").prop("checked", false);
                    }

                    var productId = $(this).closest('.gift-add-promo-product-item-info').attr("data-product-id");

                    updateProductQtyOfCurrentStep(productId);
                    promoAdd.updateCurrentStepSelectedText(currentVisibleStep, selectionsPerStep, '.gift-add-promo-group-wrapper', '.gift-add-promo-group-chosen');

                    event.stopPropagation();
                    event.preventDefault();
                }).on("click", '.gift-add-promo-wrapper-button-done',
                function(){
                    var allConfigurationOptionsSelected = promoAdd.isAllConfigurationOptionsSelected(selectionsPerStep, currentVisibleStep,'.gift-add-promo-group-wrapper', this, '.gift-add-promo-product-item-info');
                    var atLeastOneProductSelected = promoAdd.isAtLeastOneProductSelected(selectionsPerStep, currentVisibleStep);
                    var allCustomOptionsSelected = promoAdd.isAllCustomOptionsSelected(selectionsPerStep, currentVisibleStep,'.gift-add-promo-group-wrapper', this, '.gift-add-promo-product-item-info', '.gift-add-promo-product-custom-options-wrapper');
                    if(!allConfigurationOptionsSelected || !allCustomOptionsSelected){
                        $('.gift-add-promo-wrapper-error-configurations[data-step-index="' + currentVisibleStep + '"]').show();
                    }else{
                        $('.gift-add-promo-wrapper-error-configurations[data-step-index="' + currentVisibleStep + '"]').hide();
                    }
                    if(!atLeastOneProductSelected){
                        $('.gift-add-promo-wrapper-error-products[data-step-index="' + currentVisibleStep + '"]').text("Por favor selecione um produto").show();
                    }else{
                        $('.gift-add-promo-wrapper-error-products[data-step-index="' + currentVisibleStep + '"]').first().hide();
                    }
                    if(allConfigurationOptionsSelected && allCustomOptionsSelected && atLeastOneProductSelected){
                        $(".gift-add-to-cart-modal-wrapper button.action-close").trigger("click");
                        selectionsPerStep = [];
                        currentVisibleStep = 1;

                        promoAdd.performAddToCartRequest(options.promoAddToCartUrl, '.gift-add-promo-product-checkbox-container input:checked', '.gift-add-promo-product-item-info', 'data-product-id', '.gift-add-promo-product-item-qty input', true, function(){refreshCartGiftHintMessages();}, currentGiftTriggerItemsOfSameGroup);
                    }

                }).on("click", '.gift-add-promo-wrapper-button-next',
                function(){
                    var allConfigurationOptionsSelected = promoAdd.isAllConfigurationOptionsSelected(selectionsPerStep, currentVisibleStep,'.gift-add-promo-group-wrapper', this, '.gift-add-promo-product-item-info');
                    var allCustomOptionsSelected = promoAdd.isAllCustomOptionsSelected(selectionsPerStep, currentVisibleStep,'.gift-add-promo-group-wrapper', this, '.gift-add-promo-product-item-info', '.gift-add-promo-product-custom-options-wrapper');
                    var atLeastOneProductSelected = promoAdd.isAtLeastOneProductSelected(selectionsPerStep, currentVisibleStep);
                    if(!allConfigurationOptionsSelected || !allCustomOptionsSelected){
                        $('.gift-add-promo-wrapper-error-configurations[data-step-index="' + currentVisibleStep + '"]').show();
                    }else{
                        $('.gift-add-promo-wrapper-error-configurations[data-step-index="' + currentVisibleStep + '"]').hide();
                    }
                    if(!atLeastOneProductSelected){
                        $('.gift-add-promo-wrapper-error-products[data-step-index="' + currentVisibleStep + '"]').text("Por favor selecione um produto").show();
                    }else{
                        $('.gift-add-promo-wrapper-error-products[data-step-index="' + currentVisibleStep + '"]').hide();
                    }

                    if(allConfigurationOptionsSelected
                        && atLeastOneProductSelected
                        && allCustomOptionsSelected){
                        hideStep(currentVisibleStep);
                        currentVisibleStep++;
                        showStep(currentVisibleStep);
                    }
                })
                .on("change", ".gift-add-promo-product-item-qty input", function(){
                    var newProductQty = 0;
                    if($.isNumeric($(this).val())){
                        newProductQty = $(this).val();
                    }
                    var checkboxContainer = $(this).closest(".gift-add-promo-product-item-info").find(".gift-add-promo-product-checkbox-container");
                    var productId = $(this).closest(".gift-add-promo-product-item-info").attr("data-product-id");
                    if(newProductQty > 0){
                        if(!(checkboxContainer.find("input").is(":checked"))){
                            checkboxContainer.find("input").prop("checked", true);
                        }
                    }else{
                        if(checkboxContainer.find("input").is(":checked")){
                            checkboxContainer.find("input").prop("checked", false);
                        }
                    }
                    updateProductQtyOfCurrentStep(productId);
                    promoAdd.updateCurrentStepSelectedText(currentVisibleStep, selectionsPerStep, '.gift-add-promo-group-wrapper', '.gift-add-promo-group-chosen');
                });
        });
    };

});