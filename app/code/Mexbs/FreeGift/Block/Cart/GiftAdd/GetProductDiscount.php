<?php
namespace Mexbs\FreeGift\Block\Cart\GiftAdd;

use \Magento\Framework\View\Element\Template;

class GetProductDiscount extends Discount
{
    protected $_template = 'Mexbs_FreeGift::cart/promo/giftadd/discount.phtml';
}