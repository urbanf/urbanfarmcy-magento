<?php
namespace Mexbs\FreeGift\Block\Cart\GiftAdd;


use \Magento\Framework\View\Element\Template;

class BuyXGetYDiscount extends Discount
{
    protected $_template = 'Mexbs_FreeGift::cart/promo/giftadd/discount.phtml';
}