<?php
namespace Mexbs\FreeGift\Block\Cart\GiftAdd;

use \Magento\Framework\View\Element\Template;

class GetProductForEachXSpentDiscount extends Discount
{
    protected $_template = 'Mexbs_FreeGift::cart/promo/giftadd/discount.phtml';
}