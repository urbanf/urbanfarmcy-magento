<?php
namespace Mexbs\FreeGift\Block\Cart\GiftAdd;


use \Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\Product\AbstractProduct;

abstract class Discount extends AbstractProduct
{
    private $helper;
    private $rule;
    private $quote;
    private $giftQtysCanAddPerGroup;
    private $swatchesRenderer;
    private $ruleActionDetail;

    public function __construct(
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\Quote\Model\Quote $quote,
        $giftQtysCanAddPerGroup,
        \Mexbs\ApBase\Helper\Data $helper,
        \Magento\Swatches\Block\Product\Renderer\Listing\Configurable $swatchesRenderer,
        \Magento\Catalog\Block\Product\Context $context,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->rule = $rule;
        $this->quote = $quote;
        $this->giftQtysCanAddPerGroup = $giftQtysCanAddPerGroup;
        $this->swatchesRenderer = $swatchesRenderer;
        $this->swatchesRenderer->setTemplate('Mexbs_ApBase::cart/promo/add/configurableProductRenderer.phtml');
        parent::__construct($context, $data);
    }
    public function getRuleProductGroups(){
        return $this->helper->getPromoProductGroupsForRule($this->rule, $this->quote);
    }
    public function getProductImageUrl($product){
        return $this->helper->getFullProductImageUrl($product->getImage());
    }
    public function getDetailsRenderer($type = null){
        if($type == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE){
            return $this->swatchesRenderer;
        }
        return parent::getDetailsRenderer($type);
    }

    public function getRuleActionDetail(){
        if(!$this->ruleActionDetail){
            $this->ruleActionDetail = $this->helper->getLoadedActionDetail($this->rule);
        }
        return $this->ruleActionDetail;
    }

    public function getRuleProductGroupsWithDisplayDataAndProducts(){
        return  $this->getRuleActionDetail()->getPromoProductGroupsWithDisplayData();
    }

    public function getTotalStepsNumber($qtyCanAddNowPerGroup){
        $totalStepsNumber = 0;
        foreach($qtyCanAddNowPerGroup as $qtyCanAdd){
            if($qtyCanAdd > 0){
                $totalStepsNumber++;
            }
        }
        return $totalStepsNumber;
    }

    public function getGroupsCountForTriggeringItemsGroup(){
        return $this->giftQtysCanAddPerGroup;
    }
}