<?php
namespace Mexbs\FreeGift\Block\Cart;

class GiftHintMessages extends \Magento\Framework\View\Element\Template
{
    protected $cart;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        $this->cart = $cart;
        parent::__construct($context, $data);
    }

    public function getGiftHintMessages(){
        $giftHintMessages = $this->cart->getQuote()->getGiftHintMessages();
        return (is_array($giftHintMessages) ? $giftHintMessages : []);
    }

    public function getGiftQtysCanAddPerGroupPerRule(){
        return $this->cart->getQuote()->getGiftQtysCanAddPerGroup();
    }
}