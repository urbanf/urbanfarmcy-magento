<?php
namespace Mexbs\FreeGift\Model\Checkout;

class Session extends \Magento\Checkout\Model\Session
{
    public function clearCachedQuote(){
        $this->_quote = null;
    }
}