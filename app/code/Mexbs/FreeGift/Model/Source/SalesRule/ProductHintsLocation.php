<?php
namespace Mexbs\FreeGift\Model\Source\SalesRule;

use Magento\Framework\Data\OptionSourceInterface;

class ProductHintsLocation implements OptionSourceInterface
{
    const TYPE_CONFIG = "config";

    public function toOptionArray()
    {
        return [
            [
                'value' => self::TYPE_CONFIG,
                'label' => __('Use Config Value')
            ],
            [
                'value' => \Mexbs\FreeGift\Model\Source\Config\ProductHintsLocation::LOCATION_TRIGGERING_ITEM,
                'label' => __('Triggering Item Line')
            ],
            [
                'value' => \Mexbs\FreeGift\Model\Source\Config\ProductHintsLocation::LOCATION_CART_TOP,
                'label' => __('Top of the Cart')
            ]
        ];
    }
}
