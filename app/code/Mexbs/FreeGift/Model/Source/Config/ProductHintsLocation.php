<?php
namespace Mexbs\FreeGift\Model\Source\Config;

use Magento\Framework\Data\OptionSourceInterface;

class ProductHintsLocation implements OptionSourceInterface
{
    const LOCATION_TRIGGERING_ITEM = "triggering_item";
    const LOCATION_CART_TOP = "cart_top";

    public function toOptionArray()
    {
        return [
            [
                'value' => self::LOCATION_TRIGGERING_ITEM,
                'label' => __('Triggering Item Line')
            ],
            [
                'value' => self::LOCATION_CART_TOP,
                'label' => __('Top of the Cart')
            ]
        ];
    }
}
