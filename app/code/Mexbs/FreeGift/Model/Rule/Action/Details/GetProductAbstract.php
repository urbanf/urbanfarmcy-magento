<?php
namespace Mexbs\FreeGift\Model\Rule\Action\Details;

abstract class GetProductAbstract extends \Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine{
    abstract public function getDiscountMaxQty();
    abstract public function isDiscountPriceTypeApplicable();
    abstract public function isDiscountOrderTypeApplicable();

    const GET_Y_GROUP_NUMBER = '999';

    public function getNonEmptyGroups(){
        $nonEmptyGroups = [];

        $getActionDetail = $this->getProductsGetActionDetails();
        if($getActionDetail){
            $getActionDetails = $getActionDetail->getActionDetails();
            if(isset($getActionDetails[0])){
                $nonEmptyGroups[self::GET_Y_GROUP_NUMBER] = $getActionDetail;
            }
        }
        return $nonEmptyGroups;
    }

    public function getNonEmptyGroupNumbers(){
        $nonEmptyGroupNumbers = [];

        $getActionDetail = $this->getProductsGetActionDetails();
        if($getActionDetail){
            $getActionDetails = $getActionDetail->getActionDetails();
            if(isset($getActionDetails[0])){
                $nonEmptyGroupNumbers[] = self::GET_Y_GROUP_NUMBER;
            }
        }
        return $nonEmptyGroupNumbers;
    }

    public function getGetYGroupNumber(){
        return self::GET_Y_GROUP_NUMBER;
    }

    public function getGroupQty($groupNumber){
        if($groupNumber == self::GET_Y_GROUP_NUMBER){
            return $this->getGetProductQty();
        }
        return 0;
    }

    public function getGroupNumbersDisplayableInPopupAdd(){
        return [self::GET_Y_GROUP_NUMBER];
    }

    public function getRequiredGroupsForRule(){
        return [self::GET_Y_GROUP_NUMBER];
    }

    public function getGroupNumbersToIndex(){
        return [self::GET_Y_GROUP_NUMBER];
    }

    public function getGroupNumbersToActionType(){
        return [
            self::GET_Y_GROUP_NUMBER => 'get'
        ];
    }

    public function getRuleActionType(){
        return "getproduct";
    }

    public function getProductGroupActionTypeByNumber($groupNumber){
        return "get";
    }


    public function hasAddressConditionsInActionInAnyGroup(){
        return $this->getProductsGetActionDetails()->hasAddressConditionsInAction();
    }

    protected function _getDiscountExpression($discountAmount){
        $currencySymbol = $this->apHelper->getCurrentCurrencySymbol();

        if($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT){
            return ($discountAmount == 100 ? "de graça" : "with " . $discountAmount ."% de desconto");
        }elseif($this->getDiscountType() == self::DISCOUNT_TYPE_FIXED){
            return sprintf("com %s%s de desconto", $currencySymbol, $discountAmount);
        }elseif($this->getDiscountType() == self::DISCOUNT_TYPE_FIXED_PRICE){
            return sprintf("por apenas %s%s", $currencySymbol, $discountAmount);
        }

        return '';
    }


    protected function _getCouponNotValidHintMessage(
        $itemYouCanGetDiscountedNowData
    )
    {
        $hintMessage = "";

        if(!empty($itemYouCanGetDiscountedNowData)){
            $hintMessage .= "You can now add ";
            if(!empty($itemsAlreadyDiscountedData)){
                $hintMessage .= "another ";
            }
            $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedNowData['discount_amount']);

            $hintMessage .= sprintf(
                "%s%s %s",
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? (empty($itemsAlreadyDiscountedData) ? "one ": "") : $itemYouCanGetDiscountedNowData['qty']." "),
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? $itemYouCanGetDiscountedNowData['hints_singular'] : $itemYouCanGetDiscountedNowData['hints_plural']),
                (($itemYouCanGetDiscountedNowData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
            $hintMessage .= "!";
        }

        return $hintMessage;
    }

    protected function _getHintMessages(
        $itemsAlreadyDiscountedData,
        $itemYouCanGetDiscountedNowData
    )
    {
        $hintMessage = "";

        if(!empty($itemsAlreadyDiscountedData)){
            $hintMessage .= "Você ganhou! ";

            $discountExpression = $this->_getDiscountExpression($itemsAlreadyDiscountedData['discount_amount']);
            $hintMessage .= sprintf(
                "%s %s %s",
                ($itemsAlreadyDiscountedData['qty'] == 1 ? "one" : $itemsAlreadyDiscountedData['qty']),
                ($itemsAlreadyDiscountedData['qty'] == 1 ? $itemsAlreadyDiscountedData['hints_singular'] : $itemsAlreadyDiscountedData['hints_plural']),
                (($itemsAlreadyDiscountedData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
            $hintMessage .= ".";
        }

        if($hintMessage != ""){
            $hintMessage .= " ";
        }

        if(!empty($itemYouCanGetDiscountedNowData)){
            $hintMessage .= "Add ";
            if(!empty($itemsAlreadyDiscountedData)){
                $hintMessage .= "another ";
            }
            $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedNowData['discount_amount']);

            $hintMessage .= sprintf(
                "%s%s %s",
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? (empty($itemsAlreadyDiscountedData) ? "one ": "") : $itemYouCanGetDiscountedNowData['qty']." "),
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? $itemYouCanGetDiscountedNowData['hints_singular'] : $itemYouCanGetDiscountedNowData['hints_plural']),
                (($itemYouCanGetDiscountedNowData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
            $hintMessage .= "!";
        }
        return $hintMessage;
    }


    public function markMatchingItemsAndGetHint($items, $address){
        if(!$this->getRule()
            || !$this->getRule()->getId()
        ){
            return null;
        }

        $discountAmount = $this->getDiscountAmountValue();
        if(!is_numeric($discountAmount)){
            return null;
        }

        $discountType = $this->getDiscountType();
        if(!$this->_getIsDiscountTypeValid($discountType)){
            return null;
        }

        if($this->apHelper->getIsOneOfTheItemsMarkedByRule($items, $this->getRule()->getId())){
            return null;
        }

        $getProductsActionDetails = $this->getProductsGetActionDetails()->getActionDetails();
        if(!isset($getProductsActionDetails[0])){
            return null;
        }
        $getProductsActionDetail = $this->getProductsGetActionDetails();

        $getProductQty = $this->getGetProductQty();
        if(!is_numeric($getProductQty) || !$getProductQty){
            return null;
        }

        $getHintsSingular = $this->getGetHintsSingular();
        $getHintsPlural = $this->getGetHintsPlural();

        $addCartHints = (
            $this->getRule()->getDisplayCartHints()
            && $getHintsSingular
            && $getHintsPlural
        );

        $hintMessage = null;
        $hintCouponNotValidMessage = null;


        $getYItemsSlots = [
            [
                'buy_x_items_data' => [
                    'items' => [],
                    'items_qty' => 0
                ],
                'get_y_items_slots' => [
                    'items' => [],
                    'unfilled_qty' => $getProductQty
                ]
            ]
        ];


        $validGetPriceToItem = [];
        foreach($items as $item){
            $itemProductToCheckTierOrSpecialPrice = $this->_getItemToCheckTierOrSpecialPrice($item);
            if($this->_oneOfPriceTypesAppliedAndShouldSkip($item, $itemProductToCheckTierOrSpecialPrice, $this->getRule())){
                continue;
            }
            if($this->apHelper->validateActionDetail($getProductsActionDetail, $item)){
                $itemPrice = $this->apHelper->getItemPrice($item);
                $extendedArraySortableKey = strval($itemPrice*10000 + rand(0,9999));
                $validGetPriceToItem[$extendedArraySortableKey] = $item;
            }
        }

        if($this->isDiscountPriceTypeApplicable()){
            if($this->getDiscountPriceType() == self::DISCOUNT_PRICE_TYPE_CHEAPEST ){
                ksort($validGetPriceToItem);
            }elseif($this->getDiscountPriceType() == self::DISCOUNT_PRICE_TYPE_MOST_EXPENSIVE){
                krsort($validGetPriceToItem);
            }
        }elseif($this->isDiscountOrderTypeApplicable()){
            if($this->getRule()->getDiscountOrderType() == self::DISCOUNT_PRICE_TYPE_CHEAPEST ){
                ksort($validGetPriceToItem);
            }elseif($this->getRule()->getDiscountOrderType() == self::DISCOUNT_PRICE_TYPE_MOST_EXPENSIVE){
                krsort($validGetPriceToItem);
            }
        }

        $qtyLeftToApply = $getProductQty;
        $getYItemsData = [];
        $itemsQtys = [];
        $totalDiscountedGetYQty = 0;

        foreach($validGetPriceToItem as $item){
            $itemQtyToApply = min($qtyLeftToApply, $item->getQty());

            if($itemQtyToApply <= 0){
                break;
            }

            $itemApRuleMatches = $this->apHelper->getApRuleMatchesForItem($item);
            $itemApRuleMatches = (is_array($itemApRuleMatches) ? $itemApRuleMatches : []);

            $itemExpectedPricesArray = $this->_getItemExpectedPricesArray($item);
            $itemApRuleMatches[$this->getRule()->getId()]['apply'] = [
                'qty' => $itemQtyToApply,
                'expected_prices' => $itemExpectedPricesArray
            ];
            $item->setApRuleMatches($itemApRuleMatches);

            $itemsQtys[] = [
                'qty' => $itemQtyToApply,
                'item' => $item
            ];

            $getYItemsData[] = [
                'items' => [
                    [
                        'item' => $item,
                        'qty' => $itemQtyToApply
                    ]
                ]
            ];

            $totalDiscountedGetYQty += $itemQtyToApply;
            $qtyLeftToApply = max(0, ($qtyLeftToApply - $itemQtyToApply));
        }

        if($this->getRule()->getDisplayProductHints()){
            $fillingResult = $this->fillExistingGetYProductsIntoSlotsAndSetGiftItemData($getYItemsSlots, $getYItemsData, $this->getRule(), $discountAmount);

            $getYItemsSlots = $fillingResult['get_y_items_slots'];

            $this->setGiftItemData($getYItemsSlots, $this->getRule(), $this->_getDiscountExpression($discountAmount));

            $unfilledGetYItemsData = $fillingResult['unfilled_get_y_items_data'];
            $this->setQtysToReducePerAutoAddedGetYItem($unfilledGetYItemsData, $this->getRule());

            $unfilledNonGetYAutoAddedItemsForRule = $this->getUnfilledAutoAddedItems($getYItemsSlots, $address->getQuote(), $this->getRule());
            $this->setAutoAddedItemsToRemoveFlag($unfilledNonGetYAutoAddedItemsForRule);

            if($this->getRule()->getEnableAutoAdd()){
                $this->setProductsToAutoAdd($getYItemsSlots, $this->getRule(), $discountAmount, $discountType, $address->getQuote(), $this->_getDiscountExpression($discountAmount));
            }

            $discountExpression = $this->_getDiscountExpression($discountAmount);
            $this->addHintsToItemsWithEmptySlots($getYItemsSlots, $getHintsSingular, $getHintsPlural, $discountExpression, $this->getRule(), false, $address->getQuote());
        }

        if(count($itemsQtys)){
            $itemsListDescription = $this->_getItemListCompDesc($itemsQtys);
            $discountDescription = $this->_getDiscountCompDesc($this->getDiscountType(), $this->getDiscountAmountValue());

            $ruleComprehensiveDescriptionLines = ["Got ".$itemsListDescription." ".$discountDescription];

            $this->_setRuleApComprehensiveDescriptionLines($this->getRule(), $ruleComprehensiveDescriptionLines, $address);
        }

        $itemYouCanGetDiscountedNowQty = max(0, $getProductQty - $totalDiscountedGetYQty);
        if($addCartHints){
            $hintMessage = $this->_getHintMessages(
                (
                $totalDiscountedGetYQty > 0
                    ? [
                    'hints_singular' => $getHintsSingular,
                    'hints_plural' => $getHintsPlural,
                    'qty' => $totalDiscountedGetYQty,
                    'discount_amount' => $discountAmount
                ]
                    : []
                ),
                ($itemYouCanGetDiscountedNowQty > 0 ?
                    [
                        'qty' => $itemYouCanGetDiscountedNowQty,
                        'hints_singular' => $getHintsSingular,
                        'hints_plural' => $getHintsPlural,
                        'discount_amount' => $discountAmount
                    ] : []
                )
            );

            if($this->getRule()->getDisplayCartHintsIfCouponInvalid()){
                $hintCouponNotValidMessage = $this->_getCouponNotValidHintMessage(
                    ($itemYouCanGetDiscountedNowQty > 0 ?
                        [
                            'qty' => $itemYouCanGetDiscountedNowQty,
                            'hints_singular' => $getHintsSingular,
                            'hints_plural' => $getHintsPlural,
                            'discount_amount' => $discountAmount
                        ] : []
                    )
                );
            }
        }

        return [
            'cart_hint' => $hintMessage,
            'coupon_not_valid_cart_hint' => $hintCouponNotValidMessage,
            'can_get_discounted_items_now' => ($itemYouCanGetDiscountedNowQty > 0)
        ];
    }


    public function getProductsAggregatorName()
    {
        return $this->getAggregatorOption($this->getProductsAggregator());
    }

    public function getProductsGetAggregatorName()
    {
        return $this->getAggregatorOption($this->getProductsGetAggregator());
    }


    public function getProductsGetAggregatorElement()
    {
        if ($this->getProductsGetAggregator() === null) {
            foreach (array_keys($this->getAggregatorOption()) as $key) {
                $this->setProductsGetAggregator($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_productsget__' . $this->getId() . '__aggregator',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][productsget][' . $this->getId() . '][aggregator]',
                'values' => $this->getAggregatorSelectOptions(),
                'value' => $this->getProductsGetAggregator(),
                'value_name' => $this->getProductsGetAggregatorName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getProductsValueName()
    {
        if(isset($this->aggregatorValueOptions[$this->getProductsAggregatorValue()])){
            return $this->aggregatorValueOptions[$this->getProductsAggregatorValue()];
        }
        return $this->getProductsValue();
    }

    public function getProductsGetValueName()
    {
        if(isset($this->aggregatorValueOptions[$this->getProductsGetAggregatorValue()])){
            return $this->aggregatorValueOptions[$this->getProductsGetAggregatorValue()];
        }
        return $this->getProductsGetValue();
    }

    public function getProductsGetAggregatorValueElement()
    {
        if ($this->getProductsGetAggregatorValue() === null) {
            foreach (array_keys($this->aggregatorValueOptions) as $key) {
                $this->setProductsGetAggregatorValue($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_productsget__' . $this->getId() . '__aggregator_value',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][productsget][' . $this->getId() . '][aggregator_value]',
                'values' => $this->aggregatorValueOptions,
                'value' => $this->getProductsGetAggregatorValue(),
                'value_name' => $this->getProductsGetValueName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getProductsGetNewChildElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_productsget__' . $this->getId() . '__new_child',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][productsget][' . $this->getId() . '][new_child]',
                'values' => $this->getNewChildSelectOptions(),
                'value_name' => $this->getNewChildName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Newchild')
            );
    }

    public function getGetProductQtyElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][get_product_qty]',
            'value' => $this->getGetProductQty(),
            'value_name' => ($this->getGetProductQty() ? $this->getGetProductQty() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__get_product_qty',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getDiscountAmountValueElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][discount_amount_value]',
            'value' => $this->getDiscountAmountValue(),
            'value_name' => ($this->getDiscountAmountValue() ? $this->getDiscountAmountValue() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__discount_amount_value',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }


    public function getGetProductDiscountTypeElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__type',
            'hidden',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][type]',
                'value' => $this->getType(),
                'no_span' => true,
                'class' => 'hidden',
                'data-form-part' => $this->getFormName()
            ]
        );
    }


    public function getProductsGetTypeElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_productsget__' . $this->getId() . '__type',
            'hidden',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][productsget][' . $this->getId() . '][type]',
                'value' => 'Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine',
                'no_span' => true,
                'class' => 'hidden',
                'data-form-part' => $this->getFormName()
            ]
        );
    }

    public function getGetHintsSingularElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][get_hints_singular]',
            'value' => $this->getGetHintsSingular(),
            'value_name' => ($this->getGetHintsSingular() ? $this->getGetHintsSingular() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__get_hints_singular',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getGetHintsPluralElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][get_hints_plural]',
            'value' => $this->getGetHintsPlural(),
            'value_name' => ($this->getGetHintsPlural() ? $this->getGetHintsPlural() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__get_hints_plural',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function loadSubActionArray($subActionDetailsKey, $arr, $key = 'action_details', $formName = 'sales_rule_form'){
        if($subActionDetailsKey == 'productsget'){
            $this->setProductsGetAggregator($arr[$key][1]['aggregator']);
            $this->setProductsGetAggregatorValue($arr[$key][1]['aggregator_value']);
            $productsGetActionDetails = $this->_conditionFactory->create($arr[$key][1]['type'])->setFormName($formName);
            $productsGetActionDetails->setRule($this->getRule())
                ->setObject($this->getObject())
                ->setPrefix($this->getPrefix())
                ->setSubPrefix('productsget')
                ->setType('Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine')
                ->setId('1--1');

            $this->setProductsGetActionDetails($productsGetActionDetails);
            $productsGetActionDetails->loadArray($arr[$key][1], $key);
        }
    }

    public function asSubActionArray($subActionDetailsKey){
        $out = [];
        if($subActionDetailsKey == 'productsget'){
            $out = $this->getProductsGetActionDetails()->asArray();
        }
        return $out;
    }

    public function getSubActionDetailsKeys(){
        return [
            'productsget'
        ];
    }

    public function asArray(array $arrAttributes = [])
    {
        //this method shouldn't be used, instead loadSubActionArray should be
    }

}