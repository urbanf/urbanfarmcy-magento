<?php
namespace Mexbs\FreeGift\Model\Rule\Action\Details;

class GetProductForEachXSpentFixedPriceDiscount extends \Mexbs\FreeGift\Model\Rule\Action\Details\GetProductForEachXSpentAbstract{

    const SIMPLE_ACTION = 'get_product_for_each_x_spent_fixed_price_discount_action';
    protected $type = 'Mexbs\FreeGift\Model\Rule\Action\Details\GetProductForEachXSpentFixedPriceDiscount';

    public function getDiscountType(){
        return self::DISCOUNT_TYPE_FIXED_PRICE;
    }

    public function getDiscountMaxQty(){
        return INF;
    }

    public function getSimpleAction(){
        return self::SIMPLE_ACTION;
    }

    public function isDiscountPriceTypeApplicable(){
        return false;
    }

    public function isDiscountOrderTypeApplicable(){
        return true;
    }

    public function getIsFreeGift(){
        return true;
    }

    public function getCartOnAddGiftModalContentBlockType(){
        return "Mexbs\FreeGift\Block\Cart\GiftAdd\GetProductForEachXSpentFixedPriceDiscount";
    }

    public function asHtmlRecursive()
    {
        $html =  __(
                "For each %1%2 spent on all items [label for upsell cart hints - singular: %3, plural: %4] for which %5 of the following conditions are %6:",
                $this->apHelper->getCurrentCurrencySymbol(),
                $this->getEachXAmountAttributeElement()->getHtml(),
                $this->getEachXHintsSingularElement()->getHtml(),
                $this->getEachXHintsPluralElement()->getHtml(),
                $this->getEachXProductsAggregatorElement()->getHtml(),
                $this->getEachXProductsAggregatorValueElement()->getHtml()
            ).'<ul id="' .
            $this->getPrefix() .
            '_eachxproducts__' .
            $this->getId() .
            '__children" class="rule-param-children">';

        if($this->getEachXProductsActionDetails()){
            foreach($this->getEachXProductsActionDetails()->getActionDetails() as $actionDetail){
                $html .= '<li>' . $actionDetail->asHtmlRecursive() . '</li>';
            }
        }

        $html .= '<li>' . $this->getEachXProductsNewChildElement()->getHtml() . '</li></ul>';

        $html .=  __(
                "Get %1 items [label for upsell cart hints - singular: %2, plural: %3] for which %4 of the following conditions are %5:",
                $this->getGetProductQtyElement()->getHtml(),
                $this->getEachXGetHintsSingularElement()->getHtml(),
                $this->getEachXGetHintsPluralElement()->getHtml(),
                $this->getEachXProductsGetAggregatorElement()->getHtml(),
                $this->getEachXProductsGetAggregatorValueElement()->getHtml()
            ).'<ul id="' .
            $this->getPrefix() .
            '_eachxproductsget__' .
            $this->getId() .
            '__children" class="rule-param-children">';

        if($this->getEachXProductsGetActionDetails()){
            foreach($this->getEachXProductsGetActionDetails()->getActionDetails() as $actionDetail){
                $html .= '<li>' . $actionDetail->asHtmlRecursive() . '</li>';
            }
        }

        $html .= '<li>' . $this->getEachXProductsGetNewChildElement()->getHtml() . '</li></ul>';

        $html .=  __(
            "for %1%2",
            $this->apHelper->getCurrentCurrencySymbol(),
            $this->getDiscountAmountValueElement()->getHtml()
        );

        $html .= $this->getGetProductForEachXSpentDiscountTypeElement()->getHtml().
            $this->getEachXProductsTypeElement()->getHtml().
            $this->getEachXProductsGetTypeElement()->getHtml();

        if ($this->getId() != '1') {
            $html .= $this->getRemoveLinkHtml();
        }

        return "<li>".$html."</li>";
    }

    public function getDirectAttributeKeys(){
        return [
            'get_product_qty',
            'each_x_amount',
            'discount_amount_value',
            'eachx_hints_singular',
            'eachx_hints_plural',
            'eachxget_hints_singular',
            'eachxget_hints_plural'
        ];
    }
}