<?php
namespace Mexbs\FreeGift\Model\Rule\Action\Details;

class GetProductFixedDiscount extends \Mexbs\FreeGift\Model\Rule\Action\Details\GetProductAbstract{

    const SIMPLE_ACTION = 'get_product_fixed_discount_action';
    protected $type = 'Mexbs\FreeGift\Model\Rule\Action\Details\GetProductFixedDiscount';

    public function getDiscountType(){
        return self::DISCOUNT_TYPE_FIXED;
    }

    public function getDiscountMaxQty(){
        return INF;
    }

    public function getSimpleAction(){
        return self::SIMPLE_ACTION;
    }

    public function isDiscountPriceTypeApplicable(){
        return false;
    }

    public function isDiscountOrderTypeApplicable(){
        return true;
    }

    public function getIsFreeGift(){
        return true;
    }

    public function getCartOnAddGiftModalContentBlockType(){
        return "Mexbs\FreeGift\Block\Cart\GiftAdd\GetProductFixedDiscount";
    }

    public function asHtmlRecursive()
    {
        $html =  __(
                "Get %1 items [label for upsell cart hints - singular: %2, plural: %3] for which %4 of the following conditions are %5:",
                $this->getGetProductQtyElement()->getHtml(),
                $this->getGetHintsSingularElement()->getHtml(),
                $this->getGetHintsPluralElement()->getHtml(),
                $this->getProductsGetAggregatorElement()->getHtml(),
                $this->getProductsGetAggregatorValueElement()->getHtml()
            ).'<ul id="' .
            $this->getPrefix() .
            '_productsget__' .
            $this->getId() .
            '__children" class="rule-param-children">';

        if($this->getProductsGetActionDetails()){
            foreach($this->getProductsGetActionDetails()->getActionDetails() as $actionDetail){
                $html .= '<li>' . $actionDetail->asHtmlRecursive() . '</li>';
            }
        }

        $html .= '<li>' . $this->getProductsGetNewChildElement()->getHtml() . '</li></ul>';

        $html .=  __(
            "With %1%2 discount",
            $this->apHelper->getCurrentCurrencySymbol(),
            $this->getDiscountAmountValueElement()->getHtml()
        );

        $html .= $this->getGetProductDiscountTypeElement()->getHtml().
            $this->getProductsGetTypeElement()->getHtml();

        if ($this->getId() != '1') {
            $html .= $this->getRemoveLinkHtml();
        }

        return "<li>".$html."</li>";
    }

    public function getDirectAttributeKeys(){
        return [
            'get_product_qty',
            'amount',
            'discount_amount_value',
            'hints_singular',
            'hints_plural',
            'get_hints_singular',
            'get_hints_plural'
        ];
    }
}