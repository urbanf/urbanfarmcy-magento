<?php
namespace Mexbs\FreeGift\Model\Rule\Action\Details;

abstract class GetProductForEachXSpentAbstract extends \Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine{
    abstract public function getDiscountMaxQty();
    abstract public function isDiscountPriceTypeApplicable();
    abstract public function isDiscountOrderTypeApplicable();

    const GET_Y_GROUP_NUMBER = '999';

    public function getNonEmptyGroups(){
        $nonEmptyGroups = [];

        $buyActionDetail = $this->getEachXProductsActionDetails();
        if($buyActionDetail){
            $buyActionDetails = $buyActionDetail->getActionDetails();
            if(isset($buyActionDetails[0])){
                $nonEmptyGroups[1] = $buyActionDetail;
            }
        }

        $getActionDetail = $this->getEachXProductsGetActionDetails();
        if($getActionDetail){
            $getActionDetails = $getActionDetail->getActionDetails();
            if(isset($getActionDetails[0])){
                $nonEmptyGroups[self::GET_Y_GROUP_NUMBER] = $getActionDetail;
            }
        }
        return $nonEmptyGroups;
    }

    public function getNonEmptyGroupNumbers(){
        $nonEmptyGroupNumbers = [];

        $buyActionDetail = $this->getEachXProductsActionDetails();
        if($buyActionDetail){
            $buyActionDetails = $buyActionDetail->getActionDetails();
            if(isset($buyActionDetails[0])){
                $nonEmptyGroupNumbers[] = 1;
            }
        }

        $getActionDetail = $this->getEachXProductsGetActionDetails();
        if($getActionDetail){
            $getActionDetails = $getActionDetail->getActionDetails();
            if(isset($getActionDetails[0])){
                $nonEmptyGroupNumbers[] = self::GET_Y_GROUP_NUMBER;
            }
        }
        return $nonEmptyGroupNumbers;
    }

    public function getGetYGroupNumber(){
        return self::GET_Y_GROUP_NUMBER;
    }

    public function getGroupQty($groupNumber){
        if($groupNumber == self::GET_Y_GROUP_NUMBER){
            return $this->getGetProductQty();
        }
        return 0;
    }

    public function getGroupNumbersDisplayableInPopupAdd(){
        return [self::GET_Y_GROUP_NUMBER];
    }

    public function getRequiredGroupsForRule(){
        return [1, self::GET_Y_GROUP_NUMBER];
    }

    public function getGroupNumbersToIndex(){
        return [1, self::GET_Y_GROUP_NUMBER];
    }

    public function getGroupNumbersToActionType(){
        return [
            1 => 'buy',
            self::GET_Y_GROUP_NUMBER => 'get'
        ];
    }

    public function getRuleActionType(){
        return "eachamount";
    }

    public function getProductGroupActionTypeByNumber($groupNumber){
        if($groupNumber == self::GET_Y_GROUP_NUMBER){
            return "get";
        }else{
            return "buy";
        }
    }

    public function hasAddressConditionsInActionInAnyGroup(){
        return ($this->getEachXProductsActionDetails()->hasAddressConditionsInAction() || $this->getEachXProductsGetActionDetails()->hasAddressConditionsInAction());
    }

    protected function _getDiscountExpression($discountAmount){
        $currencySymbol = $this->apHelper->getCurrentCurrencySymbol();

        if($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT){
            return ($discountAmount == 100 ? "de graça" : "with " . $discountAmount ."% desconto");
        }elseif($this->getDiscountType() == self::DISCOUNT_TYPE_FIXED){
            return sprintf("com %s%s desconto", $currencySymbol, $discountAmount);
        }elseif($this->getDiscountType() == self::DISCOUNT_TYPE_FIXED_PRICE){
            return sprintf("por apenas %s%s", $currencySymbol, $discountAmount);
        }

        return '';
    }

    protected function _setQtyOfItem(
        $hintSingular,
        $hintPlural,
        $qtyToSet,
        $itemsArray
    ){
        foreach($itemsArray as $index => $item){
            if(($item['hints_singular'] == $hintSingular)
                && ($item['hints_plural'] == $hintPlural)){
                if($qtyToSet == 0){
                    unset($itemsArray[$index]);
                }else{
                    $itemsArray[$index]['qty'] = $qtyToSet;
                }
            }
        }
        return $itemsArray;
    }

    protected function _getCouponNotValidHintMessage(
        $itemsToAddTillDiscountData,
        $itemYouCanGetDiscountedIfYouAddData,
        $itemYouCanGetDiscountedNowData
    )
    {
        $hintMessage = "";
        if(!empty($itemYouCanGetDiscountedNowData)){
            $hintMessage .= "You can now add ";
            if(!empty($itemsAlreadyDiscountedData)){
                $hintMessage .= "another ";
            }
            $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedNowData['discount_amount']);

            $hintMessage .= sprintf(
                "%s%s %s",
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? (empty($itemsAlreadyDiscountedData) ? "one ": "") : $itemYouCanGetDiscountedNowData['qty']." "),
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? $itemYouCanGetDiscountedNowData['hints_singular'] : $itemYouCanGetDiscountedNowData['hints_plural']),
                (($itemYouCanGetDiscountedNowData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
            $hintMessage .= "!";
        }elseif(!empty($itemsToAddTillDiscountData)
            && !empty($itemYouCanGetDiscountedIfYouAddData)){
            $hintMessage = "Add ";

            $hintMessage .= sprintf(
                "%s%s%s worth of %s",
                $this->apHelper->getCurrentCurrencySymbol(),
                $itemsToAddTillDiscountData['worth_to_add'],
                (!isset($itemsToAddTillDiscountData['not_added_yet']) || $itemsToAddTillDiscountData['not_added_yet'] ? "" : " more"),
                $itemsToAddTillDiscountData['hints_plural']
            );


            $hintMessage .= " to cart. Then try applying the coupon again. You should get ";

            $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedIfYouAddData['discount_amount']);
            $hintMessage .= sprintf(
                "%s %s %s",
                ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? "one" : $itemYouCanGetDiscountedIfYouAddData['qty']),
                ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? $itemYouCanGetDiscountedIfYouAddData['hints_singular'] : $itemYouCanGetDiscountedIfYouAddData['hints_plural']),
                (($itemYouCanGetDiscountedIfYouAddData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
        }

        return $hintMessage;
    }

    protected function _getHintMessages(
        $itemsAlreadyDiscountedData,
        $itemsToAddTillDiscountData,
        $itemYouCanGetDiscountedIfYouAddData,
        $itemYouCanGetDiscountedNowData
    )
    {
        $hintMessage = "";

        if(!empty($itemsAlreadyDiscountedData)){
            $hintMessage .= "Você ganhou! ";

            $discountExpression = $this->_getDiscountExpression($itemsAlreadyDiscountedData['discount_amount']);
            $hintMessage .= sprintf(
                "%s %s %s",
                ($itemsAlreadyDiscountedData['qty'] == 1 ? "one" : $itemsAlreadyDiscountedData['qty']),
                ($itemsAlreadyDiscountedData['qty'] == 1 ? $itemsAlreadyDiscountedData['hints_singular'] : $itemsAlreadyDiscountedData['hints_plural']),
                (($itemsAlreadyDiscountedData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
            $hintMessage .= ".";
        }

        if($hintMessage != ""){
            $hintMessage .= " ";
        }

        if(!empty($itemYouCanGetDiscountedNowData)){
            $hintMessage .= "You can now add ";
            if(!empty($itemsAlreadyDiscountedData)){
                $hintMessage .= "another ";
            }
            $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedNowData['discount_amount']);

            $hintMessage .= sprintf(
                "%s%s %s",
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? (empty($itemsAlreadyDiscountedData) ? "one ": "") : $itemYouCanGetDiscountedNowData['qty']." "),
                ($itemYouCanGetDiscountedNowData['qty'] == 1 ? $itemYouCanGetDiscountedNowData['hints_singular'] : $itemYouCanGetDiscountedNowData['hints_plural']),
                (($itemYouCanGetDiscountedNowData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
            );
        }elseif(!empty($itemsToAddTillDiscountData)
            && !empty($itemYouCanGetDiscountedIfYouAddData)){
            $hintMessage .= sprintf(
                "Add %s%s%s worth of %s",
                $this->apHelper->getCurrentCurrencySymbol(),
                $itemsToAddTillDiscountData['worth_to_add'],
                ($itemsToAddTillDiscountData['not_added_yet'] ? "" : " more"),
                $itemsToAddTillDiscountData['hints_plural']
            );
                if(!empty($itemsAlreadyDiscountedData)){
                    $hintMessage .= ", to get your next ";
                }else{
                    $hintMessage .= ", to get  ";
                }

                $discountExpression = $this->_getDiscountExpression($itemYouCanGetDiscountedIfYouAddData['discount_amount']);
                if(!empty($itemsAlreadyDiscountedData)){
                    $hintMessage .= sprintf(
                        "%s%s %s",
                        ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? "" : $itemYouCanGetDiscountedIfYouAddData['qty']." "),
                        ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? $itemYouCanGetDiscountedIfYouAddData['hints_singular'] : $itemYouCanGetDiscountedIfYouAddData['hints_plural']),
                        (($itemYouCanGetDiscountedIfYouAddData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
                    );
                }else{
                    $hintMessage .= sprintf(
                        "%s %s %s",
                        ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? "one" : $itemYouCanGetDiscountedIfYouAddData['qty']),
                        ($itemYouCanGetDiscountedIfYouAddData['qty'] == 1 ? $itemYouCanGetDiscountedIfYouAddData['hints_singular'] : $itemYouCanGetDiscountedIfYouAddData['hints_plural']),
                        (($itemYouCanGetDiscountedIfYouAddData['qty'] == 1) || ($this->getDiscountType() == self::DISCOUNT_TYPE_PERCENT) ? $discountExpression : $discountExpression." each")
                    );
                }
        }
        $hintMessage .= "!";
        return $hintMessage;
    }


    public function markMatchingItemsAndGetHint($items, $address){
        if(!$this->getRule()
            || !$this->getRule()->getId()
        ){
            return null;
        }

        $discountAmount = $this->getDiscountAmountValue();
        if(!is_numeric($discountAmount)){
            return null;
        }

        $discountType = $this->getDiscountType();
        if(!$this->_getIsDiscountTypeValid($discountType)){
            return null;
        }

        if($this->apHelper->getIsOneOfTheItemsMarkedByRule($items, $this->getRule()->getId())){
            return null;
        }

        $eachXProductsActionDetails = $this->getEachXProductsActionDetails()->getActionDetails();
        if(!isset($eachXProductsActionDetails[0])){
            return null;
        }
        $eachXProductsActionDetail = $this->getEachXProductsActionDetails();

        $eachXProductsGetActionDetails = $this->getEachXProductsGetActionDetails()->getActionDetails();
        if(!isset($eachXProductsGetActionDetails[0])){
            return null;
        }
        $eachXProductsGetActionDetail = $this->getEachXProductsGetActionDetails();

        $eachXAmount = $this->getEachXAmount();
        if(!is_numeric($eachXAmount) || !$eachXAmount){
            return null;
        }

        $getProductQty = $this->getGetProductQty();
        if(!is_numeric($getProductQty) || !$getProductQty){
            return null;
        }

        $maxDiscountQty = INF;
        if(is_numeric($this->getRule()->getDiscountQty())
            && ($this->getRule()->getDiscountQty() > 0)){
            $maxDiscountQty = $this->getRule()->getDiscountQty();
        }

        $validEachXPriceToItem = [];
        $validEachXItemIds = [];
        foreach($items as $item){
            $itemProductToCheckTierOrSpecialPrice = $this->_getItemToCheckTierOrSpecialPrice($item);
            if($this->_oneOfPriceTypesAppliedAndShouldSkip($item, $itemProductToCheckTierOrSpecialPrice, $this->getRule())){
                continue;
            }
            if($this->apHelper->validateActionDetail($eachXProductsActionDetail, $item)){
                $itemPrice = $this->apHelper->getItemPrice($item);
                $extendedArraySortableKey = strval($itemPrice*10000 + rand(0,9999));
                $validEachXPriceToItem[$extendedArraySortableKey] = $item;
                $validEachXItemIds[] = $this->apHelper->getQuoteItemId($item);
            }
        }

        $eachXHintsSingular = $this->getEachxHintsSingular();
        $eachXHintsPlural = $this->getEachxHintsPlural();

        $eachXGetHintsSingular = $this->getEachxgetHintsSingular();
        $eachXGetHintsPlural = $this->getEachxgetHintsPlural();

        $addCartHints = (
            $this->getRule()->getDisplayCartHints()
            && $eachXHintsSingular
            && $eachXHintsPlural
            && $eachXGetHintsSingular
            && $eachXGetHintsPlural
        );

        $hintMessage = null;
        $hintCouponNotValidMessage = null;

        if(count($validEachXPriceToItem) == 0){
            if($addCartHints){
                $hintMessage = $this->_getHintMessages(
                    [],
                    [
                        'worth_to_add' => $eachXAmount,
                        'hints_plural' => $eachXHintsPlural,
                        'not_added_yet' => true
                    ],
                    [
                        'qty' => $getProductQty,
                        'hints_singular' => $eachXGetHintsSingular,
                        'hints_plural' => $eachXGetHintsPlural,
                        'discount_amount' => $discountAmount
                    ],
                    []
                );

                if($this->getRule()->getDisplayCartHintsIfCouponInvalid()){
                    $hintCouponNotValidMessage = $this->_getCouponNotValidHintMessage(
                        [
                            'worth_to_add' => $eachXAmount,
                            'hints_plural' => $eachXHintsPlural,
                            'not_added_yet' => true
                        ],
                        [
                            'qty' => $getProductQty,
                            'hints_singular' => $eachXGetHintsSingular,
                            'hints_plural' => $eachXGetHintsPlural,
                            'discount_amount' => $discountAmount
                        ],
                        []
                    );
                }
            }

            return [
                'cart_hint' => $hintMessage,
                'coupon_not_valid_cart_hint' => $hintCouponNotValidMessage,
                'can_get_discounted_items_now' => false
            ];
        }

        $getYItemsSlots = [];
        $lastRemainder = 0;
        $totalAppliedAmount = 0;
        $totalProductGetQty = 0;
        $lastGetYItemsSlots = [];

        foreach($validEachXPriceToItem as $item){
            $itemPrice = $this->apHelper->getItemPrice($item);
			
			if($itemPrice == 0){
                continue;
            }
			
            if($maxDiscountQty < INF){
                $allowedGetYQtyLeft = max($maxDiscountQty - $totalProductGetQty, 0);
            }else{
                $allowedGetYQtyLeft = INF;
            }

            $allowedEachXAmountLeft =  INF;
            if($allowedGetYQtyLeft < INF){
                $allowedEachXAmountLeft = $allowedGetYQtyLeft*$eachXAmount;
            }

            $applicableEachXAmountForItemToUse = max(0, $allowedEachXAmountLeft - $lastRemainder);
            $itemQtyToApply = min($item->getQty(), floor($applicableEachXAmountForItemToUse/$itemPrice)+1);


            $itemAmountToApply = $itemQtyToApply*$itemPrice;

            $itemAmountToApplyWithLastRemainder = $itemAmountToApply+$lastRemainder;
            $currentProductGetQty = min(floor($itemAmountToApplyWithLastRemainder/$eachXAmount)*$getProductQty, $allowedGetYQtyLeft);

            $lastRemainder = $itemAmountToApplyWithLastRemainder%$eachXAmount;
            $totalAppliedAmount += $allowedGetYQtyLeft*$eachXAmount;
            $totalProductGetQty += $currentProductGetQty;

            if(empty($lastGetYItemsSlots)){
                $lastGetYItemsSlots = [
                    'buy_x_items_data' => [
                        'items' => [],
                        'items_qty' => 0
                    ],
                    'get_y_items_slots' => [
                        'items' => [],
                        'unfilled_qty' => 0
                    ]
                ];
            }

            $lastGetYItemsSlots['buy_x_items_data']['items'][] = [
                'item' => $item,
                'qty' => $itemQtyToApply
            ];
            $lastGetYItemsSlots['get_y_items_slots']['unfilled_qty'] += $currentProductGetQty;

            if($currentProductGetQty > 0){
                $getYItemsSlots[] = $lastGetYItemsSlots;
                $lastGetYItemsSlots = [];
            }
        }

        $allowedGetYQtyLeft = max($maxDiscountQty - $totalProductGetQty, 0);

        $validEachXGetPriceToItem = [];
        foreach($items as $item){
            if(in_array($this->apHelper->getQuoteItemId($item), $validEachXItemIds)){
                continue;
            }
            $itemProductToCheckTierOrSpecialPrice = $this->_getItemToCheckTierOrSpecialPrice($item);
            if($this->_oneOfPriceTypesAppliedAndShouldSkip($item, $itemProductToCheckTierOrSpecialPrice, $this->getRule())){
                continue;
            }
            if($this->apHelper->validateActionDetail($eachXProductsGetActionDetail, $item)){
                $itemPrice = $this->apHelper->getItemPrice($item);
                $extendedArraySortableKey = strval($itemPrice*10000 + rand(0,9999));
                $validEachXGetPriceToItem[$extendedArraySortableKey] = $item;
            }
        }

        if($this->isDiscountPriceTypeApplicable()){
            if($this->getDiscountPriceType() == self::DISCOUNT_PRICE_TYPE_CHEAPEST ){
                ksort($validEachXGetPriceToItem);
            }elseif($this->getDiscountPriceType() == self::DISCOUNT_PRICE_TYPE_MOST_EXPENSIVE){
                krsort($validEachXGetPriceToItem);
            }
        }elseif($this->isDiscountOrderTypeApplicable()){
            if($this->getRule()->getDiscountOrderType() == self::DISCOUNT_PRICE_TYPE_CHEAPEST ){
                ksort($validEachXGetPriceToItem);
            }elseif($this->getRule()->getDiscountOrderType() == self::DISCOUNT_PRICE_TYPE_MOST_EXPENSIVE){
                krsort($validEachXGetPriceToItem);
            }
        }

        $qtyLeftToApply = $totalProductGetQty;
        $getYItemsData = [];
        $itemsQtys = [];
        $totalDiscountedGetYQty = 0;

        foreach($validEachXGetPriceToItem as $item){
            $itemQtyToApply = min($qtyLeftToApply, $item->getQty());
            if($itemQtyToApply <= 0){
                break;
            }

            $itemApRuleMatches = $this->apHelper->getApRuleMatchesForItem($item);
            $itemApRuleMatches = (is_array($itemApRuleMatches) ? $itemApRuleMatches : []);

            $itemExpectedPricesArray = $this->_getItemExpectedPricesArray($item);
            $itemApRuleMatches[$this->getRule()->getId()]['apply'] = [
                'qty' => $itemQtyToApply,
                'expected_prices' => $itemExpectedPricesArray
            ];
            $item->setApRuleMatches($itemApRuleMatches);

            $itemsQtys[] = [
                'qty' => $itemQtyToApply,
                'item' => $item
            ];

            $getYItemsData[] = [
                'items' => [
                    [
                        'item' => $item,
                        'qty' => $itemQtyToApply
                    ]
                ]
            ];

            $totalDiscountedGetYQty += $itemQtyToApply;
            $qtyLeftToApply = max(0, ($qtyLeftToApply - $itemQtyToApply));
        }

        if($this->getRule()->getDisplayProductHints()){
            $fillingResult = $this->fillExistingGetYProductsIntoSlotsAndSetGiftItemData($getYItemsSlots, $getYItemsData, $this->getRule(), $discountAmount);

            $getYItemsSlots = $fillingResult['get_y_items_slots'];

            $this->setGiftItemData($getYItemsSlots, $this->getRule(), $this->_getDiscountExpression($discountAmount));

            $unfilledGetYItemsData = $fillingResult['unfilled_get_y_items_data'];
            $this->setQtysToReducePerAutoAddedGetYItem($unfilledGetYItemsData, $this->getRule());

            $unfilledNonGetYAutoAddedItemsForRule = $this->getUnfilledAutoAddedItems($getYItemsSlots, $address->getQuote(), $this->getRule());
            $this->setAutoAddedItemsToRemoveFlag($unfilledNonGetYAutoAddedItemsForRule);

            if($this->getRule()->getEnableAutoAdd()){
                $this->setProductsToAutoAdd($getYItemsSlots, $this->getRule(), $discountAmount, $discountType, $address->getQuote(), $this->_getDiscountExpression($discountAmount));
            }

            $discountExpression = $this->_getDiscountExpression($discountAmount);
            $this->addHintsToItemsWithEmptySlots($getYItemsSlots, $eachXGetHintsSingular, $eachXGetHintsPlural, $discountExpression, $this->getRule(), ($this->apHelper->getProductHintLocation($this->getRule()) == \Mexbs\FreeGift\Model\Source\Config\ProductHintsLocation::LOCATION_CART_TOP ? false : true), $address->getQuote());
        }

        if(count($itemsQtys)){
            $itemsListDescription = $this->_getItemListCompDesc($itemsQtys);
            $discountDescription = $this->_getDiscountCompDesc($this->getDiscountType(), $this->getDiscountAmountValue());

            $ruleComprehensiveDescriptionLines = ["Got ".$itemsListDescription." ".$discountDescription];

            $this->_setRuleApComprehensiveDescriptionLines($this->getRule(), $ruleComprehensiveDescriptionLines, $address);
        }

        $itemYouCanGetDiscountedNowQty = max(0, $totalProductGetQty - $totalDiscountedGetYQty);
        if($addCartHints && ($allowedGetYQtyLeft > 0)){
            $amountToAddToGetDiscount = max(0,$eachXAmount - $lastRemainder);
            $hintMessage = $this->_getHintMessages(
                (
                $totalDiscountedGetYQty > 0
                    ? [
                    'hints_singular' => $eachXGetHintsSingular,
                    'hints_plural' => $eachXGetHintsPlural,
                    'qty' => $totalDiscountedGetYQty,
                    'discount_amount' => $discountAmount
                ]
                    : []
                ),
                [
                    'worth_to_add' => $amountToAddToGetDiscount,
                    'hints_plural' => $eachXHintsPlural,
                    'not_added_yet' => true
                ],
                [
                    'qty' => $getProductQty,
                    'hints_singular' => $eachXGetHintsSingular,
                    'hints_plural' => $eachXGetHintsPlural,
                    'discount_amount' => $discountAmount

                ],
                ($itemYouCanGetDiscountedNowQty > 0 ?
                [
                    'qty' => $itemYouCanGetDiscountedNowQty,
                    'hints_singular' => $eachXGetHintsSingular,
                    'hints_plural' => $eachXGetHintsPlural,
                    'discount_amount' => $discountAmount
                ] : []
                )
            );

            if($this->getRule()->getDisplayCartHintsIfCouponInvalid()){
                $hintCouponNotValidMessage = $this->_getCouponNotValidHintMessage(
                    [
                        'worth_to_add' => $amountToAddToGetDiscount,
                        'hints_plural' => $eachXHintsPlural,
                        'not_added_yet' => true
                    ],
                    [
                        'qty' => $getProductQty,
                        'hints_singular' => $eachXGetHintsSingular,
                        'hints_plural' => $eachXGetHintsPlural,
                        'discount_amount' => $discountAmount
                    ],
                    ($itemYouCanGetDiscountedNowQty > 0 ?
                        [
                            'qty' => $itemYouCanGetDiscountedNowQty,
                            'hints_singular' => $eachXGetHintsSingular,
                            'hints_plural' => $eachXGetHintsPlural,
                            'discount_amount' => $discountAmount
                        ] : []
                    )
                );
            }
        }

        return [
            'cart_hint' => $hintMessage,
            'coupon_not_valid_cart_hint' => $hintCouponNotValidMessage,
            'can_get_discounted_items_now' => ($itemYouCanGetDiscountedNowQty > 0)
        ];
    }


    public function getEachXProductsAggregatorName()
    {
        return $this->getAggregatorOption($this->getEachXProductsAggregator());
    }

    public function getEachXProductsGetAggregatorName()
    {
        return $this->getAggregatorOption($this->getEachXProductsGetAggregator());
    }

    public function getEachXProductsAggregatorElement()
    {
        if ($this->getEachXProductsAggregator() === null) {
            foreach (array_keys($this->getAggregatorOption()) as $key) {
                $this->setEachXProductsAggregator($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproducts__' . $this->getId() . '__aggregator',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproducts][' . $this->getId() . '][aggregator]',
                'values' => $this->getAggregatorSelectOptions(),
                'value' => $this->getEachXProductsAggregator(),
                'value_name' => $this->getEachXProductsAggregatorName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXProductsGetAggregatorElement()
    {
        if ($this->getEachXProductsGetAggregator() === null) {
            foreach (array_keys($this->getAggregatorOption()) as $key) {
                $this->setEachXProductsGetAggregator($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproductsget__' . $this->getId() . '__aggregator',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproductsget][' . $this->getId() . '][aggregator]',
                'values' => $this->getAggregatorSelectOptions(),
                'value' => $this->getEachXProductsGetAggregator(),
                'value_name' => $this->getEachXProductsGetAggregatorName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXProductsValueName()
    {
        if(isset($this->aggregatorValueOptions[$this->getEachXProductsAggregatorValue()])){
            return $this->aggregatorValueOptions[$this->getEachXProductsAggregatorValue()];
        }
        return $this->getEachXProductsValue();
    }

    public function getEachXProductsGetValueName()
    {
        if(isset($this->aggregatorValueOptions[$this->getEachXProductsGetAggregatorValue()])){
            return $this->aggregatorValueOptions[$this->getEachXProductsGetAggregatorValue()];
        }
        return $this->getEachXProductsGetValue();
    }

    public function getEachXProductsAggregatorValueElement()
    {
        if ($this->getEachXProductsAggregatorValue() === null) {
            foreach (array_keys($this->aggregatorValueOptions) as $key) {
                $this->setEachXProductsAggregatorValue($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproducts__' . $this->getId() . '__aggregator_value',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproducts][' . $this->getId() . '][aggregator_value]',
                'values' => $this->aggregatorValueOptions,
                'value' => $this->getEachXProductsAggregatorValue(),
                'value_name' => $this->getEachXProductsValueName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXProductsGetAggregatorValueElement()
    {
        if ($this->getEachXProductsGetAggregatorValue() === null) {
            foreach (array_keys($this->aggregatorValueOptions) as $key) {
                $this->setEachXProductsGetAggregatorValue($key);
                break;
            }
        }
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproductsget__' . $this->getId() . '__aggregator_value',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproductsget][' . $this->getId() . '][aggregator_value]',
                'values' => $this->aggregatorValueOptions,
                'value' => $this->getEachXProductsGetAggregatorValue(),
                'value_name' => $this->getEachXProductsGetValueName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXProductsNewChildElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproducts__' . $this->getId() . '__new_child',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproducts][' . $this->getId() . '][new_child]',
                'values' => $this->getNewChildSelectOptions(),
                'value_name' => $this->getNewChildName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Newchild')
            );
    }

    public function getEachXProductsGetNewChildElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproductsget__' . $this->getId() . '__new_child',
            'select',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproductsget][' . $this->getId() . '][new_child]',
                'values' => $this->getNewChildSelectOptions(),
                'value_name' => $this->getNewChildName(),
                'data-form-part' => $this->getFormName()
            ]
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Newchild')
            );
    }

    public function getGetProductQtyElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][get_product_qty]',
            'value' => $this->getGetProductQty(),
            'value_name' => ($this->getGetProductQty() ? $this->getGetProductQty() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__get_product_qty',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getDiscountAmountValueElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][discount_amount_value]',
            'value' => $this->getDiscountAmountValue(),
            'value_name' => ($this->getDiscountAmountValue() ? $this->getDiscountAmountValue() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__discount_amount_value',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }


    public function getEachXAmountAttributeElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][each_x_amount]',
            'value' => $this->getEachXAmount(),
            'value_name' => ($this->getEachXAmount() ? $this->getEachXAmount() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__each_x_amount',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getGetProductForEachXSpentDiscountTypeElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__type',
            'hidden',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][type]',
                'value' => $this->getType(),
                'no_span' => true,
                'class' => 'hidden',
                'data-form-part' => $this->getFormName()
            ]
        );
    }

    public function getEachXProductsTypeElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproducts__' . $this->getId() . '__type',
            'hidden',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproducts][' . $this->getId() . '][type]',
                'value' => 'Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine',
                'no_span' => true,
                'class' => 'hidden',
                'data-form-part' => $this->getFormName()
            ]
        );
    }

    public function getEachXProductsGetTypeElement()
    {
        return $this->getForm()->addField(
            $this->getPrefix() . '_eachxproductsget__' . $this->getId() . '__type',
            'hidden',
            [
                'name' => $this->elementName . '[' . $this->getPrefix() . '][eachxproductsget][' . $this->getId() . '][type]',
                'value' => 'Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine',
                'no_span' => true,
                'class' => 'hidden',
                'data-form-part' => $this->getFormName()
            ]
        );
    }

    public function getEachXHintsSingularElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][eachx_hints_singular]',
            'value' => $this->getEachxHintsSingular(),
            'value_name' => ($this->getEachxHintsSingular() ? $this->getEachxHintsSingular() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__eachx_hints_singular',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXGetHintsSingularElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][eachxget_hints_singular]',
            'value' => $this->getEachxgetHintsSingular(),
            'value_name' => ($this->getEachxgetHintsSingular() ? $this->getEachxgetHintsSingular() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__eachxget_hints_singular',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXHintsPluralElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][eachx_hints_plural]',
            'value' => $this->getEachxHintsPlural(),
            'value_name' => ($this->getEachxHintsPlural() ? $this->getEachxHintsPlural() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__eachx_hints_plural',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function getEachXGetHintsPluralElement()
    {
        $elementParams = [
            'name' => $this->elementName . '[' . $this->getPrefix() . '][' . $this->getId() . '][eachxget_hints_plural]',
            'value' => $this->getEachxgetHintsPlural(),
            'value_name' => ($this->getEachxgetHintsPlural() ? $this->getEachxgetHintsPlural() : "..."),
            'data-form-part' => $this->getFormName()
        ];
        return $this->getForm()->addField(
            $this->getPrefix() . '__' . $this->getId() . '__eachxget_hints_plural',
            'text',
            $elementParams
        )->setRenderer(
                $this->_layout->getBlockSingleton('Magento\Rule\Block\Editable')
            );
    }

    public function loadSubActionArray($subActionDetailsKey, $arr, $key = 'action_details', $formName = 'sales_rule_form'){
        if($subActionDetailsKey == 'eachxproducts'){
            $this->setEachXProductsAggregator($arr[$key][1]['aggregator']);
            $this->setEachXProductsAggregatorValue($arr[$key][1]['aggregator_value']);
            $eachXProductsActionDetails = $this->_conditionFactory->create($arr[$key][1]['type'])->setFormName($formName);
            $eachXProductsActionDetails->setRule($this->getRule())
                ->setObject($this->getObject())
                ->setPrefix($this->getPrefix())
                ->setSubPrefix('eachxproducts')
                ->setType('Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine')
                ->setId('1--1');

            $this->setEachXProductsActionDetails($eachXProductsActionDetails);
            $eachXProductsActionDetails->loadArray($arr[$key][1], $key);
        }elseif($subActionDetailsKey == 'eachxproductsget'){
            $this->setEachXProductsGetAggregator($arr[$key][1]['aggregator']);
            $this->setEachXProductsGetAggregatorValue($arr[$key][1]['aggregator_value']);
            $eachXProductsGetActionDetails = $this->_conditionFactory->create($arr[$key][1]['type'])->setFormName($formName);
            $eachXProductsGetActionDetails->setRule($this->getRule())
                ->setObject($this->getObject())
                ->setPrefix($this->getPrefix())
                ->setSubPrefix('eachxproductsget')
                ->setType('Mexbs\ApBase\Model\Rule\Action\Details\Condition\Product\Combine')
                ->setId('1--1');

            $this->setEachXProductsGetActionDetails($eachXProductsGetActionDetails);
            $eachXProductsGetActionDetails->loadArray($arr[$key][1], $key);
        }
    }

    public function asSubActionArray($subActionDetailsKey){
        $out = [];
        if($subActionDetailsKey == 'eachxproducts'){
            $out = $this->getEachXProductsActionDetails()->asArray();
        }elseif($subActionDetailsKey == 'eachxproductsget'){
            $out = $this->getEachXProductsGetActionDetails()->asArray();
        }
        return $out;
    }

    public function getSubActionDetailsKeys(){
        return [
            'eachxproducts',
            'eachxproductsget'
        ];
    }

    public function asArray(array $arrAttributes = [])
    {
        //this method shouldn't be used, instead loadSubActionArray should be
    }

}