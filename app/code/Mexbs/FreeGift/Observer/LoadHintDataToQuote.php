<?php
namespace Mexbs\FreeGift\Observer;

use Magento\Framework\Event\ObserverInterface;

class LoadHintDataToQuote implements ObserverInterface{

    protected $serializer;

    public function __construct(
        \Mexbs\ApBase\Serialize $serializer
    ) {
        $this->serializer = $serializer;
    }

    protected function getUnserializedData($data){
        $dataUnserialized = $data;
        if(!is_array($data)){
            $dataUnserialized = $this->serializer->unserialize($data);
        }
        return $dataUnserialized;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();

        $dataToUnserializeAndSet = ['gift_hint_messages', 'gift_qtys_can_add_per_group'];
        foreach($dataToUnserializeAndSet as $dataItemKeyToUnserializeAndSet){
            $dataItem = $quote->getData($dataItemKeyToUnserializeAndSet);
            $quote->setData($dataItemKeyToUnserializeAndSet, $this->getUnserializedData($dataItem));
        }
    }
}