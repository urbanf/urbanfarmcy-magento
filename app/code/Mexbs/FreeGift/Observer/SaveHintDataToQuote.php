<?php
namespace Mexbs\FreeGift\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveHintDataToQuote implements ObserverInterface{

    protected $serializer;

    public function __construct(
        \Mexbs\ApBase\Serialize $serializer
    ) {
        $this->serializer = $serializer;
    }

    protected function getSerializedData($data){
        $dataSerialized = $data;
        if(!is_string($data)){
            $dataSerialized =  $this->serializer->serialize($data);
        }
        return $dataSerialized;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();

        $dataToSerializeAndSet = ['gift_hint_messages', 'gift_qtys_can_add_per_group'];
        foreach($dataToSerializeAndSet as $dataItemKeyToSerializeAndSet){
            $dataItem = $quote->getData($dataItemKeyToSerializeAndSet);
            $quote->setData($dataItemKeyToSerializeAndSet, $this->getSerializedData($dataItem));
        }
    }
}