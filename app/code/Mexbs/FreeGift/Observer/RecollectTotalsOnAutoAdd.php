<?php
namespace Mexbs\FreeGift\Observer;

use Magento\Framework\Event\ObserverInterface;

class RecollectTotalsOnAutoAdd implements ObserverInterface{
    protected $quoteRepository;
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ){
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $cart = $observer->getEvent()->getCart();
        $quote = $cart->getQuote();

        $recentlyAutoAddedItems = [];
        foreach($quote->getAllItems() as $item){
            if($item->getRecentlyAutoAdded()){
                $recentlyAutoAddedItems[] = $item;
            }
        }

        if(count($recentlyAutoAddedItems)){
            $quote->setTotalsCollectedFlag(0);
            $quote->collectTotals();
            $this->quoteRepository->save($quote);

            foreach($recentlyAutoAddedItems as $recentlyAutoAddedItem){
                $recentlyAutoAddedItem->setRecentlyAutoAdded(null);
            }
        }
    }
}