<?php
namespace Mexbs\FreeGift\Observer;

use Magento\Framework\Event\ObserverInterface;

class AutoAddAndCorrectAutoAdded implements ObserverInterface{
    protected $freeGiftHelper;

    public function __construct(
        \Mexbs\FreeGift\Helper\Data $freeGiftHelper

    ) {
        $this->freeGiftHelper = $freeGiftHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /**
         * @var \Magento\Checkout\Model\Cart $cart
         */
        $cart = $observer->getEvent()->getCart();
        $this->freeGiftHelper->autoAddAndCorrectAutoAdded($cart);
    }
}