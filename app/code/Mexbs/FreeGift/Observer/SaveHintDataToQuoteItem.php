<?php
namespace Mexbs\FreeGift\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveHintDataToQuoteItem implements ObserverInterface{
    protected $serializer;

    public function __construct(
        \Mexbs\ApBase\Serialize $serializer
    ) {
        $this->serializer = $serializer;
    }

    protected function getSerializedData($data){
        $dataSerialized = $data;
        if(!is_string($data)){
            $dataSerialized = $this->serializer->serialize($data);
        }
        return $dataSerialized;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getEvent()->getItem();

        $dataToSerializeAndSet = ['gift_rule_id', 'gift_trigger_item_ids_qtys', 'gift_message', 'gift_trigger_item_ids_qtys_of_same_group', 'gift_qtys_can_add_per_group'];
        foreach($dataToSerializeAndSet as $dataItemKeyToSerializeAndSet){
            $dataItem = $item->getData($dataItemKeyToSerializeAndSet);
            $item->setData($dataItemKeyToSerializeAndSet, $this->getSerializedData($dataItem));
        }
    }
}