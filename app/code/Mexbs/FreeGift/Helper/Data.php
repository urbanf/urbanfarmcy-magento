<?php
namespace Mexbs\FreeGift\Helper;

class Data
{
    protected $apHelper;
    public function __construct(
        \Mexbs\ApBase\Helper\Data $apHelper
    ){
        $this->apHelper = $apHelper;
    }

    protected function autoAddProduct($productId , $qty, $ruleId, $cart){
        $request = new \Magento\Framework\DataObject([
            'qty' => $qty,
            'is_auto_add' => '1',
            'auto_add_rule_id' => $ruleId
        ]);

        try {
            $cart->addProduct($this->apHelper->getProductFromProductId($productId), $request);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cart
     */
    public function autoAddAndCorrectAutoAdded($cart){
        $quote = $cart->getQuote();

        if($quote->getCalledSaveInFunction()){
            return;
        }

        $madeChanges = false;

        $appliedRuleIds = explode(",", $quote->getAppliedRuleIds());

        foreach($quote->getAllVisibleItems() as $item){
            $buyRequest = $item->getBuyRequest();
            if($item->getAutoAddedToRemove()
                || ($buyRequest->getIsAutoAdd() && $buyRequest->getAutoAddRuleId() && (!in_array($buyRequest->getAutoAddRuleId(), $appliedRuleIds)))
            ){
                $item->setAutoAddedToRemove(null);

                if($item->getId()){
                    $cart->removeItem($item->getId());

                    $madeChanges = true;
                }
            }
        }


        foreach($quote->getAllVisibleItems() as $item){
            $qtyToReduce = $item->getQtyToReduce();

            if($qtyToReduce){
                $item->setQtyToReduce(null);
                $newItemQty = max(0, $item->getQty() - $qtyToReduce);
                if($newItemQty == 0){
                    if($item->getId()){
                        $cart->removeItem($item->getId());

                        $madeChanges = true;
                    }
                }else{
                    $item->setQty($newItemQty);
                    $madeChanges = true;
                }
            }
        }

        foreach($quote->getAllVisibleItems() as $item){
            $itemAutoAddProductPerRule = $item->getAutoAddProductPerRule();
            $itemHints = $this->apHelper->unserializeAndGetObjectProperty('hint_messages', $item);

            if(!is_array($itemAutoAddProductPerRule)){
                continue;
            }

            foreach($itemAutoAddProductPerRule as $ruleId => $autoAddProductData){
                $added = $this->autoAddProduct(
                    $autoAddProductData['product_id'],
                    $autoAddProductData['qty'],
                    $ruleId,
                    $cart
                );

                if($added){
                    $madeChanges = true;

                    if(isset($itemHints[$ruleId])){
                        unset($itemHints[$ruleId]);
                    }
                }
            }

            $item->setHintMessages($itemHints);
            $item->setAutoAddProductPerRule(null);
        }

        $quoteAutoAddProductPerRule = $quote->getAutoAddProductPerRule();
        $quoteHints = $this->apHelper->unserializeAndGetObjectProperty('gift_hint_messages', $quote);

        if(is_array($quoteAutoAddProductPerRule)){
            foreach($quoteAutoAddProductPerRule as $ruleId => $autoAddProductData){
                $added = $this->autoAddProduct(
                    $autoAddProductData['product_id'],
                    $autoAddProductData['qty'],
                    $ruleId,
                    $cart
                );

                if($added){
                    $madeChanges = true;

                    if(isset($quoteHints[$ruleId])){
                        unset($quoteHints[$ruleId]);
                    }
                }
            }
        }

        $quote->setGifttHintMessages($quoteHints);
        $quote->setAutoAddProductPerRule(null);


        if($madeChanges){
            $quote->setNeedToTakeCareOfAutoAdd(false);


            $cart->getQuote()->setCalledSaveInFunction(true);
            $cart->getQuote()->setTotalsCollectedFlag(true);
            $cart->save();


            $cart->unsetData('quote');

            try{
                $cart->getCheckoutSession()->clearCachedQuote();
            }catch (\Exception $e){}

            $cart->getQuote()->setTotalsCollectedFlag(false);
            $cart->getQuote()->setCalledSaveInFunction(true);
            $cart->save();
            $cart->getQuote()->setCalledSaveInFunction(false);
        }
    }
}