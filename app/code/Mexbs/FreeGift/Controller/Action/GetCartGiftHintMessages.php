<?php
namespace Mexbs\FreeGift\Controller\Action;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Controller\ResultFactory;

class GetCartGiftHintMessages extends \Magento\Framework\App\Action\Action
{
    private $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ){
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $this->_view->loadLayout();
        return $this->resultJsonFactory->create()->setData([
            'status' => 'success',
            'cart_gift_hints_html' => $this->_view->getLayout()->getBlock('freeGift.cart.hint.messages')->toHtml()
        ]);
    }
}