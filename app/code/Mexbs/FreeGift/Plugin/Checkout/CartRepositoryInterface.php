<?php
namespace Mexbs\FreeGift\Plugin\Checkout;

class CartRepositoryInterface
{
    protected $cart;
    protected $freeGiftHelper;
    protected $request;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Mexbs\FreeGift\Helper\Data $freeGiftHelper,
        \Magento\Framework\App\Request\Http $request
    ){
        $this->cart = $cart;
        $this->freeGiftHelper = $freeGiftHelper;
        $this->request = $request;
    }

    public function afterSave(\Magento\Quote\Api\CartRepositoryInterface $subject)
    {
        if($this->request->getFullActionName() == "checkout_cart_couponPost"){
            $this->freeGiftHelper->autoAddAndCorrectAutoAdded($this->cart);
        }
    }
}