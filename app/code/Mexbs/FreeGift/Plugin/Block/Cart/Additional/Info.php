<?php
namespace Mexbs\FreeGift\Plugin\Block\Cart\Additional;

class Info
{
    protected $apHelper;

    public function __construct(
        \Mexbs\ApBase\Helper\Data $apHelper
    ){
        $this->apHelper = $apHelper;
    }

    public function aroundToHtml(
        \Magento\Checkout\Block\Cart\Additional\Info $subject,
        \Closure $proceed
    ){
        $html =  $proceed();
        $item = $subject->getItem();

        $itemHintMessages = $this->apHelper->unserializeAndGetObjectProperty('hint_messages', $item);
        if($item){
            if($item->getGiftRuleId() && is_numeric($item->getGiftRuleId())){
                $giftTriggerItemIdsQtys = $this->apHelper->unserializeAndGetObjectProperty('gift_trigger_item_ids_qtys', $item);
                $html .= '<div class="cart-item-gift-message in-item" data-rule-id="'. $item->getGiftRuleId()
                    .'" data-gift-trigger-item-ids-qtys="'. (!empty($giftTriggerItemIdsQtys) ? htmlspecialchars(json_encode($giftTriggerItemIdsQtys)) : '')
                    .'" data-product-id="'.$item->getProductId()
                    .'" data-gift-message="'.$item->getGiftMessage().'"><span>'.$item->getGiftMessage().'</span></div>';
            }
            if(is_array($itemHintMessages)){
                $giftTriggerItemIdsQtysPerRule = $this->apHelper->unserializeAndGetObjectProperty('gift_trigger_item_ids_qtys_of_same_group', $item);
                $giftQtysCanAddPerGroupPerRule = $this->apHelper->unserializeAndGetObjectProperty('gift_qtys_can_add_per_group', $item);
                foreach($itemHintMessages as $ruleId => $itemHintMessage){
                    $html .= '<div class="gift_cart_hint in-item gift" data-message-type="gift-hint-message" data-rule-id="'.$ruleId.
                        '" data-gift-trigger-item-ids-qtys-of-same-group="'.
                        (isset($giftTriggerItemIdsQtysPerRule[$ruleId]) ? htmlspecialchars(json_encode($giftTriggerItemIdsQtysPerRule[$ruleId])) : '').
                        '"  data-gift-qtys-can-add-per-group="'.
                        (isset($giftQtysCanAddPerGroupPerRule[$ruleId]) ? htmlspecialchars(json_encode($giftQtysCanAddPerGroupPerRule[$ruleId])) : '')
                        .'" data-product-id="'.$item->getProductId()
                        .'" data-gift-message="'.$itemHintMessage
                        .'"><span>'.$itemHintMessage.'</span></div>';
                }
            }
        }
        return $html;
    }
}