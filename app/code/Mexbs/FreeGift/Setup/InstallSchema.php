<?php
namespace Mexbs\FreeGift\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $setup->getConnection()->addColumn(
            $installer->getTable('quote_item'),
            'gift_rule_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => true,
                'default' => null,
                'comment' => 'Gift Rule ID'
            ]
        );

        $setup->getConnection()->addColumn(
            $installer->getTable('quote_item'),
            'gift_trigger_item_ids_qtys',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '10K',
                'default' => null,
                'comment' => 'Gift Trigger Item IDs and quantities'
            ]
        );

        $setup->getConnection()->addColumn(
            $installer->getTable('quote_item'),
            'gift_message',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '1M',
                'default' => null,
                'comment' => 'Gift Message'
            ]
        );

        $setup->getConnection()->addColumn(
            $installer->getTable('quote_item'),
            'gift_qtys_can_add_per_group',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '1K',
                'default' => null,
                'comment' => 'Gift Qtys one can add per group'
            ]
        );
        $setup->getConnection()->addColumn(
            $installer->getTable('quote_item'),
            'gift_trigger_item_ids_qtys_of_same_group',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '1M',
                'default' => null,
                'comment' => 'Gift Trigger Item Ids  and quantities of the Same Group'
            ]
        );

        $setup->getConnection()->addColumn(
            $installer->getTable('quote'),
            'gift_hint_messages',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '1M',
                'default' => null,
                'comment' => 'Gift Hint Message'
            ]
        );
        $setup->getConnection()->addColumn(
            $installer->getTable('quote'),
            'gift_qtys_can_add_per_group',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'length' => '1K',
                'default' => null,
                'comment' => 'Gift qtys can add per group per rule'
            ]
        );

        $installer->endSetup();
    }
}