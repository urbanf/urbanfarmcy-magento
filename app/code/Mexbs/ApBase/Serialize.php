<?php
// @codingStandardsIgnoreFile

namespace Mexbs\ApBase;

class Serialize
{
    public function serialize($value){
        try{
            return \Magento\Framework\App\ObjectManager::getInstance()->get("Magento\\Framework\\Serialize\\Serializer\\Serialize")->serialize($value);
        }catch (\Exception $e){
           return null;
        }
    }

    public function unserialize($value){
        try{
            return \Magento\Framework\App\ObjectManager::getInstance()->get("Magento\\Framework\\Serialize\\Serializer\\Serialize")->unserialize($value);
        }catch (\Exception $e){
            return null;
        }
    }
}