<?php

namespace Urban\Integrador\Cron;

use Urban\Integrador\Logger\Logger;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Urban\Integrador\Helper\GerenciadorCliente;

class IntegrarCliente
{
    public const CONFIG_HABILITAR_INTEGRACAO_CLIENTE = "urban_integrador/geral/habilitar_integracao_cliente";
    public const CONFIG_MAX_TENTATIVAS = "urban_integrador/geral/numero_tentativas_cliente";

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @param ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param GerenciadorCliente
     */
    private $gerenciadorCliente;

    public function __construct(
        Logger $logger,
        CollectionFactory $customerCollectionFactory,
        ScopeConfigInterface $scopeConfig,
        GerenciadorCliente $gerenciadorCliente
    ) {
        $this->logger = $logger;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->gerenciadorCliente = $gerenciadorCliente;
    }

    /**
     * Monta uma collection com todos os clientes que precisam
     * ser integrados e os envia para a integração
     */
    public function execute()
    {
        $integracao_habilitada = (bool) $this->scopeConfig->getValue(self::CONFIG_HABILITAR_INTEGRACAO_CLIENTE);

        if ($integracao_habilitada) {
            $max_tentativas = (int) $this->scopeConfig->getValue(self::CONFIG_MAX_TENTATIVAS);
            $customerCollection = $this->customerCollectionFactory->create()
                ->addAttributeToSelect("*")
                ->addFieldToFilter("taxvat", ["notnull" => true, "neq" => ""]) // cujo CPF não seja nulo ou branco
                ->addFieldToFilter("default_shipping", ["notnull" => true]) // cujo endereço não seja nulo
                ->addFieldToFilter("protheus_cliente_deve_integrar__cy", ["eq" => true])
                ->addFieldToFilter("protheus_cliente_numero_tentativas__cy", ["lt" => $max_tentativas]) // 'less than'
                ->load();
            $customer_count = count($customerCollection);

            $this->logger->info(__("Cron\IntegrarCliente: Encontramos " . $customer_count
                . " cliente(s) que precisa(am) ser integrado(s)."));

            if ($customer_count > 0) {
                foreach ($customerCollection as $customer) {
                    $this->gerenciadorCliente->integrar($customer);
                }
            }
        } else {
            $this->logger->info(__("Cron\IntegrarCliente: A integração de CLIENTES está desabilitada."));
        }
    }
}
