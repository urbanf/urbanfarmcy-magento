<?php

namespace Urban\Integrador\Cron;

use Urban\Integrador\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Urban\Integrador\Helper\GerenciadorPedido;

class IntegrarPedido
{
    public const CONFIG_HABILITAR_INTEGRACAO_PEDIDO = "urban_integrador/geral/habilitar_integracao_pedido";
    public const CONFIG_MAX_TENTATIVAS = "urban_integrador/geral/numero_tentativas_pedido";

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @param ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param GerenciadorPedido
     */
    private $gerenciadorPedido;

    public function __construct(
        Logger $logger,
        CollectionFactory $orderCollectionFactory,
        ScopeConfigInterface $scopeConfig,
        GerenciadorPedido $gerenciadorPedido
    ) {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->gerenciadorPedido = $gerenciadorPedido;
    }

    /**
     * Monta uma collection com todos os pedidos que precisam
     * ser integrados e os envia para a integração
     */
    public function execute()
    {
        $integracao_habilitada = (bool) $this->scopeConfig->getValue(self::CONFIG_HABILITAR_INTEGRACAO_PEDIDO);

        if ($integracao_habilitada) {
            $max_tentativas = (int) $this->scopeConfig->getValue(self::CONFIG_MAX_TENTATIVAS);
            $orderCollection = $this->orderCollectionFactory->create()
                ->addAttributeToSelect("*")
                ->addFieldToFilter("protheus_pedido_deve_integrar__cy", ["eq" => true])
                ->addFieldToFilter("protheus_pedido_numero_tentativas__cy", ["lt" => $max_tentativas]) // 'less than'
                ->load();
            $order_count = count($orderCollection);

            $this->logger->info(__("Cron\IntegrarPedido: Encontramos " . $order_count
                . " pedido(s) que precisa(am) ser integrado(s)."));

            if ($order_count > 0) {
                foreach ($orderCollection as $order) {
                    $this->gerenciadorPedido->integrar($order);
                }
            }
        } else {
            $this->logger->info(__("Cron\IntegrarPedido: A integração de PEDIDOS está desabilitada."));
        }
    }
}
