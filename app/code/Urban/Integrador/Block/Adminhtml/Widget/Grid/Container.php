<?php

namespace Urban\Integrador\Block\Adminhtml\Widget\Grid;

use Magento\Backend\Block\Widget\Grid\Container as GridContainer;

class Container extends GridContainer
{
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');
    }
}
