<?php
namespace Urban\Integrador\Block\Adminhtml\Pedidos\Edit;

use Magento\Backend\Block\Widget\Form as WidgetForm;
use Magento\Catalog\Model\Product\Attribute\Backend\Price;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->coreRegistry = $coreRegistry;
        $this->orderRepository = $orderRepository;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('edit_form');
        $this->setTitle(__('Edit'));
    }

    /**
     * @return WidgetForm
     */
    protected function _prepareForm()
    {
        $model = $this->coreRegistry->registry('current_order');
        $orderId = $model->getId();
        $order = $this->orderRepository->get($orderId);
        $storeId = $model->getStoreId();

        $modelData = $model->getData();

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', ['order_id' => $orderId]),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ],
            ]
        );
        $form->setHtmlIdPrefix('integrador_');
        $form->setDataObject($model);

        // Customer Information
        $orderFieldset = $form->addFieldset(
            'order_fieldset',
            [
                'legend' => __('Dados gerais do pedido'),
                'class' => 'fieldset-wide'
            ]
        );

        $orderFieldset->addField(
            'customer_taxvat',
            'text',
            [
                'name' => 'customer_taxvat',
                'label' => __('CPF'),
                'title' => __('CPF'),
                'class' => 'validate-cpf',
                'required' => true
            ]
        );

        $orderFieldset->addField(
            'shipping_amount',
            'text',
            [
                'name' => 'shipping_amount',
                'label' => __('Shipping Amount'),
                'title' => __('Shipping Amount'),
                'class' => 'validate-number',
                'required' => true
            ]
        );

        $orderFieldset->addField(
            'amasty_date',
            'date',
            [
                'name' => 'amasty_date',
                'label' => __('Amasty Date'),
                'title' => __('Amasty Date'),
                'date_format' => 'yyyy-MM-dd',
                'required' => true
            ]
        );

        $orderFieldset->addField(
            'amasty_tinterval_id',
            'text',
            [
                'name' => 'amasty_tinterval_id',
                'label' => __('Amasty Interval ID'),
                'title' => __('Amasty Interval ID'),
                'required' => true
            ]
        );

        $orderFieldset->addField(
            'amasty_time',
            'text',
            [
                'name' => 'amasty_time',
                'label' => __('Amasty Time'),
                'title' => __('Amasty Time'),
                'required' => true
            ]
        );

        $orderFieldset->addField(
            'amasty_comment',
            'text',
            [
                'name' => 'amasty_comment',
                'label' => __('Amasty Comment'),
                'title' => __('Amasty Comment'),
                'required' => false
            ]
        );

        $orderFieldset->addField(
            'entrega_portaria',
            'select',
            [
                'name' => 'entrega_portaria',
                'label' => __('Pode entregar na portaria?'),
                'title' => __('Pode entregar na portaria?'),
                'required' => false,
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        // Order items
        $orderItems = $order->getAllItems();

        foreach ($orderItems as $item) {
            if ($item->getProductType() === "bundle") {
                continue;
            }

            $parent_item = $item->getParentItem();

            $orderItemFieldset = $form->addFieldset(
                'order_item_fieldset_' . $item->getData('item_id'),
                [
                    'legend' => "(" . $item->getData('sku') . ") " . $item->getData('name'),
                    'class' => 'fieldset-wide'
                ]
            );

            $orderItemFieldset->addField(
                'sku_' . $item->getData('item_id'),
                'text',
                [
                    'name' => 'sku_' . $item->getData('item_id'),
                    'label' => __('SKU'),
                    'title' => __('SKU'),
                    'required' => true
                ]
            );

            $orderItemFieldset->addField(
                'qty_ordered_' . $item->getData('item_id'),
                'text',
                [
                    'name' => 'qty_ordered_' . $item->getData('item_id'),
                    'label' => __('Quantidade'),
                    'title' => __('Quantidade'),
                    'class' => 'validate-number',
                    'required' => true
                ]
            );
            
            if ($parent_item) {
                // $porcentagem_desconto_bundle = (float) $parent_item->getData('bundle_special_price_percent');
                // $desconto = 100 - $porcentagem_desconto_bundle;
                $preco_venda = (float) $item->getData('bundle_child_original_price');

                $orderItemFieldset->addField(
                    'bundle_child_original_price_' . $item->getData('item_id'),
                    'text',
                    [
                        'name' => 'bundle_child_original_price_' . $item->getData('item_id'),
                        'label' => __('Preço de venda'),
                        'title' => __('Preço de venda'),
                        'class' => 'validate-number',
                        'required' => true
                    ]
                );

                $modelData['bundle_child_original_price_' . $item->getData('item_id')]
                    = $item->getData('bundle_child_original_price');
            } else {
                // $desconto = (float) 100 * (1 - $item->getPrice() / $item->getOriginalPrice());
                $preco_venda = (float) $item->getOriginalPrice();

                $orderItemFieldset->addField(
                    'original_price_' . $item->getData('item_id'),
                    'text',
                    [
                        'name' => 'original_price_' . $item->getData('item_id'),
                        'label' => __('Preço de venda'),
                        'title' => __('Preço de venda'),
                        'class' => 'validate-number',
                        'required' => true
                    ]
                );

                $modelData['original_price_' . $item->getData('item_id')] = $item->getData('original_price');
            }

            $modelData['sku_' . $item->getData('item_id')] = $item->getData('sku');
            $modelData['qty_ordered_' . $item->getData('item_id')] = $item->getData('qty_ordered');
        }

        $form->setValues($modelData);
        $form->setUseContainer(true);
        $this->setForm($form);

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                \Magento\Framework\View\Element\Template::class
            )->setTemplate('Urban_Integrador::order/js.phtml')
        );

        return parent::_prepareForm();
    }
}
