<?php

namespace Urban\Integrador\Block\Adminhtml\Pedidos;

class Details extends \Magento\Backend\Block\Template
{
    public const EXTENSION_ATTRIBUTES = [
        'protheus_pedido_integrado__cy' => [
            'label' => "O pedido já foi integrado?",
            'type' => "boolean"
        ],
        'protheus_pedido_deve_integrar__cy' => [
            'label' => "O pedido deve ser integrado?",
            'type' => "boolean"
        ],
        'protheus_pedido_numero_tentativas__cy' => [
            'label' => "Tentativas realizada",
            'type' => "smallint"
        ],
        'protheus_pedido_guid_ultima_tentativa__cy' => [
            'label' => "GUID da última tentativa",
            'type' => "varchar"
        ],
        'protheus_pedido_url_ultima_tentativa__cy' => [
            'label' => "URL da última tentativa",
            'type' => "text"
        ],
        'protheus_pedido_payload_ultima_tentativa__cy' => [
            'label' => "Payload da última tentativa",
            'type' => "text"
        ],
        'protheus_pedido_horario_ultima_tentativa__cy' => [
            'label' => "Horário da última tentativa",
            'type' => "timestamp"
        ],
        'protheus_pedido_codigo_ultima_tentativa__cy' => [
            'label' => "Código de retorno da última tentativa",
            'type' => "varchar"
        ],
        'protheus_pedido_mensagem_ultima_tentativa__cy' => [
            'label' => "Resposta da última tentativa",
            'type' => "text"
        ]
    ];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Eav\Model\AttributeRepository
     */
    private $attributeRepository;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param \Magento\Config\Model\Config\Source\Yesno
     */
    private $yesNoOptions;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Config\Model\Config\Source\Yesno $yesNoOptions,
        array $data = []
    ) {

        $this->_coreRegistry = $registry;
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->yesNoOptions = $yesNoOptions;
        parent::__construct($context, $data);
    }

    public function getOrderModel()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    public function getOrderAttributes()
    {
        $order = $this->getOrderModel();
        $orderAttributes = [];

        foreach (self::EXTENSION_ATTRIBUTES as $attributeCode => $attributeData) {
            $orderAttributes[$attributeCode] = [
                'code' => $attributeCode,
                'type' => $attributeData['type'],
                'label' => $attributeData['label'],
                'value' => $order->getData($attributeCode)
            ];

            if ($attributeData['type'] === 'boolean') {
                $yesNo = $this->yesNoOptions->toArray();
                $orderAttributes[$attributeCode]['value'] = $yesNo[$order->getData($attributeCode)];
            }
        }

        return $orderAttributes;
    }
}
