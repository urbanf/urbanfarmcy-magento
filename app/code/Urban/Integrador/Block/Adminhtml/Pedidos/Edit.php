<?php

namespace Urban\Integrador\Block\Adminhtml\Pedidos;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {

        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize form
     * Add standard buttons
     * Add "Save and Apply" button
     * Add "Save and Continue" button
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'Urban_Integrador';
        $this->_controller = 'adminhtml_pedidos';

        parent::_construct();

        $this->buttonList->remove('delete');
        $this->buttonList->update('save', 'label', __('Save'));

        $backUrl = $this->getUrl('*/*', ['order_id' => $this->getRequest()->getParam('order_id')]);
        $this->buttonList->update('back', 'onclick', "setLocation('{$backUrl}')");

        $this->buttonList->add(
            'save_and_edit_button',
            [
                'label' => __('Salvar e Integrar'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
                ]
            ],
            100
        );
    }
}
