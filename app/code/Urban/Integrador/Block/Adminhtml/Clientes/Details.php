<?php

namespace Urban\Integrador\Block\Adminhtml\Clientes;

class Details extends \Magento\Backend\Block\Template
{
    public const EXTENSION_ATTRIBUTES = [
        'entity_id',
        'firstname',
        'lastname',
        'email',
        'dob',
        'telefone__cy',
        'protheus_cliente_integrado__cy',
        'protheus_cliente_deve_integrar__cy',
        'protheus_cliente_numero_tentativas__cy',
        'protheus_cliente_guid_ultima_tentativa__cy',
        'protheus_cliente_url_ultima_tentativa__cy',
        'protheus_cliente_payload_ultima_tentativa__cy',
        'protheus_cliente_horario_ultima_tentativa__cy',
        'protheus_cliente_codigo_ultima_tentativa__cy',
        'protheus_cliente_mensagem_ultima_tentativa__cy'
    ];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Eav\Model\AttributeRepository
     */
    private $attributeRepository;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param \Magento\Config\Model\Config\Source\Yesno
     */
    private $yesNoOptions;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Config\Model\Config\Source\Yesno $yesNoOptions,
        array $data = []
    ) {

        $this->_coreRegistry = $registry;
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->yesNoOptions = $yesNoOptions;
        parent::__construct($context, $data);
    }

    public function getCustomerModel()
    {
        return $this->_coreRegistry->registry('current_customer');
    }

    public function getCustomerAttributes()
    {
        $customer = $this->getCustomerModel();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('attribute_code', self::EXTENSION_ATTRIBUTES, 'in')
            ->create();
        $attributesList = $this->attributeRepository->getList('customer', $searchCriteria);
        $attributes = $attributesList->getItems();
        $customerAttributes = [];

        foreach ($attributes as $attribute) {
            $attributeData = $attribute->getData();
            $attributeCode = $attributeData['attribute_code'];
            $attributeValue = $customer->getData($attributeCode);

            $customerAttributes[$attributeCode] = [
                'code' => $attributeData['attribute_code'],
                'type' => $attributeData['backend_type'],
                'label' => $attributeData['frontend_label'],
                'value' => $attributeValue
            ];

            if ($attributeData['frontend_input'] === 'boolean') {
                $yesNo = $this->yesNoOptions->toArray();
                $customerAttributes[$attributeCode]['value'] = $yesNo[$attributeValue];
            }
        }

        return $customerAttributes;
    }
}
