define([
    'jquery',
    'uiComponent'
], function($, Component) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this.formatJsonRow();
        },

        formatJsonRow: function() {
            let rows = document.querySelectorAll(
                '.protheus_pedido_payload_ultima_tentativa__cy'
                + ',.protheus_pedido_mensagem_ultima_tentativa__cy'
            );

            rows.forEach( (row) => {
                let rowContent = row.innerText;
                let jsonContent = JSON.parse(rowContent);
                let preContent = document.createElement('pre');
                preContent.innerText = JSON.stringify(jsonContent, null, 4);
                row.innerHTML = null;
                row.appendChild(preContent);
            });
        }
    });
});