define([
    'jquery',
    'uiComponent'
], function($, Component) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this.formatJsonRow();
        },

        formatJsonRow: function() {
            let row = document.querySelector('.protheus_cliente_payload_ultima_tentativa__cy');
            let rowContent = row.innerText;
            let jsonContent = JSON.parse(rowContent);
            let preContent = document.createElement('pre');
            preContent.innerText = JSON.stringify(jsonContent, null, 4);
            row.innerHTML = null;
            row.appendChild(preContent);
        }
    });
});