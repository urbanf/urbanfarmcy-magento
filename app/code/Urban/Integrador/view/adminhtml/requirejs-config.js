var config = {
    map: {
        '*': {
            'customerDetails': 'Urban_Integrador/js/customer-details',
            'orderDetails': 'Urban_Integrador/js/order-details',
            'codigosIbge': 'Urban_Integrador/js/codigos_ibge'
        }
    }
};
