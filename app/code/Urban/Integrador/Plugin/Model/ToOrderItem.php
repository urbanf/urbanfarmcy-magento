<?php

namespace Urban\Integrador\Plugin\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Quote\Model\Quote\Item\ToOrderItem as MagentoToOrderItem;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote\Address\Item as AddressItem;

class ToOrderItem
{
    /**
     * @param ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    /**
     * @param MagentoToOrderItem $subject
     * @param callable $proceed
     * @param Item|AddressItem $item
     * @param array $data
     */
    public function aroundConvert(
        MagentoToOrderItem $subject,
        callable $proceed,
        $item,
        $data
    ) {
        $result = $proceed($item, $data);
        $product = $this->productRepository->getById($item->getProductId());

        if ($item->getProductType() === "bundle") {
            $special_price = floatval($product->getData('special_price'));
            $result->setData('bundle_special_price_percent', $special_price);
        } elseif ($item->getParentItem() !== null) {
            $original_price = floatval($product->getPrice());
            $result->setData('bundle_child_original_price', $original_price);
        }

        return $result;
    }
}
