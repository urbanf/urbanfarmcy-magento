<?php

namespace Urban\Integrador\Plugin\Model\ResourceModel;

use Urban\Integrador\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order as OrderResourceModel;
use Magento\Framework\Model\AbstractModel;

class Order
{
    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger
    ) {
        $this->logger = $logger;
    }
    
    /**
     * @param OrderResourceModel $subject
     * @param AbstractModel $order
     * @return AbstractModel
     */
    public function beforeSave(
        OrderResourceModel $subject,
        AbstractModel $order
    ) {
        if ($order->getData('protheus_pedido_salvo_pela_integracao__cy')) {
            $order->setData('protheus_pedido_salvo_pela_integracao__cy', false);
        } else {
            $order->setData('protheus_pedido_deve_integrar__cy', true);
            $order->setData('protheus_pedido_numero_tentativas__cy', 0);
        }

        $dados_para_log = [
            "entity_id" => $order->getEntityId(),
            "increment_id" => $order->getIncrementId(),
            "customer_id" => $order->getCustomerId(),
            "status" => $order->getStatus()
        ];

        $this->logger->info("Plugin\Model\ResourceModel\Order: pedido salvo. " . json_encode($dados_para_log));

        return [$order];
    }
}
