<?php

namespace Urban\Integrador\Controller\Adminhtml;

abstract class Index extends \Magento\Backend\App\Action
{
    public const ADMIN_RESOURCE = 'Urban_Integrador::index';

    /**
     * Is the user allowed to view the page.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }
}
