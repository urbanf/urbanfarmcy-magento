<?php
namespace Urban\Integrador\Controller\Adminhtml\Pedidos;

use Magento\Backend\App\Action;

class Details extends \Urban\Integrador\Controller\Adminhtml\Index
{

    protected $_coreRegistry = null;

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\Model\Session
     */
    private $modelSession;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\Session $modelSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\ItemRepository $orderItemRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->modelSession = $modelSession;
        $this->orderFactory = $orderFactory;
        $this->orderResource = $orderResource;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Urban_Integrador::integrador_pedidos')
            ->addBreadcrumb(__('Pedidos'), __('Pedidos'))
            ->addBreadcrumb(__('Detalhes do Pedido'), __('Detalhes do Pedido'));
        return $resultPage;
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('entity_id');

        $orderModel = $this->orderFactory->create();
        $this->orderResource->load($orderModel, $orderId);

        $incrementId = $orderModel->getIncrementId();

        $this->_coreRegistry->register('current_order', $orderModel);

        $resultPage = $this->_initAction();

        $title = __('Edit information for order #%1', $incrementId);

        $resultPage->getConfig()->getTitle()->prepend($title);
        $resultPage->addBreadcrumb($title, $title);

        return $resultPage;
    }
}
