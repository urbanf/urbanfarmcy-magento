<?php

namespace Urban\Integrador\Controller\Adminhtml\Pedidos;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\ItemRepository;
use Urban\Integrador\Logger\Logger;
use Urban\Integrador\Helper\GerenciadorPedido;

class Save extends \Urban\Integrador\Controller\Adminhtml\Index
{
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        DateTime $date,
        TransportBuilder $transportBuilder,
        OrderRepositoryInterface $orderRepository,
        ItemRepository $orderItemRepository,
        Logger $logger,
        GerenciadorPedido $gerenciadorPedido
    ) {
        parent::__construct($context);
        $this->session = $context->getSession();
        $this->coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        $this->date = $date;
        $this->transportBuilder = $transportBuilder;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->logger = $logger;
        $this->gerenciadorPedido = $gerenciadorPedido;
    }

    public function execute()
    {
        $request = $this->getRequest();

        if ($request->getPostValue()) {
            $data = $request->getPostValue();
            $params = $request->getParams();
            $orderId = $params['order_id'];

            try {
                $order = $this->orderRepository->get($orderId);
                $increment_id = $order->getIncrementId();
                $orderItems = $order->getAllItems();

                if ($order !== null && is_array($data) && !empty($data)) {
                    $order->setData('customer_taxvat', $data['customer_taxvat']);
                    $order->setData('shipping_amount', $data['shipping_amount']);
                    $order->setData('amasty_date', $data['amasty_date']);
                    $order->setData('amasty_tinterval_id', $data['amasty_tinterval_id']);
                    $order->setData('amasty_time', $data['amasty_time']);
                    $order->setData('amasty_comment', $data['amasty_comment']);
                    $order->setData('entrega_portaria', $data['entrega_portaria']);

                    foreach ($orderItems as $item) {
                        if ($item->getProductType() === "bundle") {
                            continue;
                        }

                        $item_id = $item->getData('item_id');
                        $parent_item = $item->getParentItem();
                        
                        $item->setData('sku', $data['sku_' . $item_id]);
                        $item->setData('qty_ordered', $data['qty_ordered_' . $item_id]);

                        if ($parent_item) {
                            $item->setData('bundle_child_original_price', $data['bundle_child_original_price_' . $item_id]);
                        } else {
                            $item->setData('original_price', $data['original_price_' . $item_id]);
                        }

                        $this->orderItemRepository->save($item);
                    }

                    $this->orderRepository->save($order);

                    if (isset($params['back']) && $params['back'] === 'edit') {
                        $this->gerenciadorPedido->integrar($order);
                        $this->messageManager->addSuccessMessage(
                            "Integração do pedido #{$increment_id} enviada com sucesso."
                            . " Note que isso não significa que o Protheus integrou o pedido. "
                            . " Para confirmar isso, verifique o status do pedido na grid abaixo."
                        );
                    } else {
                        $this->messageManager->addSuccessMessage(
                            "Pedido #{$increment_id} salvo com sucesso."
                            . " Ele será adicionado à fila para integração."
                        );
                    }
                } else {
                    throw new NoSuchEntityException(__("Não foi encontrado nenhum pedido com ID {$increment_id}."));
                }
                return $this->_redirect('*/*', ['order_id' => $orderId]);
            } catch (\Exception $e) {
                $this->logger->info(__("Controller\Adminhtml\Pedidos\Save: " . $e->getMessage()));
                $this->messageManager->addError(nl2br($e->getMessage()));
                return $this->_redirect('*/*');
            }
        }
    }
}
