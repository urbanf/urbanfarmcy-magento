<?php
namespace Urban\Integrador\Controller\Adminhtml\Pedidos;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Urban\Integrador\Controller\Adminhtml\Index
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        if ($this->getRequest()->getParam('ajax')) {
            $this->_forward('grid');
            return;
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Urban_Integrador::integrador_pedidos');
        $resultPage->addBreadcrumb(__('Pedidos'), __('Pedidos'));
        $resultPage->getConfig()->getTitle()->prepend(__('Todos os pedidos'));

        return $resultPage;
    }
}

