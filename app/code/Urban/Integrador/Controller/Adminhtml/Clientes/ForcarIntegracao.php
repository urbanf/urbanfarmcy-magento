<?php

namespace Urban\Integrador\Controller\Adminhtml\Clientes;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\NoSuchEntityException;

class ForcarIntegracao extends \Urban\Integrador\Controller\Adminhtml\Index
{
    protected $_coreRegistry = null;

    protected $resultPageFactory;

    /**
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @param \Urban\Integrador\Logger\Logger
     */
    private $logger;

    /**
     * @param \Urban\Integrador\Helper\GerenciadorCliente
     */
    private $gerenciadorCliente;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Urban\Integrador\Logger\Logger $logger,
        \Urban\Integrador\Helper\GerenciadorCliente $gerenciadorCliente
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->storeManager = $storeManager;
        $this->backendSession = $backendSession;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->logger = $logger;
        $this->gerenciadorCliente = $gerenciadorCliente;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();

            if (!isset($params['entity_id'])) {
                throw new NoSuchEntityException(__("O parâmetro 'entity_id' não foi encontrado."));
            }

            $entity_id = $params['entity_id'];
            $customerCollection = $this->customerCollectionFactory->create()
                ->addAttributeToSelect("*")
                ->addFieldToFilter("entity_id", ["eq" => $entity_id])
                ->load();
            $customer_count = count($customerCollection);

            $this->logger->info(__("Controller\Adminhtml\Clientes\ForcarIntegracao: Encontramos " . $customer_count
            . " cliente(s) que precisa(am) ser integrado(s)."));

            if ($customer_count > 0) {
                foreach ($customerCollection as $customer) {
                    $this->gerenciadorCliente->integrar($customer);
                    $this->messageManager->addSuccessMessage(
                        "Integração do cliente com ID {$entity_id} enviada com sucesso."
                        . " Note que isso não significa que o Protheus integrou o cliente. "
                        . " Para confirmar isso, verifique o status do cliente na grid abaixo."
                    );
                }
            } else {
                throw new NoSuchEntityException(__("Não foi encontrado nenhum cliente com ID {$entity_id}."));
            }
        } catch (\Exception $e) {
            $this->logger->info(__("Controller\Adminhtml\Clientes\ForcarIntegracao: " . $e->getMessage()));
            $this->messageManager->addError(nl2br($e->getMessage()));
        }

        return $this->_redirect('*/*');
    }
}
