<?php

namespace Urban\Integrador\Controller\Adminhtml\Clientes;

class Grid extends \Urban\Integrador\Controller\Adminhtml\Index
{
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
