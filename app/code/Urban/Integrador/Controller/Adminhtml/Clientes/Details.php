<?php
namespace Urban\Integrador\Controller\Adminhtml\Clientes;

use Magento\Backend\App\Action;

class Details extends \Urban\Integrador\Controller\Adminhtml\Index
{

    protected $_coreRegistry = null;

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\Model\Session
     */
    private $modelSession;

    /**
     * @param \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @param \Magento\Customer\Model\ResourceModel\Customer
     */
    private $customerResource;

    /**
     * @param \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\Session $modelSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\Customer $customerResource,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->modelSession = $modelSession;
        $this->orderFactory = $orderFactory;
        $this->orderResource = $orderResource;
        $this->orderRepository = $orderRepository;
        $this->customerFactory = $customerFactory;
        $this->customerResource = $customerResource;
        $this->customerRepository = $customerRepository;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Urban_Integrador::integrador_clientes')
            ->addBreadcrumb(__('Clientes'), __('Clientes'))
            ->addBreadcrumb(__('Detalhes do Cliente'), __('Detalhes do Cliente'));
        return $resultPage;
    }

    public function execute()
    {
        $customerId = $this->getRequest()->getParam('entity_id');
        $customerModel = $this->customerFactory->create()->load($customerId);
        $fullname = $customerModel->getFirstname() . " " . $customerModel->getLastname();

        $this->_coreRegistry->register('current_customer', $customerModel);

        $resultPage = $this->_initAction();

        $title = __('Information for customer %1', $fullname);

        $resultPage->getConfig()->getTitle()->prepend($title);
        $resultPage->addBreadcrumb($title, $title);

        return $resultPage;
    }
}
