<?php
namespace Urban\Integrador\Controller\Testes;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;

class Index extends \Magento\Framework\App\Action\Action
{

    const API_URL = 'https://viacep.com.br/ws/';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \GuzzleHttp\ClientFactory
     */
    private $clientFactory;

    /**
     * @param \GuzzleHttp\Psr7\ResponseFactory
     */
    private $responseFactory;

    /**
     * @param \Urban\Integrador\Cron\IntegrarCliente
     */
    private $integrarCliente;

    /**
     * @param \Urban\Integrador\Cron\IntegrarPedido
     */
    private $integrarPedido;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        \Urban\Integrador\Api\ProtheusClientInterface $integradorClient,
        \Urban\Integrador\Cron\IntegrarCliente $integrarCliente,
        \Urban\Integrador\Cron\IntegrarPedido $integrarPedido
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->clientFactory = $clientFactory;
        $this->responseFactory = $responseFactory;
        $this->integradorClient = $integradorClient;
        $this->integrarCliente = $integrarCliente;
        $this->integrarPedido = $integrarPedido;
        return parent::__construct($context);
    }

    public function testarGuzzleHttp() {
        /** @var Client $client */
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => self::API_URL
        ]]);

        try {
            $response = $client->request(Request::HTTP_METHOD_GET, '60426055/json');
        } catch (GuzzleException $exception) {
            /** @var Response $response */
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
        }

        $jsonBody = $response->getBody()->getContents();
        $arrayBody = json_decode($jsonBody);

        return true;
    }

    public function testarClientModel() {
        $urlBase = $this->integradorClient->getBaseUrl();
        $endpoint = $this->integradorClient->getEndpoint('criar_cliente');
        $endpoint_errado = $this->integradorClient->getEndpoint('errado');
        $endpoint_em_branco = $this->integradorClient->getEndpoint('');
        $endpoint_nulo = $this->integradorClient->getEndpoint();
        $endpoint_int = $this->integradorClient->getEndpoint(1);

        return true;
    }

    public function testarIntegracaoCliente() {
        $this->integrarCliente->execute();
        return true;
    }

    public function testarIntegracaoPedido() {
        $this->integrarPedido->execute();
        return true;
    }

    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        // $this->testarGuzzleHttp();
        // $this->testarClientModel();
        $this->testarIntegracaoCliente();
        $this->testarIntegracaoPedido();

        return $this->_pageFactory->create();
    }
}
