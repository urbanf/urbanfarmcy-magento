<?php

namespace Urban\Integrador\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

/**
 * Class AddBasicAccountAttributes
 */
class AddIntegrationAttributes implements DataPatchInterface
{
    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * AddCustomerUpdatedAtAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        Config $eavConfig,
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->eavConfig = $eavConfig;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
 
        $customerEntity = $this->eavConfig->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
 
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $attributesOptionsArray = [
            'protheus_cliente_integrado__cy' => [
                'type' => 'static',
                'label' => 'O cliente está integrado?',
                'input' => 'boolean',
                'backend' => \Magento\Customer\Model\Attribute\Backend\Data\Boolean::class,
                'position' => 100,
                'visible' => false,
                'system' => false
            ],
            'protheus_cliente_deve_integrar__cy' => [
                'type' => 'static',
                'label' => 'O cliente deve ser integrado?',
                'input' => 'boolean',
                'backend' => \Magento\Customer\Model\Attribute\Backend\Data\Boolean::class,
                'position' => 100,
                'required' => false,
                'visible' => false,
                'system' => false,
                'comment' => 'Indica se este cliente deverá ser enviado ao Protheus na próxima execução da Integração'
            ],
            'protheus_cliente_numero_tentativas__cy' => [
                'type' => 'static',
                'label' => 'Tentativas realizadas',
                'input' => 'text',
                'required' => false,
                'sort_order' => 110,
                'visible' => false,
                'system' => false,
                'position' => 110
            ],
            'protheus_cliente_guid_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'GUID da última tentativa',
                'input' => 'text',
                'required' => false,
                'sort_order' => 120,
                'visible' => false,
                'system' => false,
                'position' => 120
            ],
            'protheus_cliente_url_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'URL da última tentativa',
                'input' => 'text',
                'required' => false,
                'sort_order' => 135,
                'visible' => false,
                'system' => false,
                'position' => 135
            ],
            'protheus_cliente_payload_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'Payload da última tentativa',
                'input' => 'text',
                'required' => false,
                'sort_order' => 130,
                'visible' => false,
                'system' => false,
                'position' => 130
            ],
            'protheus_cliente_horario_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'Horário da última tentativa',
                'input' => 'date',
                'required' => false,
                'sort_order' => 140,
                'visible' => false,
                'system' => false,
                'position' => 140
            ],
            'protheus_cliente_codigo_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'Código de retorno da última tentativa',
                'input' => 'text',
                'required' => false,
                'sort_order' => 150,
                'visible' => false,
                'system' => false,
                'position' => 150
            ],
            'protheus_cliente_mensagem_ultima_tentativa__cy' => [
                'type' => 'static',
                'label' => 'Resposta da última tentativa',
                'input' => 'text',
                'required' => false,
                'sort_order' => 160,
                'visible' => false,
                'system' => false,
                'position' => 160
            ]
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeParams);

            $customAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $attributeCode);
            $customAttribute->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => [
                    'adminhtml_customer',
                    'customer_account_edit',
                    'customer_account_create',
                    'adminhtml_checkout'
                ]
            ]);

            $customAttribute->save();
        }
    }
}
