<?php

namespace Urban\Integrador\Api;

interface ProtheusClientInterface
{
    /**#@+
     * Constants
     */
    public const API_USER = 'urban_integrador/protheus/user';
    public const API_PASSWORD = 'urban_integrador/protheus/password';
    public const API_BASE_URL = 'urban_integrador/endpoints/url_base';
    public const API_ENDPOINTS = [
        'criar_cliente' => 'urban_integrador/endpoints/criar_cliente',
        'editar_cliente' => 'urban_integrador/endpoints/editar_cliente',
        'criar_pedido' => 'urban_integrador/endpoints/criar_pedido',
        'status_pedido' => 'urban_integrador/endpoints/status_pedido',
        'excluir_pedido' => 'urban_integrador/endpoints/excluir_pedido'
    ];
    public const ORDER_STATUS_CORRELATION = [
        'pending_payment' => 'A',
        'payment_review' => 'A',
        'pending' => 'A',
        'holded' => 'A',
        'complete' => 'E',
        'processing' => 'P',
        'motoboys_1' => 'P',
        'motoboys_2' => 'P',
        'motoboys_3' => 'P',
        'fraud' => 'Excluido',
        'canceled' => 'Excluido',
        'closed' => 'Excluido'
    ];
    /**#@-*/

    /**
     * Get username for API Integration with Protheus.
     *
     * @return string
     */
    public function getUser(): string;

    /**
     * Get password for API Integration with Protheus.
     *
     * @return string
     */
    public function getPassword(): string;

    /**
     * Get base url for API Integration with Protheus.
     *
     * @return string
     */
    public function getBaseUrl(): string;

    /**
     * Get endpoint configuration from specified endpoint string.
     *
     * @param string $endpoint
     * @return string
     */
    public function getEndpoint(string $endpoint): ?string;

    /**
     * Get Protheus order status based on Magento status code.
     *
     * @param string $status
     * @return string
     */
    public function getOrderStatusCorrelation(string $status): ?string;
}
