<?php

namespace Urban\Integrador\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Protheus mail sending conditions source
 */
class Conditions implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            1 => __('Em todas as ocorrências'),
            2 => __('Ao esgotar o número de tentativas')
        ];
    }
}
