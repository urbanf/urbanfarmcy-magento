<?php

namespace Urban\Integrador\Model;

use Urban\Integrador\Api\ProtheusClientInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ProtheusClient implements ProtheusClientInterface
{
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritdoc
     */
    public function getUser(): string
    {
        $username = $this->scopeConfig->getValue(self::API_USER);
        return $username;
    }

    /**
     * @inheritdoc
     */
    public function getPassword(): string
    {
        $password = $this->scopeConfig->getValue(self::API_PASSWORD);
        return $password;
    }

    /**
     * @inheritdoc
     */
    public function getBaseUrl(): string
    {
        $base_url = $this->scopeConfig->getValue(self::API_BASE_URL);
        return $base_url;
    }

    /**
     * @inheritdoc
     */
    public function getEndpoint(string $endpoint = ''): ?string
    {
        if (array_key_exists($endpoint, self::API_ENDPOINTS)) {
            $endpoint_url = $this->scopeConfig->getValue(self::API_ENDPOINTS[$endpoint]);
            return $endpoint_url;
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getOrderStatusCorrelation(string $status = ''): ?string
    {
        if (array_key_exists($status, self::ORDER_STATUS_CORRELATION)) {
            $protheus_status = self::ORDER_STATUS_CORRELATION[$status];
            return $protheus_status;
        }
        return null;
    }
}
