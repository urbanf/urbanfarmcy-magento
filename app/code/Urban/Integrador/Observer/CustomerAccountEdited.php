<?php

namespace Urban\Integrador\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Customer;
use Urban\Integrador\Logger\Logger;

class CustomerAccountEdited implements ObserverInterface
{
    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param Customer
     */
    private $customerModel;

    /**
     * @param Logger $logger
     * @param Customer $customerModel
     */
    public function __construct(
        Logger $logger,
        Customer $customerModel
    ) {
        $this->logger = $logger;
        $this->customerModel = $customerModel;
    }

    /**
     * Prepara o cliente para ser integrado sempre que ele for editado/salvo
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $eventName = $observer->getEvent()->getName();
        $customer = null;

        try {
            if ($eventName === 'customer_register_success' || $eventName === 'adminhtml_customer_save_after') {
                $email = $observer->getEvent()->getCustomer()->getEmail();
                $website_id = $observer->getEvent()->getCustomer()->getWebsiteId();
                $this->customerModel->setWebsiteId($website_id);
                $customer = $this->customerModel->loadByEmail($email);
            }
    
            if ($eventName === 'customer_account_edited') {
                $customer = $this->customerModel->loadByEmail($observer->getEvent()->getEmail());
            }
    
            if ($eventName === 'customer_address_save_after') {
                $customerAddress = $observer->getEvent()->getCustomerAddress();
                $customer = $customerAddress->getCustomer();
            }
    
            if ($customer !== null) {
                $customerData = $customer->getDataModel();
                $customerData->setCustomAttribute('protheus_cliente_deve_integrar__cy', true);
                $customerData->setCustomAttribute('protheus_cliente_numero_tentativas__cy', 0);
                $customer->updateData($customerData);
                $customer->save();
    
                $dados_para_log = [
                    "id" => $customerData->getId(),
                    "email" => $customerData->getEmail(),
                    "taxvat" => $customerData->getTaxvat()
                ];
    
                $this->logger->info(
                    "Observer\CustomerAccountEdited: cliente salvo. "
                    . "Evento: {$eventName}. Cliente: " . json_encode($dados_para_log)
                );
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->logger->info(
                "Observer\CustomerAccountEdited: houve um erro ao salvar o cliente. "
                . "Evento: {$eventName}. Exception: {$message}"
            );
        }
    }
}
