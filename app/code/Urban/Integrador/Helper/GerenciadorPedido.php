<?php

namespace Urban\Integrador\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request as HttpRequest;
use GuzzleHttp\Psr7\ResponseFactory;
use Amasty\Deliverydate\Model\DeliverydateFactory;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
use Urban\Integrador\Logger\Logger;
use Urban\Integrador\Api\ProtheusClientInterface;
use Urban\Integrador\Helper\Utils;
use Urban\Integrador\Helper\GerenciadorEmail;
use \Magento\Customer\Model\Customer;
use \Magento\Customer\Model\ResourceModel\CustomerFactory;

class GerenciadorPedido
{
    public const CONFIG_MAX_TENTATIVAS = "urban_integrador/geral/numero_tentativas_cliente";

    /**
     * @param string
     */
    private $correlation_id;

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param ClientInterface
     */
    private $protheusClient;

    /**
     * @param ClientFactory
     */
    private $httpClientFactory;

    /**
     * @param ResponseFactory
     */
    private $httpResponseFactory;

    /**
     * @param Utils
     */
    private $utils;

    /**
     * @param GerenciadorEmail
     */
    private $gerenciadorEmail;

    /**
     * @param DeliverydateFactory
     */
    private $deliverydateFactory;

    /**
     * @param OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param ScopeConfigInterface
     */
    private $scopeConfig;

    protected $customer;
    protected $customerFactory;

    /**
     * @param Logger $logger
     * @param ClientInterface $protheusClient
     * @param ClientFactory $httpClientFactory
     * @param ResponseFactory $httpResponseFactory
     * @param Utils $utils
     * @param GerenciadorEmail $gerenciadorEmail
     * @param DeliverydateFactory $deliverydateFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Logger $logger,
        ProtheusClientInterface $protheusClient,
        ClientFactory $httpClientFactory,
        ResponseFactory $httpResponseFactory,
        Utils $utils,
        GerenciadorEmail $gerenciadorEmail,
        DeliverydateFactory $deliverydateFactory,
        OrderRepositoryInterface $orderRepository,
        ScopeConfigInterface $scopeConfig,
        Customer $customer,
        CustomerFactory $customerFactory
    ) {
        $this->logger = $logger;
        $this->protheusClient = $protheusClient;
        $this->httpClientFactory = $httpClientFactory;
        $this->httpResponseFactory = $httpResponseFactory;
        $this->utils = $utils;
        $this->gerenciadorEmail = $gerenciadorEmail;
        $this->deliverydateFactory = $deliverydateFactory;
        $this->orderRepository = $orderRepository;
        $this->scopeConfig = $scopeConfig;
        $this->customer = $customer;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Realiza a integração de um pedido, determinando o tipo de envio que será feito
     *
     * @param Order $order
     */
    public function integrar(Order $order)
    {
        $this->correlation_id = uniqid("");
        $estaIntegrado = (bool) $order->getData('protheus_pedido_integrado__cy');
        $numeroTentativas = (int) $order->getData('protheus_pedido_numero_tentativas__cy');
        $entity_id = $order->getEntityId();
        $increment_id = $order->getIncrementId();
        $codigo_cd = $this->utils->obtemCodigoCD($order->getData('cd_responsavel'));
        $customer_id = $order->getCustomerId();
        $max_tentativas = (int) $this->scopeConfig->getValue(self::CONFIG_MAX_TENTATIVAS);

        try {
            $endpoint = $this->protheusClient->getEndpoint('criar_pedido');
            $metodo = Request::HTTP_METHOD_POST;
            $request_options = [
                'auth' => [
                    $this->protheusClient->getUser(),
                    $this->protheusClient->getPassword()
                ],
                'headers' => [
                    'tenantid' => '01,' . $codigo_cd
                ]
            ];
            $request_body = "";

            if ($estaIntegrado) {
                $status = $this->protheusClient->getOrderStatusCorrelation($order->getStatus());
                $request_options['query']['entity_id'] = $entity_id;
                $request_options['query']['increment_id'] = $increment_id;

                if ($status === null) {
                    $orderStatus = $order->getStatus();
                    throw new LocalizedException(
                        __(
                            "Não encontramos uma correlação para o status '{$orderStatus}'. "
                            . "Possivelmente se trata de uma informação que não precisa ser enviada ao Protheus."
                        )
                    );
                }

                if ($status === "Excluido") {
                    $endpoint = $this->protheusClient->getEndpoint('excluir_pedido');
                    $metodo = Request::HTTP_METHOD_DELETE;
                } else {
                    $endpoint = $this->protheusClient->getEndpoint('status_pedido');
                    $metodo = Request::HTTP_METHOD_PUT;
                    $request_options['query']['status'] = $status;
                }
            } else {
                $request_body = $this->buildRequestBody($order);
                $request_options['body'] = json_encode($request_body);
            }

            $requestUrl = $this->protheusClient->getBaseUrl() . $endpoint;
            $response = $this->makeRequest($metodo, $endpoint, $request_options);
            $status_code = $response->getStatusCode();
            $reason_phrase = $response->getReasonPhrase();

            $order->setData('protheus_pedido_guid_ultima_tentativa__cy', $this->correlation_id);
            $order->setData('protheus_pedido_url_ultima_tentativa__cy', $requestUrl);
            $order->setData('protheus_pedido_payload_ultima_tentativa__cy', json_encode($request_body));
            $order->setData('protheus_pedido_horario_ultima_tentativa__cy', time());
            $order->setData('protheus_pedido_codigo_ultima_tentativa__cy', $status_code);
            $order->setData('protheus_pedido_mensagem_ultima_tentativa__cy', $reason_phrase);
            $order->setData('protheus_pedido_salvo_pela_integracao__cy', true);

            /**
             * Se o STATUS da Integração for:
             * 201 ou 200 - Deu tudo certo
             * 400 - A comunicação foi bem sucedida, mas houve um erro ao tentar manipular os dados (possivelmente)
             * o pedido já existe no Protheus)
             * 500 - Houve um erro no servidor, mas possivelmente por causa de dados errados no envio
             */
            if ($status_code === 200 || $status_code === 201) {
                $order->setData('protheus_pedido_integrado__cy', true);
                $order->setData('protheus_pedido_deve_integrar__cy', false);
            } elseif ($status_code === 400) {
                $this->gerenciadorEmail->enviar(
                    $increment_id,
                    $response,
                    'pedido',
                    $numeroTentativas + 1,
                    $requestUrl,
                    json_encode($request_body)
                );

                if($numeroTentativas == $max_tentativas){
                    $order->setData('protheus_pedido_integrado__cy', false);
                    $order->setData('protheus_pedido_deve_integrar__cy', true);

                    if($customer_id !== null) {
                        $customer = $this->customer->load($customer_id);
                        $customerData = $customer->getDataModel();
                        $customerData->setCustomAttribute('protheus_cliente_integrado__cy', false);
                        $customerData->setCustomAttribute('protheus_cliente_deve_integrar__cy', true);
                        $customer->updateData($customerData);
                        $customer->save();
                    }

                }

            } elseif (in_array($status_code, [401, 403, 404, 500, 502, 503])) {
                $this->gerenciadorEmail->enviar(
                    $increment_id,
                    $response,
                    'conexao'
                );
            }

            $this->logger->info($this->correlation_id . " - Response: {$status_code} - {$reason_phrase}");
        } catch (\Exception $e) {
            $order->setData('protheus_pedido_guid_ultima_tentativa__cy', null);
            $order->setData('protheus_pedido_url_ultima_tentativa__cy', null);
            $order->setData('protheus_pedido_payload_ultima_tentativa__cy', null);
            $order->setData('protheus_pedido_horario_ultima_tentativa__cy', null);
            $order->setData('protheus_pedido_codigo_ultima_tentativa__cy', null);
            $order->setData('protheus_pedido_mensagem_ultima_tentativa__cy', $e->getMessage());
            $order->setData('protheus_pedido_salvo_pela_integracao__cy', true);
        }

        $order->setData('protheus_pedido_numero_tentativas__cy', $numeroTentativas + 1);
        $this->orderRepository->save($order);
    }

    /**
     * Constrói o 'body' da requisição com base nos dados da 'order'
     *
     * @param Order $order
     * @return array
     */
    private function buildRequestBody(Order $order): array
    {
        $loja = "0001";
        $increment_id = (string) $order->getIncrementId();
        $entity_id = (string) $order->getEntityId();
        $status = $this->protheusClient->getOrderStatusCorrelation($order->getStatus());
        $cpf = preg_replace("/[^0-9]/", "", $order->getCustomerTaxvat());
        $dados_entrega = $this->obtemDadosDeEntrega($order);
        $entrega_portaria = $order->getData('entrega_portaria') ? " - Entrega na portaria autorizada." : "";
        $porcentagem_desconto = $this->obtemPorcentagemDesconto($order);
        $condicao_pagamento = $this->obtemCondicaoPagamento($order);
        $orderItems = $order->getItems();

        $body = [
            "C5_CLIENTE" => substr($cpf, 0, 8),
            "C5_LOJACLI" => $loja,
            "C5_STATMAG" => $status,
            "C5_ENTITY" => $entity_id,
            "C5_INCREME" => $increment_id,
            "C5_FRETE" => floatval($order->getShippingAmount()),
            "C5_OBS" => $dados_entrega['comentario'] . $entrega_portaria,
            "C5_PDESCAB" => $porcentagem_desconto,
            "C5_CONDPAG" => $condicao_pagamento,
            "Items" => []
        ];

        foreach ($orderItems as $item) {
            if ($item->getProductType() === "bundle") {
                continue;
            }

            $parent_item = $item->getParentItem();
            $desconto = 0;
            $preco_venda = 0;

            if ($parent_item) {
                $porcentagem_desconto_bundle = (float) $parent_item->getData('bundle_special_price_percent');
                if ($porcentagem_desconto_bundle > 0) {
                    $desconto = 100 - $porcentagem_desconto_bundle;
                }
                $preco_venda = (float) $item->getData('bundle_child_original_price');
            } else {
                //Método getRowTotal aplicado para produto brinde
                if($item->getRowTotal() > 0){
                    $desconto = (float) 100 * (1 - $item->getPrice() / $item->getOriginalPrice());
                }else{
                    $desconto = (float) 99.9 * ($item->getOriginalPrice() / $item->getDiscountAmount());
                }

                $preco_venda = (float) $item->getOriginalPrice();
            }

            $valor = (float) $item->getQtyOrdered() * $preco_venda;

            $body["Items"][] = [
                "C6_PRODUTO" => (string) $item->getSku(),
                "C6_QTDVEN" => (int) $item->getQtyOrdered(),
                "C6_PRCVEN" => $preco_venda,
                "C6_VALOR" => $valor,
                "C6_ENTREG" => $dados_entrega['data'],
                "C6_HRENTRE" => $dados_entrega['horario'],
                "C6_DESCONT" => $desconto
            ];
        }

        return $body;
    }

    /**
     * Realiza a requisição ao Protheus
     *
     * @param string $metodo
     * @param string $endpoint
     * @param array $request_options
     * @return Response
     */
    private function makeRequest(string $metodo, string $endpoint, array $request_options): Response
    {
        $baseUrl = $this->protheusClient->getBaseUrl();
        /** @var Client $client */
        $httpClient = $this->httpClientFactory->create(['config' => [
            'base_uri' => $baseUrl
        ]]);

        $this->logger->info($this->correlation_id . " Request: [{$metodo}] " . $baseUrl . $endpoint);
        $this->logger->info($this->correlation_id . " Payload: " . json_encode($request_options));

        try {
            $response = $httpClient->request($metodo, $endpoint, $request_options);
        } catch (GuzzleException $exception) {
            if ($exception->getCode() < 100 || $exception->getCode() >= 600) {
                /** @var Response $response */
                $response = $this->httpResponseFactory->create([
                    'status' => 503, // 503 - Service Unavailable
                    'reason' => $exception->getResponse()->getBody()
                ]);
            } else {
                /** @var Response $response */
                $response = $this->httpResponseFactory->create([
                    'status' => $exception->getCode(),
                    'reason' => $exception->getResponse()->getBody()
                ]);
            }
        }
        return $response;
    }

    /**
     * Obtém dados da entrega a partir do pedido
     *
     * @param Order $order
     * @return array
     */
    private function obtemDadosDeEntrega(Order $order): array
    {
        $entity_id = $order->getEntityId();
        $delivery_date = $this->deliverydateFactory->create()->load($entity_id, "order_id");
        $data_original = $delivery_date->getData('date');
        $data_entrega = substr($data_original, 8, 2) . '/'
            . substr($data_original, 5, 2) . '/'
            . substr($data_original, 0, 4);
        $horario = $delivery_date->getData('time');
        $comentario = $delivery_date->getComment();

        if ($data_original === null && $order->getData(('amasty_tinterval_id')) != null) {
            $this->logger->info("Helper\GerenciadorPedido: O pedido " . $order->getIncrementId()
                . " estava sem agendamento. Estamos atualizando.");
            
            $data_original = $order->getData('amasty_date');
            $data_entrega = substr($data_original, 8, 2) . '/'
                . substr($data_original, 5, 2) . '/'
                . substr($data_original, 0, 4);
            $horario = $order->getData('amasty_time');
            $comentario = $order->getData('amasty_comment');

            $delivery_date->setData('order_id', $order->getEntityId());
            $delivery_date->setData('increment_id', $order->getIncrementId());
            $delivery_date->setData('date', $data_original);
            $delivery_date->setData('time', $horario);
            $delivery_date->setData('tinterval_id', $order->getData(('amasty_tinterval_id')));

            if ($comentario === null) {
                $comentario = "";
            }

            $delivery_date->setData('comment', $comentario);
            $delivery_date->save();
        }

        $dados_entrega = [
            'data' => $data_entrega,
            'horario' => $horario,
            'comentario' => $comentario
        ];

        return $dados_entrega;
    }

    /**
     * Obtém porcentagem de desconto do pedido
     *
     * @param Order $order
     * @return float
     */
    private function obtemPorcentagemDesconto(Order $order): float
    {
        $orderItems = $order->getItems();
        foreach ($orderItems as $item) {
            //Método getRowTotal aplicado para produto brinde
            if($item->getRowTotal() > 0){
                $desconto_pedido = $order->getData('base_discount_amount');
            }else{
                $desconto_pedido = $order->getData('base_discount_amount') - $item->getDiscountAmount();
            }
        }

        $total_pedido = $order->getData('base_subtotal');
        $porcentagem_desconto = 0;

        if ($total_pedido !== 0 && $desconto_pedido !== null) {
            $porcentagem_desconto = abs(100 * $desconto_pedido / $total_pedido);
        }

        return $porcentagem_desconto;
    }

    /**
     * Obtém condição de pagamento a partir do método de pagamento do pedido
     *
     * @param Order $order
     * @return string
     */
    private function obtemCondicaoPagamento(Order $order): string
    {
        $payment = $order->getPayment();
        $payment_method = $payment->getMethod();
        $additional_information = $payment->getAdditionalInformation();
        $installments = "";

        if ($payment_method === "pagarme_creditcard" || $payment_method === "mundipagg_creditcard") {
            if (is_array($additional_information) && array_key_exists('cc_installments', $additional_information)) {
                $installments = "_" . $additional_information['cc_installments'] . "x";
            }
        }

        $condicao_pagamento = (string) $this->scopeConfig->getValue(
            "urban_integrador/condicao_pagamento/" . $payment_method . $installments
        );

        return $condicao_pagamento;
    }
}
