<?php

namespace Urban\Integrador\Helper;

use Magento\Framework\App\Area;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Urban\Integrador\Logger\Logger;
use Magento\Framework\App\Config\ScopeConfigInterface;
use GuzzleHttp\Psr7\Response;

class GerenciadorEmail
{
    public const EMAIL_NOTIFICACAO_HABILITADA = 'urban_integrador/notificacao/habilitar_notificacao';
    public const EMAIL_REMETENTE = 'urban_integrador/notificacao/remetente';
    public const EMAIL_TEMPLATE = 'urban_integrador/notificacao/template';
    public const EMAIL_DESTINATARIO_CLIENTE = 'urban_integrador/notificacao/destinatario_cliente';
    public const EMAIL_DESTINATARIO_PEDIDO = 'urban_integrador/notificacao/destinatario_pedido';
    public const EMAIL_DESTINATARIO_CONEXAO = 'urban_integrador/notificacao/destinatario_conexao';
    public const EMAIL_CONDICAO_ENVIO = 'urban_integrador/notificacao/condicao_envio';
    public const CONFIG_MAX_TENTATIVAS_CLIENTE = "urban_integrador/geral/numero_tentativas_cliente";
    public const CONFIG_MAX_TENTATIVAS_PEDIDO = "urban_integrador/geral/numero_tentativas_cliente";

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param StateInterface
     */
    private $inlineTranslation;

    /**
     * @param TransportBuilder
     */
    private $transportBuilder;

    /**
     * @param StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param Logger $logger
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Logger $logger,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Enviar email de acordo com os parâmetros passados
     *
     * @param Response $response
     * @param string $tipo_erro
     * @param int $num_tentativas_atual
     * @return $this | false
     *
     * @throws LocalizedException
     * @throws MailException
     */
    public function enviar(
        string $identificador,
        Response $response,
        string $tipo_erro,
        int $num_tentativas_atual = 1,
        string $request_url = "",
        string $request_body = ""
    ) {
        $notificacao_habilitada = (bool) $this->scopeConfig->getValue(self::EMAIL_NOTIFICACAO_HABILITADA);
        if (!$notificacao_habilitada) {
            $this->logger->info("Helper\GerenciadorEmail: a notificação por email está desabilitada.");
            return false;
        }

        $this->inlineTranslation->suspend();

        try {
            switch ($tipo_erro) {
                case 'cliente':
                    $label_identificador = "Cliente";
                    $condicao_envio = (string) $this->scopeConfig->getValue(self::EMAIL_CONDICAO_ENVIO);
                    $max_tentativas = (int) $this->scopeConfig->getValue(self::CONFIG_MAX_TENTATIVAS_CLIENTE);
                    $destinatarios = $this->scopeConfig->getValue(self::EMAIL_DESTINATARIO_CLIENTE);
                    break;
                case 'pedido':
                    $label_identificador = "Pedido";
                    $condicao_envio = (string) $this->scopeConfig->getValue(self::EMAIL_CONDICAO_ENVIO);
                    $max_tentativas = (int) $this->scopeConfig->getValue(self::CONFIG_MAX_TENTATIVAS_PEDIDO);
                    $destinatarios = $this->scopeConfig->getValue(self::EMAIL_DESTINATARIO_PEDIDO);
                    break;
                default:
                    $label_identificador = "Conexão";
                    $condicao_envio = null;
                    $destinatarios = $this->scopeConfig->getValue(self::EMAIL_DESTINATARIO_CONEXAO);
                    break;
            }

            if (empty($destinatarios)) {
                $this->logger->info(
                    "Helper\GerenciadorEmail: não há destinatários configurados para o tipo de erro '{$tipo_erro}'."
                );
                return false;
            }

            if ($condicao_envio === "2" && $num_tentativas_atual < $max_tentativas) {
                $this->logger->info("Helper\GerenciadorEmail: ainda não esgotou o número máximo de tentativas.");
                return false;
            }

            $destinatariosArray = explode(",", $destinatarios);
            $template = $this->scopeConfig->getValue(self::EMAIL_TEMPLATE);
            $remetente = $this->scopeConfig->getValue(self::EMAIL_REMETENTE);

            $emailVariables = [
                'identificador' => $identificador,
                'label_identificador' => $label_identificador,
                'request_url' => $request_url,
                'request_body' => $request_body,
                'codigo_erro' => $response->getStatusCode(),
                'mensagem_erro' => $response->getReasonPhrase(),
            ];

            $storeId = $this->storeManager->getStore(true)->getStoreId();
            $transport = $this->transportBuilder->setTemplateIdentifier(
                $template
            )->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $storeId
                ]
            )->setTemplateVars(
                $emailVariables
            )->setFromByScope(
                $remetente
            )->addTo(
                $destinatariosArray
            )->getTransport();

            $transport->sendMessage();
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            $this->logger->info("Helper\GerenciadorEmail: Erro ao tentar enviar email. Exception: {$message}");
        }

        $this->inlineTranslation->resume();
        return $this;
    }
}
