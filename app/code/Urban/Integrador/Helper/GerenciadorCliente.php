<?php

namespace Urban\Integrador\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\Data\CustomerInterface as CustomerDataInterface;
use Urban\Integrador\Logger\Logger;
use Urban\Integrador\Api\ProtheusClientInterface;
use Urban\Integrador\Helper\Utils;
use Urban\Integrador\Helper\GerenciadorEmail;

class GerenciadorCliente
{
    /**
     * @param string
     */
    private $correlation_id;

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param ClientInterface
     */
    private $protheusClient;

    /**
     * @param ClientFactory
     */
    private $httpClientFactory;

    /**
     * @param ResponseFactory
     */
    private $httpResponseFactory;

    /**
     * @param Utils
     */
    private $utils;

    /**
     * @param AddressFactory
     */
    private $addressFactory;

    /**
     * @param GerenciadorEmail
     */
    private $gerenciadorEmail;

    /**
     * @param Logger $logger
     * @param ClientInterface $protheusClient
     * @param ClientFactory $httpClientFactory
     * @param ResponseFactory $httpResponseFactory
     * @param Utils $utils
     * @param AddressFactory $addressFactory
     * @param GerenciadorEmail $gerenciadorEmail
     */
    public function __construct(
        Logger $logger,
        ProtheusClientInterface $protheusClient,
        ClientFactory $httpClientFactory,
        ResponseFactory $httpResponseFactory,
        Utils $utils,
        AddressFactory $addressFactory,
        GerenciadorEmail $gerenciadorEmail
    ) {
        $this->logger = $logger;
        $this->protheusClient = $protheusClient;
        $this->httpClientFactory = $httpClientFactory;
        $this->httpResponseFactory = $httpResponseFactory;
        $this->utils = $utils;
        $this->addressFactory = $addressFactory;
        $this->gerenciadorEmail = $gerenciadorEmail;
    }

    /**
     * Realiza a integração de um cliente, determinando o tipo de envio que será feito
     *
     * @param Customer $customer
     */
    public function integrar(Customer $customer)
    {
        $this->correlation_id = uniqid("");
        $customerData = $customer->getDataModel();
        $customerEmail = (string) $customer->getEmail();
        $estaIntegrado = (bool) $customer->getData('protheus_cliente_integrado__cy');
        $numeroTentativas = (int) $customer->getData('protheus_cliente_numero_tentativas__cy');
        $endpoint = $this->protheusClient->getEndpoint('criar_cliente');
        $metodo = Request::HTTP_METHOD_POST;

        if ($estaIntegrado) {
            $endpoint = $this->protheusClient->getEndpoint('editar_cliente');
            $metodo = Request::HTTP_METHOD_PUT;
        }

        try {
            $requestUrl = $this->protheusClient->getBaseUrl() . $endpoint;
            $requestBody = $this->buildRequestBody($customer);
            $jsonBody = json_encode($requestBody);
            $response = $this->makeRequest($requestBody, $endpoint, $metodo);
            $status_code = $response->getStatusCode();
            $reason_phrase = $response->getReasonPhrase();
            
            $customerData->setCustomAttribute('protheus_cliente_guid_ultima_tentativa__cy', $this->correlation_id);
            $customerData->setCustomAttribute('protheus_cliente_url_ultima_tentativa__cy', $requestUrl);
            $customerData->setCustomAttribute('protheus_cliente_payload_ultima_tentativa__cy', $jsonBody);
            $customerData->setCustomAttribute('protheus_cliente_horario_ultima_tentativa__cy', time());
            $customerData->setCustomAttribute('protheus_cliente_codigo_ultima_tentativa__cy', $status_code);
            $customerData->setCustomAttribute('protheus_cliente_mensagem_ultima_tentativa__cy', $reason_phrase);
    
            /**
             * Se o STATUS da Integração for:
             * 201 ou 200 - Deu tudo certo
             * 400 - A comunicação foi bem sucedida, mas houve um erro ao tentar manipular os dados (possivelmente)
             * o cliente já existe no Protheus)
             * 500 - Houve um erro no servidor, mas possivelmente por causa de dados errados no envio
             */
            if ($status_code === 200 || $status_code === 201) {
                $customerData->setCustomAttribute('protheus_cliente_integrado__cy', true);
                $customerData->setCustomAttribute('protheus_cliente_deve_integrar__cy', false);
            } elseif ($status_code === 400) {
                $this->gerenciadorEmail->enviar(
                    $customerEmail,
                    $response,
                    'cliente',
                    $numeroTentativas + 1,
                    $requestUrl,
                    $jsonBody
                );
            } elseif (in_array($status_code, [401, 403, 404, 500, 502, 503])) {
                $this->gerenciadorEmail->enviar(
                    $customerEmail,
                    $response,
                    'conexao'
                );
            }

            $this->logger->info($this->correlation_id . " Response: {$status_code} - {$reason_phrase}");
        } catch (\Exception $e) {
            $customerData->setCustomAttribute('protheus_cliente_guid_ultima_tentativa__cy', null);
            $customerData->setCustomAttribute('protheus_cliente_url_ultima_tentativa__cy', null);
            $customerData->setCustomAttribute('protheus_cliente_payload_ultima_tentativa__cy', null);
            $customerData->setCustomAttribute('protheus_cliente_horario_ultima_tentativa__cy', null);
            $customerData->setCustomAttribute('protheus_cliente_codigo_ultima_tentativa__cy', null);
            $customerData->setCustomAttribute('protheus_cliente_mensagem_ultima_tentativa__cy', $e->getMessage());
        }

        $customerData->setCustomAttribute('protheus_cliente_numero_tentativas__cy', $numeroTentativas + 1);
        $customer->updateData($customerData);
        $customer->save();
    }

    /**
     * Constrói o 'body' da requisição com base nos dados do 'customer'
     *
     * @param Customer $customer
     * @return array
     */
    private function buildRequestBody(Customer $customer): array
    {
        $customerData = $customer->getDataModel();
        $cpf = preg_replace("/[^0-9]/", "", $customerData->getTaxvat());
        $nome = $this->utils->retiraAcentos($customerData->getFirstName());
        $nome_completo = $this->utils->retiraAcentos($nome . " " . $customerData->getLastName());
        $email = $customerData->getEmail();
        $opcao_alimentar = $this->getOpcaoAlimentar($customerData);
        $numero_telefone = $this->getPhoneNumber($customerData);
        $data_nascimento = $this->getDob($customerData);
        $endereco = $this->getAddress($customerData);

        $body = [
            "registerSituation" => "2",
            "code" => substr($cpf, 0, 8),
            "storeId" => "0001",
            "strategicCustomerType" => "F",
            "entityType" => "F",
            "shortName" => $nome,
            "name" => $nome_completo,
            "governmentalInformation" => [
                [
                    "scope" => "Federal",
                    "name" => "CPF",
                    "id" => $cpf
                ]
            ],
            "listOfCommunicationInformation" => [
                [
                    "diallingCode" => $numero_telefone['ddd'],
                    "phoneNumber" => $numero_telefone['num'],
                    "type" => "",
                    "email" => $email,
                    "dtNasc" => $data_nascimento,
                    "opcAlimentar" => $opcao_alimentar
                ]
            ],
            "address" => $endereco
        ];

        return $body;
    }

    /**
     * Realiza a requisição ao Protheus
     *
     * @param array $requestBody
     * @param string $endpoint
     * @param string $metodo
     * @return Response
     */
    private function makeRequest(array $requestBody, string $endpoint, string $metodo): Response
    {
        $baseUrl = $this->protheusClient->getBaseUrl();
        /** @var Client $client */
        $httpClient = $this->httpClientFactory->create(['config' => [
            'base_uri' => $baseUrl
        ]]);

        $jsonBody = json_encode($requestBody);

        $this->logger->info($this->correlation_id . " Request: [{$metodo}] " . $baseUrl . $endpoint);
        $this->logger->info($this->correlation_id . " Payload: " . $jsonBody);

        try {
            $response = $httpClient->request($metodo, $endpoint, [
                'auth' => [
                    $this->protheusClient->getUser(),
                    $this->protheusClient->getPassword()
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                    'tenantid' => '01,0104'
                ],
                'body' => json_encode($requestBody)
            ]);
        } catch (GuzzleException $exception) {
            if ($exception->getCode() < 100 || $exception->getCode() >= 600) {
                /** @var Response $response */
                $response = $this->httpResponseFactory->create([
                    'status' => 503, // 503 - Service Unavailable
                    'reason' => $exception->getResponse()->getBody()
                ]);
            } else {
                /** @var Response $response */
                $response = $this->httpResponseFactory->create([
                    'status' => $exception->getCode(),
                    'reason' => $exception->getResponse()->getBody()
                ]);
            }
        }
        return $response;
    }

    /**
     * Obtém o telefone do cliente
     *
     * @param CustomerDataInterface $customerData
     * @return array
     */
    private function getPhoneNumber(CustomerDataInterface $customerData): array
    {
        $phoneAtribute = $customerData->getCustomAttribute('telefone__cy');
        $number = null;
        $phoneNumber = [
            'ddd' => null,
            'num' => null
        ];

        if ($phoneAtribute !== null) {
            $number = preg_replace("/[^0-9]/", "", (string) $phoneAtribute->getValue());
        } else {
            $addressId = $customerData->getDefaultBilling();

            if ($addressId !== null) {
                $address = $this->addressFactory->create()->load($addressId);
                $telephone = $address->getTelephone();
                $number = preg_replace("/[^0-9]/", "", (string) $telephone);
            }
        }

        if ($number !== null) {
            if ($number[0] == "0") {
                $phoneNumber['ddd'] = substr($number, 1, 3);
                $phoneNumber['num'] = substr($number, 3);
            } else {
                $phoneNumber['ddd'] = substr($number, 0, 2);
                $phoneNumber['num'] = substr($number, 2);
            }
        }

        return $phoneNumber;
    }

    /**
     * Obtém a data de nascimento do cliente
     *
     * @param CustomerDataInterface $customerData
     * @return string
     */
    private function getDob(CustomerDataInterface $customerData): string
    {
        $customerDob = $customerData->getDob();
        $dob = substr($customerDob, 8, 2) . "/" . substr($customerDob, 5, 2) . "/" . substr($customerDob, 0, 4);
        return $dob;
    }

    /**
     * Obtém a opção alimentar do cliente
     *
     * @param CustomerDataInterface $customerData
     * @return string | null
     */
    private function getOpcaoAlimentar(CustomerDataInterface $customerData): ?string
    {
        $opcaoAlimentarAttribute = $customerData->getCustomAttribute('opcao_alimentar__cy');
        if ($opcaoAlimentarAttribute !== null) {
            return $this->utils->obtemOpcaoAlimentar($opcaoAlimentarAttribute->getValue());
        }
        return null;
    }

    /**
     * Obtém o endereço do cliente
     *
     * @param CustomerDataInterface $customerData
     * @return array
     */
    private function getAddress(CustomerDataInterface $customerData): array
    {
        $addressId = $customerData->getDefaultBilling();

        if ($addressId !== null) {
            $address = $this->addressFactory->create()->load($addressId);
            $street = $address->getStreet();

            $cep = preg_replace("/[^0-9]/", "", $address->getPostcode());
            $rua = $street[0];
            $numero = $street[1];
            $complemento = isset($street[3]) ? $street[2] : "-";
            $bairro = isset($street[3]) ? $street[3] : $street[2];

            $cidade = $address->getCity();
            $codigo_cidade = $this->utils->obtemCodigoCidade($cep);

            if ($codigo_cidade === null) {
                $codigo_cidade = $this->utils->obtemCodigoCidadeEstatico($cidade);
            }

            $estado = $address->getRegion();
            $codigo_estado = $address->getRegionCode();

            $endereco = [
                "mainAddress" => true,
                "zipCode" => $cep,
                "address" => $rua . ' ' . $numero,
                "complement" => $complemento,
                "district" => $bairro,
                "city" => [
                    "cityCode" => (string) substr($codigo_cidade, 2),
                    "cityInternalId" => (string) substr($codigo_cidade, 2),
                    "cityDescription" => $cidade,
                ],
                "state" => [
                    "stateId" => $codigo_estado,
                    "stateDescription" => $estado,
                ]
            ];
        } else {
            $endereco = [
                "mainAddress" => false,
                "zipCode" => "",
                "address" => "nao encontrado",
                "complement" => "",
                "district" => "nao encontrado",
                "city" => [
                    "cityCode" => "",
                    "cityInternalId" => "",
                    "cityDescription" => "",
                ],
                "state" => [
                    "stateId" => "EX",
                    "stateDescription" => "nao encontrado",
                ]
            ];
        }
        return $endereco;
    }
}
