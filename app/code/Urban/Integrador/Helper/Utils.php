<?php

namespace Urban\Integrador\Helper;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\ResourceConnection;
use Urban\Integrador\Logger\Logger;
use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class Utils
{
    public const CODIGOS_CDS = [
        "porto_alegre" => "0104",
        "sao_paulo" => "0106"
    ];

    /**
     * @param Curl
     */
    private $curl;

    /**
     * @param ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * @param Curl $curl
     * @param ResourceConnection $resourceConnection
     * @param Logger $logger
     */
    public function __construct(
        Curl $curl,
        ResourceConnection $resourceConnection,
        Logger $logger,
        ClientFactory $httpClientFactory,
        ResponseFactory $httpResponseFactory,
        Filesystem $filesystem
    ) {
        $this->curl = $curl;
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
        $this->httpClientFactory = $httpClientFactory;
        $this->httpResponseFactory = $httpResponseFactory;
        $this->filesystem = $filesystem;
    }

    /**
     * Obtém label da opção alimentar a partir de seu ID no banco
     *
     * @param int $option_id
     * @return int
     */
    public function obtemOpcaoAlimentar($option_id): int
    {
        /**
         * TODO: refatorar esta função para não conectar ao banco de dados diretamente
         */
        $retorno = 0;
        $tabela = $this->resourceConnection->getTableName('eav_attribute_option_value');
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from(
                ['c' => $tabela],
                ['*']
            )
            ->where(
                "c.option_id = :option_id"
            );
        $bind = ['option_id' => $option_id];
        $records = $connection->fetchAll($select, $bind);
        $label = $records[0]['value'];
        if ($label == 'Não consumo nenhum produto de origem animal') {
            $retorno = 1;
        } elseif ($label == 'Não consumo carne animal') {
            $retorno = 2;
        } elseif ('Como carne e produtos de origem animal, mas também procuro opções alternativas para o meu dia a dia') {
            $retorno = 3;
        }
        return ($retorno);
    }

    /**
     * Obtém código da cidade a partir da resposta do ViaCEP
     *
     * @param string $cep
     * @return string|null
     */
    public function obtemCodigoCidade(string $cep): ?string
    {
        try {
            $baseUrl = "https://viacep.com.br/";
            $endpoint = "ws/{$cep}/json/";

            /** @var Client $client */
            $httpClient = $this->httpClientFactory->create(['config' => [
                'base_uri' => $baseUrl
            ]]);

            $response = $httpClient->request('GET', $endpoint);
            $content = json_decode($response->getBody()->getContents());

            return isset($content->ibge) ? $content->ibge : "";
        } catch (\Exception $e) {
            $this->logger->warning(
                "Houve um erro ao tentar obter o código da Cidade pelo CEP {$cep}: "
                . $e->getMessage()
            );
            return null;
        }
    }

    /**
     * Obtém código da cidade a partir do nome da cidade utilizando um JSON estático.
     * Os dados do JSON foram montados a partir de uma planilha fornecida pelo IBGE
     * acessível em https://www.ibge.gov.br/explica/codigos-dos-municipios.php
     *
     * @param string $cidade 
     * @return string|null
     */
    public function obtemCodigoCidadeEstatico(string $cidade): ?string
    {
        try {
            $mediapath = $this->filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath();
            $file = $mediapath . 'code/Urban/Integrador/view/adminhtml/web/js/codigos_ibge.json';
            $content = json_decode(file_get_contents($file), true);
            $search_key = "municipio";
            $index = array_search($cidade, array_column($content['codigos_ibge'], $search_key));
            $ibge = $content['codigos_ibge'][$index]['ibge'];
            return $ibge;
        } catch (\Exception $e) {
            $this->logger->warning(
                "Houve um erro ao tentar obter o código da Cidade pela Cidade {$cidade}: "
                . $e->getMessage()
            );
            return null;
        }
    }

    /**
     * Obtém código do CD a partir da string do CD
     *
     * @param string $cd
     * @return string | null
     */
    public function obtemCodigoCD(string $cd): ?string
    {
        /**
         * TODO: refatorar este método. Ele foi criado dessa forma por uma questão de velocidade de programação,
         * mas não está seguindo boas práticas. O código original está (ou estava) presente em
         * Nectar\Cds\Helper\CidadesPorCd
         */
        if (array_key_exists($cd, self::CODIGOS_CDS)) {
            return self::CODIGOS_CDS[$cd];
        }
        return null;
    }

    /**
     * Obtém uma nova string sem acentos
     *
     * @param string $frase
     * @return string
     */
    public function retiraAcentos(string $frase): string
    {
        $comAcentos = [
            'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë',
            'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù',
            'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È',
            'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô',
            'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú'
        ];
        $semAcentos = [
            'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e',
            'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u',
            'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E',
            'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O',
            'O', 'O', 'O', 'U', 'U', 'U'
        ];
        $nova_frase = str_replace($comAcentos, $semAcentos, $frase);
        return $nova_frase;
    }
}
