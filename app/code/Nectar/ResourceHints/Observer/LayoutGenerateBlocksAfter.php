<?php
namespace Nectar\ResourceHints\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Page\Config as PageConfig;

class LayoutGenerateBlocksAfter implements ObserverInterface
{
    /** @var PageConfig $pageConfig */
    private $pageConfig;

    /**
     * Builder constructor.
     *
     * @param PageConfig  $pageConfig
     */
    public function __construct(
        PageConfig $pageConfig
    ) {
        $this->pageConfig  = $pageConfig;
    }

    /**
     * @param Observer $observer
     *
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $resourceHints = [
            'googletagmanager' => [
                'resource' => 'https://www.googletagmanager.com',
                'type' => 'preconnect',
            ],
            'facebook' => [
                'resource' => 'https://www.facebook.com',
                'type' => 'preconnect',
            ],
            'facebooknet' => [
                'resource' => 'https://connect.facebook.net',
                'type' => 'preconnect',
            ]
        ];

        foreach ($resourceHints as $resource) {
            $this->pageConfig->addRemotePageAsset(
                $resource['resource'],
                'link_rel',
                [
                    'attributes' => ['rel' => $resource['type'] ]
                ]
            );
        }

        return $this;
    }
}