var config = {
    map: {
        '*': {
            'Pagarme_Pagarme/template/payment/default.html': 'Nectar_Pagarme/template/payment/default.html',
            'Pagarme_Pagarme/template/payment/creditcard-form.html': 'Nectar_Pagarme/template/payment/creditcard-form.html',
            'Pagarme_Pagarme/js/view/payment/boleto': 'Nectar_Pagarme/js/view/payment/boleto',
            'Pagarme_Pagarme/js/view/payment/creditcard': 'Nectar_Pagarme/js/view/payment/creditcard',
            'Pagarme_Pagarme/js/view/payment/pix': 'Nectar_Pagarme/js/view/payment/pix',
            'Pagarme_Pagarme/js/view/payment/voucher': 'Nectar_Pagarme/js/view/payment/voucher'
        }
    }
};
