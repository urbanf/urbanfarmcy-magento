define([
    'jquery',
    'Nectar_AutofillAddress/js/consulta-cep'
], function ($, ConsultaCep) {
    'use strict';

    return function(obj){
        $(obj.postcodeId).on('keyup', function(ev){
            let consulta = ConsultaCep();

            let value = $(this).val();
            let n = value.replace(/\_/g,'');

            if(n.length == 9) {
                $.ajax({
                    url: consulta.config.url,
                    type: 'get',
                    data: { postcode: value },
                    dataType: 'json',
                    beforeSend: function () {
                        $('body').trigger('processStart');
                        $('[name="street[0]"]').val('').prop('readOnly', false);
                        $('[name="street[1]"]').val('');
                        $('[name="street[2]"]').val(' ');
                        $('[name="street[3]"]').val('');
                        $('[name="city"]').val('');
                        $('[name="region_id"]').val('');
                    },
                    success: function (data, status, xhr) {
                        let complemento = data.complemento ? data.complemento : '-';
                        $('[name="street[0]"]').val(data.logradouro).prop('readOnly', true);
                        $('[name="street[3]"]').val(data.bairro);
                        $('[name="city"]').val(data.localidade);
                        $('[data-title="'+data.estado+'"]').attr('selected', 'selected');
                        $('[name="street[1]"]').focus();
                    },
                    complete: function (status) {
                        $('body').trigger('processStop');
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log('Error happens. Try again.');
                        console.log(errorThrown);
                    }
                });
            }
        });
    }
});
