define([], function () {
    'use strict';

    return function(){
        return {
            config: {
                url: '/autofill/ajax/autofill'
            }
        }
    }
});
