/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/element/abstract',
    'Nectar_AutofillAddress/js/consulta-cep',
    'Magento_Checkout/js/model/quote',
    'Nectar_Shipping/js/view/store-change-action',
    'Inputmask'
], function ($, _, Abstract, ConsultaCep, quote, storeChangeAction) {
    'use strict';

    return Abstract.extend({
        defaults: {
            imports: {
                countryOptions: '${ $.parentName }.country_id:indexedOptions',
                update: '${ $.parentName }.country_id:value',
                updateCep: '${ $.parentName }.postcode:value'
            }
        },

        /**
         * Initializes observable properties of instance
         *
         * @returns {Abstract} Chainable.
         */
        initObservable: function () {
            this._super();

            /**
             * equalityComparer function
             *
             * @returns boolean.
             */
            this.value.equalityComparer = function (oldValue, newValue) {
                return !oldValue && !newValue || oldValue === newValue;
            };

            return this;
        },

        /**
         * Method called every time country selector's value gets changed.
         * Updates all validations and requirements for certain country.
         * @param {String} value - Selected country ID.
         */
        update: function (value) {
            var isZipCodeOptional,
                option;

            if (!value) {
                return;
            }

            option = _.isObject(this.countryOptions) && this.countryOptions[value];

            if (!option) {
                return;
            }

            isZipCodeOptional = !!option['is_zipcode_optional'];

            if (isZipCodeOptional) {
                this.error(false);
            }

            this.validation['required-entry'] = !isZipCodeOptional;
            this.required(!isZipCodeOptional);
        },

        updateCep: function (value) {
            let consulta = ConsultaCep();

            Inputmask({
                'mask': '99999-999',
                'placeholder': '',
            }).mask('[name="postcode"]');

            let numericValue = value.replace(/\D/g, "");
            let i = 0;

            if(numericValue.length == 8 && i == 0) {
                i++;

                $.ajax({
                    url: consulta.config.url,
                    type: 'get',
                    data: {postcode: value},
                    dataType: 'json',
                    beforeSend: function () {
                        $('body').trigger('processStart');
                        $('[name="street[0]"]').val('').trigger('change').prop('readOnly', false);
                        $('[name="street[1]"]').val('').trigger('change');
                        $('[name="street[2]"]').val(' ').trigger('change');
                        $('[name="street[3]"]').val('').trigger('change').prop('readOnly', false);
                        $('[name="city"]').val('').trigger('change').prop('readOnly', false);
                        $('[name="region_id"]').val('').trigger('change').prop('readOnly', false);
                    },
                    success: function (data, status, xhr) {
                        $('[name="postcode"]').trigger('change');
                        $('[name="street[0]"]').val(data.logradouro).trigger('change').prop('readOnly', true);
                        $('[name="street[3]"]').val(data.bairro).trigger('change').prop('readOnly', true);
                        $('[name="city"]').val(data.localidade).trigger('change').prop('readOnly', true);
                        $('[data-title="' + data.estado + '"]').attr('selected', 'selected');
                        $('[name="region_id"]').trigger('change').prop('readOnly', true);
                        $('[name="street[1]"]').focus();
                        storeChangeAction(quote.shippingAddress());
                    },
                    complete: function (status) {
                        $('body').trigger('processStop');
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log('Error happens. Try again.');
                        console.log(errorThrown);
                    }
                });
            } else {
                return false;
            }

        },
    });
});
