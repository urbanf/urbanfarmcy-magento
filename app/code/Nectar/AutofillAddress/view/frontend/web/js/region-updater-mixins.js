define([
    'jquery'
], function($) {
    'use strict';

    return function(regionUpdater){
        $.widget('mage.regionUpdater', regionUpdater, {
            options: {
                regionTemplate:
                    '<option data-title="<%- data.title %>" value="<%- data.value %>" <% if (data.isSelected) { %>selected="selected"<% } %>>' +
                    '<%- data.title %>' +
                    '</option>',
                isRegionRequired: true,
                isZipRequired: true,
                isCountryRequired: true,
                currentRegion: null,
                isMultipleCountriesAllowed: true
            }
        });

        return $.mage.regionUpdater;
    }
});
