var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/region-updater': {
                'Nectar_AutofillAddress/js/region-updater-mixins': true
            }
        }
    },
    "map": {
        "*": {
            "Magento_Ui/js/form/element/post-code": "Nectar_AutofillAddress/js/form/element/post-code"
        }
    }
};
