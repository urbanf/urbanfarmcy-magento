<?php
namespace Nectar\AutofillAddress\Model\Checkout;

class LayoutProcessorPlugin
{
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $billingConfiguration = &$jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children'];
        $shippingConfiguration = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];

        if( isset($billingConfiguration) ){
            $billingConfiguration['street'] = [
                'component' => 'Magento_Ui/js/form/components/group',
                //'label' => __('Street Address'), removido
                'required' => false, // removido
                'dataScope' => 'billingAddress.street',
                'provider' => 'checkoutProvider',
                'sortOrder' => 70,
                'type' => 'group',
                'additionalClasses' => 'street',
                'children' => [
                    [
                        'label' => __('Logradouro'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'billingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '0',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 80],
                    ],
                    [
                        'label' => __('Número'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'billingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '1',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 10],
                    ],
                    [
                        'label' => __('Complemento'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'billingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '2',
                        'provider' => 'checkoutProvider',
                        'value' => ' ',
                        'validation' => ['required-entry' => false, "min_text_length" => 1, "max_text_length" => 30],
                    ],
                    [
                        'label' => __('Bairro'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'billingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '3',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 40],
                    ],
                ]
            ];
        }

        if( isset($shippingConfiguration) ){
            $shippingConfiguration['street'] = [
                'component' => 'Magento_Ui/js/form/components/group',
                //'label' => __('Street Address'), removido
                'required' => false, // removido
                'dataScope' => 'shippingAddress.street',
                'provider' => 'checkoutProvider',
                'sortOrder' => 70,
                'type' => 'group',
                'additionalClasses' => 'street',
                'children' => [
                    [
                        'label' => __('Logradouro'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '0',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 80],
                    ],
                    [
                        'label' => __('Número'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '1',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 10],
                    ],
                    [
                        'label' => __('Complemento'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '2',
                        'provider' => 'checkoutProvider',
                        'value' => '-',
                        'validation' => ['required-entry' => false, "min_text_length" => 1, "max_text_length" => 30],
                    ],
                    [
                        'label' => __('Bairro'),
                        'component' => 'Magento_Ui/js/form/element/abstract',
                        'config' => [
                            'customScope' => 'shippingAddress',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/input'
                        ],
                        'dataScope' => '3',
                        'provider' => 'checkoutProvider',
                        'validation' => ['required-entry' => true, "min_text_length" => 1, "max_text_length" => 40],
                    ],
                ]
            ];
        }
        return $jsLayout;
    }
}
