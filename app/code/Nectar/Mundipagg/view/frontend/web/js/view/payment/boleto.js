/*browser:true*/
/*global define*/
define(
    [
        'moment',
        'jquery',
        'ko',
        "MundiPagg_MundiPagg/js/view/payment/default",
        "MundiPagg_MundiPagg/js/core/models/BoletoModel",
        'mage/cookies'
    ],
    function (moment, $, ko, Component, $t) {

        return Component.extend({
            defaults: {
                template: "Nectar_Mundipagg/payment/default"
            },
            isMethodDisabled: ko.observable(false),

            initialize: function() {
                this._super();
                this.validateWorkingDays();

                this.isChecked.subscribe(function (value) {
                    this.validateWorkingDays(value);
                }, this);
            },

            validateWorkingDays: function() {
                if($.mage.cookies.get('urban_delivery_date')) {
                    let cookieValue = $.mage.cookies.get('urban_delivery_date');

                    let today = new Date();
                    let startDate = new Date();
                    let endDate = '';
                    let decrement = 0;

                    if(moment(cookieValue, 'D/M/YYYY', true).isValid()) {
                        let splitDate = cookieValue.split('/');
                        let newDate = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
                        endDate = new Date(newDate);
                    } else {
                        endDate = new Date(cookieValue.replace(/-/g,'/'));
                    }

                    endDate.setHours(23,59,59,999);

                    while(startDate < endDate) {
                        if( startDate.getDay() === 6 || startDate.getDay() === 0 ) {
                            decrement++;
                        }
                        startDate.setDate(startDate.getDate() + 1);
                    }

                    if(Math.abs(((endDate - today) / 86400000) - decrement) < 4) {
                        this.isMethodDisabled(true);
                    } else {
                        this.isMethodDisabled(false);
                    }
                }
            },
            
            getCode: function() {
                return "mundipagg_billet";
            },
            isActive: function() {
                return window.checkoutConfig.payment.mundipagg_billet.active;
            },
            getTitle: function() {
                return window.checkoutConfig.payment.mundipagg_billet.title;
            },
            getBase: function() {
                return "MundiPagg_MundiPagg/payment/boleto";
            },
            getForm: function() {
                return "MundiPagg_MundiPagg/payment/boleto-form";
            },
            getMultibuyerForm: function () {
                return "MundiPagg_MundiPagg/payment/multibuyer-form";
            },
            getText: function () {
                return window.checkoutConfig.payment.mundipagg_billet.text;
            },
            getModel: function() {
                return 'boleto';
            },

            getData: function () {
                var paymentMethod = window.MundiPaggCore.paymentMethod[this.getModel()];
                if (paymentMethod == undefined) {
                    return paymentMethod;
                }
                var paymentModel = paymentMethod.model;
                return paymentModel.getData();
            },
        });
    }
);