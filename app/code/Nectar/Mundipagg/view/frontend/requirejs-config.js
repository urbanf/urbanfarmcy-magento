var config = {
    map: {
        '*': {
            'MundiPagg_MundiPagg/template/payment/default.html': 'Nectar_Mundipagg/template/payment/default.html',
            'MundiPagg_MundiPagg/template/payment/creditcard-form.html': 'Nectar_Mundipagg/template/payment/creditcard-form.html',
            'MundiPagg_MundiPagg/js/view/payment/boleto': 'Nectar_Mundipagg/js/view/payment/boleto',
            'MundiPagg_MundiPagg/js/view/payment/boletocreditcard': 'Nectar_Mundipagg/js/view/payment/boletocreditcard',
            'MundiPagg_MundiPagg/js/view/payment/creditcard': 'Nectar_Mundipagg/js/view/payment/creditcard',
            'MundiPagg_MundiPagg/js/view/payment/debit': 'Nectar_Mundipagg/js/view/payment/debit',
            'MundiPagg_MundiPagg/js/view/payment/pix': 'Nectar_Mundipagg/js/view/payment/pix',
            'MundiPagg_MundiPagg/js/view/payment/twocreditcards': 'Nectar_Mundipagg/js/view/payment/twocreditcards',
            'MundiPagg_MundiPagg/js/view/payment/voucher': 'Nectar_Mundipagg/js/view/payment/voucher'
        }
    }
};
