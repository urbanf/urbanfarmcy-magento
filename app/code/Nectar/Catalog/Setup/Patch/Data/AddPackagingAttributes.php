<?php

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

class AddPackagingAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param \Nectar\Catalog\Helper\AttributeGroup
     */
    private $attributeGroup;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        \Nectar\Catalog\Helper\AttributeGroup $attributeGroup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeGroup = $attributeGroup;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $this->attributeGroup->createAttributeGroup('Packaging Information', 2);

        $attributesOptionsArray = [
            'largura_do_pacote__cy' => [
                'label' => 'Largura do pacote',
                'sort_order' => 10
            ],
            'altura_do_pacote__cy' => [
                'label' => 'Altura do pacote',
                'sort_order' => 20
            ],
            'peso_do_pacote__cy' => [
                'label' => 'Peso do pacote',
                'sort_order' => 30
            ]
        ];

        $generalAttributes = [
            'group' => 'Packaging Information',
            'required' => false,
            'type' => 'varchar',
            'input' => 'text',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => false,
            'visible_in_advanced_search' => false,
            'filterable' => false,
            'filterable_in_search' => false,
            'is_html_allowed_on_front' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {

            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);

        }

    }

}
