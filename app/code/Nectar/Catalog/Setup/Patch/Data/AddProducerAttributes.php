<?php

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

class AddProducerAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [
            \Nectar\Catalog\Setup\Patch\Data\CreateRecipeAndProducerAttributeSets::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attributesOptionsArray = [
            'produtores_coordenadas__cy' => [
                'label' => 'Coordenadas',
                'sort_order' => 50,
                'group' => 'General',
                'type' => 'varchar',
                'input' => 'text',
                'filterable' => false,
                'filterable_in_search' => false
            ],
            'produtores_historia__cy' => [
                'label' => 'História',
                'sort_order' => 20,
                'group' => 'Content',
                'type' => 'text',
                'input' => 'textarea',
                'filterable' => false,
                'filterable_in_search' => false
            ],
            'produtores_principais_culturas__cy' => [
                'label' => 'Principais culturas',
                'sort_order' => 30,
                'group' => 'General',
                'type' => 'text',
                'input' => 'multiselect',
                'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class,
                'filterable' => true,
                'filterable_in_search' => true
            ]
        ];

        $generalAttributes = [
            'attribute_set' => 'Produtores',
            'required' => false,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'visible_in_advanced_search' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {

            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);

        }

        $eavSetup->updateAttribute(
            Product::ENTITY,
            'produtores_historia__cy',
            [
                'is_pagebuilder_enabled' => 1,
                'is_html_allowed_on_front' => 1,
                'is_wysiwyg_enabled' => 1
            ]
        );

    }

}
