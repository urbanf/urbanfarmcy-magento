<?php

declare(strict_types=1);

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Adiciona novos atributos ao produto
 */
class AddProteinAttribute implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param \Magento\Eav\Api\AttributeGroupRepositoryInterface
     */
    private $attributeGroupRepository;

    /**
     * @param \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory
     */
    private $attributeGroupFactory;

    /**
     * @param \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @param \Nectar\Catalog\Helper\AttributeGroup
     */
    private $attributeGroup;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        \Nectar\Catalog\Helper\AttributeGroup $attributeGroup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeGroup = $attributeGroup;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [
            \Nectar\Catalog\Setup\Patch\Data\AddNutritionalInformationAttributes::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attributesOptionsArray = [
            'nutricional_proteina__cy' => [
                'label' => 'Proteína',
                'sort_order' => 100
            ]
        ];

        $generalAttributes = [
            'group' => 'Nutritional Information',
            'required' => false,
            'type' => 'varchar',
            'input' => 'text',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'visible_in_advanced_search' => false,
            'filterable' => false,
            'filterable_in_search' => false,
            'is_html_allowed_on_front' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {
            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);
        }
    }
    
}

