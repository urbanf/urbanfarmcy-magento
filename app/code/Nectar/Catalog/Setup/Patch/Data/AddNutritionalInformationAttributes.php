<?php

declare(strict_types=1);

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Adiciona novos atributos ao produto
 */
class AddNutritionalInformationAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param \Magento\Eav\Api\AttributeGroupRepositoryInterface
     */
    private $attributeGroupRepository;

    /**
     * @param \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory
     */
    private $attributeGroupFactory;

    /**
     * @param \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @param \Nectar\Catalog\Helper\AttributeGroup
     */
    private $attributeGroup;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        \Nectar\Catalog\Helper\AttributeGroup $attributeGroup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeGroup = $attributeGroup;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $this->attributeGroup->createAttributeGroup('Nutritional Information', 2);

        $attributesOptionsArray = [
            'nutricional_valor_energetico__cy' => [
                'label' => 'Valor Energético',
                'sort_order' => 10
            ],
            'nutricional_carboidratos__cy' => [
                'label' => 'Carboidratos',
                'sort_order' => 20
            ],
            'nutricional_fibra_alimentar__cy' => [
                'label' => 'Fibra alimentar',
                'sort_order' => 30
            ],
            'nutricional_sodio__cy' => [
                'label' => 'Sódio',
                'sort_order' => 40
            ],
            'nutricional_gorduras_totais__cy' => [
                'label' => 'Gorduras Totais',
                'sort_order' => 50
            ],
            'nutricional_gorduras_saturadas__cy' => [
                'label' => 'Gorduras Saturadas',
                'sort_order' => 60
            ],
            'nutricional_gorduras_poliinsaturadas__cy' => [
                'label' => 'Gorduras Poli-insaturadas',
                'sort_order' => 70
            ],
            'nutricional_gorduras_monoinsaturadas__cy' => [
                'label' => 'Gorduras Monoinsaturadas',
                'sort_order' => 80
            ],
            'nutricional_gorduras_trans__cy' => [
                'label' => 'Gorduras Trans',
                'sort_order' => 90
            ]
        ];

        $generalAttributes = [
            'group' => 'Nutritional Information',
            'required' => false,
            'type' => 'varchar',
            'input' => 'text',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'visible_in_advanced_search' => false,
            'filterable' => false,
            'filterable_in_search' => false,
            'is_html_allowed_on_front' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {
            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);
        }
    }
    
}

