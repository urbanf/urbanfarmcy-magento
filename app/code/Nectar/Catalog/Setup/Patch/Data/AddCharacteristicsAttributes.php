<?php

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

class AddCharacteristicsAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attributesOptionsArray = [
            'livre_de__cy' => [
                'label' => 'Livre de',
                'sort_order' => 100
            ],
            'alto_conteudo_de__cy' => [
                'label' => 'Alto conteúdo de',
                'sort_order' => 110
            ],
            'fonte_de__cy' => [
                'label' => 'Fonte de',
                'sort_order' => 120
            ],
            'maior_quantidade__cy' => [
                'label' => 'Maior quantidade de',
                'sort_order' => 130
            ],
            'menor_quantidade__cy' => [
                'label' => 'Menor quantidade de',
                'sort_order' => 140
            ]
        ];

        $generalAttributes = [
            'group' => 'General',
            'required' => false,
            'type' => 'text',
            'input' => 'multiselect',
            'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'visible_in_advanced_search' => true,
            'filterable' => true,
            'filterable_in_search' => true,
            'is_html_allowed_on_front' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {

            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);

        }

    }

}
