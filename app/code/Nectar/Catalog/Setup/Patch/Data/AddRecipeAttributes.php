<?php

namespace Nectar\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;

class AddRecipeAttributes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * PatchInitial constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [
            \Nectar\Catalog\Setup\Patch\Data\CreateRecipeAndProducerAttributeSets::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attributesOptionsArray = [
            'receitas_banner__cy' => [
                'label' => 'Banner',
                'sort_order' => 10,
                'group' => 'Content'
            ],
            'receitas_descricao_do_prato__cy' => [
                'label' => 'Descrição do prato',
                'sort_order' => 20,
                'group' => 'Content'
            ],
            'receitas_ingredientes__cy' => [
                'label' => 'Ingredientes',
                'sort_order' => 30,
                'group' => 'Content'
            ],
            'receitas_preparo__cy' => [
                'label' => 'Modo de preparo',
                'sort_order' => 40,
                'group' => 'Content'
            ],
            'receitas_conservacao__cy' => [
                'label' => 'Conservação',
                'sort_order' => 50,
                'group' => 'Content'
            ]
        ];

        $generalAttributes = [
            'attribute_set' => 'Receitas',
            'required' => false,
            'type' => 'text',
            'input' => 'textarea',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'visible' => true,
            'searchable' => true,
            'visible_in_advanced_search' => false,
            'filterable' => false,
            'filterable_in_search' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {

            $attributeParams = array_merge($attributeParams, $generalAttributes);
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeParams);

            $eavSetup->updateAttribute(
                Product::ENTITY,
                $attributeCode,
                [
                    'is_pagebuilder_enabled' => 1,
                    'is_html_allowed_on_front' => 1,
                    'is_wysiwyg_enabled' => 1
                ]
            );

        }

    }

}
