<?php

namespace Nectar\Catalog\Plugin\Model;

class Product
{
    /**
     * @param \Magento\InventoryApi\Api\GetSourceItemsBySkuInterface
     */
    private $getSourceItemsBySku;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\InventorySalesApi\Api\StockResolverInterface
     */
    private $stockResolver;

    /**
     * @param \Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface
     */
    private $getStockItemConfiguration;

    public function __construct(
        \Magento\InventoryApi\Api\GetSourceItemsBySkuInterface $getSourceItemsBySku,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Magento\InventorySalesApi\Api\StockResolverInterface $stockResolver,
        \Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface $getStockItemConfiguration
    ) {
        $this->getSourceItemsBySku = $getSourceItemsBySku;
        $this->storeManager = $storeManager;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->stockResolver = $stockResolver;
        $this->getStockItemConfiguration = $getStockItemConfiguration;
    }

    /**
     * Make the product available if it is already available, or if it's a Bundle Product with status "Enabled"
     * It also checks the availability of the product in the various Inventory Sources
     * @param \Magento\Catalog\Model\Product $subject
     * @param $result
     * @return bool
     */
    public function afterIsAvailable(
        \Magento\Catalog\Model\Product $subject,
        $result
    ): bool {
        $sku = $subject->getSku();
        $sourceItemsPerSku = $this->getSourceItemsBySku->execute($sku);
        $currentStoreCode = $this->storeManager->getStore()->getCode();
        $stock = $this->stockResolver->execute('website', $this->storeManager->getWebsite()->getCode());
        $stockId = $stock->getStockId();
        $productQuantity = 0;
        $cdResponsavel = $this->cidadesPorCd->getCdResponsavel();
        $hasAvailableOptions = true;
        $outOfStockThreshold = 0;

        if ($subject->getTypeId() == "bundle") {
            $childrenSkus = $this->getBundleOptions($subject);
            foreach ($childrenSkus as $childSku) {
                $sources = [];
                $sourceItemsPerSku = $this->getSourceItemsBySku->execute($childSku);
                foreach ($sourceItemsPerSku as $source) {
                    $sources[] = $source->getSourceCode();
                }

                if (!in_array($cdResponsavel, $sources)) {
                    $hasAvailableOptions = false;
                } else {
                    $stockItemConfig = $this->getStockItemConfiguration->execute($childSku, $stockId);
                    $minQty = $stockItemConfig->getMinQty();

                    foreach ($sourceItemsPerSku as $source) {
                        if ($currentStoreCode !== "default" && $source->getSourceCode() === $cdResponsavel) {
                            if ((int)$source->getData()['quantity'] <= $minQty) {
                                $hasAvailableOptions = false;
                            }
                        }
                    }
                }
            }
        } else {
            $hasAvailableOptions = false;
            $stockItemConfig = $this->getStockItemConfiguration->execute($sku, $stockId);
            $outOfStockThreshold = $stockItemConfig->getMinQty();

            foreach ($sourceItemsPerSku as $source) {
                if ($currentStoreCode === "default") {
                    $productQuantity += (int)$source->getData()['quantity'];
                } elseif ($source->getSourceCode() === $cdResponsavel) {
                    $productQuantity = (int)$source->getData()['quantity'];
                }
            }
        }

        return ($result && $productQuantity > $outOfStockThreshold) || ($currentStoreCode === "default") || $hasAvailableOptions;
    }

    /**
     * Get bundle product Options to add product to cart
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    private function getBundleOptions(\Magento\Catalog\Model\Product $product): array
    {
        $selectionCollection = $product->getTypeInstance()
            ->getSelectionsCollection(
                $product->getTypeInstance()->getOptionsIds($product),
                $product
            );
        $bundleOptions = [];
        foreach ($selectionCollection as $selection) {
            $bundleOptions[] = $selection->getData('sku');
        }
        return $bundleOptions;
    }
}
