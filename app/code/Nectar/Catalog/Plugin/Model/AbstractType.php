<?php
namespace Nectar\Catalog\Plugin\Model;

class AbstractType
{
    public function afterIsPossibleBuyFromList(
        \Magento\Catalog\Model\Product\Type\AbstractType $subject,
        $result,
        $product
    ) {
        return $result || $product->getTypeId() === "bundle";
    }
}
