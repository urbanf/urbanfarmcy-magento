<?php

namespace Nectar\Catalog\Controller\Attribute;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class PopupBlock extends \Magento\Framework\App\Action\Action
{
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    protected $context;

    /**
     * @param \Magento\Framework\View\Result\PageFactory
     */
    private $_pageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $blockCode = $requestParams['block_code'];
        $blockId = $requestParams['block_id'];

        // Tenta achar um bloco estático de nome <block_code>_<block_id>
        $blockHtml = $this->_pageFactory->create()->getLayout()
          ->createBlock('Magento\Cms\Block\Block')
          ->setBlockId($blockCode . '_' . $blockId)
          ->toHtml();

        // Se estiver vazio, tenta achar um bloco de nome <block_code>_default
        if(empty($blockHtml)) {
            $blockHtml = $this->_pageFactory->create()->getLayout()
              ->createBlock('Magento\Cms\Block\Block')
              ->setBlockId($blockCode . '_default')
              ->toHtml();
        }

        // Se estiver vazio, retorna uma mensagem
        if(empty($blockHtml)) {
            $blockHtml = '<span class="empty">' . __('No additional information was found for this subject') . '</span>';
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["html" => $blockHtml]);
        
        return $resultJson;
    }
}
