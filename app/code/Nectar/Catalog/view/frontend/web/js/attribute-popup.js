define([
    'jquery',
    'mage/url',
    'Magento_Ui/js/modal/modal'
], function($, url) {
    'use strict';

    $.widget('nectar.attributePopup', {
        options: {
            target: "#pdp-attribute-popup",
            modalOpener: ".open-attribute-popup",
            modalContent: "#pdp-attribute-popup-content",
            loaderHtml: '<span class="loader"></span>'
        },

        _create: function() {

            if(!$('body').hasClass('catalog-product-view-produtores')) {
                this._prepareModalOpeners();
                this._createModal();
            }
        },

        _prepareModalOpeners: function() {
            let buttons = document.querySelectorAll(this.options.modalOpener);
            buttons.forEach( (button) => {
                button.removeEventListener('click', this._openModal);
                button.addEventListener('click', this._openModal);
            });
        },

        _openModal: function(event) {
            event.preventDefault();
            
            let modalContent = document.getElementById('pdp-attribute-popup-content');
            let blockCode = 'composicao';
            let blockId = event.target.getAttribute('data-id');
            let controllerUrl = url.build('nectarcatalog/attribute/popupBlock');

            modalContent.innerHTML = '';
            modalContent.innerHTML = '<span class="loader"></span>';
            
            $('#pdp-attribute-popup-content').modal('openModal');
            
            $.ajax({
                url: controllerUrl,
                dataType: 'json',
                type: 'POST',
                data: {
                    block_code: blockCode,
                    block_id: blockId
                },
                success:function(res) {
                    modalContent.innerHTML = '';
                    modalContent.innerHTML = res.html;
                }
            });
        },

        _createModal: function() {

            $(this.options.modalContent).modal({
                type: 'popup',
                title: '',
                modalClass: 'pdp-attribute-popup',
                clickableOverlay: true,
                autoOpen: false,
                responsive: true,
                buttons: []
            });

        }

    });

    return $.nectar.attributePopup;

});