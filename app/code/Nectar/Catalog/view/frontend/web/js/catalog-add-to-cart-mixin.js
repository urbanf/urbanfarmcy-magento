 define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'mage/cookies'
], function ($, customerData) {
    'use strict';

    return function (widget) {

        $.widget('mage.catalogAddToCart', widget, {
            options: {
                cookieName: 'urban_delivery_postcode'
            },

            /**
             * @param {jQuery} form
             */
            ajaxSubmit: function (form) {
                if($.mage.cookies.get(this.options.cookieName) && $.mage.cookies.get(this.options.cookieName).length > 0) {
                    // let attr = form.context.attributes;
                    // let ff = form.serializeArray();
                    // let ar = [];

                    // $.each(ff, function(i,item){
                    //     ar[item.name] = item.value;
                    // });

                    // let buttonclass = 'action primary';
                    // let customerInfo = customerData.get('customer')();

                    // console.log('esta logado:' + customerInfo.firstname);

                    // if (!customerInfo.firstname){
                        
                    //     buttonclass = 'action primary proceed-to-checkout';
                    // }

                    // let _html = '<h3>'+$.mage.__('Adicionado ao carrinho')+'</h3>';
                    // _html += '<div class="product-image">'+attr.getNamedItem('data-product-image').value+'</div>';
                    // _html += '<h3>'+attr.getNamedItem('data-product-name').value+'</h3>';
                    // _html += '<div class="product-qty">'+$.mage.__('Quantidade: %1').replace('%1', ar.qty)+'</div>';
                    // _html += attr.getNamedItem('data-product-price').value;

                    // var popup = $('<div class="add-to-cart-modal-popup"/>').html(_html).modal({
                    //     modalClass: 'add-to-cart-popup',
                    //     buttons: [
                    //         {
                    //             text: $.mage.__('Continuar comprando'),
                    //             class: 'action secondary',
                    //             click: function () {
                    //                 // window.location = window.checkout.shoppingCartUrl
                    //                 this.closeModal();
                    //             }
                    //         },
                    //         {
                    //             text: $.mage.__('Finalizar compra'),
                    //             class: buttonclass,
                    //             click: function () {

                    //                 if (customerInfo.firstname){
                    //                     window.location = window.checkout.checkoutUrl
                    //                 }
                    //             }
                    //         }
                    //     ]
                    // });
                    //popup.modal('openModal');

                    return this._super(form);
                } else {
                    let modal = $("#delivery-address-modal-content");
                    modal.modal('openModal');
                }
            }
            
        });

        return $.mage.catalogAddToCart;
    };
});
