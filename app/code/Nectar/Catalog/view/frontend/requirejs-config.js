var config = {
    map: {
        '*': {
            'attribute_popup':'Nectar_Catalog/js/attribute-popup'
        }
    },
    config: {
        mixins: {
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Nectar_Catalog/js/catalog-add-to-cart-mixin': true
            }
        }
    }
};