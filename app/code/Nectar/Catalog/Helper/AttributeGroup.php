<?php

namespace Nectar\Catalog\Helper;

class AttributeGroup
{
    /**
     * @param \Magento\Eav\Api\AttributeGroupRepositoryInterface
     */
    private $attributeGroupRepository;

    /**
     * @param \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory
     */
    private $attributeGroupFactory;

    /**
     * @param \Magento\Catalog\Model\Product
     */
    private $product;

    public function __construct(
        \Magento\Eav\Api\AttributeGroupRepositoryInterface $attributeGroupRepository,
        \Magento\Eav\Api\Data\AttributeGroupInterfaceFactory $attributeGroupFactory,
        \Magento\Catalog\Model\Product $product
    )
    {
        $this->attributeGroupRepository = $attributeGroupRepository;
        $this->attributeGroupFactory = $attributeGroupFactory;
        $this->product = $product;
    }

    public function createAttributeGroup($groupName, $sortOrder) {
        $attributeSetId = $this->product->getDefaultAttributeSetId();
        $attributeGroup = $this->attributeGroupFactory->create();

        $attributeGroup->setAttributeSetId($attributeSetId);
        $attributeGroup->setSortOrder($sortOrder);
        $attributeGroup->setAttributeGroupName($groupName);
        $this->attributeGroupRepository->save($attributeGroup);
    }
}
