<?php

namespace Nectar\Catalog\Helper;

class AttributeSet
{

    /**
     * @param \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @param \Magento\Eav\Api\Data\AttributeSetInterfaceFactory
     */
    private $attributeSetFactory;

    /**
     * @param \Magento\Catalog\Api\AttributeSetManagementInterface
     */
    private $attributeSetManagement;

    public function __construct(
        \Magento\Eav\Api\Data\AttributeSetInterfaceFactory $attributeSetFactory,
        \Magento\Catalog\Api\AttributeSetManagementInterface $attributeSetManagement,
        \Magento\Catalog\Model\Product $product
    )
    {
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->product = $product;
    }

    public function createAttributeSet($setName) {
        $defaultAttributeSetId = $this->product->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeSet->setAttributeSetName($setName);
        $this->attributeSetManagement->create($attributeSet, $defaultAttributeSetId);
    }
}
