define([
        'ko',
        'moment',
        'jquery',
        'Magento_Ui/js/form/element/select'
    ], function (
    ko,
    moment,
    $,
    AbstractField
    ) {
        'use strict';

        return AbstractField.extend({
            defaults: {
                deliverydateConfig: window.checkoutConfig.amasty.deliverydate,
                elementTmpl: 'Amasty_Deliverydate/form/element/select'
            },
            initConfig: function (config) {
                this._super();

                this.abridgeIntervalsSet = config.options.abridge == undefined ? config.options[1] : config.options.abridge;
                this.fullIntervalsSet = config.options.full == undefined ? config.options[0] : config.options.full;
                this.displayed = this.fullIntervalsSet;
            },
            onUpdate: function () {
                this.bubble('update', this.hasChanged());
                this.validate();
            },

            calculateChosenDate: function() {
                let dateField = document.querySelector('[name="amdeliverydate_date"]');
                let dateFieldValue = "";
                let chosenDate = "";

                if(dateField) {
                    dateFieldValue = dateField.value;
                }

                if(dateFieldValue.length > 0) {
                    if(moment(dateFieldValue, 'D/M/YYYY', true).isValid()) {
                        let splitDate = dateFieldValue.split('/');
                        let newDate = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
                        chosenDate = new Date(newDate);
                    } else {
                        chosenDate = new Date(dateFieldValue.replace(/-/g,'/'));
                    }
                }

                return chosenDate;
            },

            initObservable: function () {
                this._super();

                let chosenDate = this.calculateChosenDate();
                const storeCity = ['barueri', 'cachoeirinha', 'carapicuiba','sao_paulo'];

                var newOptions = [];
                $.each(this.displayed, function (index, value) {

                    if (storeCity.includes(window.checkoutConfig.storeCode)) {

                        if(chosenDate instanceof Date) {

                            if(chosenDate.getDay() === 6) {
                                newOptions.push({
                                    value: value.value,
                                    label: value.label,
                                    disabled: value.disabled
                                });
                                return false;
                            }else if(chosenDate.getDay() === 1 && index > 0){
                                newOptions.push({
                                    value: value.value,
                                    label: value.label,
                                    disabled: value.disabled
                                });
                            }else if(chosenDate.getDay() > 1 && chosenDate.getDay() < 6){
                                newOptions.push({
                                    value: value.value,
                                    label: value.label,
                                    disabled: value.disabled
                                });
                            }
                        }
                    }else{
                        if(chosenDate instanceof Date) {
                            if(chosenDate.getDay() === 1 && index > 0){
                                newOptions.push({
                                    value: value.value,
                                    label: value.label,
                                    disabled: value.disabled
                                });
                            }else if(chosenDate.getDay() > 1 && chosenDate.getDay() < 6){
                                newOptions.push({
                                    value: value.value,
                                    label: value.label,
                                    disabled: value.disabled
                                });
                            }
                        }
                    }
                });

                if (!storeCity.includes(window.checkoutConfig.storeCode)) {
                    //Entra RS
                    if(chosenDate instanceof Date) {
                        var day  = chosenDate.getDate(),  
                        month = chosenDate.getMonth() + 1;

                        var closeDate = day + "/" + month;
                        if(closeDate == "3/2" || closeDate == "2/3"){
                            console.log(closeDate + "janela manhã fechada em " + window.checkoutConfig.storeCode);
                            newOptions.shift();
                        }
                    }
                }else{
                    //Entra SP
                    if(chosenDate instanceof Date) {
                        var day  = chosenDate.getDate(),  
                        month = chosenDate.getMonth() + 1;

                        var closeDate = day + "/" + month;
                        if(closeDate == "2/3"){
                            console.log(closeDate + "janela manhã fechada em " + window.checkoutConfig.storeCode);
                            newOptions.shift();
                        }
                    }
                }

                this.options(newOptions);

                return this;
            }

        });
    }
);