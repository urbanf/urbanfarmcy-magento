var config = {
    map: {
        '*': {
            // Adicionado os .js com os estados e suas cidades.
            'Cidades': 'Nectar_NewsLetter/js/data/cidades',
            'Estados': 'Nectar_NewsLetter/js/data/estados',
            'AC': 'Nectar_NewsLetter/js/data/cidades/acre',
            'AL': 'Nectar_NewsLetter/js/data/cidades/alagoas',
            'AM': 'Nectar_NewsLetter/js/data/cidades/amazonas',
            'AP': 'Nectar_NewsLetter/js/data/cidades/amapa',
            'BA': 'Nectar_NewsLetter/js/data/cidades/bahia',
            'CE': 'Nectar_NewsLetter/js/data/cidades/ceara',
            'DF': 'Nectar_NewsLetter/js/data/cidades/distrito-federal',
            'ES': 'Nectar_NewsLetter/js/data/cidades/espirito-santo',
            'GO': 'Nectar_NewsLetter/js/data/cidades/goias',
            'MA': 'Nectar_NewsLetter/js/data/cidades/maranhao',
            'MG': 'Nectar_NewsLetter/js/data/cidades/minas-gerais',
            'MS': 'Nectar_NewsLetter/js/data/cidades/mato-grosso-do-sul',
            'MT': 'Nectar_NewsLetter/js/data/cidades/mato-grosso',
            'PA': 'Nectar_NewsLetter/js/data/cidades/para',
            'PB': 'Nectar_NewsLetter/js/data/cidades/paraiba',
            'PE': 'Nectar_NewsLetter/js/data/cidades/pernambuco',
            'PI': 'Nectar_NewsLetter/js/data/cidades/piaui',
            'PR': 'Nectar_NewsLetter/js/data/cidades/parana',
            'RJ': 'Nectar_NewsLetter/js/data/cidades/rio-de-janeiro',
            'RN': 'Nectar_NewsLetter/js/data/cidades/rio-grande-do-norte',
            'RO': 'Nectar_NewsLetter/js/data/cidades/rondonia',
            'RR': 'Nectar_NewsLetter/js/data/cidades/roraima',
            'RS': 'Nectar_NewsLetter/js/data/cidades/rio-grande-do-sul',
            'SC': 'Nectar_NewsLetter/js/data/cidades/santa-catarina',
            'SE': 'Nectar_NewsLetter/js/data/cidades/sergipe',
            'SP': 'Nectar_NewsLetter/js/data/cidades/sao-paulo',
            'TO': 'Nectar_NewsLetter/js/data/cidades/tocantins',
            'Nectar_NewsLetter/js/cidade-datalist': 'Nectar_NewsLetter/js/cidade-datalist'
        }
    },
    config: {
        mixins: {
            'mage/validation': {
                'Nectar_NewsLetter/js/validation-mixin': true
            }
        }
    }
};
