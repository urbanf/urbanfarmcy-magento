define([
    'jquery',
    'uiComponent',
    'Estados',
    'AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO',
    'Cidades'
], function($, Component, Estados, AC, AL, AM, AP, BA, CE, DF, ES, GO, MA, MG, MS, MT, PA, PB, PE, PI, PR, RJ, RN, RO, RR, RS, SC, SE, SP, TO, Cidades) {
    'use strict';

    return Component.extend({
        initialize: function() {
            this._prepareStateList();
            this._prepareDataList();
        },

        // Adicionado o StateList para adicionar a lista de escolha de estados.
        _prepareStateList: function() {
            let datalist = document.createElement('datalist');
            let options = '';
            datalist.classList.add('state-datalist');
            datalist.setAttribute('id', 'state-datalist');

            Estados.estados.forEach( (estado) => {
                options += '<option value="' + estado + '" />';
            });

            datalist.innerHTML = options;
            document.body.appendChild(datalist);

            this._prepareStateFields();

        },

        // Adicionado StateFields para colocar a lista no html com for/id do estado_cliente e estado_cliente-popup.
        _prepareStateFields: function() {
            let stateFields = document.querySelectorAll('[name="estado_cliente"]');
            stateFields.forEach( (field) => {
                field.setAttribute('list', 'state-datalist');
                field.classList.add('validate-state-exists');
            });
        },

        _prepareDataList: function() {
            let datalist = document.createElement('datalist');
            let estado_cliente = document.getElementById('estado_cliente');
            let estado_cliente_popup = document.getElementById('estado_cliente-popup');
            let options = '';
            datalist.classList.add('city-datalist');
            datalist.setAttribute('id', 'city-datalist');

            // função de filtragem do id estado_cliente, se selecionar o estado, só vai aparecer as cidades desse estado
            estado_cliente.onchange = function() {
                options = '';
                
                // switch case para filtrar as cidades de acordo com o estado
                switch (estado_cliente.value) {
                    case "Acre":
                        AC.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Alagoas":
                        AL.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Amazonas":
                        AM.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Amapá":
                        AP.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Bahia":
                        BA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Ceará":
                        CE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Distrito Federal":
                        DF.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Espírito Santo":
                        ES.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;
                    
                    case "Goiás":
                        GO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Maranhão":
                        MA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Minas Gerais":
                        MG.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Mato Grosso do Sul":
                        MS.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Mato Grosso":
                        MT.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Pará":
                        PA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;
                    
                    case "Paraíba":
                        PB.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Pernambuco":
                        PE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Piauí":
                        PI.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Paraná":
                        PR.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio de Janeiro":
                        RJ.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio Grande do Norte":
                        RN.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rondônia":
                        RO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Roraima":
                        RR.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio Grande do Sul":
                        RS.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Santa Catarina":
                        SC.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Sergipe":
                        SE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "São Paulo":
                        SP.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Tocantins":
                        TO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    default:
                        /* Cidades.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        }); */
                        break;
                }

                datalist.innerHTML = options;
                document.body.appendChild(datalist);
            };

            // filtragem de id para o pop da newsletter
            estado_cliente_popup.onchange = function() {
                options = '';

                // switch case para filtrar as cidades de acordo com o estado no newsletter popup
                switch (estado_cliente_popup.value) {
                    case "Acre":
                        AC.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Alagoas":
                        AL.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Amazonas":
                        AM.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Amapá":
                        AP.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Bahia":
                        BA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Ceará":
                        CE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Distrito Federal":
                        DF.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Espírito Santo":
                        ES.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;
                    
                    case "Goiás":
                        GO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Maranhão":
                        MA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Minas Gerais":
                        MG.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Mato Grosso do Sul":
                        MS.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Mato Grosso":
                        MT.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Pará":
                        PA.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;
                    
                    case "Paraíba":
                        PB.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Pernambuco":
                        PE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Piauí":
                        PI.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Paraná":
                        PR.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio de Janeiro":
                        RJ.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio Grande do Norte":
                        RN.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rondônia":
                        RO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Roraima":
                        RR.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Rio Grande do Sul":
                        RS.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Santa Catarina":
                        SC.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Sergipe":
                        SE.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "São Paulo":
                        SP.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    case "Tocantins":
                        TO.cidades.forEach( (cidade) => {
                            options += '<option value="' + cidade + '" />';
                        });
                        break;

                    default:
                        break;
                }

                datalist.innerHTML = options;
                document.body.appendChild(datalist);
            };

            this._prepareCityFields();
        },

        _prepareCityFields: function() {
            let cityFields = document.querySelectorAll('[name="cidade_cliente"]');
            cityFields.forEach( (field) => {
                field.setAttribute('list', 'city-datalist');
                field.classList.add('validate-city-exists');
            });
        }
    });
});