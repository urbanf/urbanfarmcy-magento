define([
    'jquery',
    'Estados',
    'Cidades'
], function ($, Estados, Cidades) {
    "use strict";

    return function () {

        // Adicionado a validação para os estados
        $.validator.addMethod(
            'validate-state-exists',
            function(value) {
                if(!value) {
                    return true;
                }

                let exists = false;
                Estados.estados.some( (estado) => {
                    if(value.toLowerCase() === estado.toLowerCase()) {
                        exists = true;
                    }
                });
                return exists;
            }, $.mage.__('Insira um estado existente (verifique se a ortografia está correta)')
        );

        $.validator.addMethod(
            'validate-city-exists',
            function(value) {
                if(!value) {
                    return true;
                }
                
                let exists = false;
                Cidades.cidades.some( (cidade) => {
                    if(value.toLowerCase() === cidade.toLowerCase()) {
                        exists = true;
                    }
                });
                return exists;
            }, $.mage.__('Insira uma cidade existente (verifique se a ortografia está correta)')
        );
        
    };
});