<?php
namespace Nectar\NewsLetter\Observer;

class ControllerActionPredispatchNewsletterManageSave implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\App\Request\Http
     */
    private $request;

    public function __construct(
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sub=$observer->getEvent()->getSubscriber();
        
        if ($this->request->isPost() && $this->request->getPost('cidade_cliente')) {
          $CidadeCliente = $this->request->getPost('cidade_cliente');
        }
        $sub->setCidadeCliente($CidadeCliente);
    
        return $this;
    }
}