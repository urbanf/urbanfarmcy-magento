<?php

namespace Nectar\ExaminarPedidosPendentes\Logger;

use Monolog\Logger;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;

class Handler extends Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/ExaminarPedidosPendentes.log';
    
    /**
     * @param Context $context
     * @param DriverInterface $filesystem
     */
    public function __construct(
        Context $context,
        DriverInterface $filesystem
    ) {
        $date = date('Y-m-d');
        $this->fileName = "/var/log/ExaminarPedidosPendentes-{$date}.log";
        parent::__construct($filesystem, null, $this->fileName);
    }
}
