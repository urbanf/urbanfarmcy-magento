<?php
namespace Nectar\ExaminarPedidosPendentes\Plugin\Model;

class Order
{
    public function __construct()
    {
    }

    public function afterSave(
        \Magento\Sales\Model\ResourceModel\Order $subject,
        $result,
        \Magento\Framework\Model\AbstractModel $object
    ) {
        return $result;
    }

}
