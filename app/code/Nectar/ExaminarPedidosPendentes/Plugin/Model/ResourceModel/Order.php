<?php

namespace Nectar\ExaminarPedidosPendentes\Plugin\Model\ResourceModel;

use Nectar\ExaminarPedidosPendentes\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order as OrderResourceModel;
use Magento\Framework\Model\AbstractModel;

class Order
{
    /**
     * @param Logger
     */
    private $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger
    ) {
        $this->logger = $logger;
    }
    
    /**
     * @param OrderResourceModel $subject
     * @param AbstractModel $order
     * @return AbstractModel
     */
    public function beforeSave(
        OrderResourceModel $subject,
        AbstractModel $order
    ) {
        $dados_para_log = [
            "entity_id" => $order->getEntityId(),
            "increment_id" => $order->getIncrementId(),
            "customer_id" => $order->getCustomerId(),
            "status" => $order->getStatus()
        ];

        $this->logger->info("Before Save: " . json_encode($dados_para_log));
        $this->logger->info($this->generateCallTrace());

        return [$order];
    }

    /**
     * @param OrderResourceModel $subject
     * @param AbstractModel $order
     * @return AbstractModel
     */
    public function afterSave(
        OrderResourceModel $subject,
        $result,
        AbstractModel $order
    ) {
        $dados_para_log = [
            "entity_id" => $order->getEntityId(),
            "increment_id" => $order->getIncrementId(),
            "customer_id" => $order->getCustomerId(),
            "status" => $order->getStatus()
        ];

        $this->logger->info("After Save: " . json_encode($dados_para_log));
        $this->logger->info($this->generateCallTrace());

        return [$order];
    }

    public function generateCallTrace()
    {
        $e = new \Exception();
        $trace = explode("\n", $e->getTraceAsString());
        // reverse array to make steps line up chronologically
        $trace = array_reverse($trace);
        array_shift($trace); // remove {main}
        array_pop($trace); // remove call to this method
        $length = count($trace);
        $result = array();
    
        for ($i = 0; $i < $length; $i++)
        {
            $result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
        }
    
        return "\t" . implode("\n\t", $result);
    }
}
