<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Nectar\Cds\Model\Cidades;

/**
 * @api
 * @since 100.0.2
 */
class Options implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(\Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory)
    {
        $this->_groupCollectionFactory = $groupCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'barueri', 'label' => __('Barueri')],
            ['value' => 'cachoeirinha', 'label' => __('Cachoeirinha')],
            ['value' => 'canoas', 'label' => __('Canoas')],
            ['value' => 'carapicuiba', 'label' => __('Carapicuiba')],
            ['value' => 'esteio', 'label' => __('Esteio')],
            ['value' => 'gravatai', 'label' => __('Gravataí')],
            ['value' => 'novo_hamburgo', 'label' => __('Novo Hamburgo')],
            ['value' => 'porto_alegre', 'label' => __('Porto Alegre')],
            ['value' => 'santana_de_parnaiba', 'label' => __('Santana de Parnaíba')],
            ['value' => 'sao_leopoldo', 'label' => __('São Leopoldo')],
            ['value' => 'sao_paulo', 'label' => __('São Paulo')]                
                ];
    }
}
