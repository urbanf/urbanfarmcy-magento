<?php

namespace Nectar\Cds\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class AddBasicAccountAttributes
 * @package Nectar\Customer\Setup\Patch
 */
class UpdateFretes20210922 implements DataPatchInterface
{

    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $tabela = $this->resourceConnection->getTableName('shipping_price');
        $connection = $this->resourceConnection->getConnection();
        $query =  "DELETE FROM " .  $tabela . " WHERE descricao='" . "sao_paulo" . "';";
        $connection->query($query);

        $min=[0,15];
        $max=[15,25];
        $frete=[10.90,14.90];

        for ($i = 0; $i < sizeof($min); $i++) {
            $insert = "INSERT INTO " . $tabela . "(descricao, raioMin, raioMax, frete) VALUES " . "('" .  "sao_paulo" . "', '" . $min[$i] . "', '" . $max[$i] . "', '" . $frete[$i] . "');";
            $connection->query($insert);
        }
    }
}
