<?php

namespace Nectar\Cds\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class AddBasicAccountAttributes
 * @package Nectar\Customer\Setup\Patch
 */
class CreateCds implements DataPatchInterface
{

    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $tabela = $this->resourceConnection->getTableName('shipping_price');
        $connection = $this->resourceConnection->getConnection();

        $min=[0,5,10,15,20,25,30,35];
        $max=[5,10,15,20,25,30,35,40];
        $frete=[10.9,10.9,10.9,10.9,16.9,16.9,22.9,22.9];

        for ($i = 0; $i < sizeof($min); $i++) {
            $insert = "INSERT INTO " . $tabela . "(descricao, raioMin, raioMax, frete) VALUES " . "('" .  "porto_alegre" . "', '" . $min[$i] . "', '" . $max[$i] . "', '" . $frete[$i] . "');";
            $connection->query($insert);
        }
    }
}
