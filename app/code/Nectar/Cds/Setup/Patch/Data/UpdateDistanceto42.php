<?php

namespace Nectar\Cds\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class AddBasicAccountAttributes
 * @package Nectar\Customer\Setup\Patch
 */
class UpdateDistanceto42 implements DataPatchInterface
{

    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $tabela = $this->resourceConnection->getTableName('shipping_price');
        $connection = $this->resourceConnection->getConnection();
        $query = "UPDATE " . $tabela . " SET raioMax=42 where raioMax=40;";
        $connection->query($query);
    }
}
