<?php

namespace Nectar\Cds\Helper;

class CidadesPorCd extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var cidades
     */
    public $cidades;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }
    public function getCds()
    {
        return ["porto_alegre", "sao_paulo"];
    }

    public function getCdCode($cd)
    {
        $codes= ["porto_alegre" => "0104", "sao_paulo" => "0106"];
        return isset($codes[$cd])?$codes[$cd]:Null;
    }

    public function getCidadesAtendidas($storeId = null)
    {
        $cidades = $this->getCds();
        $vetor = [];
        foreach ($cidades as $cidade) {
            $list = $this->scopeConfig->getValue(
                "cds/" . $cidade .  "/cidades",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            );
            if ($list !== null) {
                $vetor[] = explode(',', $list);
            }
        }
        return $vetor;
    }

    public function getCidadesAtendidasPorCd($storeId = null)
    {
        $cidades = $this->getCds();
        $vetor = [];
        foreach ($cidades as $cidade) {
            $list = $this->scopeConfig->getValue(
                "cds/" . $cidade .  "/cidades",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            );
            if ($list !== null) {
                $vetor[$cidade] = explode(',', $list);
            }
        }
        return $vetor;
    }

    public function getCdResponsavel()
    {
        $codigo = $this->storeManager->getStore()->getCode(); //pega o código da loja atual (uma por cidade)
        $cdsDisponiveis = $this->getCds(); //traz os cds disponíveis 
        $cidadesAtendidas = $this->getCidadesAtendidas(); //traz todas as cidades separadas por cd que as atende (o cd é o indice do vetor, ver __construct do arquivo \Cds\Helper\CidadesPorCd)
        for ($i = 0; $i < sizeof($cidadesAtendidas); $i++) { //percorre o vetor de cds
            if (in_array(str_replace("_", " ", $codigo), str_replace("_", " ", $cidadesAtendidas[$i]))) { //confere se dentro do vetor de cidades do cd esta a cidade da loja atual
                $cdResponsavel = $cdsDisponiveis[$i]; //caso a loja esteja salva o nome do cd responsavel
            }
        }
        return isset($cdResponsavel) ? $cdResponsavel : null;
    }
}
