<?php
namespace Nectar\Shipping\Block;

class DeliveryAddress extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Customer\Model\SessionFactory
     */
    private $sessionFactory;

    /**
     * @param \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * @param \Magento\Customer\Model\AddressFactory
     */
    private $addressFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $sessionFactory,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\App\Http\Context $httpContext,
        array $data = []
    ) {
        $this->sessionFactory = $sessionFactory;
        $this->customer = $customer;
        $this->addressFactory = $addressFactory;
        $this->httpContext = $httpContext;
        parent::__construct($context, $data);
    }

    public function getCurrentCustomerPostcode() {
        $customerId = $this->sessionFactory->create()->getCustomerId();
        $customer = $this->customer->load($customerId);
        $customerData = $customer->getDataModel();

        $addressId = $customerData->getDefaultShipping();
        $address = $this->addressFactory->create();
        $address->load($addressId);

        return $address->getPostcode();
    }

    public function getCurrentCustomerCity() {
        $customerId = $this->sessionFactory->create()->getCustomerId();
        $customer = $this->customer->load($customerId);
        $customerData = $customer->getDataModel();

        $addressId = $customerData->getDefaultShipping();
        $address = $this->addressFactory->create();
        $address->load($addressId);

        return $address->getCity();
    }

    public function isCustomerLoggedIn() {
        return (bool)$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

}
