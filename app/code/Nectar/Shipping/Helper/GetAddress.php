<?php

namespace Nectar\Shipping\Helper;

class GetAddress
{
    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\InventoryApi\Api\SourceRepositoryInterface
     */
    private $sourceRepository;

    /**
     * @param \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode
     */
    private $getCityCode;

    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository,
        \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode $getCityCode,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->sourceRepository = $sourceRepository;
        $this->getCityCode = $getCityCode;
        $this->resourceConnection = $resourceConnection;
    }

    public function getAddress($searchtext)
    {
        try {
            $url = "https://geocoder.ls.hereapi.com/search/6.2/geocode.json?languages=pt-BR&maxresults=1&searchtext=" . $searchtext . "&apiKey=" . $this->getKey();
            $this->curl->get($url);
            $response = $this->curl->getBody();
            $resposta = json_decode($response);
            if (isset($resposta->Response->View[0]->Result) && isset($resposta->Response->View[0]->Result[0]->Location->Address->City)) {
                $latCustomer = $resposta->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude;
                $longCustomer = $resposta->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude;
                $cidadesAtendidas = $this->cidadesPorCd->getCidadesAtendidas();
                $cidade = strtolower(str_replace(" ", "_", $this->getCityCode->retiraAcento($resposta->Response->View[0]->Result[0]->Location->Address->City)));
                foreach ($cidadesAtendidas as $cd => $cidadesDoCD) {
                    foreach ($cidadesDoCD as $cidadeAtendida) {
                        if ($cidadeAtendida == $cidade) {
                            $sourceCode = $this->cidadesPorCd->getCds()[$cd];
                            $sourceInfo = $this->sourceRepository->get($sourceCode);
                            $lat = $sourceInfo['latitude'];
                            $long = $sourceInfo['longitude'];
                            $distLat = $latCustomer - $lat;
                            $distLong = $longCustomer - $long;
                            $distLatemKm = $distLat * 1852 * 60;
                            $distLongemKm = $distLong * 1852 * 60;
                            $raio = pow(($distLatemKm * $distLatemKm) + ($distLongemKm * $distLongemKm), 0.5) / 1000;
                            $raios = $this->getRaiosDisponiveis($sourceCode);
                            foreach ($raios as $distancia) {
                                if ($raio > $distancia['raioMin'] && $raio <= $distancia['raioMax']) {
                                    $retorno = $distancia['frete'];
                                }
                            }
                        }
                    }
                }
                if (isset($retorno)) {
                    return ($resposta);
                } else {
                    throw new \Exception(__('Desculpe, ainda não entregamos no seu endereço. CEP: ' . $searchtext));
                }
            } else {
                throw new \Exception(__('Desculpe, não conseguimos identificar sua cidade. CEP: ' . $searchtext));
            }
        } catch (\Exception $e) {
            throw $e;
            return "";
        }
    }

    public function getRaiosDisponiveis($cd)
    {
        $tabela = $this->resourceConnection->getTableName('shipping_price');
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                ['c' => $tabela],
                ['*']
            )
            ->where(
                "c.descricao = :descricao"
            );
        $bind = ['descricao' => $cd];
        $records = $connection->fetchAll($select, $bind);
        return ($records);
    }

    private function getKey()
    {
        return $this->scopeConfig->getValue("integrations/delivery/key");
    }
}
