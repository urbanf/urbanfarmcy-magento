<?php

namespace Nectar\Shipping\Helper;

use Magento\Framework\App\ActionInterface;
use Magento\Store\Api\StoreResolverInterface;

class ChangeStore
{
    /**
     * @param \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @param \Magento\Framework\App\Http\Context
     */
    private $httpContext;

    /**
     * @param \Magento\Store\Api\StoreCookieManagerInterface
     */
    private $storeCookieManager;

    /**
     * @param \Magento\Framework\App\Action\Action
     */
    private $action;

    /**
     * @param \Magento\Framework\App\Action\Context
     */
    private $actionContext;

    /**
     * @param \Magento\Store\Model\StoreSwitcher\RedirectDataGenerator
     */
    private $redirectDataGenerator;

    /**
     * @param \Magento\Store\Model\StoreSwitcher\ContextInterfaceFactory
     */
    private $contextFactory;

    /**
     * @param \Magento\Framework\Url\Helper\Data
     */
    private $urlHelper;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Store\Api\StoreCookieManagerInterface $storeCookieManager,
        \Magento\Framework\App\Action\Action $action,
        \Magento\Framework\App\Action\Context $actionContext,
        \Magento\Store\Model\StoreSwitcher\RedirectDataGenerator $redirectDataGenerator,
        \Magento\Store\Model\StoreSwitcher\ContextInterfaceFactory $contextFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->quoteRepository = $quoteRepository;
        $this->httpContext = $httpContext;
        $this->storeCookieManager = $storeCookieManager;
        $this->action = $action;
        $this->actionContext = $actionContext;
        $this->redirectDataGenerator = $redirectDataGenerator;
        $this->contextFactory = $contextFactory;
        $this->urlHelper = $urlHelper;
    }

    public function changeStore($storeCode) {
        // $quote = $this->checkoutSession->getQuote();
        // $storeId = $this->storeManager->getStore($storeCode)->getId();
        
        // if ($quote->getIsActive()) {
            //     $quote->setStoreId($storeId);
            //     $quote->getItemsCollection(false);
            //     $this->quoteRepository->save($quote);
            // }
            
            // $this->httpContext->setValue(Store::ENTITY, $storeCode);
            // $this->storeCookieManager->setStoreCookie($store);
            
            // $this->storeManager->setCurrentStore($this->storeManager->getStore($storeCode)->getId());
            
        $store = $this->storeManager->getStore($storeCode);
        $currentStoreCode = $this->storeManager->getStore()->getCode();

        $urlOnTargetStore = $store->getCurrentUrl(false);
        $encodedUrl = $this->urlHelper->getEncodedUrl($urlOnTargetStore);

        $redirectData = $this->redirectDataGenerator->generate(
            $this->contextFactory->create(
                [
                    'fromStore' => $currentStoreCode,
                    'targetStore' => $storeCode,
                    'redirectUrl' => $this->action->_redirect->getRedirectUrl()
                ]
            )
        );

        $query = [
            '___from_store' => $currentStoreCode,
            StoreResolverInterface::PARAM_NAME => $storeCode,
            ActionInterface::PARAM_NAME_URL_ENCODED => $encodedUrl,
            'data' => $redirectData->getData(),
            'time_stamp' => $redirectData->getTimestamp(),
            'signature' => $redirectData->getSignature(),
        ];

        $arguments = [
            '_nosid' => true,
            '_query' => $query
        ];

        $this->action->_redirect->redirect($this->action->_response, 'stores/store/switch', $arguments);

        return null;
    }

}
