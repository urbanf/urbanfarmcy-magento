<?php

namespace Nectar\Shipping\Helper;

class ValidateShippingAddress
{
    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Customer\Model\SessionFactory
     */
    private $sessionFactory;

    /**
     * @param \Nectar\Shipping\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Model\SessionFactory $sessionFactory,
        \Nectar\Shipping\Logger\Logger $logger
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->sessionFactory = $sessionFactory;
        $this->logger = $logger;
    }

    public function canCustomerProceed($cep = null)
    {
        $mensagem = "";
        $resultParcial = true;
        if ($resultParcial) {
            if (!is_Null($cep)) {
                $cep = preg_replace("/[^0-9]/", "", $cep);
                $cepCookie = preg_replace("/[^0-9]/", "", $_COOKIE['urban_delivery_postcode']);
            } else {
                $result = false;
                $mensagem = "Falha ao acessar o endereço de entrega";
            }
            if (isset($cepCookie) && $cepCookie == $cep) {
                $result = true;
            } else {
                $result = false;
                $mensagem = "CEP de entrega diferente do CEP informado, favor informe seu CEP novamente.";
            }
        } else {
            $result = false;
            $mensagem = "Falha no cadastro do cliente, favor tente novamente.";
        }
        $resultado = ['sucesso' => $result, 'mensagem' => $mensagem];
        return $resultado;
    }
}
