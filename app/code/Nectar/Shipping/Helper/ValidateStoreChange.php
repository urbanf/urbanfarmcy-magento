<?php

namespace Nectar\Shipping\Helper;

use Magento\InventoryApi\Api\Data\SourceItemInterface;

class ValidateStoreChange
{
    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\Quote\Model\Quote
     */
    private $quote;

    /**
     * @param \Magento\InventoryApi\Api\SourceItemRepositoryInterface
     */
    private $sourceItemRepository;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param \Magento\InventoryApi\Api\GetSourceItemsBySkuInterface
     */
    private $sourceItemsBySku;

    /**
     * @param \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Magento\Quote\Model\Quote $quote,
        \Magento\InventoryApi\Api\SourceItemRepositoryInterface $sourceItemRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\InventoryApi\Api\GetSourceItemsBySkuInterface $sourceItemsBySku,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->storeManager = $storeManager;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->quote = $quote;
        $this->sourceItemRepository = $sourceItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceItemsBySku = $sourceItemsBySku;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * A cidade do endereço cadastrado existe como uma Store no Magento?
     * 
     * @param string $city
     * @return bool
     */
    public function cidadeExiste($city)
    {
        setlocale(LC_ALL, 'en_US.UTF-8');

        $currentStoreCode = $this->storeManager->getStore()->getCode();
        $sanitizedCity = strtolower(str_replace(' ', '_', iconv('UTF-8', 'ASCII//TRANSLIT', $city)));

        $cidadesAtendidas = $this->cidadesPorCd->getCidadesAtendidas();

        foreach($cidadesAtendidas as $cidade) {
            if(in_array($sanitizedCity, $cidade)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * De acordo com a cidade recebida, precisa mudar de Store?
     * 
     * @param string $city
     * @return bool
     */
    public function deveMudarDeStore($city)
    {
        setlocale(LC_ALL, 'en_US.UTF-8');

        $currentStoreCode = $this->storeManager->getStore()->getCode();
        $sanitizedCity = strtolower(str_replace(' ', '_', iconv('UTF-8', 'ASCII//TRANSLIT', $city)));
        
        // Se a cidade recebida for diferente da Store atual, precisa mudar de Store
        return $currentStoreCode !== $sanitizedCity;
    }

    /**
     * De acordo com a cidade recebida, vai ser preciso mudar de CD?
     * 
     * @param string $city
     * @return bool|string
     */
    public function deveMudarDeCd($city)
    {
        setlocale(LC_ALL, 'en_US.UTF-8');
        
        $currentStoreId = $this->storeManager->getStore()->getId();
        $sanitizedCity = strtolower(str_replace(' ', '_', iconv('UTF-8', 'ASCII//TRANSLIT', $city)));
        $cdResponsavel = $this->cidadesPorCd->getCdResponsavel();
        $cidadesAtendidas = $this->cidadesPorCd->getCidadesAtendidasPorCd();

        // Se não estiver no array do CD Responsável atual, significa que a cidade pode estar em outro CD
        if(!in_array($sanitizedCity, $cidadesAtendidas[$cdResponsavel])) {

            // Percorre todas as cidades por CD
            foreach($cidadesAtendidas as $cd => $cidade) {
                // Se a cidade do endereço cadastrado existir em algum lugar (certamente em outro CD) retorna o código do CD responsável
                if(in_array($sanitizedCity, $cidade)) {
                    return (string) $cd;
                }
            }
        }
        
        return false;
    }

    /**
     * Verifica se algum produto ficará indisponível no novo CD. Se sim, retorna os produtos
     *
     * @param string $cd
     */
    public function algumProdutoVaiFicarIndisponivelNesseCD($cd) {
        $sku = null;
        $qty = null;

        // Capturar os produtos da quote atual
        $quote = $this->checkoutSession->getQuote();
        $quoteItems = $quote->getItemsCollection(false);

        // Capturar as quantidades solicitadas pelo cliente para cada produto da quote atual
        $originalQuantityPerProduct = [];
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getProductType() !== "bundle") {
                $sku = $quoteItem->getSku();
                $originalQuantityPerProduct[$sku] = (int)$quoteItem->getQty();
            }
        }

        // Capturar os estoques de cada CD para cada produto da quote atual
        $allStocksPerProduct = [];
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getProductType() !== "bundle") {
                $sku = $quoteItem->getSku();
                $sourceItemsPerSku = $this->sourceItemsBySku->execute($sku);

                foreach ($sourceItemsPerSku as $source) {
                    $allStocksPerProduct[$sku][$source->getSourceCode()] = $source->getData();
                }
            }
        }

        // Capturar a quantidade disponível de cada produto no CD de destino
        $destinationQuantityPerProduct = [];
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getProductType() !== "bundle") {
                $sku = $quoteItem->getSku();
                if (array_key_exists($cd, $allStocksPerProduct[$sku])) {
                    $qty = $allStocksPerProduct[$sku][$cd]['quantity'];
                } else {
                    $qty = 0;
                }
                $destinationQuantityPerProduct[$sku] = (int) $qty;
            }
        }

        // Se algum produto estiver sem estoque ou com estoque menor do que o cliente está solicitando, retornar esse produto
        $failedProducts = [];
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getProductType() !== "bundle") {
                $sku = $quoteItem->getSku();
                if ($destinationQuantityPerProduct[$sku] < $originalQuantityPerProduct[$sku]) {
                    $failedProducts[] = $quoteItem->getData();
                }
            }
        }

        if (!empty($failedProducts)) {
            return $failedProducts;
        }

        return false;

    }

    public function trocaDeLoja($loja) {
        // Código retirado da classe Magento\Checkout\Model\StoreSwitcher\RedirectDataPostprocessor


        // $quote = $this->checkoutSession->getQuote();
        // if ($quote->getIsActive()) {
        //     // Update quote items so that product names are updated for current store view
        //     $quote->setStoreId($context->getTargetStore()->getId());
        //     $quote->getItemsCollection(false);
        //     $this->quoteRepository->save($quote);
        // }
    }

}
