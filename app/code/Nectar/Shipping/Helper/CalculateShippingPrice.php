<?php

namespace Nectar\Shipping\Helper;

class CalculateShippingPrice
{
    /**
     * @param \Nectar\Shipping\Helper\GetAddress
     */
    private $getAddress;

    /**
     * @param \Magento\InventoryApi\Api\SourceRepositoryInterface
     */
    private $sourceRepository;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Nectar\Shipping\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Nectar\Shipping\Helper\GetAddress $getAddress,
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Nectar\Shipping\Logger\Logger $logger
    ) {
        $this->getAddress = $getAddress;
        $this->sourceRepository = $sourceRepository;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }


    public function getLatLongCustomer($address)
    {
        $lat = $address->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude;
        $long = $address->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude;
        return (["lat" => $lat, "long" => $long]);
    }

    public function getLatLongCd()
    {
        try {
            $sourceCode = $this->cidadesPorCd->getCdResponsavel();
            $sourceInfo = $this->sourceRepository->get($sourceCode);
            $lat = $sourceInfo['latitude'];
            $long = $sourceInfo['longitude'];
            return (["lat" => $lat, "long" => $long]);
        } catch (\Exception $e) {
            throw $e;
            return (["lat" => 0, "long" => 0]);
        }
    }

    public function calculaRaio($lat1, $long1, $lat2, $long2)
    {
        $distLat = $lat1 - $lat2;
        $distLong = $long1 - $long2;
        $distLatemKm = $distLat * 1852 * 60;
        $distLongemKm = $distLong * 1852 * 60;
        $raio = pow(($distLatemKm * $distLatemKm) + ($distLongemKm * $distLongemKm), 0.5);
        return ($raio);
    }

    public function calculateShippingPrice($endereco)
    {
        $address=$this->getAddress->getAddress($endereco);
        $this->logger->info(json_encode("Address"));
        $this->logger->info(json_encode($address));
        if (isset($address->Response->View[0]->Result)) {
            $custommerLatLong = $this->getLatLongCustomer($address);
            $this->logger->info(json_encode("custommerLatLong"));
            $this->logger->info(json_encode($custommerLatLong));
            $cdLatLong = $this->getLatLongCd();
            $this->logger->info(json_encode("cdLatLong"));
            $this->logger->info(json_encode($cdLatLong));
            $raio = $this->calculaRaio($custommerLatLong['lat'], $custommerLatLong['long'], $cdLatLong['lat'], $cdLatLong['long']);
            $this->logger->info(json_encode("raio"));
            $this->logger->info(json_encode($raio/1000));
            $dist = $raio / 1000;
            $raios = $this->getRaiosDisponiveis();
            $this->logger->info(json_encode("raios"));
            $this->logger->info(json_encode($raios));
            foreach($raios as $distancia)
            {
                if($dist > $distancia['raioMin'] && $dist <= $distancia['raioMax']){
                    $retorno=$distancia['frete'];
                }
            }
        }
        return (isset($retorno)?$retorno:"Desculpe, não conseguimos identificar sua cidade, favor tente novamente.");        
    }

    public function getRaiosDisponiveis()
    {
        $tabela = $this->resourceConnection->getTableName('shipping_price');
        $connection = $this->resourceConnection->getConnection();
        $cd=$this->cidadesPorCd->getCdResponsavel();

        $select = $connection->select()
            ->from(
                ['c' => $tabela],
                ['*']
            )
            ->where(
                "c.descricao = :descricao"
            );
        $bind = ['descricao' => $cd];
        $records = $connection->fetchAll($select, $bind);
        return ($records);
    }
}
