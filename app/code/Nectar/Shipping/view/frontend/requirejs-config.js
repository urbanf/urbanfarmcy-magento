var config = {
    map: {
        '*': {
            'calculate_shipping_price':'Nectar_Shipping/js/calculate-shipping-price',
            'delivery_address':'Nectar_Shipping/js/delivery-address',
            'Magento_Checkout/js/view/shipping-address/list': 'Nectar_Shipping/js/view/shipping-address-list',
            'Magento_Checkout/js/view/shipping': 'Nectar_Shipping/js/view/shipping',
            'Nectar_Shipping/js/view/store-change-action': 'Nectar_Shipping/js/view/store-change-action',
            'Magento_Checkout/js/action/select-shipping-address': 'Nectar_Shipping/js/action/select-shipping-address',
            'Magento_Checkout/js/model/checkout-data-resolver': 'Nectar_Shipping/js/model/checkout-data-resolver'
        }
    }
};