define([
    'jquery',
    'uiComponent',
    'mage/url'
], function($, Component, url) {
    'use strict';

    return Component.extend({
        /**
         * @override
         */
        initialize: function() {
            $('#calculate-shipping-price').on('submit', function(event) {
                event.preventDefault();

                let form = document.getElementById('calculate-shipping-price');
                let inputText = form.querySelector('input');

                let controllerUrl = url.build('nectarshipping/shipping/calculateShippingPrice');

                $.ajax({
                    url: controllerUrl,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        address: inputText.value
                    },
                    beforeSend: function () {
                        $('body').trigger('processStart');
                    },
                    complete: function() {
                        $('body').trigger('processStop');
                    },
                    success:function(res) {
                        let result = form.querySelector('.result');
                        result.innerText = res.message;
                    }
                });

            });
        }
    });

});