/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
 define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Nectar_Shipping/js/view/store-change-action',
    'mage/cookies'
], function ($, quote, storeChangeAction) {
    'use strict';

    return function (shippingAddress) {
        console.log("Endereço anterior");
        console.log(quote.shippingAddress());

        if(quote.shippingAddress() && quote.shippingAddress().postcode) {
            let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);
            $.mage.cookies.set(
                'urban_temporary_address_postcode',
                quote.shippingAddress().postcode,
                { expires: cookieExpires, domain: '' }
            );
        }

        quote.shippingAddress(shippingAddress);

        console.log("Endereço novo");
        console.log(quote.shippingAddress());

        storeChangeAction(shippingAddress);

    };
});
