define([
    'uiComponent',
    'jquery',
    'ko',
    'Magento_Customer/js/customer-data',
    'Magento_Catalog/js/price-utils',
    'mage/url',
    'mage/cookies'
], function (Component, $, ko, customerData, priceUtils, url) {
    'use strict';

    const globalPriceFormat = {
        decimalSymbol: ',',
        groupLength: 3,
        groupSymbol: ".",
        integerRequired: false,
        pattern: "R$%s",
        precision: 2,
        requiredPrecision: 2
    };

    return Component.extend({
        defaults: {
            template: 'Nectar_Shipping/minicart/shipping'
        },
        
        initialize: function() {
            this._super();
        },

        calculateShippingPrice: function() {
            let controllerUrl = url.build('nectarshipping/shipping/calculateShippingPrice');
            let postcode = "";

            if($.mage.cookies.get('urban_delivery_postcode') && $.mage.cookies.get('urban_delivery_postcode').length > 0) {
                postcode = $.mage.cookies.get('urban_delivery_postcode');

                $.ajax({
                    url: controllerUrl,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        address: postcode
                    },
                    success:function(res) {
                        let result = document.querySelector('.shipping-rates .totals .value');
                        result.innerText = priceUtils.formatPrice(res.message, globalPriceFormat);
                    }
                });
            } else {

                let result = document.querySelector('.shipping-rates .totals .value');
                result.innerText = "--";

            }
        },

        getPostcode: function() {
            let postcode = '';

            if($.mage.cookies.get('urban_delivery_postcode') && $.mage.cookies.get('urban_delivery_postcode').length > 0) {
                postcode = $.mage.cookies.get('urban_delivery_postcode');
            }

            let postcodeContainer = document.querySelector('.shipping-rates .postcode .value');
            postcodeContainer.innerText = postcode;
        },

        openPostcodeModal: function() {
            $('#delivery-address-modal-content').modal('openModal');
        }

    });

});
