define([
    'jquery',
    'mage/url',
    'mage/dataPost',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Ui/js/modal/modal',
    'mage/cookies'
], function ($, url, dataPost, priceUtils, selectShippingAddressAction, checkoutData) {
    'use strict';

    let shippingAddress;

    function removeAccents(str) {
        let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
        let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
        str = str.split('');
        str.forEach((letter, index) => {
            let i = accents.indexOf(letter);
            if (i != -1) {
                str[index] = accentsOut[i];
            }
        })
        return str.join('');
    }

    function changeStoreInCheckout(targetStore) {
        let controllerUrl = url.build('nectarshipping/checkout/changeStore');
        let redirectUrl = url.build('checkout');
        let postcode = shippingAddress.postcode;
        let city = shippingAddress.city;
        let sanitizedCity = removeAccents(city).replace(/\W+/g, '_').toLowerCase();
        let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

        $.mage.cookies.set(
            'urban_delivery_postcode',
            postcode,
            { expires: cookieExpires, domain: '' }
        );

        $.mage.cookies.set(
            'urban_current_store_code',
            sanitizedCity,
            { expires: cookieExpires, domain: '' }
        );

        dataPost().postData({
            action: controllerUrl,
            data: {
                store_code: targetStore,
                redirect_url: redirectUrl
            }
        });
    }

    function setupPopupContent(products) {
        let imageData = window.checkoutConfig.imageData;
        let priceFormat = window.checkoutConfig.basePriceFormat;

        let html = document.createElement('div');
        html.setAttribute('id', 'redirect-popup');
        html.classList.add('redirect-popup-content');

        let warning = document.createElement('p');
        let address = shippingAddress.street[0];

        if(shippingAddress.street[1] && shippingAddress.street[1].length > 0) {
            address += ', ' + shippingAddress.street[1];
        }

        if(shippingAddress.street[2] && shippingAddress.street[2].length > 0) {
            address += ' - ' + shippingAddress.street[2];
        }

        if(shippingAddress.street[3] && shippingAddress.street[3].length > 0) {
            address += ' - ' + shippingAddress.street[3];
        }

        address +=  ', ' + shippingAddress.city + '/' + shippingAddress.regionCode;

        warning.innerHTML = 'Alguns produtos estão indisponíveis para o endereço <strong>' + address + '</strong>';
        
        let list = document.createElement('ul');
        list.classList.add('list');

        products.forEach( (product) => {

            let item = document.createElement('li');
            item.classList.add('item');
            let productDetails = document.createElement('div');
            productDetails.classList.add('product-item-details');
            
            let productThumbnail = document.createElement('img');
            productThumbnail.classList.add('image');

            if(imageData[product.item_id] !== undefined) {
                productThumbnail.setAttribute('alt', imageData[product.item_id].alt);
                productThumbnail.setAttribute('src', imageData[product.item_id].src);
                productThumbnail.setAttribute('height', imageData[product.item_id].height);
                productThumbnail.setAttribute('width', imageData[product.item_id].width);
            }

            productDetails.innerHTML += '<strong class="product-name">' + product.name + '</strong>';
            productDetails.innerHTML += '<div class="price-wrapper"><span class="price">' + priceUtils.formatPrice(product.row_total, priceFormat) + '</span><span class="qty">' + product.qty + ' unidade(s)</span></div>';

            item.appendChild(productThumbnail);
            item.appendChild(productDetails);
            list.appendChild(item);

        });
        
        html.appendChild(warning);
        html.appendChild(list);

        return html;
    }

    function removeProducts(unavailableProducts, storeCode) {
        let controllerUrl = url.build('nectarshipping/checkout/changeStore');
        let skus = [];

        unavailableProducts.forEach( (product) => {
            skus.push(product.sku);
        });

        $.ajax({
            url: controllerUrl,
            dataType: 'json',
            type: 'POST',
            data: {
                unavailable_products: skus
            },
            beforeSend: function () {
                $('body').trigger('processStart');
            },
            complete: function() {
                $('body').trigger('processStop');
            },
            success:function(response) {
                console.log(response);

                if(response.success) {
                    changeStoreInCheckout(storeCode);
                }
            }
        });
    }

    function showPopup(unavailableProducts, storeCode) {
        let popupContent = setupPopupContent(unavailableProducts);
        
        $(popupContent).modal({
            type: 'popup',
            title: 'Produto indisponível',
            modalClass: 'redirect-popup',
            clickableOverlay: false,
            autoOpen: false,
            responsive: true,
            closed: function() {
                if(window.checkoutConfig.customerData.addresses) {
                    let tempPostcode = $.mage.cookies.get('urban_temporary_address_postcode');
                    let addressKey;

                    if(tempPostcode !== undefined) {
                        let addresses = window.checkoutConfig.customerData.addresses;
                        let addressesKeys = Object.keys(window.checkoutConfig.customerData.addresses);
    
                        addressesKeys.forEach( key => {
                            if(addresses[key].postcode === tempPostcode) {
                                addressKey = key;
                            }
                        });
                    } else {
                        addressKey = Object.keys(window.checkoutConfig.customerData.addresses)[0];
                    }
                    
                    let addressContainer = $('[data-address-id="' + addressKey + '"]');
                    let button = addressContainer.find('.action.action-select-shipping-item');
                    button.click();
                } else {
                    $('[name="postcode"]').val('').trigger('change');
                    $('[name="street[0]"]').val('').trigger('change');
                    $('[name="street[1]"]').val('').trigger('change');
                    $('[name="street[2]"]').val(' ').trigger('change');
                    $('[name="street[3]"]').val('').trigger('change');
                    $('[name="city"]').val('').trigger('change');
                    $('[name="region_id"]').val('').trigger('change');
                }
            },
            buttons: [
                {
                    text: $.mage.__('Remover produtos e continuar'),
                    class: 'action primary remove',
                    click: $.proxy(function() {
                        removeProducts(unavailableProducts, storeCode);
                    }, this)
                },
                {
                    text: $.mage.__('Adicionar mais produtos'),
                    class: 'action secondary continue',
                    click: function() {
                        window.location.href = url.build('');
                    }
                },
                {
                    text: $.mage.__('Mudar endereço'),
                    class: 'action secondary close',
                    click: function () {
                        this.closeModal();
                    }
                }
            ]
        });

        $(popupContent).modal('openModal');
    }

    function showNotAvailablePopup() {
        let popupContent = document.createElement('div');
        popupContent.innerHTML += "<p><strong>Ah não! Ainda não entregamos aí.</strong></p>";
        popupContent.innerHTML += "<p>Mas daqui a pouco a gente chega.</p>";

        console.log(window.checkoutConfig.customerData);
        
        $(popupContent).modal({
            type: 'popup',
            title: 'Será que entrega?',
            modalClass: 'nao-entrega-popup',
            clickableOverlay: false,
            autoOpen: true,
            responsive: true,
            closed: function() {
                if(window.checkoutConfig.customerData.addresses) {
                    let tempPostcode = $.mage.cookies.get('urban_temporary_address_postcode');
                    let addressKey;

                    if(tempPostcode !== undefined) {
                        let addresses = window.checkoutConfig.customerData.addresses;
                        let addressesKeys = Object.keys(window.checkoutConfig.customerData.addresses);
    
                        addressesKeys.forEach( key => {
                            if(addresses[key].postcode === tempPostcode) {
                                addressKey = key;
                            }
                        });
                    } else {
                        addressKey = Object.keys(window.checkoutConfig.customerData.addresses)[0];
                    }

                    let addressContainer = $('[data-address-id="' + addressKey + '"]');
                    let button = addressContainer.find('.action.action-select-shipping-item');
                    button.click();
                } else {
                    $('[name="postcode"]').val('').trigger('change');
                    $('[name="street[0]"]').val('').trigger('change');
                    $('[name="street[1]"]').val('').trigger('change');
                    $('[name="street[2]"]').val(' ').trigger('change');
                    $('[name="street[3]"]').val('').trigger('change');
                    $('[name="city"]').val('').trigger('change');
                    $('[name="region_id"]').val('').trigger('change');
                }
            },
            buttons: [
                {
                    text: $.mage.__('Escolher outro endereço'),
                    class: 'action secondary close',
                    click: function () {
                        this.closeModal();
                    }
                }
            ]
        });

        $(popupContent).modal('openModal');
    }

    return function (address) {

        if(window.location.href.indexOf(window.checkoutConfig.cartUrl) === -1) {

            shippingAddress = address;
            console.log("Shipping Address: " + shippingAddress);
            let city = shippingAddress != null ? shippingAddress.city : null;

            if(city) {
                let controllerUrl = url.build('nectarshipping/checkout/validateStoreChange');
        
                $.ajax({
                    url: controllerUrl,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        city: city
                    },
                    beforeSend: function () {
                        $('body').trigger('processStart');
                    },
                    complete: function() {
                        $('body').trigger('processStop');
                    },
                    success:function(response) {
                        console.log(response);
        
                        if(response.cidadeExiste === false) {
                            console.log("%c A cidade não é atendida", 'color: red');
                            showNotAvailablePopup();
                        } else {
                            if(response.deveMudarDeStore) {
                                console.log("%c Precisa mudar de loja...", 'color: blue');
            
                                if(response.deveMudarDeCd) {
                                    console.log("%c ... e precisa mudar para o CD " + response.qualCd + "...", 'color: blue');
            
                                    if(response.produtosIndisponiveisNaTrocaDeCd.length > 0) {
                                        console.log("%c ... mas nem todos os produtos estão disponíveis em " + response.qualCd, 'color: orange');
                                        showPopup(response.produtosIndisponiveisNaTrocaDeCd, response.qualStore);
                                    } else {
                                        console.log("%c ... e todos os produtos estão disponíveis em " + response.qualCd + ", pode trocar tranquilamente.", 'color: green');
                                        changeStoreInCheckout(response.qualStore);
                                    }
                                } else {
                                    console.log("%c ... mas não precisa mudar de CD, então apenas troque a loja e recarregue a página", 'color: green');
                                    changeStoreInCheckout(response.qualStore);
                                }
                            } else {
                                console.log("%c Não precisa mudar de loja.", 'color: blue');
                                let sanitizedCity = removeAccents(city).replace(/\W+/g, '_').toLowerCase();
                                let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

                                $.mage.cookies.set(
                                    'urban_delivery_postcode',
                                    shippingAddress.postcode,
                                    { expires: cookieExpires, domain: '' }
                                );
                        
                                $.mage.cookies.set(
                                    'urban_current_store_code',
                                    sanitizedCity,
                                    { expires: cookieExpires, domain: '' }
                                );
                            }
                        }
                    }
                });
            }

        }

    };

});