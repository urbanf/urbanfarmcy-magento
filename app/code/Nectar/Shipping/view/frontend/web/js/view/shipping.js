/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

 define([
    'jquery',
    'moment',
    'mage/url',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'mage/translate',
    'Nectar_Shipping/js/view/store-change-action',
    'Magento_Checkout/js/model/shipping-rate-service',
    'mage/cookies'
], function (
    $,
    moment,
    url,
    _,
    Component,
    ko,
    customer,
    addressList,
    addressConverter,
    quote,
    createShippingAddress,
    selectShippingAddress,
    shippingRatesValidator,
    formPopUpState,
    shippingService,
    selectShippingMethodAction,
    rateRegistry,
    setShippingInformationAction,
    stepNavigator,
    modal,
    checkoutDataResolver,
    checkoutData,
    registry,
    $t,
    storeChangeAction
) {
    'use strict';

    var popUp = null;

    return Component.extend({
        defaults: {
            template: 'Nectar_Shipping/shipping',
            shippingFormTemplate: 'Magento_Checkout/shipping-address/form',
            shippingMethodListTemplate: 'Magento_Checkout/shipping-address/shipping-method-list',
            shippingMethodItemTemplate: 'Magento_Checkout/shipping-address/shipping-method-item',
            imports: {
                countryOptions: '${ $.parentName }.shippingAddress.shipping-address-fieldset.country_id:indexedOptions'
            }
        },
        visible: ko.observable(!quote.isVirtual()),
        errorValidationMessage: ko.observable(false),
        isCustomerLoggedIn: customer.isLoggedIn,
        isFormPopUpVisible: formPopUpState.isVisible,
        isFormInline: addressList().length === 0,
        isNewAddressAdded: ko.observable(false),
        saveInAddressBook: 1,
        quoteIsVirtual: quote.isVirtual(),

        /**
         * @return {exports}
         */
        initialize: function () {
            var self = this,
                hasNewAddress,
                fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';

            this._super();

            if (!quote.isVirtual()) {
                stepNavigator.registerStep(
                    'shipping',
                    '',
                    $t('Shipping'),
                    this.visible, _.bind(this.navigate, this),
                    this.sortOrder
                );
            }
            checkoutDataResolver.resolveShippingAddress();

            hasNewAddress = addressList.some(function (address) {
                return address.getType() == 'new-customer-address'; //eslint-disable-line eqeqeq
            });

            this.isNewAddressAdded(hasNewAddress);

            this.isFormPopUpVisible.subscribe(function (value) {
                if (value) {
                    self.getPopUp().openModal();
                }
            });

            quote.shippingMethod.subscribe(function () {
                self.errorValidationMessage(false);
            });

            registry.async('checkoutProvider')(function (checkoutProvider) {
                var shippingAddressData = checkoutData.getShippingAddressFromData();

                if (shippingAddressData) {
                    checkoutProvider.set(
                        'shippingAddress',
                        $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                    );
                }
                checkoutProvider.on('shippingAddress', function (shippingAddrsData) {
                    if (shippingAddrsData.street && !_.isEmpty(shippingAddrsData.street[0])) {
                        checkoutData.setShippingAddressFromData(shippingAddrsData);
                    }
                });
                shippingRatesValidator.initFields(fieldsetName);
            });

            return this;
        },

        /**
         * Navigator change hash handler.
         *
         * @param {Object} step - navigation step
         */
        navigate: function (step) {
            step && step.isVisible(true);
        },

        /**
         * @return {*}
         */
        getPopUp: function () {
            var self = this,
                buttons;

            if (!popUp) {
                buttons = this.popUpForm.options.buttons;
                this.popUpForm.options.buttons = [
                    {
                        text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                        class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                        click: self.saveNewAddress.bind(self)
                    },
                    {
                        text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
                        class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',

                        /** @inheritdoc */
                        click: this.onClosePopUp.bind(this)
                    }
                ];

                /** @inheritdoc */
                this.popUpForm.options.closed = function () {
                    self.isFormPopUpVisible(false);
                };

                this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                this.popUpForm.options.keyEventHandlers = {
                    escapeKey: this.onClosePopUp.bind(this)
                };

                /** @inheritdoc */
                this.popUpForm.options.opened = function () {
                    // Store temporary address for revert action in case when user click cancel action
                    self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
                };
                popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
            }

            return popUp;
        },

        /**
         * Revert address and close modal.
         */
        onClosePopUp: function () {
            checkoutData.setShippingAddressFromData($.extend(true, {}, this.temporaryAddress));
            this.getPopUp().closeModal();
        },

        /**
         * Show address form popup
         */
        showFormPopUp: function () {
            this.isFormPopUpVisible(true);
        },

        /**
         * Save new shipping address
         */
        saveNewAddress: function () {
            var addressData,
                newShippingAddress;

            this.source.set('params.invalid', false);
            this.triggerShippingDataValidateEvent();

            if (!this.source.get('params.invalid')) {
                addressData = this.source.get('shippingAddress');
                // if user clicked the checkbox, its value is true or false. Need to convert.
                addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

                // New address must be selected as a shipping address
                newShippingAddress = createShippingAddress(addressData);
                selectShippingAddress(newShippingAddress);
                checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));

                // Enviar cidade para método que chamará um Controller
                // storeChangeAction(newShippingAddress);

                this.getPopUp().closeModal();
                this.isNewAddressAdded(true);
            }
        },

        /**
         * Shipping Method View
         */
        rates: shippingService.getShippingRates(),
        isLoading: shippingService.isLoading,
        isSelected: ko.computed(function () {
            return quote.shippingMethod() ?
                quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                null;
        }),

        /**
         * @param {Object} shippingMethod
         * @return {Boolean}
         */
        selectShippingMethod: function (shippingMethod) {
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);

            return true;
        },

        canCustomerProceed: function() {
            let shippingAddress = quote.shippingAddress();

            if(shippingAddress != null) {

                let postcode = shippingAddress.postcode;
                let controllerUrl = url.build('nectarshipping/checkout/validateShippingAddress');
                
                $.ajax({
                    url: controllerUrl,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        postcode: postcode
                    },
                    beforeSend: function () {
                        $('body').trigger('processStart');
                    },
                    complete: function() {
                        $('body').trigger('processStop');
                    },
                    success: $.proxy(function(res) {
                        if(!res.message.sucesso) {
                            let html = document.querySelector('#checkout-step-shipping .customer-can-proceed');
                            let span = html.querySelector('span');
                            html.classList.add('visible');
                            span.innerText = res.message.mensagem;
                        }
                    }, this)
                });

            }
        },

        setPostcodeFromCookie: function() {
            console.log("Loja atual: " + window.checkoutConfig.storeCode);
            console.log("O valor do campo postcode é: " + $('.form-shipping-address [name="postcode"]').val());
            if($('.form-shipping-address [name="postcode"]').val() !== undefined) {
                if($('.checkout-shipping-address .field.addresses').length === 0) {
                    let postcode = $.mage.cookies.get('urban_delivery_postcode');
                    $('.form-shipping-address [name="postcode"]').val(postcode).trigger('change').trigger('blur');
                }
            } else {
                setTimeout(this.setPostcodeFromCookie, 500);
            }
        },

        /**
         * Set shipping information handler
         */
        setShippingInformation: function () {

            let controllerUrl = url.build('nectarshipping/checkout/validateShippingAddress');
            let shippingAddress = quote.shippingAddress();
            let postcode = shippingAddress.postcode;

            $.ajax({
                url: controllerUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    postcode: postcode
                },
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                complete: function() {
                    $('body').trigger('processStop');
                },
                success: $.proxy(function(res) {
                    if(res.message.sucesso) {
                        if (this.validateShippingInformation()) {

                            let cookieExpires = new Date(new Date().getTime() + 1 * 60 * 60 * 1000);
                            let field = document.querySelector('[name="amdeliverydate_date"]');

                            $.mage.cookies.set(
                                'urban_delivery_date',
                                field.value,
                                { expires: cookieExpires, domain: '' }
                            );

                            let paymentMethod = document.querySelector('.payment-method.pagarme_billet');
                            if(paymentMethod) {
                                let today = new Date();
                                let startDate = new Date();
                                let endDate = '';
                                let decrement = 0;

                                if(moment(field.value, 'D/M/YYYY', true).isValid()) {
                                    let splitDate = field.value.split('/');
                                    let newDate = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
                                    endDate = new Date(newDate);
                                } else {
                                    endDate = new Date(field.value.replace(/-/g,'/'));
                                }
                                
                                endDate.setHours(23,59,59,999);

                                while(startDate < endDate) {
                                    if( startDate.getDay() === 6 || startDate.getDay() === 0 ) {
                                        decrement++;
                                    }
                                    startDate.setDate(startDate.getDate() + 1);
                                }

                                if(Math.abs(((endDate - today) / 86400000) - decrement) < 4) {
                                    paymentMethod.classList.add('deactivated');
                                } else {
                                    paymentMethod.classList.remove('deactivated');
                                }
                            }

                            quote.billingAddress(null);
                            checkoutDataResolver.resolveBillingAddress();
                            registry.async('checkoutProvider')(function (checkoutProvider) {
                                var shippingAddressData = checkoutData.getShippingAddressFromData();
            
                                if (shippingAddressData) {
                                    checkoutProvider.set(
                                        'shippingAddress',
                                        $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                                    );
                                }
                            });
                            setShippingInformationAction().done(
                                function () {
                                    stepNavigator.next();
                                }
                            );
                        }
                    } else {
                        let html = document.querySelector('#checkout-step-shipping .customer-can-proceed');
                        let span = html.querySelector('span');
                        html.classList.add('visible');
                        span.innerText = res.message.mensagem;
                        
                        window.scroll({
                            top: 0,
                            left: 0,
                            behavior: 'smooth'
                        });
                    }
                }, this)
            });
            
        },

        /**
         * @return {Boolean}
         */
        validateShippingInformation: function () {
            var shippingAddress,
                addressData,
                loginFormSelector = 'form[data-role=email-with-possible-login]',
                emailValidationResult = customer.isLoggedIn(),
                field,
                option = _.isObject(this.countryOptions) && this.countryOptions[quote.shippingAddress().countryId],
                messageContainer = registry.get('checkout.errors').messageContainer;

            if (!quote.shippingMethod()) {
                this.errorValidationMessage(
                    $t('The shipping method is missing. Select the shipping method and try again.')
                );

                return false;
            }

            if (!customer.isLoggedIn()) {
                $(loginFormSelector).validation();
                emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
            }

            if (this.isFormInline) {
                this.source.set('params.invalid', false);
                this.triggerShippingDataValidateEvent();

                if (!quote.shippingMethod()['method_code']) {
                    this.errorValidationMessage(
                        $t('The shipping method is missing. Select the shipping method and try again.')
                    );
                }

                if (emailValidationResult &&
                    this.source.get('params.invalid') ||
                    !quote.shippingMethod()['method_code'] ||
                    !quote.shippingMethod()['carrier_code']
                ) {
                    this.focusInvalid();

                    return false;
                }

                shippingAddress = quote.shippingAddress();
                addressData = addressConverter.formAddressDataToQuoteAddress(
                    this.source.get('shippingAddress')
                );

                //Copy form data to quote shipping address object
                for (field in addressData) {
                    if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
                        shippingAddress.hasOwnProperty(field) &&
                        typeof addressData[field] != 'function' &&
                        _.isEqual(shippingAddress[field], addressData[field])
                    ) {
                        shippingAddress[field] = addressData[field];
                    } else if (typeof addressData[field] != 'function' &&
                        !_.isEqual(shippingAddress[field], addressData[field])) {
                        shippingAddress = addressData;
                        break;
                    }
                }

                if (customer.isLoggedIn()) {
                    shippingAddress['save_in_address_book'] = 1;
                }
                selectShippingAddress(shippingAddress);
            } else if (customer.isLoggedIn() &&
                option &&
                option['is_region_required'] &&
                !quote.shippingAddress().region
            ) {
                messageContainer.addErrorMessage({
                    message: $t('Please specify a regionId in shipping address.')
                });

                return false;
            }

            if (!emailValidationResult) {
                $(loginFormSelector + ' input[name=username]').focus();

                return false;
            }

            return true;
        },

        /**
         * Trigger Shipping data Validate Event.
         */
        triggerShippingDataValidateEvent: function () {
            this.source.trigger('shippingAddress.data.validate');

            if (this.source.get('shippingAddress.custom_attributes')) {
                this.source.trigger('shippingAddress.custom_attributes.data.validate');
            }
        }
    });
});
