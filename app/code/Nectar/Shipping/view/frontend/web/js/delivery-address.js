define([
    'jquery',
    'uiComponent',
    'mage/url',
    'mage/cookies',
    'Inputmask',
    'Magento_Ui/js/modal/modal'
], function($, Component, url) {
    'use strict';

    const removeAccents = function(str) {
        let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
        let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
        str = str.split('');
        str.forEach((letter, index) => {
            let i = accents.indexOf(letter);
            if (i != -1) {
                str[index] = accentsOut[i];
            }
        })
        return str.join('');
    };

    return Component.extend({
        options: {
            modalContentSelector: 'delivery-address-modal-content',
            modalInitialStepSelector: '[data-step="initial"]',
            modalSuccessStepSelector: '[data-step="success"]',
            modalFailureStepSelector: '[data-step="failure"]',
            cookieName: 'urban_delivery_postcode',
            cookieLifetime: 1,
            config: null,
        },

        /**
         * @override
         */
        initialize: function(config) {
            // Inicializar variáveis
            this.options.config = config;

            if(config.modalOpenerSelector !== undefined) {
                this.options.modalOpenerSelector = config.modalOpenerSelector;
            }

            $.proxy(this._setCurrentValues(), this);
            $.proxy(this._createModal(), this);
            $.proxy(this._initializeSteps(), this);
        },

        _setCurrentValues: function() {

            if($.mage.cookies.get(this.options.cookieName) && $.mage.cookies.get(this.options.cookieName).length > 0) {
                let postcode = $.mage.cookies.get(this.options.cookieName);
                document.getElementById('delivery-address-modal-opener').querySelector('span').innerText = postcode;
                document.getElementById('delivery-address-postcode').value = postcode;
                this._changeStore();
            } else {
                let container = document.querySelector('.page-header .delivery-address');
                let postcode = container.getAttribute('data-current-customer-postcode');
                let city = container.getAttribute('data-current-customer-city');
                let sanitizedCity = removeAccents(city).replace(/\W+/g, '_').toLowerCase();

                if(postcode) {
                    let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

                    $.mage.cookies.set(
                        'urban_delivery_postcode',
                        postcode,
                        { expires: cookieExpires, domain: '' }
                    );

                    $.mage.cookies.set(
                        'urban_current_store_code',
                        sanitizedCity,
                        { expires: cookieExpires, domain: '' }
                    );

                    document.getElementById('delivery-address-modal-opener').querySelector('span').innerText = postcode;
                    document.getElementById('delivery-address-postcode').value = postcode;

                    this._changeStore();
                }
            }

        },

        _createModal: function() {
            $("#" + this.options.modalContentSelector).modal({
                type: 'popup',
                title: '',
                modalClass: 'delivery-address-popup',
                clickableOverlay: true,
                autoOpen: false,
                trigger: "#" + this.options.modalOpenerSelector,
                responsive: true,
                buttons: [],
                closed: $.proxy(function() {
                    this._changeStore();
                }, this),
                opened: $.proxy(function() {
                    let modalContent = document.getElementById(this.options.modalContentSelector);
                    let input = modalContent.querySelector('[name="delivery-address-postcode"]');
                    input.focus();
                }, this)
            });

            $("#" + this.options.modalContentSelector).on('modalclosed', this._changeStore);
        },

        _initializeSteps: function() {
            let modalContent = document.getElementById(this.options.modalContentSelector);
            let deliveryAddressForm = modalContent.querySelector('.form');
            let submitButton = deliveryAddressForm.querySelector('button[type="submit"]');
            let initialStep = modalContent.querySelector(this.options.modalInitialStepSelector);

            initialStep.classList.add('active');

            Inputmask({
                'mask': '99999-999',
                'numericInput': true,
                'oncomplete': function() {
                    submitButton.removeAttribute('disabled');
                },
                'onincomplete': function() {
                    submitButton.setAttribute('disabled', 'disabled');
                },
                onBeforeMask: function (value, opts) {
                    if(value.length > 8) {
                        submitButton.removeAttribute('disabled');
                    }
                }
            }).mask('[name="delivery-address-postcode"]');

            deliveryAddressForm.addEventListener('submit', $.proxy(this._searchAddressByPostcode, this));

            let failureStep = modalContent.querySelector(this.options.modalFailureStepSelector);
            let gobackButton = failureStep.querySelector('.go-back');

            gobackButton.addEventListener('click', function(event) {
                event.preventDefault();

                failureStep.classList.remove('active');
                initialStep.classList.add('active');
            });
        },

        _searchAddressByPostcode: function(event) {
            event.preventDefault();

            let form = event.target;
            let postcodeInput = form.querySelector('[name="delivery-address-postcode"]');
            let postcode = postcodeInput.value;
            let controllerUrl = url.build('nectarshipping/shipping/getDeliveryAddress');

            $.ajax({
                url: controllerUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    postcode: postcode
                },
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                complete: function() {
                    $('body').trigger('processStop');
                },
                success:  $.proxy(function (response) {
                    let address = response.message.Response.View[0].Result[0].Location.Address;
                    let cities = JSON.parse(response.cities);

                    $.ajax({
                        url: 'https://viacep.com.br/ws/' + postcode + '/json/',
                        type: 'GET',
                        beforeSend: function () {
                            $('body').trigger('processStart');
                        },
                        complete: function() {
                            $('body').trigger('processStop');
                        },
                        success: $.proxy(function(response) {
                            let neighborhood = response.bairro;
                            let street = response.logradouro;
                            let city = response.localidade;
                            let sanitizedCity = removeAccents(city).replace(/\W+/g, '_').toLowerCase();
                            let modalContent = document.getElementById(this.options.modalContentSelector);
                            let initialStep = modalContent.querySelector(this.options.modalInitialStepSelector);

                            initialStep.classList.remove('active');

                            let hasCity = (cities.indexOf(sanitizedCity) > -1);

                            if(hasCity) { // Se houver uma cidade cadastrada
                                let state = address.State; 
                                let successStep = modalContent.querySelector(this.options.modalSuccessStepSelector);
                                let continueButton = successStep.querySelector('.action.continue');
                                let modal = $("#" + this.options.modalContentSelector);

                                successStep.classList.add('active');
            
                                successStep.querySelector('.cidade').innerText = city;
                                successStep.querySelector('.estado').innerText = state;

                                let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

                                $.mage.cookies.set(
                                    'urban_delivery_postcode',
                                    postcode,
                                    { expires: cookieExpires, domain: '' }
                                );

                                $.mage.cookies.set(
                                    'urban_current_store_code',
                                    sanitizedCity,
                                    { expires: cookieExpires, domain: '' }
                                );

                                $.mage.cookies.set(
                                    'urban_current_city',
                                    city + ' - ' + state,
                                    { expires: cookieExpires, domain: '' }
                                );

                                successStep.querySelector('.bairro').innerText = neighborhood;
                                successStep.querySelector('.logradouro').innerText = street;
                                successStep.querySelector('.address').classList.add('active');
                                successStep.querySelector('.loader').classList.add('hidden');

                                continueButton.addEventListener('click', function(event) {
                                    event.preventDefault();

                                    modal.modal('closeModal');
                                    successStep.classList.remove('active');
                                    initialStep.classList.add('active');
                                });
            
                            } else {
                                let failureStep = modalContent.querySelector(this.options.modalFailureStepSelector);
                                let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

                                $.mage.cookies.set(
                                    'urban_delivery_postcode', '',{ expires: cookieExpires, domain: '' }
                                );

                                $.mage.cookies.set(
                                    'urban_current_store_code', '', { expires: cookieExpires, domain: '' }
                                );

                                $.mage.cookies.set(
                                    'urban_current_city', '', { expires: cookieExpires, domain: '' }
                                );

                                failureStep.classList.add('active');
                            }
                            
                            
                        }, this)
                    });
                    
                }, this),
                error: $.proxy(function(xhr, textStatus, error){
                    
                    let modalContent = document.getElementById(this.options.modalContentSelector);
                    let initialStep = modalContent.querySelector(this.options.modalInitialStepSelector);
                    let failureStep = modalContent.querySelector(this.options.modalFailureStepSelector);

                    initialStep.classList.remove('active');
                    failureStep.classList.add('active');
                    
                }, this)

            });

        },

        _changeStore: function() {
            let storeCode = $.mage.cookies.get('urban_current_store_code');
            // let clearCart = false;

            if($.mage.cookies.get('urban_current_store_code')) {
                // Checa se a loja do cookie é a loja atual
                if(document.querySelector('.switcher-trigger').getAttribute('data-store-code') !== storeCode) {
                    // clearCart = true;

                    if(!document.querySelector('.switcher-option [data-store-code="' + storeCode + '"]')) {
                        storeCode = 'main_website_store';
                    }
                }
            } else {
                storeCode = 'main_website_store';
            }

            // if(clearCart) {
            //     let controllerUrl = url.build('nectarstore/cart/emptyCart');

            //     $.ajax({
            //         url: controllerUrl,
            //         type: 'POST',
            //         beforeSend: function () {
            //             $('body').trigger('processStart');
            //         },
            //         complete: function() {
            //             $('body').trigger('processStop');
            //         },
            //         success: function(response) {
            //             $('.switcher-option [data-store-code="' + storeCode + '"]').click();
            //         }
            //     });

            // } else {
                $('.switcher-option [data-store-code="' + storeCode + '"]').click();
            // }

        },

    });

});