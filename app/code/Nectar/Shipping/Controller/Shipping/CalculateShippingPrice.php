<?php
namespace Nectar\Shipping\Controller\Shipping;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class CalculateShippingPrice extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Nectar\Shipping\Helper\CalculateShippingPrice
     */
    private $calculateShippingPrice;

    protected $context;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Nectar\Shipping\Helper\CalculateShippingPrice $calculateShippingPrice
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->context = $context;
        $this->calculateShippingPrice = $calculateShippingPrice;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();

        $address = $requestParams['address'];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["message" => $this->calculateShippingPrice->calculateShippingPrice($address)]);
        
        return $resultJson;
    }
}
