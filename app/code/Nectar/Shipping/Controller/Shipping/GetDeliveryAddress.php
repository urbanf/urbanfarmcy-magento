<?php

namespace Nectar\Shipping\Controller\Shipping;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class GetDeliveryAddress extends \Magento\Framework\App\Action\Action
{
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    protected $context;

    /**
     * @param \Magento\Framework\View\Result\PageFactory
     */
    private $_pageFactory;

    /**
     * @param \Nectar\Shipping\Helper\GetAddress
     */
    private $getAddress;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Nectar\Shipping\Helper\GetAddress $getAddress
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Nectar\Shipping\Helper\GetAddress $getAddress,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->getAddress = $getAddress;
        $this->cidadesPorCd = $cidadesPorCd;
        return parent::__construct($context);
    }

    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $postcode = $requestParams['postcode'];

        $address = $this->getAddress->getAddress($postcode);

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["message" => $address, "cities" => $this->getAvailableCities()]);
        
        return $resultJson;
    }

    protected function getAvailableCities() {
        $cdsDisponiveis = [];

        foreach ($this->cidadesPorCd->getCidadesAtendidas() as $cd => $cidades){
            foreach($cidades as $cidade){
                $cdsDisponiveis[] = $cidade;
            }
        }

        return json_encode($cdsDisponiveis);
    }

}
