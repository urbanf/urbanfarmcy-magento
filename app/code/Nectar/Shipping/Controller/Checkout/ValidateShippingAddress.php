<?php

namespace Nectar\Shipping\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class ValidateShippingAddress extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Nectar\Shipping\Helper\ValidateShippingAddress
     */
    private $validateShippingAddress;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Nectar\Shipping\Helper\ValidateShippingAddress $validateShippingAddress
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->validateShippingAddress = $validateShippingAddress;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $postcode = $requestParams['postcode'];

        $canCustomerProceed = $this->validateShippingAddress->canCustomerProceed($postcode);

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["message" => $canCustomerProceed]);
        
        return $resultJson;
    }
}
