<?php

namespace Nectar\Shipping\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class ValidateStoreChange extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Nectar\Shipping\Helper\ValidateStoreChange
     */
    private $validateStoreChange;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Nectar\Shipping\Helper\ValidateStoreChange $validateStoreChange
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->validateStoreChange = $validateStoreChange;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $city = $requestParams['city'];

        $cidadeExiste = false;
        $deveMudarDeStore = false;
        $deveMudarDeCd = false;
        $produtosIndisponiveisNaTrocaDeCd = null;
        $qualStore = null;

        $cidadeExiste = $this->validateStoreChange->cidadeExiste($city);

        if($cidadeExiste) {
            $deveMudarDeStore = $this->validateStoreChange->deveMudarDeStore($city);

            setlocale(LC_ALL, 'en_US.UTF-8');
            $qualStore = strtolower(str_replace(' ', '_', iconv('UTF-8', 'ASCII//TRANSLIT', $city)));
        }
        
        if($deveMudarDeStore) {
            $deveMudarDeCd = $this->validateStoreChange->deveMudarDeCd($city);
        }

        // Se tiver que mudar de CD, eu preciso puxar as informações desse CD
        if((bool)$deveMudarDeCd) {
            // Verifica se algum produto vai ficar indisponível ao trocar de CD
            $produtosIndisponiveisNaTrocaDeCd = $this->validateStoreChange->algumProdutoVaiFicarIndisponivelNesseCD($deveMudarDeCd);
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData([
            "cidadeExiste" => $cidadeExiste,
            "deveMudarDeStore" => $deveMudarDeStore,
            "qualStore" => $qualStore,
            "deveMudarDeCd" => (bool) $deveMudarDeCd,
            "qualCd" => $deveMudarDeCd,
            "produtosIndisponiveisNaTrocaDeCd" => $produtosIndisponiveisNaTrocaDeCd
        ]);
        
        return $resultJson;
    }
}
