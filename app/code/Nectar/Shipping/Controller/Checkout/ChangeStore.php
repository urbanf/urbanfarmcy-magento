<?php

namespace Nectar\Shipping\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Store\Api\StoreResolverInterface;

class ChangeStore extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Magento\Framework\Url\Helper\Data
     */
    private $urlHelper;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\Store\Model\StoreSwitcher\RedirectDataGenerator
     */
    private $redirectDataGenerator;

    /**
     * @param \Magento\Store\Model\StoreSwitcher\ContextInterfaceFactory
     */
    private $contextFactory;

    /**
     * @param \Magento\Store\Api\StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @param \Magento\Framework\UrlInterface
     */
    private $urlInterface;

    /**
     * @param \Magento\Quote\Model\Quote
     */
    private $quote;

    /**
     * @param \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\StoreSwitcher\RedirectDataGenerator $redirectDataGenerator,
        \Magento\Store\Model\StoreSwitcher\ContextInterfaceFactory $contextFactory,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->urlHelper = $urlHelper;
        $this->storeManager = $storeManager;
        $this->redirectDataGenerator = $redirectDataGenerator;
        $this->contextFactory = $contextFactory;
        $this->storeRepository = $storeRepository;
        $this->urlInterface = $urlInterface;
        $this->quote = $quote;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();

        if(isset($requestParams['unavailable_products'])) {
            $response = $this->removeProductsFromQuote($requestParams['unavailable_products']);

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(["success" => true, "message" => $response]);
            
            return $resultJson;
        } else {
            $storeCode = $requestParams['store_code'];
            $redirectUrl = $requestParams['redirect_url'];

            $this->changeStore($storeCode, $redirectUrl);
        }

    }

    public function changeStore($storeCode, $redirectUrl) {
        $currentStore = $this->storeManager->getStore();
        $currentStoreCode = $currentStore->getCode();
        $fromStore = $this->storeRepository->get($currentStoreCode);
        $targetStore = $this->storeRepository->get($storeCode);

        $urlOnTargetStore = $redirectUrl . '?___store=' . $storeCode;
        $encodedUrl = $this->urlHelper->getEncodedUrl($urlOnTargetStore);

        $redirectData = $this->redirectDataGenerator->generate(
            $this->contextFactory->create(
                [
                    'fromStore' => $fromStore,
                    'targetStore' => $targetStore,
                    'redirectUrl' => $this->_redirect->getRedirectUrl()
                ]
            )
        );

        $query = [
            '___from_store' => $currentStoreCode,
            StoreResolverInterface::PARAM_NAME => $storeCode,
            ActionInterface::PARAM_NAME_URL_ENCODED => $encodedUrl,
            'data' => $redirectData->getData(),
            'time_stamp' => $redirectData->getTimestamp(),
            'signature' => $redirectData->getSignature(),
        ];

        $arguments = [
            '_nosid' => true,
            '_query' => $query
        ];

        $this->_redirect->redirect($this->_response, 'stores/store/switch', $arguments);

        return null;
    }

    public function removeProductsFromQuote($productSkus) {
        $quote = $this->checkoutSession->getQuote();
        $allItems = $quote->getAllItems();
        $bundleItems = [];

        foreach ($allItems as $item) {
            $sku = $item->getSku();
            if (in_array($sku, $productSkus)) {
                if ($item->getParentItem() === null) {
                    $quote->deleteItem($item);
                } else {
                    $bundleItems[$item->getParentItem()->getSku()] = $item->getParentItem();
                }
            }
        }
        array_unique($bundleItems);

        foreach ($bundleItems as $bundleItem) {
            $quote->deleteItem($bundleItem);
        }

        $quote->save();
        return $quote->getAllItems();
    }

}
