<?php

namespace Nectar\Shipping\Plugin\Model;

class Method
{

    /**
     * @param \Magento\Quote\Model\Quote
     */
    private $quote;

    /**
     * @param \Nectar\Shipping\Helper\CalculateShippingPrice
     */
    private $calculateShippingPrice;

    /**
     * @param \Nectar\Shipping\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Magento\Quote\Model\Quote $quote,
        \Nectar\Shipping\Helper\CalculateShippingPrice $calculateShippingPrice,
        \Nectar\Shipping\Logger\Logger $logger
    ) {
        $this->quote = $quote;
        $this->calculateShippingPrice = $calculateShippingPrice;
        $this->logger = $logger;
    }

    public function beforeSetPrice(
        \Magento\Quote\Model\Quote\Address\RateResult\Method $subject,
        $price
    ) {
        if ($price == 0) {
            $this->logger->info('Grátis');
            return ($price);
        } else {
            $address = $this->quote->getShippingAddress();
            $rua = $address->getStreetFull();
            if (!is_null($rua)) {
                $price = $this->calculateShippingPrice->calculateShippingPrice($rua);
                $this->logger->info($rua);
            } elseif (isset($_COOKIE['urban_delivery_postcode'])) {
                $this->logger->info('Cookie');
                $price = $this->calculateShippingPrice->calculateShippingPrice($_COOKIE['urban_delivery_postcode']);
                $this->logger->info($price);
            }
            return [$price];
        }
    }
}
