<?php
namespace Nectar\ShippingMenu\Cron;

class AtualizaOrders
{
    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderGroups
     */
    private $getOrderGroups;

    /**
     * @param \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @param \Nectar\ShippingMenu\Helper\ShipGroupOrders
     */
    private $shipGroupOrders;

    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Nectar\ShippingMenu\Helper\GetOrderGroups $getOrderGroups,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Nectar\ShippingMenu\Helper\ShipGroupOrders $shipGroupOrders,
        \Nectar\ShippingMenu\Logger\Logger $logger
    )
    {
        $this->getOrderGroups = $getOrderGroups;
        $this->orderFactory = $orderFactory;
        $this->shipGroupOrders = $shipGroupOrders;
        $this->logger = $logger;
        
    }

    public function execute()
    {
        $orderGroups=$this->getOrderGroups->getOrderGroups();
        foreach($orderGroups as $group) {
            foreach($group['orders'] as $orderData) {
                $increment = $orderData['id_Magento'];
                $order = $this->orderFactory->create()->load($increment, "increment_id");
                if (
                    $order->getStatus() != "complete" &&
                    $order->getStatus() != "closed" &&
                    $order->getStatus() != $group['status']
                ) {
                    $this->logger->info('Atualizando status da order ' . $order->getIncrementId() . " de: " . $order->getStatus() . " para: " . $group['status']);
                    $this->shipGroupOrders->changeStatus($order,$group['status']);
                }
            }
        }
    }
}
