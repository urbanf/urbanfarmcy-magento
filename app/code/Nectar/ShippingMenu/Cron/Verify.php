<?php

namespace Nectar\ShippingMenu\Cron;

use Exception;

class Verify
{
    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderGroups
     */
    private $getOrderGroups;

    /**
     * @param  \Magento\Sales\Model\ResourceModel\Order\CollectionFactory 
     */
    private $collectionFactory;

    /**
     * @param \Nectar\ShippingMenu\Helper\ShipGroupOrders
     */
    private $shipGroupOrders;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    /**
     * @param \Nectar\ShippingMenu\Helper\SetGroupOrdersData
     */
    private $setGroupOrdersData;

    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    public function __construct(
        \Nectar\ShippingMenu\Helper\GetOrderGroups $getOrderGroups,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory  $collectionFactory,
        \Nectar\ShippingMenu\Helper\ShipGroupOrders $shipGroupOrders,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nectar\ShippingMenu\Logger\Logger $logger,
        \Nectar\ShippingMenu\Helper\SetGroupOrdersData $setGroupOrdersData,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        $this->getOrderGroups = $getOrderGroups;
        $this->collectionFactory = $collectionFactory;
        $this->shipGroupOrders = $shipGroupOrders;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->setGroupOrdersData = $setGroupOrdersData;
        $this->curl = $curl;
    }

    public function execute()
    {
        try {
            $this->logger->info('Chegou na cron Verify!');
            $groups = $this->getOrderGroups->getOrderGroups();

            foreach ($groups as $group) {

                if($group['status'] != "motoboys_4") {
                    //add rotina que verifica na motoboys.com o status e altera ele
                    $urlBase = $this->scopeConfig->getValue("integrations/motoboys/acompanhar");
                    $chave = $this->scopeConfig->getValue("integrations/motoboys/chave");
                    $url = $urlBase . "entrega=" . $group['id'] . "&datasCompletas=true";

                    $this->curl->addHeader('access_token', $chave);
                    $this->curl->post($url, "");
                    $status = $this->curl->getStatus();

                    if ($status != 200) {
                        throw new Exception("Erro ao verificar status do grupo. (codigo: " . $status . ")");
                    } else {
                        $response = $this->curl->getBody();
                        $resposta = json_decode($response);

                        if ($resposta->success == false) {
                            throw new Exception(isset($resposta->msg) ? $resposta->msg : "Falha na integração com motoboys.com");
                        } else {
                            $groupStatus = "motoboys_" . $resposta->status->id;
                            $this->logger->info($url);
                            $this->logger->info($response);
                            $this->logger->info($group['status']);
                            $this->logger->info($groupStatus);

                            if ($groupStatus != $group['status']) {
                                $this->setGroupOrdersData->setStatus($group['id'], $groupStatus);

                                $increments = [];
                                foreach ($group['orders'] as $ord) {
                                    $increments[] = $ord['id_Magento'];
                                }

                                $collection = $this->collectionFactory->create()  //traz todas as orders com os increments
                                    ->addAttributeToSelect('*')
                                    ->addFieldToFilter(
                                        'increment_id',
                                        ['in' => $increments]
                                    )
                                    ->setOrder('created_at', 'desc');

                                foreach ($collection as $order) {
                                    $this->shipGroupOrders->changeStatus($order, $groupStatus);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
