<?php

namespace Nectar\ShippingMenu\Controller\Adminhtml\Entregas;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Send extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Nectar\ShippingMenu\Helper\SendOrderGroup
     */
    private $sendOrderGroup;

    /**
     * @param \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Nectar\ShippingMenu\Helper\SendOrderGroup $sendOrderGroup,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->sendOrderGroup = $sendOrderGroup;
        $this->messageManager = $messageManager;
        return parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $orders = $requestParams['orders'];

        $response = $this->sendOrderGroup->send($orders);

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["message" => $response]);
        
        return $resultJson;
    }

    /**
     * Is the user allowed to view the page.
    *
    * @return bool
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Nectar_ShippingMenu::entregas_lista');
    }
}
