<?php

namespace Nectar\ShippingMenu\Controller\Adminhtml\Entregas;

class Index extends \Magento\Backend\App\Action
{
        protected $resultPageFactory = false;
        public function __construct(
                \Magento\Backend\App\Action\Context $context,
                \Magento\Framework\View\Result\PageFactory $resultPageFactory
        ) {
                parent::__construct($context);
                $this->resultPageFactory = $resultPageFactory;
        }

        public function execute()
        {
                $resultPage = $this->resultPageFactory->create();
                $resultPage->setActiveMenu('Nectar_ShippingMenu::entregas_lista');
                $resultPage->getConfig()->getTitle()->prepend(__('Entregas Urban Farmcy'));
                return $resultPage;
        }
        protected function _isAllowed()
        {
                return $this->_authorization->isAllowed('Nectar_ShippingMenu::entregas_lista');
        }
}
