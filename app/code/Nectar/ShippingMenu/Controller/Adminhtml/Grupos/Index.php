<?php

namespace Nectar\ShippingMenu\Controller\Adminhtml\Grupos;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Nectar_ShippingMenu::entregas_grupos');
        $resultPage->getConfig()->getTitle()->prepend(__('Grupos de Entrega Urban Farmcy'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Nectar_ShippingMenu::entregas_grupos');
    }
}
