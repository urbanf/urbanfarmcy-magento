<?php

namespace Nectar\ShippingMenu\Controller\Adminhtml\Grupos;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Ship extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;
    
    /**
     * @param \Nectar\ShippingMenu\Helper\ShipGroupOrders
     */
    private $shipGroupOrders;

    /**
     * @param \Magento\Sales\Model\OrderRepository
     */
    private $orderRepository;

    /**
     * @param \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @param \Nectar\ShippingMenu\Cron\Verify
     */
    private $verify;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Nectar\ShippingMenu\Helper\ShipGroupOrders $shipGroupOrders,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Nectar\ShippingMenu\Cron\Verify $verify
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->shipGroupOrders = $shipGroupOrders;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->verify = $verify;
    }

    public function execute()
    {
        // $order = $this->orderRepository->get(41);

        // $response = $this->shipGroupOrders->changeStatus($order, "motoboys_4");
        // $this->verify->execute();
        // $response = "oi";

        // $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        // $resultJson->setData($response);
        
        // return $resultJson;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Nectar_ShippingMenu::entregas_grupos');
    }
}
