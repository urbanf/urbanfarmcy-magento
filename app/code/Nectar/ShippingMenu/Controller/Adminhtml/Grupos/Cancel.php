<?php

namespace Nectar\ShippingMenu\Controller\Adminhtml\Grupos;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Cancel extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @param \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @param \Nectar\ShippingMenu\Helper\DeleteOrderGroup
     */
    private $deleteOrderGroup;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Nectar\ShippingMenu\Helper\DeleteOrderGroup $deleteOrderGroup
    )
    {
        $this->context = $context;
        $this->_pageFactory = $pageFactory;
        $this->messageManager = $messageManager;
        $this->deleteOrderGroup = $deleteOrderGroup;
        return parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $requestParams = $this->context->getRequest()->getParams();
        $groupId = $requestParams['groupId'];

        $response = $this->deleteOrderGroup->deleteGroup($groupId * 1);

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["message" => $response]);
        
        return $resultJson;
    }

}
