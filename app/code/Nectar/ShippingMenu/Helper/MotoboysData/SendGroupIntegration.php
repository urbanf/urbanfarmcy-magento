<?php

namespace Nectar\ShippingMenu\Helper\MotoboysData;

class SendGroupIntegration
{
    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderData
     */
    private $getOrderData;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    /**
     * @param \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode
     */
    private $getCityCode;

    public function __construct(
        \Nectar\ShippingMenu\Helper\GetOrderData $getOrderData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Nectar\ShippingMenu\Logger\Logger $logger,
        \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode $getCityCode
    ) {
        $this->getOrderData = $getOrderData;
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->logger = $logger;
        $this->getCityCode = $getCityCode;
    }

    public function sendOrders($orders, $code, $sourceInfo, $increments)
    {
        $i = 0;
        foreach ($increments as $increment) {
            foreach ($orders as $order) {
                if ($increment == $order->id_Magento) {
                    $dados[] = $order; //ordena os pedidos do grupo conforme o vetor increments
                }
            }
        }
        if (sizeof($dados) != sizeof($orders)) {
            return ["status" => false, "message" => "Orders não foram encontradas"];
        }

        $urlBase = $this->scopeConfig->getValue("integrations/motoboys/criar");
        $url = $urlBase . $this->montaRequest($dados, $code, $sourceInfo);
        $url = str_replace(" ", "%20", $url);
        $chave = $this->scopeConfig->getValue("integrations/motoboys/chave");
        $this->curl->addHeader('access_token', $chave);
        $this->curl->post($url, "");
        $status = $this->curl->getStatus();
        $response = $this->curl->getBody();
        $this->logger->info($url);
        $this->logger->info($response);
        $resposta = json_decode($response);
        $retorno = [$resposta, $status];
        return ($retorno);
    }

    public function montaRequest($dados, $code, $sourceInfo)
    {
        $request = "cidade=" . $code . "&forma_pagamento=FATURADO&necessidade_entrega=AGENDAMENTO&";
        $CDcep = preg_replace("/[^0-9]/", "", $sourceInfo->getPostCode() . "");
        $rua = explode(',', $sourceInfo->getStreet());
        $CDrua = $this->getCityCode->retiraAcento($rua[0]);
        $CDnumero = $rua[1];
        $requestp2 = 'endereco1_cep=' . $CDcep . '&endereco1_rua=' . $CDrua . '&endereco1_numero=' . $CDnumero;
        $this->logger->info("endereco1_cep=" . $CDcep);
        $this->logger->info("endereco1_rua=" . $CDrua);
        $this->logger->info("endereco1_numero=" . $CDnumero);
        $requestp3 = "";
        for ($i = 0; $i < sizeof($dados); $i++) {
            $j = $i + 2;
            $complemento= (isset($dados[$i]->rua[2]) && $dados[$i]->rua[2] != "" && $dados[$i]->rua[2] != "-") ? "&endereco" . $j . "_complemento=" . $this->getCityCode->retiraAcento($dados[$i]->rua[2]) : "" ;
            $this->logger->info($complemento);
            $requestp3 = $requestp3 . "&endereco" . $j . "_cep=" . preg_replace("/[^0-9]/", "", $dados[$i]->cep) . "&endereco" . $j . "_rua=" . $this->getCityCode->retiraAcento($dados[$i]->rua[0]) . "&endereco" . $j . "_numero=" . $dados[$i]->rua[1] . $complemento .  "&endereco" . $j . "_nome=" . $this->getCityCode->retiraAcento($dados[$i]->nome) .  "&endereco" . $j . "_telefone=" . $this->getCityCode->retiraAcento($dados[$i]->telefone) . "&endereco" . $j . "_comentario=" . $this->getCityCode->retiraAcento($dados[$i]->comentario);
        }

        $request = $request . $requestp2 . $requestp3 . '&datahora_agendamento=' . $dados[0]->data . "-" . substr($dados[0]->hora, 0, 5);
        return (($request));
    }
}
