<?php

namespace Nectar\ShippingMenu\Helper\MotoboysData;

class GetCityCode
{
    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    public function __construct(
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd
    ) {
        $this->cidadesPorCd = $cidadesPorCd;
    }

    public function getStoreCode($order) //formata o nome da store onde foi feito o pedido para o codigo ex: São Paulo para sao_paulo e retorna o codigo
    {
        $storeName = $order->getStoreName();
        $stores = explode("\n", $storeName);
        $cityname =  explode(" ", $stores[1]);
        $cityCode = $cityname[0];
        for ($i = 1; $i < sizeof($cityname); $i++) {
            $cityCode = $cityCode . "_" . $cityname[$i];
        }
        $cityCode = $this->retiraAcento($cityCode);
        $cityCode = strtolower($cityCode);
        return ($cityCode);
    }

    public function retiraAcento($string)
    {
        $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
        $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U');
        return (str_replace($comAcentos, $semAcentos, $string));
    }

    public function getCd($codigo)
    {
        $cdsDisponiveis = $this->cidadesPorCd->getCds(); //traz os cds disponíveis 
        $cidadesAtendidas = $this->cidadesPorCd->getCidadesAtendidas(); //traz todas as cidades separadas por cd que as atende (o cd é o indice do vetor, ver __construct do arquivo \Cds\Helper\CidadesPorCd)
        for ($i = 0; $i < sizeof($cidadesAtendidas); $i++) { //percorre o vetor de cds
            if (in_array(str_replace("_", " ", $codigo), str_replace("_", " ", $cidadesAtendidas[$i]))) { //confere se dentro do vetor de cidades do cd esta a cidade da loja atual
                $cdResponsavel = $cdsDisponiveis[$i]; //caso a loja esteja salva o nome do cd responsavel
            }
        }
        return (isset($cdResponsavel) ? $cdResponsavel : false);
    }

    public function getCode($storeInfo, $cd = false)
    {
        if ($cd != false) {
            $region = $storeInfo->getRegion();
            $estados = $this->getEstados();
            foreach ($estados as $sigla => $nome) {
                if ($nome == $region) {
                    $retorno = strtolower($sigla);

                    $cidade = str_replace("_", "-", $cd);
                    $retorno = $retorno . "/" . $cidade;
                }
            }
        }
        return (isset($retorno) ? $retorno : false);
    }

    public function getEstados()
    {
        $estadosBrasileiros = [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        ];
        return ($estadosBrasileiros);
    }
}
