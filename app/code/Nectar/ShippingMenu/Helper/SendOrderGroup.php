<?php

namespace Nectar\ShippingMenu\Helper;

use Exception;

class SendOrderGroup
{
    /**
     * @param \Nectar\ShippingMenu\Helper\CreateOrderGroup
     */
    private $createOrderGroup;

    /**
     * @param \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode
     */
    private $getCityCode;

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    /**
     * @param \Magento\InventoryApi\Api\SourceRepositoryInterface
     */
    private $sourceRepository;

    /**
     * @param \Nectar\ShippingMenu\Helper\MotoboysData\SendGroupIntegration
     */
    private $sendGroupIntegration;

    /**
     * @param \Nectar\ShippingMenu\Helper\ShipGroupOrders
     */
    private $shipGroupOrders;

    public function __construct(
        \Nectar\ShippingMenu\Helper\CreateOrderGroup $createOrderGroup,
        \Nectar\ShippingMenu\Helper\MotoboysData\GetCityCode $getCityCode,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Nectar\ShippingMenu\Logger\Logger $logger,
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository,
        \Nectar\ShippingMenu\Helper\MotoboysData\SendGroupIntegration $sendGroupIntegration,
        \Nectar\ShippingMenu\Helper\ShipGroupOrders $shipGroupOrders
    ) {
        $this->createOrderGroup = $createOrderGroup;
        $this->getCityCode = $getCityCode;
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
        $this->sourceRepository = $sourceRepository;
        $this->sendGroupIntegration = $sendGroupIntegration;
        $this->shipGroupOrders = $shipGroupOrders;
    }

    public function send($groupOrder)
    {
        $orders = json_decode($groupOrder);
        foreach($orders as $order){
            $dias[]=$order->data;
            $horas[]=$order->hora;
        }
        $dia = array_unique($dias);
        $hora = array_unique($horas);
        if(sizeof($dia)!=1 || sizeof($hora)!=1)
        {
            return ["status" =>false, "message" => "O intervalo de entrega dos pedidos são diferentes"];
        }
        foreach ($orders as $order) { //pega os jsons de order enviados separa 1 a 1
            $increments[] = $order->id_Magento; //salva os increment ids de cada json
        }

        $collection = $this->collectionFactory->create() //traz a collection com todas as orders que foram enviadas
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'increment_id',
                ['in' => $increments]
            )
            ->setOrder('created_at', 'desc');
        foreach ($collection as $order) {
            $code = $this->getCityCode->getStoreCode($order);
            $codes[] = $code;
            $cdsResponsaveis[] = $this->getCityCode->getCd($code);
        }
        if (isset($cdsResponsaveis)) {
            $cdResponsavel = array_unique($cdsResponsaveis);
            if (sizeof($cdResponsavel) != 1) {
                //throw new Exception (__("Os pedidos selecionados possuem mais de um Cd responsável"));
                $retorno = ["success" => false, "message" => "Os pedidos selecionados possuem mais de um Cd responsável"];
            } else {
                try {
                    $sourceInfo = $this->sourceRepository->get($cdResponsavel[0]);
                    $code = $this->getCityCode->getCode($sourceInfo,$cdResponsavel[0]);
                    if($code==false){
                        throw new Exception(__('Falha ao encontrar o CD responsável.'));
                    }else{
                        $return=$this->sendGroupIntegration->sendOrders($orders, $code, $sourceInfo, $increments);                        
                        if(isset($return[0]->success)){
                            $retorno=["success" => $return[0]->success, "message" => $return[0]->msg];
                            if($retorno["success"]==true)
                            {   
                                try{
                                    $this->createOrderGroup->crateGroup($orders, $return[0]->entrega);
                                    foreach($collection as $order){
                                        $ship=$this->shipGroupOrders->changeStatus($order,"motoboys_1");
                                        if($ship["success"]!=true)
                                        {
                                            throw new Exception("Erro na tentativa de troca de status do pedido " . $order->getIncrementId());                                            
                                        }
                                    }
                                }catch(Exception $exception){
                                    $this->logger->error($exception->getMessage());
                                    $retorno = ["success" => false, "message" => "Grupo enviado para Motoboys. Erro ao salvar no Magento: " . $exception->getMessage()];
                                }
                            }
                        }else{
                            throw new Exception(__('Erro ao tentar acessar a resposta da Motoboys.com -> Código recebido: ' . $return[1]));
                        }
                    }
                } catch (Exception $exception) {
                    $this->logger->error($exception->getMessage());
                    $retorno = ["success" => false, "message" => $exception->getMessage()];
                }
            }
        }

        return $retorno;
    }
}
