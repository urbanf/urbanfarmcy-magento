<?php

namespace Nectar\ShippingMenu\Helper;

class GetOrders
{
    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Nectar\ShippingMenu\Logger\Logger $logger
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
    }

    public function getOrders()
    {//traz todas as orders que estão faturadas
        $statuses = ['nfemitida'];
        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'status',
                ['in' => $statuses]
            )
            ->setOrder('created_at', 'desc');
        return $collection;
    }
}
