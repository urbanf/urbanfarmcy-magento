<?php

namespace Nectar\ShippingMenu\Helper;

class CreateOrderGroup
{
    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->collectionFactory = $collectionFactory;
    }

    public function crateGroup($orders, $motoboysID)
    {
        foreach ($orders as $order) { //pega os jsons de order enviados separa 1 a 1
            $increments[] = $order->id_Magento; //salva os increment ids de cada json
        }
        $incrementIds = json_encode($increments); //salva como string para add no banco

        $status = "motoboys_1";
        $tabela = $this->resourceConnection->getTableName('order_groups');
        $connection = $this->resourceConnection->getConnection();
        $insert = "INSERT INTO " . $tabela . "(increment_ids, status, motoboys_id) VALUES " . "('" . $incrementIds . "','" . $status . "','" . $motoboysID . "');";
        $connection->query($insert);//cria o registro com o id, status de criado, json de incrementids

        $this->registerOrderId($increments, $motoboysID);//chama função que associa todas as orders com o id do grupo
        return ($motoboysID);
    }

    public function registerOrderId($increments, $motoboysID)
    {
        $collection = $this->collectionFactory->create() //traz a collection com todas as orders que foram enviadas
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'increment_id',
                ['in' => $increments]
            )
            ->setOrder('created_at', 'desc');
        foreach($collection as $order) //salva o id em cada uma das orders
        {
            $order->setData('order_group',$motoboysID);
        }
    }
}
