<?php

namespace Nectar\ShippingMenu\Helper;

use Magento\Framework\Exception\NoSuchEntityException;

class GetOrderData
{
    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrders
     */
    private $getOrders;

    /**
     * @param \Amasty\Deliverydate\Model\DeliverydateFactory
     */
    private $deliverydateFactory;

    /**
     * @param \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    public function __construct(
        \Nectar\ShippingMenu\Helper\GetOrders $getOrders,
        \Amasty\Deliverydate\Model\DeliverydateFactory $deliverydateFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->getOrders = $getOrders;
        $this->deliverydateFactory = $deliverydateFactory;
        $this->customerFactory = $customerFactory;
    }

    public function getOrdersInfo()
    {
        $orders = $this->getOrders->getOrders();
        foreach ($orders as $order) {
            $json[] = $this->getOrderData($order);
        }
        return (json_encode($json));
    }

    public function getOrderData(\Magento\Sales\Model\Order $order)
    { //monta o json com todos os dados necessarios de uma order
        $orderId = $order->getEntityId();
        $customerId = $order->getCustomerId();
        $customer = $this->customerFactory->create()->load($customerId);
        $address = $order->getShippingAddress();
        if ($address != Null) {
            $json['status_label'] = $order->getStatusLabel();
            $json['id_Magento'] = $order->getIncrementId();
            $json['id_Protheus'] = $order->getData('id_protheus');
            $json['telefone'] = $customer->getData("telefone__cy");
            $json['entrega_portaria'] = $order->getData('entrega_portaria');
            $json['nome'] = $address->getName();
            $json['rua'] = $address->getStreet();
            $json['cep'] = $address->getPostcode();
            $json['cidade'] = $address->getCity();
            $json['estado'] = $address->getRegion();
            $date = $this->deliverydateFactory->create()->load($orderId, "order_id");
            $diaSemFormatacao = $date->getData('date');
            $dia = substr($diaSemFormatacao, 8, 2) . '/' .  substr($diaSemFormatacao, 5, 2) . '/' . substr($diaSemFormatacao, 0, 4);
            $hora = $date->getData('time');
            $comment = $date->getComment();
            if ($dia != Null || $hora != Null) {
                if ($comment != Null) {
                    $json['comentario'] = $comment;
                } else {
                    $json['comentario'] = "";
                }
                if($order->getData('entrega_portaria')){
                    $json['comentario'] = $json['comentario'] . " ENTREGAR NA PORTARIA";
                }
                $json['data'] = $dia;
                $json['hora'] = $hora;
            } else {
                throw new NoSuchEntityException(__('[Pedido: ' . $order->getData['id_protheus'] .  '] Dia/Hora de entrega não encontrado!'));
            }
        } else {
            throw new NoSuchEntityException(__('[Pedido: ' . $order->getData['id_protheus'] .  '] Endereço de entrega não encontrado!'));
        }
        return ($json);
    }
}
