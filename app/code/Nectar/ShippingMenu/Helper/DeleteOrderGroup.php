<?php

namespace Nectar\ShippingMenu\Helper;

use Exception;

class DeleteOrderGroup
{
    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param \Nectar\ShippingMenu\Helper\ShipGroupOrders
     */
    private $shipGroupOrders;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Nectar\ShippingMenu\Helper\ShipGroupOrders $shipGroupOrders
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->shipGroupOrders = $shipGroupOrders;
    }

    public function deleteGroup($motoboyID)
    {
        try {
            //cancelar order na motoboys.com e setar como NFEmitida
            $urlBase = $this->scopeConfig->getValue("integrations/motoboys/cancelar");
            $chave = $this->scopeConfig->getValue("integrations/motoboys/chave");
            $url = $urlBase . "entrega=" . $motoboyID . "&tipo=CANCELAR";
            $this->curl->addHeader('access_token', $chave);
            $this->curl->post($url, "");
            $status = $this->curl->getStatus();
            if ($status != 200) {
                throw new Exception("Erro ao deletar o grupo. (codigo: " . $status);
            } else {
                $response = $this->curl->getBody();
                $resposta = json_decode($response);
                if ($resposta->success == true || ((isset($resposta->errorMessage)) && strpos($resposta->errorMessage, "Não foi possível localizar o PV pelo entity_id") != false)) {
                    $tabela = $this->resourceConnection->getTableName('order_groups');
                    $connection = $this->resourceConnection->getConnection();

                    $select = $connection->select() //traz a linha que tem com o id enviado
                        ->from(
                            ['c' => $tabela],
                            ['*']
                        )
                        ->where(
                            "c.motoboys_id = " . $motoboyID
                        );
                    $bind = ['motoboys_id' => $motoboyID];
                    $records = $connection->fetchAll($select, $bind);

                    $this->unSetGroupId(json_decode($records[0]['increment_ids'])); // retira o id de todas as orders associadas ao id
                    $delete = "DELETE FROM " .  $tabela . " WHERE motoboys_id='" . $motoboyID . "';"; // deleta a linha do grupo na tabela
                    $connection->query($delete);
                } elseif (isset($resposta->msg)) {
                    throw new Exception($resposta->msg);
                } else {
                    throw new Exception("Falha na integração com motoboys.com");
                }
                $retorno = ["success" => true, "message" => ""];
            }
        } catch (Exception $e) {
            $retorno = ["success" => false, "message" => $e->getMessage()];
        }
        return ($retorno);
    }
    
    public function unSetGroupId($increments)
    {
        $collection = $this->collectionFactory->create() //traz todas as orders que possuem increment id enviado
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'increment_id',
                ['in' => $increments]
            )
            ->setOrder('created_at', 'desc');
        foreach ($collection as $order) {
            $this->shipGroupOrders->changeStatus($order, "nfemitida");
            $order->setData('order_group', '0'); //seta 0 no id do grupo
        }
    }
}
