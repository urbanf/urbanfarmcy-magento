<?php
namespace Nectar\ShippingMenu\Helper;

class GetOrderGroups
{

    private $statusLabels;

    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;


    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderData
     */
    private $getOrderData;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Nectar\ShippingMenu\Helper\GetOrderData $getOrderData
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->collectionFactory = $collectionFactory;
        $this->getOrderData = $getOrderData;

        $this->statusLabels = [
            'motoboys_1' => "Contatando Motoboy",
            'motoboys_2' => "Motoboy Confirmado",
            'motoboys_3' => "Saiu para entrega",
            'motoboys_4' => "Entregue"
        ];
    }

    public function getOrderGroups()
    {//traz todos os grupos de pedidos enviados
        $json = [];
        $tabela = $this->resourceConnection->getTableName('order_groups');
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
          ->from(
            ['c' => $tabela],
            ['*']
          );
        $records = $connection->fetchAll($select);//traz todos os grupos
        $i=0;
        $grupos=[];
        foreach($records as $group)//separa todos os grupos
        {
            $increments=json_decode($group['increment_ids']);//vetor em que cada posição tem um vetor de ids de um grupo
            $collection = $this->collectionFactory->create()  //traz todas as orders com os increments
            ->addAttributeToSelect('*')
            ->addFieldToFilter(
                'increment_id',
                ['in' => $increments]
            )
            ->setOrder('created_at', 'desc');
            $grupos[$i]["id"]=$group['motoboys_id'];
            $grupos[$i]["status"]=$group['status'];

            if(isset($this->statusLabels[$group['status']])) {
                $grupos[$i]["status_label"] = $this->statusLabels[$group['status']];
            } else {
                $grupos[$i]["status_label"] = $group['status'];
            }

            foreach($collection as $order)
            {
                $grupos[$i]["orders"][]= $this->getOrderData->getOrderData($order);
            }
            $i++;
        }
        return ($grupos);
    }
}
