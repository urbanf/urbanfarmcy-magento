<?php
namespace Nectar\ShippingMenu\Helper;

class SetGroupOrdersData
{
    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
        
    }

    public function setStatus($motoboysID, $status)
    {
        $tabela = $this->resourceConnection->getTableName('order_groups');
        $connection = $this->resourceConnection->getConnection();
        $update="UPDATE " . $tabela . " SET status ='" . $status . "' WHERE motoboys_id=" . $motoboysID . ";";
        $connection->query($update);
    }
}
