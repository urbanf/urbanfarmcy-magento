<?php

namespace Nectar\ShippingMenu\Helper;

use Exception;
use Magento\Sales\Api\Data\ShipmentCreationArgumentsExtensionInterfaceFactory;

class ShipGroupOrders
{
    /**
     * @param \Nectar\ShippingMenu\Logger\Logger
     */
    private $logger;

    /**
     * @param \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Magento\Sales\Model\ShipOrder
     */
    private $shipOrder;

    /**
     * @param \Magento\Sales\Model\Order\Shipment\CreationArguments
     */
    private $shipmentCreationArguments;

    /**
     * @param ShipmentCreationArgumentsExtensionInterfaceFactory
     */
    private $argumentExtensionFactory;

    /**
     * @param \Magento\Sales\Model\Order\ShipmentRepository
     */
    private $shipmentRepository;

    /**
     * @param \Magento\Sales\Model\Order\ShipmentFactory
     */
    private $shipmentFactory;

    /**
     * @param \Magento\Shipping\Model\ShipmentNotifier
     */
    private $shipmentNotifier;

    /**
     * @param \Magento\Sales\Api\Data\OrderInterfaceFactory
     */
    private $orderFactory;

    /**
     * @param \Magento\Sales\Model\Convert\OrderFactory
     */
    private $convertOrderFactory;

    /**
     * @param \Magento\Sales\Model\Service\OrderService
     */
    private $orderService;

    /**
     * @param \Magento\Sales\Api\Data\OrderStatusHistoryInterfaceFactory
     */
    private $orderStatusHistoryInterfaceFactory;

    public function __construct(
        \Nectar\ShippingMenu\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ShipOrder $shipOrder,
        \Magento\Sales\Model\Order\Shipment\CreationArguments $shipmentCreationArguments,
        ShipmentCreationArgumentsExtensionInterfaceFactory $argumentExtensionFactory,
        \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository,
        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier,
        \Magento\Sales\Api\Data\OrderInterfaceFactory $orderFactory,
        \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory,
        \Magento\Sales\Model\Service\OrderService $orderService,
        \Magento\Sales\Api\Data\OrderStatusHistoryInterfaceFactory $orderStatusHistoryInterfaceFactory
    ) {
        $this->logger = $logger;
        $this->url = $url;
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->shipOrder = $shipOrder;
        $this->shipmentCreationArguments = $shipmentCreationArguments;
        $this->argumentExtensionFactory = $argumentExtensionFactory;
        $this->shipmentRepository = $shipmentRepository;
        $this->shipmentFactory = $shipmentFactory;
        $this->shipmentNotifier = $shipmentNotifier;
        $this->orderFactory = $orderFactory;
        $this->convertOrderFactory = $convertOrderFactory;
        $this->orderService = $orderService;
        $this->orderStatusHistoryInterfaceFactory = $orderStatusHistoryInterfaceFactory;
    }

    public function changeStatus(\Magento\Sales\Model\Order $order, $statusCode)
    {
        try {
            if ($statusCode == "motoboys_4") {
                $increment_id = $order->getIncrementId();

                if($order->canShip()) { // Se o pedido puder ser enviado
                    $convertOrder = $this->convertOrderFactory->create();
                    $shipment = $convertOrder->toShipment($order);
    
                    foreach($order->getAllItems() AS $orderItem) {
                        if(!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                            continue;
                        }
                    
                        $qtyShipped = $orderItem->getQtyToShip();
                        $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
                        $shipment->addItem($shipmentItem);
                    }
    
                    $shipment->register();
                    $shipment->getOrder()->setIsInProcess(true);

                    $order->addStatusToHistory('complete', '', true); // Apesar de estar como TRUE, os testes mostraram que ele não tenta enviar o email de fato
                    $shipment->getOrder()->save();

                    $this->shipmentNotifier->notify($shipment); // aqui ele envia o email
                    $shipment->save();

                    $this->logger->info("Status alterado. Pedido concluído. Increment ID: " . $increment_id);
    
                    return (["success" => true, "message" => "Status alterado. Pedido concluído. Increment ID: " . $increment_id]);
                    
                } else { // se não puder ser enviado
                    $shipments = $order->getShipmentsCollection();
                    $shipmentItems = $shipments->getItems();
                    $state = $order->getState();

                    if($state === "complete") { // Se o pedido já está concluído e completo, apenas muda o status para 'complete'
                        $order->addStatusToHistory('complete', '', false);
                        $order->save();
                        $this->logger->info("Este pedido já foi concluído. Increment ID: " . $increment_id);
                        return (["success" => false, "message" => "Este pedido já foi concluído. Increment ID: " . $increment_id]);
                    }

                    if(!empty($shipmentItems)) { // Se o pedido já foi enviado, muda o status e o state dele para 'complete'
                        $order->setState('complete');
                        $order->addStatusToHistory('complete', '', false);
                        $order->save();

                        $this->logger->info("Status Alterado. O pedido já havia sido concluído. Increment ID: " . $increment_id);
                        return (["success" => true, "message" => "Status Alterado. O pedido já havia sido concluído. Increment ID: " . $increment_id]);
                    }

                    $this->logger->error("Este pedido não permite que um envio seja criado. Increment ID: " . $increment_id);
                    return (["success" => false, "message" => "Este pedido não permite que um envio seja criado. Increment ID: " . $increment_id]);

                }
                
            } else {
                
                if ($statusCode == "motoboys_1" || $statusCode == "motoboys_2") {
                    $is_customer_notified = 0;
                } else {
                    $is_customer_notified = 1;
                }
                $orderStatusHistory=$this->orderStatusHistoryInterfaceFactory->create();
                $orderStatusHistory->setEntityId($order->getEntityId());
                $orderStatusHistory->setIsCustomerNotified($is_customer_notified);
                $orderStatusHistory->setStatus($statusCode);
                $orderStatusHistory->setIsVisibleOnFront(1);
                $orderStatusHistory->setComment("");
                $this->orderService->addComment($order->getEntityId(),$orderStatusHistory);
                $order->addStatusToHistory($statusCode, "", $is_customer_notified);
                $this->logger->info("Pós comando de troca de status -> " . $order->getStatus());

                if ($order->getStatus() == $statusCode) {
                    $order->save();
                    $retorno = (["success" => true, "message" => "Status Alterado"]);
                } else {
                    $retorno = (["success" => false, "message" => "Falha na alteração"]);
                }
            }

        } catch (Exception $exception) {
            if(strpos($exception->getMessage(), "The order does not allow a shipment to be created") || strpos($exception->getMessage(), "O pedido não permite a criação de uma entrega")) {
                $order->setState('complete');
            }
            $this->logger->error($exception->getMessage());
            $retorno = ["success" => false, "message" => $exception->getMessage()];
        }
        
        return ($retorno);
    }

    public function ship($order)
    {
        try {
            $this->logger->info("SHIPPING ORDER: " . $order->getIncrementId());
            $items = $order->getAllItems();
            $i = 0;
            foreach ($items as $item) {
                $json["items"][$i]["order_item_id"] = $item->getItemId() * 1;
                $json["items"][$i]["qty"] = $item->getQtyOrdered() * 1;
                $i++;
            }
            $json["notify"] = true;
            $baseUrl = $this->url->getBaseUrl();
            $id = $order->getEntityId();
            $url = $baseUrl . "rest/all/V1/order/" . $id . "/ship";

            $chave = $this->scopeConfig->getValue("integrations/motoboys/token");
            $this->curl->addHeader('Authorization', "Bearer " . $chave);
            $this->curl->addHeader('Content-Type', 'application/json');
            $this->curl->post($url, json_encode($json));
            $response = $this->curl->getBody();
            $resposta = json_decode($response);
            if (isset($resposta->success) && $resposta->success != true) {
                $this->logger->info($url);
                $this->logger->info(json_encode($json));
                $this->logger->info($response);
                $retorno = ["success" => false, "message" => "Falha ao enviar o pedido, veja os logs"];
            } else {
                $retorno = ["success" => true, "message" => "Pedido enviado"];
            }
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            $retorno = ["success" => false, "message" => $exception->getMessage()];
        }
        return ($retorno);
    }
}
