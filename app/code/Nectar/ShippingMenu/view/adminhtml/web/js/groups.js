define([
    'jquery',
    'uiComponent',
    'ko',
    'mage/url',
    'mage/backend/notification',
    'Magento_Ui/js/modal/modal'
], function($, Component, ko, url, notification) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Nectar_ShippingMenu/group'
        },
        orderGroups: ko.observableArray(),

        initialize: function () {
            var self = this;
            this._super();

            console.log(self.groups);

            self.groups.forEach( (group) => {
                self.orderGroups.push(group);
            });

            // let buttonTeste = document.getElementById('teste-shiporder');
            // buttonTeste.addEventListener('click', $.proxy(function() {

            //     $.ajax({
            //         url: this.actionUrlTeste,
            //         dataType: 'json',
            //         type: 'POST',
            //         data: {
            //             form_key: window.FORM_KEY
            //         },
            //         beforeSend: function () {
            //             $('body').trigger('processStart');
            //         },
            //         complete: function() {
            //             $('body').trigger('processStop');
            //         },
            //         success: $.proxy(function(res) {
            //             alert(res.message);
            //         }, self)
            //     });

            // }, this));

            return this;
        },

        openOrdersModal: function(group) {
            let id = group.id;
            let selector = '#orders-' + id;
            
            $(selector).modal({
                type: 'slide',
                title: 'Pedidos',
                modalClass: 'shipping-orders',
                clickableOverlay: false,
                responsive: true,
                buttons: []
            });

            $(selector).modal('openModal');
            
        },

        cancelOrderGroup: function(self, group) {
            let confirmation = confirm("Tem certeza de que deseja cancelar o Grupo de Pedidos nº " + group.id + "?");

            if(confirmation) {
                $.ajax({
                    url: self.actionUrl,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        form_key: window.FORM_KEY,
                        groupId: group.id
                    },
                    beforeSend: function () {
                        $('body').trigger('processStart');
                    },
                    complete: function() {
                        $('body').trigger('processStop');
                    },
                    success: $.proxy(function(res) {
                        console.log(res);
                        if(res.message && res.message.success) {
                            window.location.reload();
                        } else {
                            self.showError(res.message.message);
                        }
                    }, self)
                });
            }

        },

        showError: function(error) {

            $('#messages').show();
            $('[data-ui-id="messages-message-error"]').text(error);

            setTimeout(function() {
                $('#messages').hide();
            }, 6000);
            
        }

    });

});