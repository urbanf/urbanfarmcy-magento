define([
    'jquery',
    'uiComponent',
    'ko',
    'mage/url',
    'mage/backend/notification',
    'Magento_Ui/js/modal/modal'
], function($, Component, ko, url, notification) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Nectar_ShippingMenu/list'
        },
        invoicedOrders: ko.observableArray(),
        addedOrders: ko.observableArray(),

        initialize: function () {
            var self = this;
            this._super();

            console.log(this.orders);

            self.orders.forEach( (order) => {
                order.visible = ko.observable(true);
                self.invoicedOrders.push(order);
            });

            return this;
            
        },

        addOrderToList: function(self, orderItem) {
            orderItem.visible(false);
            self.addedOrders.push(orderItem);
        },

        removeOrderFromList: function(self, orderItem) {
            orderItem.visible(true);
            self.addedOrders.remove(orderItem);
        },

        sendBatchOrders: function(self) {
            let orders = self.addedOrders();

            $("#addedorders-modal").modal({
                type: 'slide',
                title: 'Confirmação',
                modalClass: 'shipping-orders',
                clickableOverlay: false,
                responsive: true,
                buttons: [
                    {
                        text: $.mage.__('Cancelar'),
                        class: 'cancel',
                        click: function () {
                            this.closeModal();
                        }
                    },
                    {
                        text: $.mage.__('Enviar'),
                        class: 'action-primary',
                        click: $.proxy(function () {
                            self.send();
                        }, self)
                    }
                ]
            });

            $("#addedorders-modal").modal('openModal');
        },

        send: function() {

            console.log(this.actionUrl);

            $.ajax({
                url: this.actionUrl,
                dataType: 'json',
                type: 'POST',
                data: {
                    form_key: window.FORM_KEY,
                    orders: JSON.stringify(this.addedOrders())
                },
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                complete: function() {
                    $('body').trigger('processStop');
                },
                success: $.proxy(function(res) {
                    if(res.message && res.message.success) {
                        window.location.reload();
                    } else {
                        this.showError(res.message.message);
                    }
                }, this)
            });

        },

        showError: function(error) {
            $('body').notification('clear').notification('add', {
                error: true,
                message: error,

                /**
                 * @param {String} message
                 */
                insertMethod: function (message) {
                    var $wrapper = $('<div/>').html(message);
                    $('.page-main-actions').after($wrapper);
                }
            });
        }

    });

});