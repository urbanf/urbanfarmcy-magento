<?php

namespace Nectar\ShippingMenu\Block\Adminhtml;

class Grupos extends \Magento\Backend\Block\Template
{

    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderGroups
     */
    private $getOrderGroups;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Nectar\ShippingMenu\Helper\GetOrderGroups $getOrderGroups,
        array $data = []
    ) {
        $this->getOrderGroups = $getOrderGroups;
        parent::__construct($context, $data);
    }

    public function getAjaxUrl()
    {
        return $this->getUrl('nectarshippingmenu/grupos/cancel');
    }

    public function getAjaxUrlTeste()
    {
        return $this->getUrl('nectarshippingmenu/grupos/ship');
    }

    public function getGroups()
    {
        $groups = $this->getOrderGroups->getOrderGroups();
        return json_encode($groups);
    }
    
}
