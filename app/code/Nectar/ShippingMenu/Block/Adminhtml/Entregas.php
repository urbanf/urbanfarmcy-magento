<?php

namespace Nectar\ShippingMenu\Block\Adminhtml;

class Entregas extends \Magento\Backend\Block\Template
{
    /**
     * @param \Nectar\ShippingMenu\Helper\GetOrderData
     */
    private $getOrderData;

    /**
     * @param \Nectar\ShippingMenu\Helper\SendOrderGroup
     */
    private $sendOrderGroup;

    /**
     * @param \Magento\Backend\Model\UrlInterface
     */
    private $urlBuilder;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Nectar\ShippingMenu\Helper\GetOrderData $getOrderData,
        \Nectar\ShippingMenu\Helper\SendOrderGroup $sendOrderGroup,
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->getOrderData = $getOrderData;
        $this->sendOrderGroup = $sendOrderGroup;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    public function getAjaxUrl()
    {
        return $this->getUrl('nectarshippingmenu/entregas/send');
    }

    public function getOrders()
    {
        $orders=$this->getOrderData->getOrdersInfo();
        return ($orders);
    }
    
}
