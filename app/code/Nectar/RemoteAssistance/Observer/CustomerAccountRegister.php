<?php

namespace Nectar\RemoteAssistance\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Customer;
use Magento\LoginAsCustomerAssistance\Api\SetAssistanceInterface;

class CustomerAccountRegister implements ObserverInterface
{
    /**
     * @param SetAssistanceInterface
     */
    private $setAssistance;

    /**
     * @param SetAssistanceInterface $setAssistance
     */
    public function __construct(
        SetAssistanceInterface $setAssistance
    ) {
        $this->setAssistance = $setAssistance;
    }

    /**
     * Habilita a assistência remota por padrão quando o cliente se cadastrar
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $customerId = $customer->getId();

        $this->setAssistance->execute($customerId, true);
    }
}
