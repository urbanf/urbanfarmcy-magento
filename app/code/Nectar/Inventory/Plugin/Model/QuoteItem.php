<?php

namespace Nectar\Inventory\Plugin\Model;

use Magento\InventoryApi\Api\Data\SourceItemInterface;

class QuoteItem
{
    /**
     * @param \Magento\InventoryApi\Api\SourceItemRepositoryInterface
     */
    private $sourceItemRepository;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\InventorySalesApi\Api\StockResolverInterface
     */
    private $stockResolver;

    /**
     * @param \Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface
     */
    private $getStockItemConfiguration;

    public function __construct(
        \Magento\InventoryApi\Api\SourceItemRepositoryInterface $sourceItemRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\InventorySalesApi\Api\StockResolverInterface $stockResolver,
        \Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface $getStockItemConfiguration
    ) {
        $this->sourceItemRepository = $sourceItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->storeManager = $storeManager;
        $this->stockResolver = $stockResolver;
        $this->getStockItemConfiguration = $getStockItemConfiguration;
    }

    public function beforeSetQty(
        \Magento\Quote\Model\Quote\Item $subject,
        $qty
    ) {
        /**
         * Se o item tiver um parent (faz parte de um bundle), não deve alterar a quantidade desse item
        */
        if ($subject->getParentItem()) {
            return $subject;
        }

        $stock = $this->stockResolver->execute('website', $this->storeManager->getWebsite()->getCode());
        $stockId = $stock->getStockId();

        $originalQty = $subject->getQty();
        $cdResponsavel = $this->cidadesPorCd->getCdResponsavel();

        /**
         * Se o item for bundle, não dá para pesquisar pela SKU, pois o inventory não fica no bundle.
         * Nesse caso, vou ter que verificar a quantidade em estoque dos filhos desse bundle
         */
        if ($subject->getProductType() === "bundle") {
            $maxQuantity = $qty;
            $canUpdateQuantity = true;
            $children = $subject->getChildren();

            foreach ($children as $child) {
                $sku = $child->getSku();
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter(SourceItemInterface::SKU, $sku)
                    ->create();
                $items = $this->sourceItemRepository->getlist($searchCriteria)->getItems();

                foreach ($items as $item) {
                    if (isset($cdResponsavel) && $item->getSourceCode() === $cdResponsavel) {
                        $quantity = $item->getQuantity();
                        if ($quantity < $qty) {
                            $canUpdateQuantity = false;
                            if ($quantity < $maxQuantity) {
                                $maxQuantity = $quantity;
                            }
                        }
                    }
                }
            }

            if ($canUpdateQuantity) {
                return [$qty];
            }

            $subject->addErrorInfo(null, null, "No momento, temos apenas {$maxQuantity} unidade(s) desse produto em estoque.");
            return [$originalQty];
        }

        // Se o produto não for bundle, segue o fluxo normal
        $sku = $subject->getSku(); //pega a sku do item adicionado
        $stockItemConfig = $this->getStockItemConfiguration->execute($sku, $stockId);
        $outOfStockThreshold = $stockItemConfig->getMinQty();
        $quantidade_no_cd = 0;
        $searchCriteria = $this->searchCriteriaBuilder //cria uma pesquisa por aquela sku dentro de todos os estoques habilitados
            ->addFilter(SourceItemInterface::SKU, $sku)
            ->create();
        $items = $this->sourceItemRepository->getlist($searchCriteria)->getItems(); //traz todos os estoques que possuem aquele item
           
        $cdResponsavel = $this->cidadesPorCd->getCdResponsavel();

        foreach ($items as $item) { //executa o codigo para cada estoque que possui a sku
            $quantidade_no_cd = $item->getQuantity() - $outOfStockThreshold; //pega o valor de estoque da sku no cd
            $code = $item->getSourceCode(); //pega o codigo do cd que possui esse estoque
            if (isset($cdResponsavel) && $code == $cdResponsavel) { //compara se a loja em que o cliente se encontra seja de um cd existente e o cd for o mesmo do item
                if ($quantidade_no_cd >= $qty) { //caso esteja no cd correto compara se a quantidade esta disponível, se sim, retorna a quantidade
                    return [$qty];
                }
            }
        }

        /**
         * Retorna false para que o Magento renderize o
         * erro padrão de estoque indisponível
         */
        return false;
    }
}
