<?php

namespace Nectar\Inventory\Model\Source\Command;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\InventoryApi\Api\Data\StockSourceLinkInterface;
use Magento\InventoryApi\Api\GetSourcesAssignedToStockOrderedByPriorityInterface;
use Magento\InventoryApi\Api\GetStockSourceLinksInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;
use Magento\Inventory\Model\Source\Command\GetSourcesAssignedToStockOrderedByPriority;

/**
 * @inheritdoc
 */
class MyGetSourcesAssignedToStockOrderedByPriority extends GetSourcesAssignedToStockOrderedByPriority
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SourceRepositoryInterface
     */
    private $sourceRepository;

    /**
     * @var GetStockSourceLinksInterface
     */
    private $getStockSourceLinks;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SourceRepositoryInterface $sourceRepository
     * @param GetStockSourceLinksInterface $getStockSourceLinks
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SourceRepositoryInterface $sourceRepository,
        GetStockSourceLinksInterface $getStockSourceLinks,
        SortOrderBuilder $sortOrderBuilder,
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceRepository = $sourceRepository;
        $this->getStockSourceLinks = $getStockSourceLinks;
        $this->cidadesPorCd = $cidadesPorCd;
        $this->logger = $logger;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * @inheritdoc
     */
    public function execute(int $stockId): array
    {
        try {
            $stockSourceLinks = $this->getStockSourceLinks($stockId);
            $sources = [];
            $mysources = [];
            $cdResponsavel=$this->cidadesPorCd->getCdResponsavel();
            $name = str_replace("_", " ", $cdResponsavel);
            foreach ($stockSourceLinks as $link) {
                $source = $this->sourceRepository->get($link->getSourceCode());
                $name = strtolower($source->getSourceCode());
                $sources[] = $source;
                if ($name == $cdResponsavel) {
                    $mysources[] = $source;
                }
            }
            return $mysources;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new LocalizedException(__('Could not load Sources for Stock'), $e);
        }
    }

    /**
     * Get all stock-source links by given stockId
     *
     * @param int $stockId
     * @return array
     */
    private function getStockSourceLinks(int $stockId): array
    {
        $sortOrder = $this->sortOrderBuilder
            ->setField(StockSourceLinkInterface::PRIORITY)
            ->setAscendingDirection()
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(StockSourceLinkInterface::STOCK_ID, $stockId)
            ->addSortOrder($sortOrder)
            ->create();
        $searchResult = $this->getStockSourceLinks->execute($searchCriteria);

        return $searchResult->getItems();
    }
}
