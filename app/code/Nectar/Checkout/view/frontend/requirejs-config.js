var config = {
    map: {
        '*': {
            'Magento_Checkout/template/billing-address/details.html': 'Nectar_Checkout/template/billing-address/details.html',
            'Magento_Checkout/js/model/place-order': 'Nectar_Checkout/js/model/place-order-override'
        }
    },
    config: {
        mixins: {
            'Nectar_Shipping/js/view/shipping': {
                'Nectar_Checkout/js/checkout/shipping-mixin': true
            },
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': {
                'Nectar_Checkout/js/shipping-save-processor/payload-extender-mixin': true
            },
            'Magento_Ui/js/lib/validation/validator': {
                'Nectar_Checkout/js/validation-mixin': true
            },
            'mage/validation': {
                'Nectar_Checkout/js/another-validation-mixin': true
            }
        }
    }
};
