define([
    'jquery',
    'uiComponent',
    'mage/url'
], function($, Component, url) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Nectar_Checkout/continuar-comprando'
        },
        getBaseUrl: function() {
            return url.build('produtos/cardapio-completo.html');
        }            
    });
});
