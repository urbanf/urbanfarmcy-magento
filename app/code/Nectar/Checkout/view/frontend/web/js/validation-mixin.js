define([
    'jquery',
    'jquery/validate',
    'cpf',
    'Inputmask'
], function ($) {
    "use strict";

    return function (validator) {

        validator.addRule(
            'validate-cpf',
            function (value) {
                Inputmask({
                    'mask': ['999.999.999-99'],
                    'keepStatic': true
                }).mask('[name="vat_id');

                let numericValue = window.CPF.strip(value);                
                return window.CPF.isValid(numericValue, true);
            },
            $.mage.__("Digite um CPF válido")
        );

        validator.addRule(
            'validate-telephone',
            function (value) {
                Inputmask({
                    'mask': ['(99) 9999-9999', '(99) 99999-9999'],
                    'keepStatic': true
                }).mask('[name="telephone');

                if(value) {
                    let numericValue = value.replace(/\D/g, "");
                    return numericValue.length === 10 || numericValue.length === 11;
                }
                return true;
            },
            $.mage.__("Insira um telefone/celular válido com DDD.")
        );

        return validator;
    };
});