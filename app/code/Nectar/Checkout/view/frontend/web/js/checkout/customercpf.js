define([
    'jquery',
    'ko',
    'uiComponent',
    'mage/translate',
    'mage/mage'
], function($, ko, Component, $t) {
    'use strict';

    return Component.extend({
        currentCpf: '',
        defaults: {
            template: 'Nectar_Checkout/customer-cpf',
            fieldCpf: '',
            labelCpf: $t('Informe seu CPF*')
        },

        initObservable: function () {
            this._super()
                .observe([
                    'fieldCpf',
                    'labelCpf'
                ]);

            return this;
        },

        initialize: function () {
            let self = this;
            let cpf = self.getCPF();

            this._super();

            if(cpf == ''){
                self.labelCpf($t('Informe seu CPF*'));
            }

            self.fieldCpf(cpf);
            self.currentCpf = cpf;
            localStorage.removeItem("customer_cpf");
            localStorage.setItem("customer_cpf", cpf);
        },

        setCpfMask: function () {
            Inputmask({'mask':'999.999.999-99'}).mask('#taxvat');
        },

        getCPF: function() {
            let cpf = '';

            console.group("Buscando CPF");
            console.log("início");

            $.ajax({
                url: '/nectarcustomer/account/getCpf',
                type: 'get',
                dataType: 'json',
                async: false,
                success: function (data) {
                    cpf = data.cpf;

                    let info = cpf ? "CPF encontrado (" + cpf + ")" : "CPF não encontrado";
                    let color = cpf ? "green" : "red";
                    console.info('%c ' + info, 'color: ' + color);
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                    console.log(errorThrown);
                }
            });

            console.log("fim");
            console.groupEnd();

            return cpf;
        },

        saveCPF: function(data, event){
            let self = this;

            console.log('FORM', $(event.currentTarget.form).valid());

            if($(event.currentTarget.form).valid()){
                console.group("Gravando CPF");
                console.log("início");

                $.ajax({
                    url: '/nectarcustomer/account/saveCpf',
                    type: 'get',
                    data: { cpf: event.currentTarget.value },
                    dataType: 'json',
                    async: false,
                    success: function (data, status, xhr) {
                        if(data.success) {
                            console.info('%c CPF gravado com sucesso', 'color: green');

                            self.labelCpf($t('Informe seu CPF*'));
                            self.currentCpf = event.currentTarget.value
                            localStorage.setItem("customer_cpf", event.currentTarget.value);
                        } else {
                            console.info('%c erro ao gravar CPF', 'color: red');
                        }
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log('Error happens. Try again.');
                        console.log(errorThrown);
                    }
                });

                console.log("fim");
                console.groupEnd();
            }
        }
    });
});
