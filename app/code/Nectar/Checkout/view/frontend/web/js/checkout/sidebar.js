define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote'
], function($, Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Nectar_Checkout/entregaportaria-sidebar'
        },

        getEntregaPortaria: function() {
            if(quote.nectarEntregaPortaria) {
                if(quote.nectarEntregaPortaria.entrega === "true") {
                    return "Sim";
                } else {
                    return "Não"
                }
            } else {
                return false;
            }
        }
            
    });
});
