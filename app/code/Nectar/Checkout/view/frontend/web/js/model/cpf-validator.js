define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/model/messageList'
], function ($, $t, messageList) {
    'use strict';
    return {
        validate: function () {
            let isValid = localStorage.getItem("customer_cpf") ? true : false;

            $('#taxvat').addClass("error")
                .attr("aria-describedby", "taxvat-error")
                .attr("aria-invalid", "true");

            if (!isValid) {
                messageList.addErrorMessage({ message: $t('CPF é obrigatório!') });

                $('html, body').animate({scrollTop: $('#checkout').offset().top}, 'slow');
            }

            return isValid;
        }
    }
});
