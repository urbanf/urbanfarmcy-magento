define([
    'Magento_Ui/js/form/element/single-checkbox',
    'mage/translate',
    'uiRegistry'
], function (AbstractField, $t, registry) {
    'use strict';

    return AbstractField.extend({
        onCheckedChanged: function (checkedValue) {
            this._super();

            let field = document.querySelector('[name="shippingAddress.custom_attributes.entrega_portaria"]');
            if(field) {
                if(checkedValue) {
                    field.classList.add('checked');
                } else {
                    field.classList.remove('checked');
                }
            }
        }
    });
});