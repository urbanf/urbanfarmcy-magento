define([
    'jquery',
    'cpf',
], function($) {
    'use strict';

    return function () {

        $.validator.addMethod(
            'validate-cpf',
            function(value) {
                let numericValue = window.CPF.strip(value);
                return window.CPF.isValid(numericValue, true);
            }, $.mage.__('Digite um CPF válido')
        );

        $.validator.addMethod(
            'validate-telephone',
            function(value) {
                if(value) {
                    let numericValue = value.replace(/\D/g, "");
                    return numericValue.length === 10 || numericValue.length === 11;
                }
                return true;
            }, $.mage.__('Insira um telefone/celular válido com DDD.')
        );
        
    };

});