define([
    'underscore',
    'jquery',
    'uiRegistry',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function (_, $, registry, wrapper, quote) {
    'use strict';

    return function (payloadExtender) {
        return wrapper.wrap(payloadExtender, function (original, payload) {
            var payloadOriginal = original(payload),
                payloadWithDeliveryDateInfo = payloadOriginal;

            var entregaPortaria = $('[name="shippingAddress.custom_attributes.entrega_portaria"] .admin__control-checkbox');

            quote.nectarEntregaPortaria = [];
            quote.nectarEntregaPortaria.entrega = entregaPortaria.val();

            var deliveryData = {
                entrega_portaria: quote.nectarEntregaPortaria.entrega
            }

            _.extend(payloadWithDeliveryDateInfo.addressInformation.extension_attributes, deliveryData);

            return payloadWithDeliveryDateInfo;
        });
    };
});
