define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Nectar_Checkout/js/model/cpf-validator'
    ],
    function (Component, additionalValidators, yourValidator) {
        'use strict';
        additionalValidators.registerValidator(yourValidator);
        return Component.extend({});
    }
);
