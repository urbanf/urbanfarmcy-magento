define([
    'jquery',
    'mage/cookies'
], function ($) {
        'use strict';

        return function (Shipping) {
            return Shipping.extend({
                validateShippingInformation: function () {
                    var validationResult = this._super();
                    var additionalFields = this.getChild('shippingAdditional');

                    this.saveAmastyValuesToCookie();

                    if (additionalFields && additionalFields.hasChild('entrega_portaria')) {
                        return validationResult && additionalFields.getChild('entrega_portaria').validate()
                    }
                    return validationResult;
                },

                saveAmastyValuesToCookie: function() {

                    let cookieExpires = new Date(new Date().getTime() + 10 * 60 * 60 * 1000);

                    let dateField = document.querySelector('[name="amdeliverydate_date"]');
                    let timeField = document.querySelector('[name="amdeliverydate_time"]');
                    let commentField = document.querySelector('[name="amdeliverydate_comment"]');

                    $.mage.cookies.set(
                        'urban_amasty_date',
                        dateField.value,
                        { expires: cookieExpires, domain: '' }
                    );

                    $.mage.cookies.set(
                        'urban_amasty_time_interval_id',
                        timeField.value,
                        { expires: cookieExpires, domain: '' }
                    );

                    $.mage.cookies.set(
                        'urban_amasty_comment',
                        commentField.value,
                        { expires: cookieExpires, domain: '' }
                    );

                }

            });
        }
    }
);
