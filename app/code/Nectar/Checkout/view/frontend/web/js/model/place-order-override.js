/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * @api
 */
 define([
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/payment/place-order-hooks',
    'underscore',
    'jquery',
    'Magento_Ui/js/modal/modal'
], function (storage, errorProcessor, fullScreenLoader, customerData, hooks, _, $) {
    'use strict';

    return function (serviceUrl, payload, messageContainer) {
        var headers = {};

        fullScreenLoader.startLoader();
        _.each(hooks.requestModifiers, function (modifier) {
            modifier(headers, payload);
        });

        return storage.post(
            serviceUrl, JSON.stringify(payload), true, 'application/json', headers
        ).fail(
            function (response) {
                errorProcessor.process(response, messageContainer);

                let messageContent = $(document.createElement('div'));
                messageContent.append('Não foi possível finalizar seu pedido. Por favor, verifique se os dados de pagamento estão corretos e tente novamente.');
                console.log(response.responseText);

                messageContent.modal({
                    type: 'popup',
                    title: '',
                    modalClass: 'checkout-payment-failed',
                    clickableOverlay: false,
                    autoOpen: false,
                    responsive: true,
                    buttons: [
                        {
                            text: $.mage.__(' OK '),
                            class: 'confirm',
                            click: function () {
                                this.closeModal();
                            }
                        },
                    ]
                });

                messageContent.modal('openModal');

            }
        ).success(
            function (response) {
                var clearData = {
                    'selectedShippingAddress': null,
                    'shippingAddressFromData': null,
                    'newCustomerShippingAddress': null,
                    'selectedShippingRate': null,
                    'selectedPaymentMethod': null,
                    'selectedBillingAddress': null,
                    'billingAddressFromData': null,
                    'newCustomerBillingAddress': null
                };

                if (response.responseType !== 'error') {
                    customerData.set('checkout-data', clearData);
                }
            }
        ).always(
            function () {
                fullScreenLoader.stopLoader();
                _.each(hooks.afterRequestListeners, function (listener) {
                    listener();
                });
            }
        );
    };

});
