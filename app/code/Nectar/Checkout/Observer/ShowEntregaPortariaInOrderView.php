<?php

namespace Nectar\Checkout\Observer;

use Magento\Framework\Event\Observer as EventObserver;

class ShowEntregaPortariaInOrderView implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\View\Element\Template
     */
    private $template;

    public function __construct(
        \Magento\Framework\View\Element\Template $template
    )
    {
        $this->template = $template;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($observer->getElementName() === "order_info") {
            $orderViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderViewBlock->getOrder();
            $entrega_portaria = $order->getEntregaPortaria();

            $this->template->setEntregaPortaria($entrega_portaria);
            $this->template->setTemplate('Nectar_Checkout::order_info_shipping_info.phtml');

            $html = $observer->getTransport()->getOutput() . $this->template->toHtml();
            $observer->getTransport()->setOutput($html);
        }
    }
}