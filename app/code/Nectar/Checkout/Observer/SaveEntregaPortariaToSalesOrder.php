<?php

namespace Nectar\Checkout\Observer;

class SaveEntregaPortariaToSalesOrder implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Quote\Model\QuoteRepository
     */
    private $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    )
    {
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $order->setEntregaPortaria($quote->getEntregaPortaria());

        return $this;
    }
}