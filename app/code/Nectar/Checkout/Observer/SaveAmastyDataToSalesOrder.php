<?php

namespace Nectar\Checkout\Observer;

use Amasty\Deliverydate\Api\Data\DeliverydateInterface;
use Magento\Framework\Stdlib\DateTime as DateTimeConverter;

class SaveAmastyDataToSalesOrder implements \Magento\Framework\Event\ObserverInterface
{

    const RESETED_DATE_VALUE = '0000-00-00';

    /**
     * @param \Magento\Quote\Model\QuoteRepository
     */
    private $quoteRepository;

    /**
     * @param \Amasty\Deliverydate\Helper\Data
     */
    private $amHelper;

    /**
     * @param \Amasty\Deliverydate\Model\ResourceModel\Tinterval
     */
    private $tintervalResourceModel;

    /**
     * @param \Amasty\Deliverydate\Model\TintervalFactory
     */
    private $tintervalFactory;

    /**
     * @param \Amasty\Deliverydate\Model\DeliverydateConfigProvider
     */
    private $configProvider;

    /**
     * @var \Magento\Framework\Data\Form\Filter\DateFactory
     */
    private $dateFactory;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Amasty\Deliverydate\Helper\Data $amHelper,
        \Amasty\Deliverydate\Model\ResourceModel\Tinterval $tintervalResourceModel,
        \Amasty\Deliverydate\Model\TintervalFactory $tintervalFactory,
        \Amasty\Deliverydate\Model\DeliverydateConfigProvider $configProvider,
        \Magento\Framework\Data\Form\Filter\DateFactory $dateFactory
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->amHelper = $amHelper;
        $this->tintervalResourceModel = $tintervalResourceModel;
        $this->tintervalFactory = $tintervalFactory;
        $this->configProvider = $configProvider;
        $this->dateFactory = $dateFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $data = [
            'date' => isset($_COOKIE['urban_amasty_date']) ? $_COOKIE['urban_amasty_date'] : '',
            'tinterval_id' => isset($_COOKIE['urban_amasty_time_interval_id']) ? $_COOKIE['urban_amasty_time_interval_id'] : '',
            'comment' => isset($_COOKIE['urban_amasty_comment']) ? $_COOKIE['urban_amasty_comment'] : ''
        ];

        if(is_array($data)) {

            if ($data['date'] && $data['date'] !== self::RESETED_DATE_VALUE) {
                $format = $this->getFormat();
                // convert from js date format to php date format Y-m-d
                $convertedDate = $this->convertDate($data['date'], $format);
                $data['converted_date'] = $convertedDate;
                $order->setAmastyDate($data['converted_date']);
            }
            
            if (!empty($data[DeliverydateInterface::TINTERVAL_ID])) {
                /* load Time interval by ID and combine it to string */
                $tint = $this->tintervalFactory->create();
                $this->tintervalResourceModel->load($tint, (int)$data['tinterval_id']);
                $data['time'] = $tint->getTimeFrom() . " - " . $tint->getTimeTo();
                $order->setAmastyTime($data['time']);
            }
        }
        
        $order->setAmastyComment($data['comment']);
        $order->setAmastyTintervalId($data['tinterval_id']);

        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getFormat()
    {
        $format = $this->configProvider->getPickerDateFormat();
        return $format;
    }

    public function convertDate($date, $format)
    {
        $filter = $this->dateFactory->create(['format' => $format]);
        try {
            $convertedDate = $filter->inputFilter($date);
            $dateValidation = \DateTime::createFromFormat(DateTimeConverter::DATE_PHP_FORMAT, $convertedDate);
        } catch (\Throwable $e) {
            $dateValidation = false;
        }

        if (!$dateValidation && $format !== \Amasty\Deliverydate\Model\DeliverydateConfigProvider::OUTPUT_DATE_FORMAT) {
            return $this->convertDate($date, \Amasty\Deliverydate\Model\DeliverydateConfigProvider::OUTPUT_DATE_FORMAT);
        }
        if (!$dateValidation) {
            // data is invalid
            return null;
        }

        return $convertedDate;
    }

}