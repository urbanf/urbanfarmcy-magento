<?php

namespace Nectar\Checkout\Plugin\Block\Checkout;

class LayoutProcessor extends \Magento\Framework\View\Element\Template
{
    
    public function afterProcess(\Magento\Checkout\Block\Checkout\LayoutProcessor $subject, array $jsLayout)
    {
        $portariaAttributeCode = 'entrega_portaria';
        $portariaField = [
            'component' => 'Nectar_Checkout/js/single-checkbox',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'tooltip' => [
                    'description' => __('Alguns itens podem ser congelados'),
                ],
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $portariaAttributeCode,
            'label' => __('Pode deixar na portaria?'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 10,
            'visible' => true,
            'initialValue' => true,
            'valueMap' => [
                'true' => true,
                'false' => false
            ]
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shippingAdditional']['children'][$portariaAttributeCode] = $portariaField;

        return $jsLayout;
    }
    
}
