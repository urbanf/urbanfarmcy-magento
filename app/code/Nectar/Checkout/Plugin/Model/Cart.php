<?php
namespace Nectar\Checkout\Plugin\Model;

class Cart
{
    public function __construct()
    {
    }

    public function beforeAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        $productInfo,
        $requestInfo = null
    ) {
        if($productInfo->getTypeId() === "bundle") {
            if($requestInfo !== null && !array_key_exists('bundle_option', $requestInfo)) {
                $requestInfo['bundle_option'] = $this->getBundleOptions($productInfo);// opções do bundle
            }
        }
        return [$productInfo, $requestInfo];
    }

    /**
     * Get bundle product Options to add product to cart
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    private function getBundleOptions(\Magento\Catalog\Model\Product $product): array
    {
        $selectionCollection = $product->getTypeInstance()
            ->getSelectionsCollection(
                $product->getTypeInstance()->getOptionsIds($product),
                $product
            );
        $bundleOptions = [];
        foreach ($selectionCollection as $selection) {
            $bundleOptions[$selection->getOptionId()][] = $selection->getSelectionId();
        }
        return $bundleOptions;
    }

}
