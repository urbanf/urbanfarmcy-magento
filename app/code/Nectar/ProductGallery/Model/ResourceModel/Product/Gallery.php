<?php
namespace Nectar\ProductGallery\Model\ResourceModel\Product;

/**
 * Class Gallery
 * @package Nectar\ProductGallery\Model\ResourceModel\Product
 */
class Gallery extends \Magento\Catalog\Model\ResourceModel\Product\Gallery
{
    /**
     * @param $subject
     * @param $result
     * @param $storeId
     * @return mixed
     */
    public function afterCreateBatchBaseSelect($subject, $result, $storeId)
    {
        $select = $result->columns([
            'image_position' => $this->getConnection()->getIfNullSql('`value`.`image_position`', '`default_value`.`image_position`'),
            'image_position_default' => 'default_value.image_position',
        ]);

        return $select;
    }
}
