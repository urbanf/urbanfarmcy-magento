<?php

namespace Nectar\ProductGallery\Plugin\Model\Product\Gallery;

class CreateHandler
{

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Gallery
     */
    private $resourceModel;

    /**
     * @param \Magento\Framework\EntityManager\MetadataPool
     */
    private $metadata;

    /**
     * @param \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Gallery $resourceModel,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->resourceModel = $resourceModel;
        $this->metadata = $metadataPool->getMetadata(\Magento\Catalog\Api\Data\ProductInterface::class);
    }

    public function afterExecute(
        \Magento\Catalog\Model\Product\Gallery\CreateHandler $subject,
        $result,
        $product,
        $arguments = []
    ) {

        $attrCode = $subject->getAttribute()->getAttributeCode();
        $value = $product->getData($attrCode);

        if (!is_array($value) || !isset($value['images'])) {
            return $product;
        }

        if (!is_array($value['images']) && strlen($value['images']) > 0) {
            $value['images'] = $this->jsonHelper->jsonDecode($value['images']);
        }

        if (!is_array($value['images'])) {
            $value['images'] = [];
        }

        if (!is_array($value) || !isset($value['images']) || $product->isLockedAttribute($attrCode)) {
            return $product;
        }
        
        $this->processImages($subject, $product, $value['images']);

        $product->setData($attrCode, $value);
        
        return $product;

    }

    protected function processImages($subject, $product, array &$images) {

        $a = 1;

        foreach ($images as &$image) {
            if (empty($image['removed'])) {
                $data = $this->processNewImage($subject, $product, $image);

                if (!$product->isObjectNew()) {
                    $this->resourceModel->deleteGalleryValueInStore(
                        $image['value_id'],
                        $product->getData($this->metadata->getLinkField()),
                        $product->getStoreId()
                    );
                }
                // Add per store labels, position, disabled
                $data['value_id'] = $image['value_id'];
                $data['label'] = isset($image['label']) ? $image['label'] : '';
                $data['position'] = isset($image['position']) ? (int)$image['position'] : 0;
                $data['disabled'] = isset($image['disabled']) ? (int)$image['disabled'] : 0;
                $data['image_position'] = isset($image['image_position']) && !empty($image['image_position']) ? $image['image_position'] : "50%";
                $data['store_id'] = (int)$product->getStoreId();

                $data[$this->metadata->getLinkField()] = (int)$product->getData($this->metadata->getLinkField());

                $this->resourceModel->insertGalleryValueInStore($data);
            }
        }

    }

    protected function processNewImage($subject, $product, array &$image) {
        $data = [];

        $data['value'] = $image['file'];
        $data['attribute_id'] = $subject->getAttribute()->getAttributeId();

        if (!empty($image['media_type'])) {
            $data['media_type'] = $image['media_type'];
        }

        $image['value_id'] = $this->resourceModel->insertGallery($data);

        $this->resourceModel->bindValueToEntity(
            $image['value_id'],
            $product->getData($this->metadata->getLinkField())
        );

        return $data;
    }

}
