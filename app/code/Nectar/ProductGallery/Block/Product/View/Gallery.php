<?php

namespace Nectar\ProductGallery\Block\Product\View;

/**
 * Class Gallery
 * @package Nectar\ProductGallery\Block\Product\View
 */
class Gallery extends \Magento\Catalog\Block\Product\View\Gallery
{
    /**
     * @param $subject
     * @param $result
     * @return false|string
     */
    public function afterGetGalleryImagesJson($subject, $result)
    {
        $r = [];
        $k = 0;

        foreach($this->getGalleryImages()->getItems() as $image){
            $r[$k]['thumb'] = $image->getData('small_image_url');
            $r[$k]['img'] = $image->getData('medium_image_url');
            $r[$k]['full'] = $image->getData('large_image_url');
            $r[$k]['caption'] = ($image->getLabel() ?: $this->getProduct()->getName());
            $r[$k]['position'] = $image->getData('position');
            $r[$k]['image_position'] = $image->getData('image_position');
            $r[$k]['isMain'] = $this->isMainImage($image);
            $r[$k]['type'] = str_replace('external-', '', $image->getMediaType());
            $r[$k]['videoUrl'] = $image->getVideoUrl();
            $k++;
        }

        return json_encode($r);
    }
}
