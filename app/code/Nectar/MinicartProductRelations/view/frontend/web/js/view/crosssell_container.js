define([
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'mage/url',
    'slick',
    'mage/cookies',
    'catalogAddToCart'
], function ($, Component, customerData, url) {
    'use strict';

    let miniCart = $('[data-block=\'minicart\']');
    let initializeCrosssellSlick = function () {
        if($('.minicart-crosssell-items > .list').children().length > 0) {
            $('.minicart-crosssell-items > .list').slick({
                autoplay: true,
                arrows: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1
            });
        }
    };

    miniCart.on('dropdowndialogopen', function () {
        if($('.minicart-crosssell-items > .list.slick-initialized').length === 0) {
            initializeCrosssellSlick();
        } else {
            $('.minicart-crosssell-items > .list.slick-initialized').slick("refresh");
        }
    });

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
        },

        initializeSlick: function() {
            initializeCrosssellSlick();
        }
    });
});
