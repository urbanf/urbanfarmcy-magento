/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'mage/url',
    'Magento_Catalog/js/price-utils',
    'mage/cookies',
    'catalogAddToCart'
], function ($, Component, customerData, url, priceUtils) {
    'use strict';

    const globalPriceFormat = {
        decimalSymbol: ',',
        groupLength: 3,
        groupSymbol: ".",
        integerRequired: false,
        pattern: "R$%s",
        precision: 2,
        requiredPrecision: 2
    };

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();

            let cartData = customerData.get('cart');
            cartData.subscribe(function (updatedCart) {
                console.log("Itens no carrinho: ");
                console.log(updatedCart);
            }, this);
        },

        getActionUrl: function() {
            let actionUrl = url.build('checkout/cart/add');
            return actionUrl;
        },

        getProductPrice: function(item) {
            return priceUtils.formatPrice(item.price, globalPriceFormat);
        },

        getProductSpecialPrice: function(item) {
            if(item.special_price) {
                return priceUtils.formatPrice(item.special_price, globalPriceFormat);
            }
            return null;
        },

        hasSpecialPrice: function(item) {
            if(item.special_price) {
                return true;
            }
            return false;
        },

        getFormKey: function() {
            return $.mage.cookies.get('form_key');
        },

        addToCart: function(form) {
            var jqForm = $(form).catalogAddToCart({
                bindSubmit: false
            });

            jqForm.catalogAddToCart('submitForm', jqForm);

            return false;
        }

    });
});
