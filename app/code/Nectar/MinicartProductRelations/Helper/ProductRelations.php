<?php

namespace Nectar\MinicartProductRelations\Helper;

class ProductRelations
{
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_catalogConfig = $context->getCatalogConfig();
        $this->productRepository = $productRepository;
    }

    /**
     * Get upsell collection for specified product (id)
     * @param string $product_id
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getUpsellProductsForCurrentItem(string $product_id)
    {
        $upsell_collection = [];
        $product = $this->productRepository->getById($product_id);
        
        $upsell_collection = $product->getUpSellProductCollection()->setPositionOrder()->addStoreFilter();
        $upsell_collection = $this->_addProductAttributesAndPrices($upsell_collection);
        $upsell_collection->load();

        return $upsell_collection;
    }

    /**
     * Get crosssell collection for specified product (id)
     * @param string $product_id
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getCrosssellProductsForCurrentItem(string $product_id)
    {
        $crosssell_collection = [];
        $product = $this->productRepository->getById($product_id);
        
        $crosssell_collection = $product->getCrossSellProductCollection()
            ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
            ->setPositionOrder()
            ->addStoreFilter();
        $crosssell_collection = $this->_addProductAttributesAndPrices($crosssell_collection);
        $crosssell_collection->load();

        return $crosssell_collection;
    }

    /**
     * Get related products collection for specified product (id)
     * @param string $product_id
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getRelatedProductsForCurrentItem(string $product_id)
    {
        $related_products_collection = [];
        $product = $this->productRepository->getById($product_id);
        
        $related_products_collection = $product->getCrossSellProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter();
        $related_products_collection = $this->_addProductAttributesAndPrices($related_products_collection);

        $related_products_collection->load();

        return $related_products_collection;
    }

    /**
     * Add all attributes and apply pricing logic to products collection
     * to get correct values in different products lists.
     * E.g. crosssells, upsells, new products, recently viewed
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _addProductAttributesAndPrices(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        return $collection
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
            ->addUrlRewrite();
    }
}
