<?php

namespace Nectar\MinicartProductRelations\Plugin\CustomerData;

class Cart
{
    /**
     * @param \Nectar\MinicartProductRelations\Helper\ProductRelations
     */
    private $productRelationsHelper;

    /**
     * @param \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface
     */
    private $itemResolver;

    public function __construct(
        \Nectar\MinicartProductRelations\Helper\ProductRelations $productRelationsHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface $itemResolver
    ) {
        $this->itemResolver = $itemResolver;
        $this->productRelationsHelper = $productRelationsHelper;
        $this->imageHelper = $imageHelper;
    }

    public function afterGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        $result
    ) {
        $items = $result['items'];
        $relations_data = [
            "upsell_items" => [],
            "crosssell_items" => [],
            "related_items" => []
        ];

        if (!empty($items)) {
            $cart_items_ids = array_column($items, 'product_id');

            foreach ($items as $item) {
                $product_id = $item['product_id'];
                $upsell_collection = $this->productRelationsHelper->getUpsellProductsForCurrentItem($product_id);
                $crosssell_collection = $this->productRelationsHelper->getCrosssellProductsForCurrentItem($product_id);
                $related_collection = $this->productRelationsHelper->getRelatedProductsForCurrentItem($product_id);

                foreach ($upsell_collection as $upsell_product) {
                    $product_data = $upsell_product->getData();

                    // Evita de inserir na lista produtos que já estão adicionados ao carrinho
                    if (in_array($product_data['entity_id'], $cart_items_ids)) {
                        continue;
                    }

                    // Evita exibir produtos duplicados
                    if (isset($relations_data['upsell_items']['items'])) {
                        $search_value = $product_data['entity_id'];
                        $search_key = 'entity_id';
                        $search_array = $relations_data['upsell_items']['items'];
                        $search_result = array_search($search_value, array_column($search_array, $search_key));
                        if ($search_result !== false) {
                            continue;
                        }
                    }

                    $imageHelper = $this->imageHelper->init($upsell_product, 'cart_cross_sell_products');
                    $product_data['product_image'] = [
                        'src' => $imageHelper->getUrl(),
                        'alt' => $imageHelper->getLabel(),
                        'width' => $imageHelper->getWidth(),
                        'height' => $imageHelper->getHeight(),
                    ];
                    $product_data['product_url'] = $upsell_product->getProductUrl();
                    $relations_data['upsell_items']['items'][] = $product_data;
                }
        
                foreach ($crosssell_collection as $crosssell_product) {
                    $product_data = $crosssell_product->getData();

                    // Evita de inserir na lista produtos que já estão adicionados ao carrinho
                    if (in_array($product_data['entity_id'], $cart_items_ids)) {
                        continue;
                    }

                    // Evita exibir produtos duplicados
                    if (isset($relations_data['crosssell_items']['items'])) {
                        $search_value = $product_data['entity_id'];
                        $search_key = 'entity_id';
                        $search_array = $relations_data['crosssell_items']['items'];
                        $search_result = array_search($search_value, array_column($search_array, $search_key));
                        if ($search_result !== false) {
                            continue;
                        }
                    }

                    $imageHelper = $this->imageHelper->init($crosssell_product, 'cart_cross_sell_products');
                    $product_data['product_image'] = [
                        'src' => $imageHelper->getUrl(),
                        'alt' => $imageHelper->getLabel(),
                        'width' => $imageHelper->getWidth(),
                        'height' => $imageHelper->getHeight(),
                    ];
                    $product_data['product_url'] = $crosssell_product->getProductUrl();

                    if ($product_data['type_id'] === 'bundle') {
                        $regular_price = $crosssell_product->getPriceInfo()->getPrice('regular_price');
                        $original_price = $regular_price->getMinimalPrice()->getValue();
                        $product_data['price'] = $original_price;

                        if ($product_data['special_price']) {
                            $final_price = $crosssell_product->getPriceInfo()->getPrice('final_price');
                            $special_price = $final_price->getAmount()->getValue();
                            $product_data['special_price'] = $special_price;
                        }
                    }

                    $relations_data['crosssell_items']['items'][] = $product_data;
                }
        
                foreach ($related_collection as $related_product) {
                    $product_data = $related_product->getData();

                    // Evita de inserir na lista produtos que já estão adicionados ao carrinho
                    if (in_array($product_data['entity_id'], $cart_items_ids)) {
                        continue;
                    }

                    // Evita exibir produtos duplicados
                    if (isset($relations_data['related_items']['items'])) {
                        $search_value = $product_data['entity_id'];
                        $search_key = 'entity_id';
                        $search_array = $relations_data['related_items']['items'];
                        $search_result = array_search($search_value, array_column($search_array, $search_key));
                        if ($search_result !== false) {
                            continue;
                        }
                    }

                    $imageHelper = $this->imageHelper->init($related_product, 'cart_cross_sell_products');
                    $product_data['product_image'] = [
                        'src' => $imageHelper->getUrl(),
                        'alt' => $imageHelper->getLabel(),
                        'width' => $imageHelper->getWidth(),
                        'height' => $imageHelper->getHeight(),
                    ];
                    $product_data['product_url'] = $related_product->getProductUrl();
                    $relations_data['related_items']['items'][] = $product_data;
                }
            }
        }

        return array_merge($result, $relations_data);
    }

    protected function getProductForThumbnail($item) {
        return $this->itemResolver->getFinalProduct($item);
    }
}
