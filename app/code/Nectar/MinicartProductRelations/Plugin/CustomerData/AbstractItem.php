<?php

namespace Nectar\MinicartProductRelations\Plugin\CustomerData;

class AbstractItem
{
    /**
     * @param \Nectar\MinicartProductRelations\Helper\ProductRelations
     */
    private $productRelationsHelper;

    public function __construct(
        \Nectar\MinicartProductRelations\Helper\ProductRelations $productRelationsHelper
    ) {
        $this->productRelationsHelper = $productRelationsHelper;
    }

    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item $item
    ) {
        $data = $proceed($item);

        $relations_data = [
            "upsell_items" => [],
            "crosssell_items" => [],
            "related_items" => []
        ];
        
        $product_id = $item->getProductId();
        $upsell_collection = $this->productRelationsHelper->getUpsellProductsForCurrentItem($product_id);
        $crosssell_collection = $this->productRelationsHelper->getCrosssellProductsForCurrentItem($product_id);
        $related_products_collection = $this->productRelationsHelper->getRelatedProductsForCurrentItem($product_id);

        foreach ($upsell_collection as $upsell_product) {
            $relations_data['upsell_items'][] = $upsell_product->getData();
        }

        foreach ($crosssell_collection as $crosssell_product) {
            $relations_data['crosssell_items'][] = $crosssell_product->getData();
        }

        foreach ($related_products_collection as $related_product) {
            $relations_data['related_items'][] = $related_product->getData();
        }
        
        return array_merge($data, $relations_data);
    }

}
