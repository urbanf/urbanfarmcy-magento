<?php

namespace Nectar\Elasticsearch\Plugin\Model\Adapter\Index;

class Builder
{

    /**
     * Adiciona 'asciifolding' aos filtros para retornar palavras com acento na busca
     * 
     * @param \Magento\Elasticsearch\Model\Adapter\Index\Builder $subject
     * @param array $result
     * @return array
     */
    public function afterBuild(\Magento\Elasticsearch\Model\Adapter\Index\Builder $subject, $result) : array {
        $result['analysis']['analyzer']['default']['filter'][] = 'asciifolding';
        return $result;
    }

}
