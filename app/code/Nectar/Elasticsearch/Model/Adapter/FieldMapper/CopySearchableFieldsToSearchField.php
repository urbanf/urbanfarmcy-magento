<?php

namespace Nectar\Elasticsearch\Model\Adapter\FieldMapper;

use \Magento\Elasticsearch\Model\Adapter\FieldMapper\CopySearchableFieldsToSearchField as Magento_CopySearchableFieldsToSearchField;

class CopySearchableFieldsToSearchField extends Magento_CopySearchableFieldsToSearchField
{
    
    /**
     * List of field types to copy
     */
    private const FIELD_TYPES = ['text', 'keyword'];
    /**
     * Add "copy_to" parameter for default search field to index fields.
     *
     * Emulates catch all field (_all) for elasticsearch
     *
     * @param array $mapping
     * @return array
     */
    public function process(array $mapping): array
    {
        foreach ($mapping as $field => $definition) {
            if ($this->isSearchable($definition)) {
                if($definition['type'] !== "keyword") { // Se for "keyword" (select, multiselect, etc.), não adiciona à busca parcial
                    $definition['copy_to'][] = '_search';
                    $mapping[$field] = $definition;
                }
            }
        }
        return $mapping;
    }

    /**
     * Determine if the field is searchable by mapping
     *
     * The field is searchable if it's indexed and its mapping type is either "text" or "keyword"
     *
     * @param array $mapping
     * @return bool
     */
    private function isSearchable(array $mapping): bool
    {
        return in_array($mapping['type'] ?? null, self::FIELD_TYPES) && (($mapping['index'] ?? true) !== false);
    }
    
}
