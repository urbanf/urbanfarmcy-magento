<?php
namespace Nectar\CatalogProductHandle\Plugin;

use Exception;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;

class ProductNewHandle
{
    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    public function __construct(
        AttributeSetRepositoryInterface $attributeSetRepository
    ) {
        $this->attributeSetRepository = $attributeSetRepository;
    }

    /**
     * @param int $attributeSetId
     * @return AttributeSetInterface
     */
    public function getAttributeSet($attributeSetId)
    {
        try {
            $attributeSet = $this->attributeSetRepository->get($attributeSetId);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        return $attributeSet;
    }

    public function beforeInitProductLayout(\Magento\Catalog\Helper\Product\View $subject, $resultPage, $product){
        $attributeSet = $this->getAttributeSet($product->getAttributeSetId());
        $resultPage->addPageLayoutHandles(
            ['grupo' => strtolower($attributeSet['attribute_set_name'])]
        );
    }
}
