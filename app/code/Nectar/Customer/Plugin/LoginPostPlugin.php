<?php
namespace Nectar\Customer\Plugin;

/**
 * Class LoginPostPlugin
 * @package Nectar\Customer\Plugin
 */
class LoginPostPlugin
{
    const LOGIN_REDIRECT_URL = 'before_login_redirect';

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $session;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\Url\EncoderInterface
     */
    private $encoder;

    /**
     * LoginPostPlugin constructor.
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Url\EncoderInterface $encoder
     */
    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Url\EncoderInterface $encoder
    )
    {
        $this->session = $session;
        $this->url = $url;
        $this->request = $request;
        $this->encoder = $encoder;
    }

    /**
     * @param \Magento\Customer\Controller\Account\LoginPost $subject
     * @param \Magento\Framework\Controller\Result\Redirect $result
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function afterExecute(
        \Magento\Customer\Controller\Account\LoginPost $subject,
        \Magento\Framework\Controller\Result\Redirect $result
    ) {
        if($this->session->isLoggedIn()){
            $cookie = isset($_COOKIE[self::LOGIN_REDIRECT_URL]) ? $_COOKIE[self::LOGIN_REDIRECT_URL] : "";

            if($cookie) {
                $result->setPath($cookie);
            } else {
                $result->setPath('*/*/');
            }
        } else {
            $request = $this->request;
            $pathInfo = $request->getPathInfo();
            $pathUrl = $this->url->getUrl($pathInfo);
            $CustomRedirectionUrl = $this->url->getUrl('customer/account/login', array('referer' => $this->encoder->encode($pathUrl)));

            $result->setPath($CustomRedirectionUrl);
        }
        return $result;
    }
}
