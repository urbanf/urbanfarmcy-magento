<?php
namespace Nectar\Customer\Plugin;

use Magento\Newsletter\Model\Subscriber as SubscriberModel;

class EmailSubscriberNotification extends \Magento\Newsletter\Model\Subscriber
{
    public function aroundSendConfirmationSuccessEmail(SubscriberModel $subject, callable $proceed) {
        return $subject;
    }
}
