<?php
namespace Nectar\Customer\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

/**
 * Class RedirectPlugin
 * @package Nectar\Customer\Plugin
 */
class RedirectPlugin
{
    const LOGIN_REDIRECT_URL = 'before_login_redirect';

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * RedirectPlugin constructor.
     * @param Registry $registry
     * @param UrlInterface $url
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Registry $registry,
        UrlInterface $url,
        ResultFactory $resultFactory
    ) {
        $this->coreRegistry = $registry;
        $this->url = $url;
        $this->resultFactory = $resultFactory;
    }

    /**
     * @param $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\Controller\Result\Redirect|mixed
     */
    public function aroundGetRedirect ($subject, \Closure $proceed)
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $cookie = isset($_COOKIE[self::LOGIN_REDIRECT_URL]) ? $_COOKIE[self::LOGIN_REDIRECT_URL] : "";

        if($cookie) {
            $redirect->setPath($cookie);
            return $redirect;
        }

        return $proceed();
    }
}
