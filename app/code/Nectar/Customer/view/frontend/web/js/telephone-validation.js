require([], function() {
    'use strict';

    window.addEventListener('load', () => {
        let telephoneFields = document.querySelectorAll('[name="telefone__cy"]');
        
        if(telephoneFields.length > 0) {
            telephoneFields.forEach( (field) => {
                field.classList.add('validate-telephone');
            });
        }
    });
    
});
