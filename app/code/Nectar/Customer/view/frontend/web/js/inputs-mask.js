require([
    'Inputmask'
], function() {
    'use strict';

    Inputmask({'mask': '99/99/9999'}).mask('[name="dob');
    Inputmask({'mask': '999.999.999-99'}).mask('[name="taxvat');
    Inputmask({'mask': '99999-999'}).mask('[name="postcode');

    Inputmask({
        'mask': ['(99)9999-9999', '(99)99999-9999'],
        'keepStatic': true
    }).mask('[name="telefone__cy').mask('[name="telephone');

    let telephoneFields = document.querySelectorAll('[name="telefone__cy"]');
    telephoneFields.forEach( (field) => {
        field.classList.add('validate-telephone');
    });

    Inputmask({
        'mask': ['999.999.999-99'],
        'keepStatic': true
    }).mask('[name="vat_id');
});
