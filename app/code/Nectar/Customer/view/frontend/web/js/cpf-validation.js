require([
    'jquery',
    'cpf'
], function($) {
    'use strict';

    function validateCpf(field, cpfNumber) {
        let numericValue = window.CPF.strip(cpfNumber);
        let fieldName = field.getAttribute('name');
        let fieldError = document.getElementById(fieldName + '-error');
        let button = document.querySelector('.action.primary');

        field.classList.remove('mage-error');
        button.removeAttribute('disabled');

        if(fieldError) {            
            fieldError.remove();
        }

        if(!window.CPF.isValid(numericValue, true)) {
            let error = document.createElement('div');
            let errorMessage = document.createTextNode('CPF inválido!');

            field.classList.remove('valid');
            field.classList.add('mage-error');
            field.setAttribute('aria-invalid', true);
            field.setAttribute('aria-describedby', fieldName + "-error");

            error.appendChild(errorMessage);
            error.setAttribute('for', fieldName);
            error.setAttribute('id', fieldName + "-error");
            error.setAttribute('generated', "true");
            error.classList.add('field-error');

            field.parentNode.appendChild(error);

            return false;
        }

        return true;

    };

    window.addEventListener('load', (event) => {
        
        let accountForms = document.querySelectorAll('.form-create-account, .form-edit-account, .form-address-edit');
        let cpfFields = document.querySelectorAll('[name="taxvat"], [name="vat_id"]');

        if(accountForms.length > 0) {
            accountForms.forEach( (form) => {
                form.addEventListener('submit', function(event) {
                    let isValid = null;

                    if(cpfFields.length > 0) {
                        cpfFields.forEach( (field) => {
                            isValid = validateCpf(field, field.value);
                        });
                    }

                    if(isValid === false) {
                        event.preventDefault();

                        window.scroll({
                            top: form.offsetTop,
                            left: 0,
                            behavior: 'smooth'
                        });
                    }
                });
            });
        }

        if(cpfFields.length > 0) {
            cpfFields.forEach( (field) => {
                field.addEventListener('blur', function(event) {
                    validateCpf(field, field.value);
                });
            });
        }

    });
    
});
