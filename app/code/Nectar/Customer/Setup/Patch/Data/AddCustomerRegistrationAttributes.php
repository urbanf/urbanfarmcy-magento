<?php

namespace Nectar\Customer\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

/**
 * Class AddBasicAccountAttributes
 * @package Nectar\Customer\Setup\Patch
 */
class AddCustomerRegistrationAttributes implements DataPatchInterface
{
    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * AddCustomerUpdatedAtAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        Config $eavConfig,
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->eavConfig = $eavConfig;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
 
        $customerEntity = $this->eavConfig->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
 
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $attributesOptionsArray = [
            'telefone__cy' => [
                'label' => 'Celular',
                'type' => 'varchar',
                'input' => 'text',
                'required' => true,
                'sort_order' => 100,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible' => true,
                'system' => false,
                'user_defined' => 1
            ],
            'opcao_alimentar__cy' => [
                'label' => 'Qual a sua opção alimentar?',
                'type' => 'varchar',
                'input' => 'select',
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                'required' => true,
                'sort_order' => 110,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible' => true,
                'system' => false,
                'user_defined' => 1,
                'option' => [
                    'values' => [
                        'Não consumo nenhum produto de origem animal',
                        'Não consumo carne animal',
                        'Como carne e produtos de origem animal, mas também procuro opções alternativas para o meu dia a dia'
                    ]
                ]
            ]
        ];

        foreach ($attributesOptionsArray as $attributeCode => $attributeParams) {
            
            $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeParams);

            $customAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $attributeCode);
            $customAttribute->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => [
                    'adminhtml_customer',
                    'customer_account_edit',
                    'customer_account_create',
                    'adminhtml_checkout'
                ]
            ]);

            $customAttribute->save();

        }

    }

}
