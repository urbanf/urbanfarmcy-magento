<?php

namespace Nectar\Customer\Controller\Account;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class SaveCPF extends Action implements HttpGetActionInterface
{
    /**
     * @var \Nectar\Customer\Helper\MyCustomerRepository
     */
    private $myCustomerRepository;

    /**
     * SaveCPF constructor.
     * @param Context $context
     * @param \Nectar\Customer\Helper\MyCustomerRepository $myCustomerRepository
     */
    public function __construct(
        Context $context,
        \Nectar\Customer\Helper\MyCustomerRepository $myCustomerRepository
    ) {
        $this->myCustomerRepository = $myCustomerRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(['success' => false]);

        $cpf = $this->getRequest()->getParam('cpf');

        if($result = $this->myCustomerRepository->setCpf($cpf)){
            $resultJson->setData(['success' => $result]);
        }

        return $resultJson;
    }
}
