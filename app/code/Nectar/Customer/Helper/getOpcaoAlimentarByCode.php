<?php

namespace Nectar\Customer\Helper;

class getOpcaoAlimentarByCode
{
    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function getLabel($option_id)
    {
        $retorno = 0;
        $tabela = $this->resourceConnection->getTableName('eav_attribute_option_value');
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from(
                ['c' => $tabela],
                ['*']
            )
            ->where(
                "c.option_id = :option_id"
            );
        $bind = ['option_id' => $option_id];
        $records = $connection->fetchAll($select, $bind);
        $label = $records[0]['value'];
        if ($label == 'Não consumo nenhum produto de origem animal') {
            $retorno = 1;
        } elseif ($label == 'Não consumo carne animal') {
            $retorno = 2;
        } elseif ('Como carne e produtos de origem animal, mas também procuro opções alternativas para o meu dia a dia') {
            $retorno = 3;
        }
        return ($retorno);
    }
}
