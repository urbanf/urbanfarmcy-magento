<?php
namespace Nectar\Customer\Helper;

class MyCustomerRepository
{
    /**
     * @param \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    private $customerSessionFactory;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * MyCustomerRepository constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\SessionFactory $customerSessionFactory
     * @param \Magento\Customer\Model\Customer $customer
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\SessionFactory $customerSessionFactory,
        \Magento\Customer\Model\Customer $customer
    )
    {
        $this->customerRepository = $customerRepository;
        $this->customerSessionFactory = $customerSessionFactory;
        $this->customer = $customer;
    }

    public function getCustomerId(){
        return $this->customerSessionFactory->create()->getCustomerId();
    }

    public function getCpf(){
        $customerId = $this->getCustomerId();
        return $this->customer->load($customerId)->getTaxvat();
    }

    public function setCpf($cpf){
        $customerId = $this->getCustomerId();

        if($customer = $this->customerRepository->getById($customerId)) {
            if ($this->customerRepository->save($customer->setTaxvat($cpf))) {
                return true;
            }
        }
        return false;
    }
}
