<?php
namespace Nectar\Customer\Helper;

class getCityCode
{
    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    public function __construct(
        \Magento\Framework\HTTP\Client\Curl $curl
    )
    {
        $this->curl = $curl;
    }

    public function getCode($cep) {
        $this->curl->addHeader("Content-Type", "application/json");
        $this->curl->get('https://viacep.com.br/ws/' . $cep . '/json/');
        $resposta = $this->curl->getBody();
        $response = $this->curl->getBody();
        $resposta = json_decode($response);
        return isset($resposta->ibge) ? $resposta->ibge : "";
    }
}
