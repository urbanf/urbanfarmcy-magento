<?php

namespace Nectar\Customer\Helper;

class sendCustomerInfo
{
    /**
     * @param \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param \Nectar\Customer\Logger\Logger
     */
    private $logger;

    /**
     * @param \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Nectar\Customer\Helper\getCityCode
     */
    private $getCityCode;

    /**
     * @param \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * @param \Nectar\Customer\Helper\getOpcaoAlimentarByCode
     */
    private $getOpcaoAlimentarByCode;

    /**
     * @param \Magento\Customer\Model\AddressFactory
     */
    private $addressFactory;

    /**
     * @param \Nectar\Customer\Helper\RetiraAcentos
     */
    private $retiraAcentos;

    public function __construct(
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Nectar\Customer\Logger\Logger $logger,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Nectar\Customer\Helper\getCityCode $getCityCode,
        \Magento\Customer\Model\Customer $customer,
        \Nectar\Customer\Helper\getOpcaoAlimentarByCode $getOpcaoAlimentarByCode,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Nectar\Customer\Helper\RetiraAcentos $retiraAcentos
    ) {
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->getCityCode = $getCityCode;
        $this->customer = $customer;
        $this->getOpcaoAlimentarByCode = $getOpcaoAlimentarByCode;
        $this->addressFactory = $addressFactory;
        $this->retiraAcentos = $retiraAcentos;
    }

    public function create($dados, $id, $criacao = true)
    {
        try {
            $this->logger->info($id);
            $json = json_encode($dados);
            $this->logger->info("Tentativa de criar novo cliente");
            $this->logger->info("Json enviado: " . $json);
            $userName = $this->scopeConfig->getValue("integrations/protheus/user");
            $password = $this->scopeConfig->getValue("integrations/protheus/password");
            $url = $this->scopeConfig->getValue("integrations/customer/criar");
            $this->curl->setCredentials($userName, $password);
            $this->curl->addHeader('tenantid', '01,0104');
            $this->curl->post($url, $json);
            $response = $this->curl->getBody();
            $resposta = json_decode($response);
            $this->logger->info("Json recebido: " . $response);

            $tabela = $this->resourceConnection->getTableName('customer_not_sended');
            $connection = $this->resourceConnection->getConnection();

            $isSent = "INSERT INTO " . $tabela . "  (id_customer) VALUES ('" . $id . "');";
            $connection->query($isSent);
            if (!isset($resposta->errorCode) || (isset($resposta->errorMessage) && $resposta->errorMessage == "Codigo e Loja informados já estão cadastrados.")) {
                $this->logger->info('Processo completado com sucesso!');
                $select = $connection->select()
                    ->from(
                        ['c' => $tabela],
                        ['*']
                    )
                    ->where(
                        "c.id_customer = :id_customer"
                    );
                $bind = ['id_customer' => $id];
                $records = $connection->fetchAll($select, $bind);
                if (isset($records[0])) {
                    $delete = "DELETE FROM " .  $tabela . " WHERE id_customer='" . $id . "';";
                    $connection->query($delete);
                }
                $this->edit($dados, $id);
            } elseif ($criacao) {
                $this->logger->info('Falha no envio do cliente');
                if (isset($tabela) && isset($id)) {
                    $isSent = "INSERT INTO " . $tabela . "  (id_customer) VALUES ('" . $id . "');";
                    $connection->query($isSent);
                }
            }
        } catch (\Exception $e) {
            $this->logger->info("Erro na tentativa de criação:" . $e->getMessage());
            $this->logger->info('Falha no envio do cliente');
            $isSent = "INSERT INTO " . $tabela . "  (id_customer) VALUES ('" . $id . "');";
            $connection->query($isSent);
        }
    }

    public function edit($dados, $customerId)
    {
        $codigo = $dados['code'] . "0001";
        $json = json_encode($dados);
        $this->logger->info("Tentativa de editar cliente");
        $this->logger->info("Json enviado: " . $json);
        $userName = $this->scopeConfig->getValue("integrations/protheus/user");
        $password = $this->scopeConfig->getValue("integrations/protheus/password");
        $url = $this->scopeConfig->getValue("integrations/customer/editar");
        $token = base64_encode("{$userName}:{$password}");

        //codigo para fazer um PUT
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . $token,
                'Content-Type: application/json',
                'tenantid: 01,0104'
            ),
        ));

        $response = curl_exec($curl);
        $this->logger->info("Json recebido: " . $response);
        $resposta = json_decode($response);

        curl_close($curl);

        if ((isset($resposta->Sucesso) && $resposta->Sucesso == "Cliente alterado")) {
            $this->logger->info('Processo completado com sucesso!');
        } elseif ((isset($resposta->errorMessage) && $resposta->errorMessage == "Codigo e Loja não localizados no cadastro.")) {
            $this->create($dados, $customerId, false);
        } else {
            $this->logger->info('Falha no envio do cliente');
        }
    }

    public function criarNovamente($customerId)
    {

        $customer = $this->customer->load($customerId);
        $customerData = $customer->getDataModel();
        $cpf = $customerData->getTaxvat();
        $cpf = preg_replace("/[^0-9]/", "", $cpf);

        $json["registerSituation"] = "2"; //1 - inativo || 2 -ativo

        $json["code"] = substr($cpf, 0, 8); //8 primeiros digitos cpf
        $json["storeId"] = "0001";

        $json["strategicCustomerType"] = "F";
        $json["shortName"] = $this->retiraAcentos->retira($customerData->getFirstName());
        $json["name"] =  $this->retiraAcentos->retira($customerData->getFirstName() . " " . $customerData->getLastName());
        $json["entityType"] = "F";
        $json["governmentalInformation"][0]["scope"] = "Federal";
        $json["governmentalInformation"][0]["id"] =  $cpf;
        $json["governmentalInformation"][0]["name"] = "CPF";

        //$phoneAtribute=$customerData->getCustomAttribute('telefone__cy');
        if ($cpf != Null) {
            // $cel = preg_replace("/[^0-9]/", "", $phoneAtribute->getValue() . "");
            // if ($cel[0] == "0") {
            //     $ddd = substr($cel, 1, 3);
            //     $num = substr($cel, 3);
            // } else {
            //     $ddd = substr($cel, 0, 2);
            //     $num = substr($cel, 2);
            // }

            $customerDob = $customerData->getDob();
            $dob = substr($customerDob, 8, 2) . "/" . substr($customerDob, 5, 2) . "/" . substr($customerDob, 0, 4);
            // $json["listOfCommunicationInformation"][0]["diallingCode"] = $ddd;
            // $json["listOfCommunicationInformation"][0]["phoneNumber"] = $num;
            $json["listOfCommunicationInformation"][0]["type"] = "";

            $json["listOfCommunicationInformation"][0]["email"] = $customerData->getEmail();
            $json["listOfCommunicationInformation"][0]["dtNasc"] = $dob;
            $json["listOfCommunicationInformation"][0]["opcAlimentar"] = "" .  $this->getOpcaoAlimentarByCode->getLabel($customerData->getCustomAttribute('opcao_alimentar__cy')->getValue());

            $addressId = $customerData->getDefaultBilling();
            $endereco = $this->addressFactory->create()->load($addressId);
            if (!isset($endereco)) {
                $atuar = true;
                $this->logger->info('ATUAR AQUI');
            } else {
                $numero = isset($endereco->getStreet()[1]) ? ", " . $endereco->getStreet()[1] : "";
                $rua = isset($endereco->getStreet()[0]) ? $endereco->getStreet()[0] : "Nao encontrado";
            }

            if (isset($endereco)) { //tirar esse if se quiser jogar exception e nao ficar sem mandar endereço
                //$json["address"]["number"] = isset($endereco->getStreet()[1]) && $endereco->getStreet()[1] != "" ? $endereco->getStreet()[1] : "nao encontrado";
                if (isset($endereco->getStreet()[0]) && isset($endereco->getStreet()[1]) && isset($endereco->getStreet()[3])) {
                    if (!isset($endereco->getStreet()[2])) {
                        $endereco->setStreet([
                            $endereco->getstreet()[0],
                            $endereco->getstreet()[1],
                            "-",
                            $endereco->getstreet()[3],
                        ]);
                    } elseif ($endereco->getStreet()[2] == "") {
                        $endereco->setStreet([
                            $endereco->getstreet()[0],
                            $endereco->getstreet()[1],
                            "-",
                            $endereco->getstreet()[3],
                        ]);
                    }
                }

                $cel = preg_replace("/[^0-9]/", "", $endereco->getTelephone() . "");
                if ($cel[0] == "0") {
                    $ddd = substr($cel, 1, 3);
                    $num = substr($cel, 3);
                } else {
                    $ddd = substr($cel, 0, 2);
                    $num = substr($cel, 2);
                }

                $json["listOfCommunicationInformation"][0]["diallingCode"] = $ddd;
                $json["listOfCommunicationInformation"][0]["phoneNumber"] = $num;

                $json["address"]["address"] = $this->retiraAcentos->retira(($rua . $numero) != "" ? $rua . $numero : "nao encontrado");
                $json["address"]["mainAddress"] = true;
                $json["address"]["zipCode"] = is_null($endereco->getPostcode()) ? "" : preg_replace("/[^0-9]/", "", $endereco->getPostcode());
                $json["address"]["state"]["stateId"] = is_null($endereco->getRegionCode()) ? "EX" : ($endereco->getRegionCode());
                $json["address"]["state"]["stateDescription"] = $this->retiraAcentos->retira(is_null($endereco->getRegion()) ? "nao encontrado" : ($endereco->getRegion()));
                $json["address"]["complement"] = $this->retiraAcentos->retira(isset($endereco->getStreet()[2]) && $endereco->getStreet()[2] != "" ? $endereco->getStreet()[2] : "");
                $json["address"]["district"] = $this->retiraAcentos->retira(isset($endereco->getStreet()[3]) && $endereco->getStreet()[3] != "" ? $endereco->getStreet()[3] : "nao encontrado");
                $json["address"]["city"]["cityCode"] = (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) ? (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) : "";
                $json["address"]["city"]["cityInternalId"] = (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) ? (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) : "";
                $json["address"]["city"]["cityDescription"] = is_null($endereco->getCity()) ? "Sao Paulo" : ($endereco->getCity());
            } else {
                //$json["address"]["number"] = "nao encontrado";
                $json["address"]["address"] = "nao encontrado";
                $json["address"]["mainAddress"] = false;
                $json["address"]["zipCode"] = "";
                $json["address"]["state"]["stateId"] = "EX";
                $json["address"]["state"]["stateDescription"] =  "nao encontrado";
                $json["address"]["complement"] = "";

                $json["address"]["district"] = "nao encontrado";
                $json["address"]["city"]["cityCode"] = "";
                $json["address"]["city"]["cityInternalId"] = "";
                $json["address"]["city"]["cityDescription"] = "Sao Paulo";
            }
            $this->logger->info("Tentativa de recriar cliente");
            $this->logger->info(json_encode($json));
            $this->create($json, $customerId, false);
        } else {

            $this->logger->info("Tentativa de recriar cliente falhou devido falta de cpf.");
        }
    }
}