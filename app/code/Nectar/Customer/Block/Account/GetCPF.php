<?php

namespace Nectar\Customer\Block\Account;

class GetCPF extends \Magento\Framework\View\Element\Template
{
    /**
     * GetCPF constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @return $this|GetCPF
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->pageConfig->getTitle()->set(__('Cadastro de estudante'));

        return $this;
    }
}
