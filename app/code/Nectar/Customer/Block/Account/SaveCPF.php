<?php

namespace Nectar\Customer\Block\Account;

class SaveCPF extends \Magento\Framework\View\Element\Template
{
    /**
     * SaveCPF constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @return $this|SaveCPF
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->pageConfig->getTitle()->set(__('Cadastro de estudante'));

        return $this;
    }
}
