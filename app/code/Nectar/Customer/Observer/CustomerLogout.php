<?php

namespace Nectar\Customer\Observer;

class CustomerLogout implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
    )
    {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
        $metadata->setPath('/');

        if ($this->cookieManager->getCookie('urban_delivery_postcode')) {
            $this->cookieManager->deleteCookie('urban_delivery_postcode', $metadata);
        }

        if ($this->cookieManager->getCookie('urban_current_city')) {
            $this->cookieManager->deleteCookie('urban_current_city', $metadata);
        }

        if ($this->cookieManager->getCookie('urban_current_store_code')) {
            $this->cookieManager->deleteCookie('urban_current_store_code', $metadata);
        }
        
    }
}