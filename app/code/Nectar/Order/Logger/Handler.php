<?php
namespace Nectar\Order\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/Order.log';
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context, // based on class thi will change
        \Magento\Framework\Filesystem\DriverInterface $filesystem
    ){
       $this->fileName='/var/log/Order-' . date('Y-m-d') . '.log';
       parent::__construct($filesystem, Null ,$this->fileName);
    }
}