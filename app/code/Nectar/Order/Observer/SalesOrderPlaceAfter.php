<?php
namespace Nectar\Order\Observer;

class SalesOrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        $orderCollection = $this->collectionFactory->create()->addFieldToFilter('customer_id',$order->getCustomerId());
        $orderArray = [];
        foreach($orderCollection as $orderCollectionItem) {
            $orderArray[] = $orderCollectionItem;
        }

        if(sizeof($orderArray) == 0 || sizeof($orderArray) == 1 && $orderArray[0]->getEntityId() == $order->getEntityId()){
            $order->setData('first_order', true);
        }else{
            $order->setData('first_order', false);
        }
    }
}