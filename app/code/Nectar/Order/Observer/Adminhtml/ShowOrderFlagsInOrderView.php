<?php
namespace Nectar\Order\Observer\Adminhtml;

class ShowOrderFlagsInOrderView implements \Magento\Framework\Event\ObserverInterface
{
    const EXTENSION_ATTRIBUTES = [
        [
            'code' => 'first_order',
            'label' => 'Is this the customer\'s first order?'
        ]
    ];

    /**
     * @param \Magento\Framework\View\Element\Template
     */
    private $template;

    public function __construct(
        \Magento\Framework\View\Element\Template $template
    )
    {
        $this->template = $template;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($observer->getElementName() === "order_info") {
            $orderViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderViewBlock->getOrder();
            $attributes = [];

            foreach(self::EXTENSION_ATTRIBUTES as $attribute) {
                $attributes[$attribute['code']]['value'] = $order->getData($attribute['code']);
                $attributes[$attribute['code']]['label'] = $attribute['label'];
            }

            $this->template->setFlag($attributes);
            $this->template->setTemplate('Nectar_Order::order_info_order_flags.phtml');

            $html = $observer->getTransport()->getOutput() . $this->template->toHtml();
            $observer->getTransport()->setOutput($html);
        }
    }
}