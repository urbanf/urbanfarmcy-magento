<?php
namespace Nectar\Order\Helper;

class StatusProtheus
{
    public function __construct()
    {
        
    }

    public function statusCorrelation($status){
        switch($status){
            
            case 'processing':
                return ('P');
            break;
            
            case 'motoboys_1':
                return ('P');
            break;
            
            case 'motoboys_2':
                return ('P');
            break;

            case 'motoboys_3':
                return ('P');
            break;
            
            case 'fraud':
                return ('Excluido');
            break;
            
            case 'pending_payment':
                return ('A');
            break;
            
            case 'payment_review':
                return ('A');
            break;
            
            case 'pending':
                return ('A');
            break;
            
            case 'holded':
                return ('A');
            break;
            
            case 'canceled':
                return ('Excluido');
            break;

            case 'complete':
                return ('E');
            break;
            
            case 'closed':
                return ('Excluido');
            break;

        }
    }
}
