<?php

namespace Nectar\Order\Helper;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;

class SendCustomerData
{
    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * @param \Nectar\Customer\Helper\getOpcaoAlimentarByCode
     */
    private $getOpcaoAlimentarByCode;

    /**
     * @param \Magento\Customer\Model\AddressFactory
     */
    private $addressFactory;

    /**
     * @param \Nectar\Customer\Helper\getCityCode
     */
    private $getCityCode;

    /**
     * @param \Nectar\Customer\Helper\sendCustomerInfo
     */
    private $sendCustomerInfo;

    /**
     * @param \Nectar\Order\Logger\Logger
     */
    private $logger;

    /**
     * @param \Nectar\Customer\Helper\RetiraAcentos
     */
    private $retiraAcentos;

    public function __construct(
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Nectar\Order\Helper\StatusProtheus $statusProtheus,
        \Amasty\Deliverydate\Model\DeliverydateFactory $deliverydateFactory,
        \Magento\Customer\Model\Customer $customer,
        \Nectar\Customer\Helper\getOpcaoAlimentarByCode $getOpcaoAlimentarByCode,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Nectar\Customer\Helper\getCityCode $getCityCode,
        \Nectar\Customer\Helper\sendCustomerInfo $sendCustomerInfo,
        \Nectar\Order\Logger\Logger $logger,
        \Nectar\Customer\Helper\RetiraAcentos $retiraAcentos
    ) {
        $this->cidadesPorCd = $cidadesPorCd;
        $this->statusProtheus = $statusProtheus;
        $this->deliverydateFactory = $deliverydateFactory;
        $this->customer = $customer;
        $this->getOpcaoAlimentarByCode = $getOpcaoAlimentarByCode;
        $this->addressFactory = $addressFactory;
        $this->getCityCode = $getCityCode;
        $this->sendCustomerInfo = $sendCustomerInfo;
        $this->logger = $logger;
        $this->retiraAcentos = $retiraAcentos;
    }

    /**
     * Prepara informações do cliente para editar seu cadastro no Protheus
     */
    public function sendCustomerData($order)
    {
        $customerId = $order->getCustomerId();
        if ($customerId) {
            $customer = $this->customer->load($customerId);
            $customerData = $customer->getDataModel();
            $cpf = $customerData->getTaxvat();
            $cpf = preg_replace("/[^0-9]/", "", $cpf);

            $json["registerSituation"] = "2"; //1 - inativo || 2 -ativo

            $json["code"] = substr($cpf, 0, 8); //8 primeiros digitos cpf
            $json["storeId"] = "0001";

            $json["strategicCustomerType"] = "F";
            $json["shortName"] = $this->retiraAcentos->retira($customerData->getFirstName());
            $json["name"] =  $this->retiraAcentos->retira($customerData->getFirstName() . " " . $customerData->getLastName());
            $json["entityType"] = "F";
            $json["governmentalInformation"][0]["scope"] = "Federal";
            $json["governmentalInformation"][0]["id"] =  $cpf;
            $json["governmentalInformation"][0]["name"] = "CPF";
            // $cel = preg_replace("/[^0-9]/", "", $customerData->getCustomAttribute('telefone__cy')->getValue() . "");
            // if ($cel[0] == "0") {
            //     $ddd = substr($cel, 1, 3);
            //     $num = substr($cel, 3);
            // } else {
            //     $ddd = substr($cel, 0, 2);
            //     $num = substr($cel, 2);
            // }

            $customerDob = $customerData->getDob();
            $dob = substr($customerDob, 8, 2) . "/" . substr($customerDob, 5, 2) . "/" . substr($customerDob, 0, 4);
            // $json["listOfCommunicationInformation"][0]["diallingCode"] = $ddd;
            // $json["listOfCommunicationInformation"][0]["phoneNumber"] = $num;
            $json["listOfCommunicationInformation"][0]["type"] = "";

            $json["listOfCommunicationInformation"][0]["email"] = $customerData->getEmail();
            $json["listOfCommunicationInformation"][0]["dtNasc"] = $dob;
            $json["listOfCommunicationInformation"][0]["opcAlimentar"] = $opcaoAlimentar;

            $addressId = $customerData->getDefaultBilling();
            $endereco = $this->addressFactory->create()->load($addressId);

            if (!isset($endereco)) {
                $atuar = true;
                $this->logger->info('ATUAR AQUI');
            } else {
                $numero = isset($endereco->getStreet()[1]) ? ", " . $endereco->getStreet()[1] : "";
                $rua = isset($endereco->getStreet()[0]) ? $endereco->getStreet()[0] : "Nao encontrado";
            }

            if (isset($endereco)) { //tirar esse if se quiser jogar exception e nao ficar sem mandar endereço
                //$json["address"]["number"] = isset($endereco->getStreet()[1]) && $endereco->getStreet()[1] != "" ? $endereco->getStreet()[1] : "nao encontrado";
                if (isset($endereco->getStreet()[0]) && isset($endereco->getStreet()[1]) && isset($endereco->getStreet()[3])) {
                    if (!isset($endereco->getStreet()[2])) {
                        $endereco->setStreet([
                            $endereco->getstreet()[0],
                            $endereco->getstreet()[1],
                            "-",
                            $endereco->getstreet()[3],
                        ]);
                    } elseif ($endereco->getStreet()[2] == "") {
                        $endereco->setStreet([
                            $endereco->getstreet()[0],
                            $endereco->getstreet()[1],
                            "-",
                            $endereco->getstreet()[3],
                        ]);
                    }
                } 
                
                $cel = preg_replace("/[^0-9]/", "", $endereco->getTelephone() . "");
                if ($cel[0] == "0") {
                    $ddd = substr($cel, 1, 3);
                    $num = substr($cel, 3);
                } else {
                    $ddd = substr($cel, 0, 2);
                    $num = substr($cel, 2);
                }

                $json["listOfCommunicationInformation"][0]["diallingCode"] = $ddd;
                $json["listOfCommunicationInformation"][0]["phoneNumber"] = $num;

                $json["address"]["address"] = $this->retiraAcentos->retira(($rua . $numero) != "" ? $rua . $numero : "nao encontrado");
                $json["address"]["mainAddress"] = true;
                $json["address"]["zipCode"] = is_null($endereco->getPostcode()) ? "" : preg_replace("/[^0-9]/", "", $endereco->getPostcode());
                $json["address"]["state"]["stateId"] = is_null($endereco->getRegionCode()) ? "EX" : ($endereco->getRegionCode());
                $json["address"]["state"]["stateDescription"] = $this->retiraAcentos->retira(is_null($endereco->getRegion()) ? "nao encontrado" : ($endereco->getRegion()));        
                $json["address"]["complement"] = $this->retiraAcentos->retira(isset($endereco->getStreet()[2]) && $endereco->getStreet()[2] != "" ? $endereco->getStreet()[2] : "");
                $json["address"]["district"] = $this->retiraAcentos->retira(isset($endereco->getStreet()[3]) && $endereco->getStreet()[3] != "" ? $endereco->getStreet()[3] : "nao encontrado");
                $json["address"]["city"]["cityCode"] = (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) ? (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) : "";
                $json["address"]["city"]["cityInternalId"] = (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) ? (substr($this->getCityCode->getCode($json["address"]["zipCode"]), 2)) : "";
                $json["address"]["city"]["cityDescription"] = is_null($endereco->getCity()) ? "Sao Paulo" : ($endereco->getCity());
            } else {
                //$json["address"]["number"] = "nao encontrado";
                $json["address"]["address"] = "nao encontrado";
                $json["address"]["mainAddress"] = false;
                $json["address"]["zipCode"] = "";
                $json["address"]["state"]["stateId"] = "EX";
                $json["address"]["state"]["stateDescription"] =  "nao encontrado";
                $json["address"]["complement"] = "";
            
                $json["address"]["district"] = "nao encontrado";
                $json["address"]["city"]["cityCode"] = "";
                $json["address"]["city"]["cityInternalId"] = "";
                $json["address"]["city"]["cityDescription"] = "Sao Paulo";
            }
            $this->logger->info("Cliente editado");
            $this->logger->info(json_encode($json));
            $this->sendCustomerInfo->edit($json, $customerId);
        } else {
            $this->logger->info('problema');
        }
        return [$order];
    }
}
