<?php

namespace Nectar\Order\Cron;

class OrderExpire
{
  /**
   * @param \Nectar\Order\Logger\Logger
   */
  private $logger;

  /**
   * @param \Magento\Framework\App\ResourceConnection
   */
  private $resourceConnection;


  /**
   * @param \Magento\Sales\Model\OrderFactory
   */
  private $orderFactory;

  /**
   * @param \Nectar\Order\Helper\SendOrder
   */
  private $sendOrder;

  /**
   * @param \Nectar\Order\Helper\OrderExistente
   */
  private $orderExistente;

  public function __construct(
    \Nectar\Order\Logger\Logger $logger,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Magento\Sales\Model\OrderFactory $orderFactory
  ) {
    $this->logger = $logger;
    $this->resourceConnection = $resourceConnection;
    $this->orderFactory = $orderFactory;
  }

    public function execute() {

        $this->logger->info("OrderExpire : ENTROU NO CÓDIGO");
        $tabela = $this->resourceConnection->getTableName('sales_order');
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
          ->from(
            ['c' => $tabela],
            ['*']
          )
          ->join(
            ['sop' => 'sales_order_payment'],
            'c.entity_id=sop.parent_id'
          )
          ->where(
            "c.created_at < DATE_ADD(CURRENT_DATE, INTERVAL - 2 day)
            AND c.status = 'pending'
            AND (sop.method like '%pix' OR sop.method like '%billet')"
          );
        //$bind = ['created_at' => 0];
        $records = $connection->fetchAll($select);
        if (sizeof($records) > 0) {
          $this->logger->info("OrderExpire : " . json_encode("Tentativa de cancelar orders por falta de pagamento"));
          $this->logger->info("OrderExpire : "  . json_encode("Orders canceladas por falta de pagamento: "));
        }
        foreach ($records as $order) {
          $connection = $this->resourceConnection->getConnection();
          $OrderToCancel = "UPDATE " . $tabela . " SET status = 'canceled' WHERE entity_id=" . $order['entity_id'] . ";";
          $connection->query($OrderToCancel);
          $this->logger->info("OrderExpire : " . 'Orders canceladas: ' . $order['entity_id']);
        }       
    }
}