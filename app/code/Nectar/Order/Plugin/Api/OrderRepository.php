<?php
namespace Nectar\Order\Plugin\Api;

use Magento\Sales\Api\Data\OrderItemExtensionFactory;
use Magento\Sales\Api\Data\OrderItemExtensionInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderItemSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderRepository
{
    const ATTRIBUTE_NAME = 'entrega_portaria';

    /**
     * Order Extension Attributes Factory
     *
     * @var OrderItemExtensionFactory
     */
    protected $extensionFactory;

    /**
     * OrderItemRepositoryPlugin constructor
     *
     * @param OrderItemExtensionFactory $extensionFactory
     */
    public function __construct(OrderItemExtensionFactory $extensionFactory)
    {
        $this->extensionFactory = $extensionFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        $result,
        $id
    ) {
        $order = $result;

        $entregaPortaria = $order->getData(self::ATTRIBUTE_NAME);
        $extensionAttributes = $order->getExtensionAttributes();
        $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
        
        $extensionAttributes->setEntregaPortaria($entregaPortaria);
        $order->setExtensionAttributes($extensionAttributes);

        return $order;
    }

    public function afterGetList(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        $result,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    ) {

        $searchResult = $result;
        $orders = $searchResult->getItems();

        foreach ($orders as &$item) {
            $entregaPortaria = $item->getData(self::ATTRIBUTE_NAME);

            $extensionAttributes = $item->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
            
            $extensionAttributes->setEntregaPortaria($entregaPortaria);
            $item->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

}
