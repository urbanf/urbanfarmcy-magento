<?php

namespace Nectar\Order\Plugin\Api;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;

class BeforeOrderManagement
{
    /**
     * @param \Nectar\Cds\Helper\CidadesPorCd
     */
    private $cidadesPorCd;

    /**
     * @param \Nectar\Order\Logger\Logger
     */
    private $logger;

    public function __construct(
        \Nectar\Cds\Helper\CidadesPorCd $cidadesPorCd,
        \Nectar\Order\Logger\Logger $logger
    ) {
        $this->cidadesPorCd = $cidadesPorCd;
        $this->logger = $logger;
    }

    /**
     * @param OrderManagementInterface $subject
     * @param OrderInterface           $order
     *
     * @return OrderInterface[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforePlace(
        OrderManagementInterface $subject,
        OrderInterface $order
    ): array {
        $this->logger->info("Chegou no beforePlace");
        $cd = $this->cidadesPorCd->getCdResponsavel();
        $order->setData('cd_responsavel', $cd);
        $order->setData("was_received", "0");
        $order->setData("was_sent", "0");
        $this->logger->info("Saiu do beforePlace");
        return [$order];
    }
}
