define([
    "jquery",
    "Magento_Customer/js/customer-data"
], function($, customerData) {
    "use strict";

    return function (config, element) {

        if(customerData.get('customer')().firstname !== undefined) {
            element.innerText = 'Oi, ' + customerData.get('customer')().firstname;
        } else {
            customerData.reload(['customer']).then(() => {
                element.innerText = 'Oi, ' + customerData.get('customer')().firstname;
            });
        }
        
    };
});