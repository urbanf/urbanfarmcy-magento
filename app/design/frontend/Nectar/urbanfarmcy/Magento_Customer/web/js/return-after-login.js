define([
    "jquery",
    "mage/cookies"
], function($) {
    "use strict";

    return function (config) {

        let cookieName = "before_login_redirect";
        let action = $(config.action).find("a");
        let currentURL = config.currentURL;
        let cookieExpires = new Date(new Date().getTime() + 1 * 60 * 60 * 1000);

        action.on('click', function(ev){
            $.mage.cookies.set(
                cookieName,
                currentURL,
                { expires: cookieExpires, domain: "" }
            );
        });
    };
});
