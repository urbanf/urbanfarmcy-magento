require([
    'jquery',
    'matchMedia',
], function ($, mediaCheck) {
    'use strict';

    // TODO: refatorar script, ele está "entulhado"

    window.addEventListener('load', () => {

        let lastKnownScrollPosition = 0;
        let ticking = false;
        let bar = document.querySelector('.product-info-main');
        let gallery = document.querySelector('.product.media');
        let details = document.querySelector('.product.info.detailed');
        let height = 0;

        // bar.classList.add('sticky');

        function stickyBar(scrollPos, directionClass) {
            let staticBar = document.querySelector('.product-info-main:not(.sticky)');
            if(staticBar) {
                height = staticBar.offsetHeight;
            }

            if(directionClass === "scrolling-down") {

//                 if(scrollPos >= details.offsetTop - 20) {
                if(scrollPos >= gallery.offsetTop + height + 20) {
                    bar.classList.add('sticky');
                    console.log("iupi");
//                     gallery.style.marginBottom = height + "px";
                } else {
                    if(scrollPos + window.innerHeight >= gallery.offsetHeight + gallery.offsetTop) {
                        bar.classList.remove('sticky');
                        console.log("oi");
                        // gallery.style.marginBottom = "0px";
                    }
                }

            } else {
                
//                 if(scrollPos + window.innerHeight < gallery.offsetHeight + gallery.offsetTop + 20) {
//                     bar.classList.add('sticky');
//                     gallery.style.marginBottom = height + "px";
//                 } else {
                    console.log("ieie");
                    if(scrollPos < gallery.offsetTop + height + 20) {
                        bar.classList.remove('sticky');
                        console.log("tchau");
                        // gallery.style.marginBottom = "0px";
                    }
//                 }

            }
        }

        function stickyBarMobile(scrollPos, directionClass) {
            let staticBar = document.querySelector('.product-info-main:not(.sticky)');
            if(staticBar) {
                height = staticBar.offsetHeight;
            }

            if(directionClass === "scrolling-down") {

                if(scrollPos >= details.offsetTop - 20) {
                    bar.classList.add('sticky');
                    console.log("iupi");
                    gallery.style.marginBottom = height + "px";
                } else {
                    if(scrollPos + window.innerHeight >= gallery.offsetHeight + gallery.offsetTop) {
                        bar.classList.remove('sticky');
                        console.log("oi");
                        gallery.style.marginBottom = "0px";
                    }
                }

            } else {
                
                if(scrollPos + window.innerHeight < gallery.offsetHeight + gallery.offsetTop + 20) {
                    bar.classList.add('sticky');
                    gallery.style.marginBottom = height + "px";
                } else {
                    console.log("ieie");
                    if(scrollPos < details.offsetTop) {
                        bar.classList.remove('sticky');
                        console.log("tchau");
                        gallery.style.marginBottom = "0px";
                    }
                }

            }
        }

        document.addEventListener('scroll', function(e) {
            let directionClass = null;
            let st = window.pageYOffset || document.documentElement.scrollTop;
            if (st > lastKnownScrollPosition){
                // downscroll code
                directionClass = "scrolling-down";
            } else {
                // upscroll code
                directionClass = "scrolling-up";
            }

            lastKnownScrollPosition = st <= 0 ? 0 : st;

            if (!ticking) {
                window.requestAnimationFrame(function() {
                    // stickyBar(lastKnownScrollPosition, directionClass);

                    mediaCheck({
                        media: '(max-width: 1024px)',
                        entry: function() {
                            stickyBarMobile(lastKnownScrollPosition, directionClass);
                        },
                        exit: function() {
                            stickyBar(lastKnownScrollPosition, directionClass);
                        }
                    });

                    ticking = false;
                });

                ticking = true;
            }
        });
        
    });
    
});