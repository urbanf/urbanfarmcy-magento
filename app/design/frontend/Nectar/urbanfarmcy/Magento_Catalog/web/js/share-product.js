define([
    'jquery',
    'mage/url',
    'Magento_Ui/js/modal/modal'
], function($, url) {
    'use strict';

    $.widget('nectar.shareProduct', {
        options: {
            modalContent: '.product-share .social-links',
            modalOpener: '#share-product'
        },

        _create: function() {

            let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
            if (isMobile) {
                if (navigator.share) {
                    this.activateShareLink();
                } else {
                    this._createModal();
                }
            } else {
                this._createModal();
            }

        },

        activateShareLink: function() {

            let shareButton = document.getElementById('share-product');
            let shareData = {
                url: shareButton.getAttribute('data-product-url'),
                title: shareButton.getAttribute('data-product-name'),
                text: shareButton.getAttribute('data-product-name')
            };

            shareButton.addEventListener('click', event => {
                navigator.share(shareData).then(() => {
                    
                })
                .catch(console.error);
            });
        },

        _createModal: function() {

            $(this.options.modalContent).modal({
                type: 'popup',
                title: '',
                modalClass: 'pdp-share-product',
                clickableOverlay: true,
                trigger: this.options.modalOpener,
                autoOpen: false,
                responsive: true,
                buttons: []
            });

        }

    });

    return $.nectar.shareProduct;

});