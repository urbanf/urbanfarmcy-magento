define(['jquery'], function($){
    'use strict';

    return function(params){
        let qtyField = document.getElementById('qty-' + params.id);

        if(parseInt(qtyField.value) === parseInt(qtyField.getAttribute('min'))) {
            $('#dec-' + params.id).addClass('disabled');
        }

        $('#dec-' + params.id).on('click', function(ev){
            ev.preventDefault();

            let field = document.getElementById('qty-' + params.id);
            if(parseInt(field.value) > parseInt(field.getAttribute('min'))) {
                field.value = parseInt(field.value) - 1;
            }

            if(parseInt(qtyField.value) === parseInt(qtyField.getAttribute('min'))) {
                $('#dec-' + params.id).addClass('disabled');
            }
        });

        $('#inc-' + params.id).on('click', function(ev){
            ev.preventDefault();

            let field = document.getElementById('qty-' + params.id);
            field.value = parseInt(field.value) + 1;

            $('#dec-' + params.id).removeClass('disabled');
        });
    }
});
