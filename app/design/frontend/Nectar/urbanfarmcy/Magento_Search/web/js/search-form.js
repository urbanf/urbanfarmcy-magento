 define([
    'jquery',
    'uiComponent'
], function ($, Component) {
    'use strict';

    return Component.extend({
        options: {
            openButton: '.block-search .block-title',
            closeButton: '.block-search .close-block'
        },

        initialize: function() {
            $(this.options.openButton).on('click', this._openSearchBar);
            $(this.options.closeButton).on('click', this._openSearchBar);
        },

        _openSearchBar: function() {
            let searchBar = document.querySelector('.block-search .block-content');
            searchBar.classList.toggle('-active');
        }
        
    });
});
