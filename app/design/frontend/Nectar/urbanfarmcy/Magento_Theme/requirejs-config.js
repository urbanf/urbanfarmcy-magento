var config = {
    map: {
        '*': {
            choices: 'https://cdn.jsdelivr.net/npm/choices.js@4/public/assets/scripts/choices.min.js',
            use_choices: 'Magento_Theme/js/use-choices',
            mobile_navigation: 'Magento_Theme/js/mobile-navigation',
            header_panel_slider: 'Magento_Theme/js/header-panel-slider',
            display_footer: 'Magento_Theme/js/display-footer',
            sticky_header: 'Magento_Theme/js/sticky-header',
            Inputmask: 'Magento_Theme/js/lib/_old-inputmask.min',
            highlight: 'Magento_Theme/js/lib/highlight.pack',
            modernizr: 'Magento_Theme/js/lib/modernizr.custom.min',
            gsap: 'Magento_Theme/js/lib/gsap3/gsap.min',
            TweenMax: 'Magento_Theme/js/lib/greensock/TweenMax.min',
            TweenLite: 'Magento_Theme/js/lib/greensock/TweenLite.min',
            TimelineMax: 'Magento_Theme/js/lib/greensock/TimelineMax.min',
            ScrollMagic: 'Magento_Theme/js/lib/ScrollMagic',
            animation: 'Magento_Theme/js/lib/plugins/animation.gsap',
            indicators: 'Magento_Theme/js/lib/plugins/debug.addIndicators',
            cpf: 'Magento_Theme/js/lib/cpf.min',
        }
    }
};
