require([
    'jquery', 'jquery/jquery.cookie', 'slick'
], function ($) {
    let _cookieImpacto = 'urban-display-impacto';
    let _cookieAllowed = 'user_allowed_save_cookie';

    if($.cookie(_cookieImpacto) == null && $.cookie(_cookieAllowed)){
        $('.display-impacto').addClass('show');
    }

    if($.cookie(_cookieAllowed) == null){
        $('.display-impacto').addClass('allowed');
    }

    $(".slider-display-impacto").slick({
        slidesToShow: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 4000
    });

    $('#btn-cookie-allow').on('click', function(ev){
        $('.display-impacto').addClass('show').removeClass('allowed');
    });

    $(".toggle-display-impacto").on("click", function(ev){
        $(".slider-display-impacto").slick('unslick');

        let _this = $(this);
        let _parent = _this.parents('.display-impacto');

        _parent.toggleClass('open');
        $(".slider-display-impacto").slick({
            slidesToShow: 1,
            arrows: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 4000
        });

        $(".slider-display-impacto").slick('slickNext');
    });

    $('.column-button .pagebuilder-button-link').on('click', function(ev){
        $(this).parents('.display-impacto').hide();

        let date = new Date();
        let minutes = 60;
            date.setTime(date.getTime() + (minutes * 60 * 24));
        $.cookie(_cookieImpacto, 'hide', {path: '/', expires: date});
    });
});
