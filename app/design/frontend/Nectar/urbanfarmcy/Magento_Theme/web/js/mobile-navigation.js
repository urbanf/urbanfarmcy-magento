define([
    'jquery',
    'matchMedia',
    'uiComponent'
], function ($, mediaCheck, Component) {
    'use strict';

    return Component.extend({
        initialize: function() {
            this._super();

            mediaCheck({
                media: '(max-width: 767px)',
                entry: function() {
    
                    let headerMenu = document.getElementById('header-menu-container');
                    let menuLists = headerMenu.querySelectorAll('li > ul');
    
                    menuLists.forEach( (list) => {
                        let container = list.parentNode;
                        let link = container.querySelector(':scope > a');

                        if(link !== null) {
                            container.classList.add('has-children');
    
                            link.addEventListener('click', function(event) {
                                event.preventDefault();
                                container.classList.toggle('active');
                            });
        
                            let categoryUrl = link.getAttribute('href');
                            let menu = $(list);
                            let categoryLink = $('<a>').attr('href', categoryUrl).text('Ver todos');
                            let categoryParent = $('<li>').addClass('all-category').html(categoryLink);
                            
                            if (menu.find('.all-category').length === 0) {
                                menu.prepend(categoryParent);
                            }
                        }
    
                    });
    
                }
    
            });
        }
    });
    
});