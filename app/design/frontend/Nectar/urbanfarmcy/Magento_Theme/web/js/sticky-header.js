require([
    'jquery'
], function ($, mediaCheck) {
    'use strict';

    // TODO: refatorar script, ele está "entulhado"

    let lastKnownScrollPosition = 0;
    let currentScrollPosition = 0;
    let ticking = false;

    function stickyHeader(scrollPos, directionClass) {
        let headerContent = document.querySelector('.header.content');
        let mainContent = document.getElementById('maincontent');
        let headerPanel = document.querySelector('.panel.wrapper');
        let height = headerContent.offsetHeight;

        if(directionClass === "scrolling-down") {
            headerContent.classList.remove('visible');
            mainContent.style.paddingTop = "0px";

            if(scrollPos > height) {
                headerContent.classList.add('invisible');
                mainContent.style.paddingTop = height + "px";
            }

        } else {
            headerContent.classList.remove('invisible');
            if(scrollPos <= headerPanel.offsetHeight) {
                headerContent.classList.remove('visible');
                mainContent.style.paddingTop = "0px";
            } else {
                headerContent.classList.add('visible');
                mainContent.style.paddingTop = height + "px";
            }
        }
    }

    document.addEventListener('scroll', function(e) {
        let directionClass = null;
        let st = window.pageYOffset || document.documentElement.scrollTop;
        if (st > lastKnownScrollPosition){
            // downscroll code
            directionClass = "scrolling-down";
        } else {
            // upscroll code
            directionClass = "scrolling-up";
        }

        lastKnownScrollPosition = st <= 0 ? 0 : st;

        if (!ticking) {
            window.requestAnimationFrame(function() {
                stickyHeader(lastKnownScrollPosition, directionClass);
                ticking = false;
            });

            ticking = true;
        }
    });
    
});