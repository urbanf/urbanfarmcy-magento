require(['jquery'], function($){
    let elements = $('.pagebuilder-banner.animate');
    let winHeight = $(window).height();
    let viewPortBottom = 0;
    let elPos = [];

    $(window).on('scroll', function() {
        viewPortBottom = $(this).scrollTop() + winHeight;

        elements.each(function(i, item) {
            elPos[i] = $(item).offset().top + ($(item).height() * 0.8);

            if(viewPortBottom > elPos[i]) {
                $(item).addClass('show');
            } else if(viewPortBottom < elPos[i]) {
                $(item).removeClass('show');
            }
        });
    });
});
