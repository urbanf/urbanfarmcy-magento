define([
    'jquery',
    'mage/url',
    'mage/cookies',
    'Magento_Ui/js/modal/modal'
], function($, url) {
    'use strict';

    $.widget('nectar.newsletterPopup', {
        options: {
            blockId: null,
            cookieName: 'open-newsletter-popup'
        },

        _create: function() {

            if(!$.mage.cookies.get(this.options.cookieName)) {
                this._createModal();
            }

            let form = document.querySelector('#newsletter-popup .form.subscribe');
            form.addEventListener('submit', function() {
                let cookieExpires = new Date(new Date().getTime() + 1 * 60 * 60 * 1000);

                $.mage.cookies.set(
                    'open-newsletter-popup',
                    "1",
                    { expires: cookieExpires, domain: '' }
                );
            });

        },

        _createModal: function() {

            $(this.options.blockId).modal({
                type: 'popup',
                title: '',
                modalClass: 'newsletter-popup',
                clickableOverlay: true,
                autoOpen: false,
                responsive: true,
                buttons: []
            });

            $(this.options.blockId).on('modalclosed', $.proxy(this._onModalClosed, this));

            window.setTimeout($.proxy(function() {
                $(this.options.blockId).modal('openModal');
            }, this), 30000);

        },

        _onModalClosed: function() {

            let cookieExpires = new Date(new Date().getTime() + 1 * 60 * 60 * 1000);

            $.mage.cookies.set(
                this.options.cookieName,
                "1",
                { expires: cookieExpires, domain: '' }
            );

        }

    });

    return $.nectar.newsletterPopup;

});